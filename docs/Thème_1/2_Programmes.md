---
title : "Programme"

---


# Histoire de l’informatique

*Extrait du programme*


Cette rubrique transversale se décline dans chacune des cinq autres.

Comme tous les concepts scientifiques et techniques, ceux de l’informatique ont une histoire et ont été forgés par des personnes. Les algorithmes sont présents dès l’Antiquité, les machines à calculer apparaissent progressivement au XVIIe siècle, les sciences de l’information sont fondées au XIXe siècle, mais c’est en 1936 qu’apparaît le concept de machine universelle, capable d’exécuter tous les algorithmes, et que les notions de machine, algorithme, langage et information sont pensées comme un tout cohérent. Les premiers ordinateurs ont été construits en 1948 et leur puissance a ensuite évolué exponentiellement. Parallèlement, les ordinateurs se sont diversifiés dans leurs tailles, leurs formes et leurs emplois : téléphones, tablettes, montres connectées, ordinateurs personnels, serveurs, fermes de calcul, méga-ordinateurs. Le réseau Internet, développé depuis 1969, relie aujourd’hui ordinateurs et objets connectés.

<br>

<table>
    <thead>
        <tr>
            <th >Contenus</th>
            <th >Capacités attendues</th>
            <th >Commentaires</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Événements clés de l’histoire de l’informatique.</td>
            <td>Situer dans le temps les principaux événements de l’histoire de l’informatique et leurs protagonistes.<br>Identifier l’évolution des rôles relatifs des logiciels et des matériels.</td>
            <td>Ces repères viennent compléter ceux qui ont été introduits en première.<br> Ces repères historiques sont construits au fur et à mesure de la présentation des concepts et techniques</td>
        </tr>
    </tbody>
</table>


