---
title: "Cahier de texte"

---


# Cahier de texte





??? note "Semaine 52"
    Vacances


??? note "Semaine 51"
    Vacances





??? note "Semaine 50"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :





??? note "Semaine 49"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :





??? note "Semaine 48"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :





??? note "Semaine 47"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :






??? note "Semaine 46"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :





???+ note "Semaine 45"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :





??? note "Semaine 44"
    Vacances





??? note "Semaine 43"
    Vacances





??? note "Semaine 42"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :





??? note "Semaine 41"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :






??? note "Semaine 40"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :




??? note "Semaine 39"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :





??? note "Semaine 38"
    **Séance 1 :**
    Contenu de la séance :

    
    <br/>

    Travail donné :

    

    **Séance 2 :**
    Contenu de la séance :

    <br/>

    Travail donné :

   
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :





??? note "Semaine 37"
    **Séance 1 :**
    Contenu de la séance :

    - *1) Bilan du cours sur les tests*

    - *2) Correction des exercices 1, 2, 3, 4 de la feuille d'exercice*
  
    - *3) Exercice 7*
  
    - *4) Exercice 8 en autonomie. Voir le fichier notebook en pièce jointe.*

    <br/>

    Travail donné :

    - *Rédiger sur feuille l'exercice 8 + listez et expliquer les différents tests (unitaires, d'intégration, fonctionnels...)*



    **Séance 2 :**
    Contenu de la séance :
    - *1) Lire le chapitre sur le débogage de 1. Introduction à 4.2 Utiliser les `asserts`*

    - *2) Lecture et explication de la feuille sur les types d'erreurs.*

    - *3) TD : trouver et corriger des bugs.[ici](docs/Thème_5/1.Mise_au_point_des_programmes/6.0_Activite_bug)*

    <br/>

    Travail donné :

    ??? Example "Devoir à la maison n°1"
        - 1) Créer un fichier `fonctions.py` contenant le code des fonctions travaillées en classe :
    
        - la fonction `recherche`
        - la fonction `correspond`
        - la fonction `moyenne`

        Dans chaque cas, on rédigera *une `docstring`* de la fonction. On ajoutera l'*annotation de type* pour les fonctions recherche et correspond.

        - 2) Créer une fonction `test_recherche` afin  d'implémenter un jeu de test pour la fonction recherche.
        Vous veillerez à proposer des tests variés, couvrant les différentes situations possibles (la lettre est dans le mot, n'y est pas, y est plusieurs fois etc.)

        - 3) Déposer le fichier `fonctions.py` dans l'ENT.
  
    **Séance 3 :**
    Contenu de la séance :

    <br/>

    Travail donné :


??? note "Semaine 36"
    **Séance 1 :**

    Contenu de la séance :

    *Cours sur le chapitre mise aux points des programmes [Mise au points des programmes](./Thème_5/1.Mise_au_point_des_programmes/2.2_Documenter.md), partie documenter.*

    *Utilisation de `help` pour consulter les docstrings*

    *Réalisation et correction de la fonction `recherche()`, avec sa documentation.*

    <br/>


    Travail donné :

    - *Écrire en langage naturel le codage de la fonction `correspond()`*.

    <br/>


    **Séance 2 :**

    Contenu de la séance : 

    *Codage de la fonction `correspond()` avec l'annotation de type et la docstring.*

    *Suite du cours sur la mise au point d'un programme : paragraphe 3.1 compris. Début de l'exercice sur la fonction moyenne.*

    <br/>

    Travail donné :
    
    - *Relire le cours jusqu'au paragraphe 3.1 inclus.* 
    - *Écrire en langage naturel le codage de la fonction moyenne.*


    <br/>


    **Séance 3 :**
    Contenu de la séance : 

    - *Correction de la fonction `moyenne`*
    - *Retour sur les différences et les cas d'usage des boucles : `for`*
    - *Rappel sur la structure de donnée : `tuple`*
    - *Premier pas avec des assertions.*

    <br/>

    Travail donné :

    - *Lire les cours : sur les tests et sur les tests unitaires*
    - *Faire la fiche orientation (pour vendredi)*




??? note "Semaine 35"
    - Présentation
    - Début du cours : [Mise au points des programmes](./Thème_5/1_Sommaire.md) 
    Étude des parties : introduction, types d'erreurs, coder beau, et début de documenter.
