---
title : "Récursivité"

---


# Récursivité



# 1. Qu'est-ce que la programmation récursive

*récursivité* est un terme emprunté à l'anglais dans les années 1920, le mot anglais tirant son origine du latin *recurrere* : courir en arrière.


!!! note "Définition"
    On appelle récursive toute fonction ou procédure qui s’appelle elle même.



!!! note "Définition"
    La programmation récursive est une technique de programmation qui remplace les instructions de boucle (while, for, etc.) par des appels de fonction.

<br/>

Le mécanisme le plus simple pour faire boucler une fonction : elle se rappelle elle-même.


!!! example "Exemple"

    ```Python
    def  afficher_message(n):
    	print(n," messages")
    	afficher_message(n+1)

    afficher_message(2)
    ```

    ```python
    2 messages
    3 messages
    4 messages
    5 messages
    … # Le programme ne termine pas !
    ```

<br/>

Comme avec les boucles, il va falloir être vigilent sur la structure afin que le programme se termine !


<center>
![Récursivité sans fin](./image/sans_fin.png){width="600" loading="lazy"}
</center>




## 2. Bien formuler une fonction récursive

### 2.1 Terminaison

Pour qu’une fonction récursive se termine, il faut que sa structure le lui permettent.

!!! example "Exemple"
    ```python
    def decompte(nombre):
        if nombre == 0:
            print("Fin !")
        else :
            print(nombre)
            decompte(nombre - 1)

    decompte(3)
    ```

    ```python
    3
    2
    1
    Fin !
    ```

    <br/>


    Étudions la structure de ce programme. On constate que le programme distingue deux situations :

    ```python
     if nombre == 0:
            print("Fin !")
     else :
        …
    ```

    <br/>

    La première situation, ne fait pas d’appel récursif et permet de sortir de la fonction. On appelle cette situation, un situation de terminaison ou situation d’arrêt ou cas d’arrêt ou cas de base.

    ```python 
        …
        else :
            print(nombre)
            decompte(nombre - 1)
    ```
    <br/>


    La seconde situation comporte un appel récursif, mais le paramètre passé permet, au fur et a mesure des appels, de se rapprocher de la condition de terminaison.



!!! note "En résumé"
    **Règle 1 :**

    Tout algorithme récursif doit distinguer plusieurs cas dont l’un au moins ne doit pas contenir d’appels récursifs, sinon il y a risque de cercle vicieux et de calcul infini.

    <br/>

    **Règle 2 :**

    Tout appel récursif doit se faire avec des données plus proches des données satisfaisant les conditions de terminaison.



On obtient donc un structure du type :

```txt
fonction récursive(paramètres): 
		si TEST_D’ARRET: 
			instructions du point d’arrêt
		sinon :
			instructions
			fonction récursive(paramètres changés);    # appel récursif
```


Généralement, on utilise la récursivité pour décomposer un problème en sous-problèmes “plus simples”. À leur tour, ces sous-problèmes seront décomposés jusqu'à un niveau d'opérations ”élémentaires", faciles à réaliser.



??? note "Remarque"
    Il faut s’assurer que l’on passe bien par le cas de base !

    ```python
    def somme(n):
        if n==0:
            return 0
        else:
            return somme(n-2)+n

    somme(4)
    # On passe bien par le cas de base la fonction s’arrête.

    somme(5)
    # Le cas de base est sauter (comme pour toute valeurs impaires) ! La fonction ne s’arrête pas !
    ```

    Cette fonction récursive est mal formée !


