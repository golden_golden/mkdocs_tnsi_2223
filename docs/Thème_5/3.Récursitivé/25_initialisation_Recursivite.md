---
title : "Initialisation de la récursivité"

---

# Initialisation de la récursivité

Il peut être nécessaire de créer une fonction pour permettant “d’initialiser” un certain nombre de paramètre, conditions… Avant de lancer la fonction récursive.



!!! example "Exemple 1 : Programmation défensive de fonction récursive"

    On souhaite sécuriser une fonction qui somme les entiers compris entre n et m.

    Dans le cadre de la programmation défensive, on vas vérifier que n et m sont bien de type “int” et que n < m.

    ```python 
    def somme(n, m):
        assert isinstance(n, int) and isinstance(m, int); "n et m doivent appartenir aux type int !"
        assert n <= m; "n doit être inférieur ou égal à m !"
        if n == m:
            return m
        else:
            return n + somme(n + 1, m)

    print(somme(4, 100))
    ```

    Le problème ici, est que les *assert* vont être fait à chaque appels, or ce n’est pas nécessaire ! Les *assert* n’ont de sens qu’au premier appel de la fonction ! Alors comment faire ?

    ```python 
    def somme(n, m):
        assert isinstance(n, int) and isinstance(m, int); "n et m doivent appartenir aux type int !"
        assert n <= m; "n doit être inférieur ou égal à m !"
        return somme_recursive(n, m)

    def somme_recursive(n, m):
        if n == m:
            return m
        else:
            return n + somme(n + 1, m)

    print(somme(4, 100))
    ```




