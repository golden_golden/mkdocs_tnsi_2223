---
title : "Types de récursivité"

---

  

# Les différents types de récursivité

## 1. Récursivité simple

!!! note "Définition"
    Pour ce type de récursivité, on fait un seul appel récursif pour la fonction P dans le corps d'une fonction récursive P.



!!! example "Exemple"

    ```python
    def puissance(x, n):
        if n == 1:
            return x
        else:
            return x * puissance(x, n - 1)

    print(puissance(2, 6))
    ```



## 2. Récursivité multiple

!!! note "Définition" 
    Une récursivité est multiple, s‘il y a plusieurs appels récursifs a une fonction P dans le corps d'une fonction récursive P.


!!! example "Exemple"

    ```python
    def fibonacci(n):
        if n <= 1:
            return 1
        else:
            return fibonacci(n - 1) + fibonacci(n - 2)

    print(fibonacci(4))
    ```



## 3. Récursion imbriquée

!!! note "Définition"
    Une récursivité est dite imbriquée si une fonction récursive P contient un appel imbriqué.



!!! example "Exercice"

    ```python
    def Ack(n, p):
        if n == 0:
            return p + 1
        elif n > 0 and p == 0:
            return Ack(n - 1, 1)
        else:
            return Ack(n - 1, Ack(n, p - 1))

    print(Ack(2, 3))
    ```



## 4. Récursion mutuelle ou croisée

!!! note "Définition"
    Une récursivité est mutuelle ou croisée quand une fonction P appelle une autre fonction Q qui déclenche un appel récursif à P.



??? note "Remarque"
    La situation est obligatoirement symétrique, puisque Q déclenchera un appel de P, qui déclenchera à son tour un appel de Q.



!!! example "**Exemple :** pair/impair"

    ```python
    def pair(n):
        if n == 0:
            return True
        else:
            return impair(n - 1)
    
    
    def impair(n):
        if n == 0:
            return False
        else:
            return pair(n - 1)
    
    
    print(pair(5))
    ```
