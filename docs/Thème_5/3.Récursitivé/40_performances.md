---
title : "Performances"

---


# Récursivité et performances

## 1. Calcul de complexité pour un algorithme récursif

### A. Généralités

La comparaison entre un algorithme récursif et un algorithme itératif repose sur deux aspects de l'évaluation :

1. La complexité temporelle : on mesure le nombre d'opérations. Pour une analyse plus fine de l'occupation mémoire, il convient donc de calculer le nombre d'appels imbriqués.

2. La complexité spatiale : on mesure l'espace mémoire occupée par les variables.

   

**Rappels :**

> La complexité temporelle d'un algorithme est l'ordre de grandeur du nombre d'opérations élémentaires qu'il va nécessiter pour résoudre un problème de taille n. Ce nombre est à peu près proportionnel au temps effectif de calcul. 

> La complexité spatiale d'un algorithme est l'ordre de grandeur du nombre de mémoires qu'il va utiliser pour résoudre un problème de taille n. 



Dans les deux cas on distingue la complexité : 

- En moyenne ;
- dans le meilleur des cas ; 
- dans le pire des cas. 

La plus utile est la première, la plus discriminante est la dernière.



### B. Étude de quelques exemples

**La fonction factorielle**

```python
# version récursive 
def fac(n):
    if n == 0:
        return 1
    else:
        return n * fac(n - 1)
```



```python
# Une version itérative
def fac(n):
    resultat = 1
    for i in range(n):
        resultat *= i
    return resultat
```



Pour calculer n! par la méthode itérative, il faut : 

- (n - 1) multiplications : la complexité temporelle est donc en *O(n).*

- 4 mémoires : la complexité spatiale est donc en *O(1)*. 

  

Pour calculer n! par la méthode récursive, il faut : 

- n tests, (n - 1) multiplications : la complexité temporelle est donc en *O(n)*.

- 2n mémoires (2 à chaque appel) : la complexité spatiale est donc en *O(n)*. 

Les deux méthodes s'exécutent en à peu près avec la même durée, mais la deuxième est plus gourmande en mémoire.



-----

**La suite de Fibonacci**


```python 
# Version itérative
def fib(n):
    if n <= 1:
        return n
    else :
        a,b = 0,1
        for i in range(2,n+1):
            a,b = b,a + b
        return b
```

- Nombre d'opérations : *1(+2) + (n - 1)(1(+2))* soit une complexité temporelle *O(n)* : raisonnable. 

- Nombre de mémoires : 5 soit une complexité spatiale *O(1)* : excellente. 

  

```python
# Version récursive
def fib(n):
    if n <= 1:
        return n
    else :
        return fib(n - 1) + fib(n + 1)
```

Soit *M(n)* le nombre de mémoires utilisée par fib(n) : 
$$
∀n ≥ 2 \space ,\space M(n) = 2 + M(n - 1)
$$
car contrairement au temps, la mémoire est recyclable : on peut réutiliser celles du calcul de fib(n-1) pour fib(n-2) . On a directement : 
$$
M(n) = M(0) + 2n
$$
donc *M(n) = O(n)* : la complexité spatiale est raisonnable, mais moins bonne qu'en itératif.



Soit *C(n*) le nombre de calculs faits pour fib(n) : 
$$
∀n ≥ 2 \space ,\space C(n) = 1 + C(n - 1) + 1 + C(n - 2)
$$
On pose *U(n) = C(n) + 2* car *-2* est le point fixe de la récurrence :
$$
∀n ≥ 2 \space ,\space U(n) = U(n - 1) + U(n - 2)
$$
C'est une récurrence classique qu'on résout par la méthode de l'équation caractéristique ; on trouve : 
$$
U(n) = αr^n + βs^n \space \space où \space \space r = \frac{1 +  √5}{2}\simeq 1.6 \space ,\space s = \frac{1 - √5}{2} \simeq -0.6
$$
On en déduit : 
$$
{U(n)=O(r^n)}  \space \space puis \space \space {C(n) = O (r^n)}
$$
La complexité temporelle est *très mauvaise car exponentielle !*



**Remarque** 

> Pour calculer fib(100), la machine fera de l'ordre de (r ^100) opérations. Or : 
> $$
> log_{10}(r^{100}) = 100 \space log_{10}(r)\simeq 20
> $$
> ce qui signifie qu'il y aura environ 10^20 opérations. Ä‚Â€ raison de 10^9 opérations par seconde, le calcul prendra de l'ordre de 10^11 secondes, soit environ 3000 ans ! 
>
> D'une manière générale, les algorithmes qui ont une complexité temporelle du type O(q^n) avec q > 1 ne peuvent pas être exécutés en temps acceptable pour n grand, contrairement à ceux en O(n^p) qui peuvent l'être (surtout si p n'est pas trop grand). Ces derniers sont dits de type P.

-----



#### C. Conclusion

La taille mémoire occupée par une fonction itérative est inférieure à celle utilise par une fonction récursive, en raison des appels successifs dans une cette dernière.

La même remarque pour la complexité temporelle, bien que la récursivité ne produise pas fondamentalement de programmes plus lents ou plus rapides que les autres, le coût d'exécution d'une fonction itérative est généralement inferieure au temps d'une exécution d'une fonction récursive pour le même problème.



## 2. De récursifs à itératifs

Rappelons que la thèse de Church - Turing  nous indique que l'ensemble des paradigmes utilisé en programmation sont équivalents ! On ne sera donc pas surpris d'apprendre que :



**Propriété :**
> Tout algorithme récursif peut être transformé en un algorithme itératif



**Exemple :**

```python
# Version récursive non terminale
def fac(n):
    if n == 0:
        return 1
    else:
        return n * fac(n - 1)
```

```python 
# Version récursive terminale
def fac(n, acc=1):
    if n == 0:
        return acc
    else:
        return fac(n - 1, n * acc)
```

```python 
# Version itérative
def fac(n):
    resultat = 1
    while n > 0:
        resultat = resultat * n
        n = n - 1
    return resultat
```

Pour transformer un algorithme récursif en itératif, il peut être parfois plus simple de passer par la forme récursif terminal de l'algorithme.



Il n'est cependant pas toujours aisé de trouver la forme itérative d'un problème qui s'exprime facilement en récursif (cf. jeu des tours de Hanoï).



## 3.Avantages et inconvénients des algorithmes récursifs

### A. Les inconvénients

- Les fonctions récursives requièrent généralement plus d'espace mémoire, ce qui peut dans certain cas devenir rédhibitoire ; 

- Une fonction récursive peut être de plus grande compacité dans son écriture mais n'est pas forcément de plus petite complexité temporelle. Le nombre de calculs effectués est souvent bien plus grand qu'en programmation itérative.

  

### B. Les avantages

- La récursivité offre au programmeur un autre moyen, souvent élégant et concis, de résoudre certains problèmes.

  - dans la programmation des jeux solitaires du type Sudoku, labyrinthes, …
- dans la programmation des jeux à deux joueurs du type Échecs, Dames, Othello,  …
  - et dans bien d'autres domaines encore … comme le point de croix en broderie (cf. [ici](http://sandrine.toonywood.org/pageperso/pcroix/index.php))

- La résolution de certains problèmes complexes est parfois grandement facilité *(une solution récursive décrit comment calculer la solution à partir d'un cas plus simple, au lieu de préciser chaque action à réaliser, on décrit ce qu'on veut obtenir, c'est ensuite au système de réaliser les actions nécessaires pour obtenir le résultat demandé.)*
- La récursivité permet de traduire simplement certaines opérations mathématiques ; 
- La récursivité permet des résonnements plus naturel lors du travail sur certaines structure de données, tel que : liste, arbre, graphe.
- La compacité des algorithmes récursifs facilite la lisibilité et le débogage.

- La récursivité rend certains mécanisme de preuve de programme plus aisée.  



## 4. Alors itératif ou récursif ?

Il n'existe pas de réponse définitive à la question de savoir si un algorithme récursif est préférable à un algorithme itératif ou le contraire. Ces deux paradigmes de programmation étant équivalents ; tout algorithme itératif possède une version récursive, et réciproquement. 

Cependant, certains problèmes sont définis de manière éminemment récursive et trouverons des solutions bien plus simple en récursif qu'en itératif, comme par exemple : le tri rapide (*quicksort*), le tri fusion (*mergesort*), la recherche dichotomique dans une liste ordonnée, l'évaluation des expressions algébriques, la résolution du jeu des tour le Hanoï, le tracé de certains fractals (courbes de Von Koch, éponge de Sierpinsky, etc), le parcours en profondeur dans un graphe, l'algorithme de Karatsuba de multiplication de deux grands entiers .

Pour d'autres problèmes, définis de manière récursive ou pas, le programme peut être écrit sans trop de difficultés en itératif. Si on recherche l'efficacité *(une exécution rapide)*, on préférera alors la version itérative ! C'est par exemple le cas : d'une recherche dichotomique, d'un parcours en profondeur, d'une conversion d'un entier en base 2, du calcul du pgcd par l'algorithme d'Euclide,de l'algorithme de transformée de Fourier rapide et discrète (en traitement du signal),…

Ainsi, le choix d'une version récursive ou itérative d'un programme, doit se faire selon plusieurs critères, les performances, bien sur, mais aussi celui de la facilité d'implémentation : laquelle des versions est-elle la plus facile à comprendre ? Laquelle traduit le mieux la nature du problème ? Laquelle est la plus souple, et vous permettra d'ajouter des modifications/améliorations de l'algorithme ensuite ? Le choix du langage peut également avoir son importance : un langage fonctionnel tel Caml est conçu pour exploiter la récursivité et le programmeur est naturellement amené à choisir la version récursive de l'algorithme qu'il souhaite écrire. À l'inverse, Python, même s'il l'autorise, ne favorise pas l'écriture récursive (limitation basse,1000 par défaut, du nombre d'appels récursifs).

Il est cependant essentiel pour un programmeur de s'habituer aux deux styles de programmation, pour pouvoir faire un choix  objectif ensuite. Une personne qui n'aurait fait que de l'itératif aura toujours tendance à trouver la récursion “compliquée“, et passera à côté d'opportunités très intéressantes, tout comme un programmeur ne faisant que de la récursion aura parfois une manière compliquée de coder ce qui se fait simplement avec une boucle.

Pour finir, il faut savoir qu'itération et récursion ne sont pas antinomiques. De nombreux problèmes de combinatoire et de dénombrement utilisent des appels récursifs dans des structures itératives !


