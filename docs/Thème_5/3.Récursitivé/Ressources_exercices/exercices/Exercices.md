-----

**Exercice**

![image-20201008010351126](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20201008010351126.png)

***Correction***

```python
def puissance(a,m):
    if m == 1:
        return a
    else:
        return puissance(a,m-1) * a
```

----







----

**Exercice:**

![image-20201008010157692](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20201008010157692.png)

***Correction***

```python
def premier_chiffre(entier):
    if entier // 10 == 0:
        return entier
    else:
        return premier_chiffre(entier//10)
```

----





-----

**Exercice**

![image-20201008010806597](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20201008010806597.png)

***Correction***

```python
def entier_fin_42(entier):
    if entier % 100 == 42:
        return entier
    else:
        return entier_fin_42(entier+1)

print(entier_fin_42(2020))
```

----







-----

**Exercice 1**

![image-20201008005715140](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20201008005715140.png)



***Correction***

```python
def somme_chiffre(entier):
    if entier // 10 == 0:
        return entier
    else:
        return somme_chiffre (entier//10)+ entier%10

print(somme_chiffre(105))

```

-----





-----

**Exercice 2**

![image-20201008005053914](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20201008005053914.png)





***Correction :***

```python
def multi_paysan_russe(a,b):
    if a== 0:
        return 0
    if a%2 == 0:
        return multi_paysan_russe(a//2,2*b)
    else:
        return multi_paysan_russe(a-1,b)+b


print(multi_paysan_russe(3,8))

```

---



----

**Exercice**

trouver la plus grande valeur dans une liste. 

```
[4,53,2,1,72,8,41,12,8]
72
```

***Correction***

```python
def max (liste):
    if len(liste)==1:
        return liste[0]
    else:
        if liste[0] > max(liste[1:]):
            return liste[0]
        else:
            return max(liste[1:])
```

----





-----

**Exercice**

implémenter une fonction qui prend en paramètre deux listes ordonnées et qui les fusionne en une liste ordonée.

```python
Liste1 = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]
Liste2=[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]

print(fusion(liste1,liste2))

[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
```

***correction***

```python
def fusion(liste1,liste2):
    if not liste1 and liste2:
        return liste2
    if not liste2 and liste1:
        return liste1
    if liste1[-1]>liste2[-1]:
        return fusion(liste1[:-1], liste2) + liste1[-1:]
    else:
         return fusion(liste1, liste2[:-1]) + liste2[-1:]
```

---



**Exercice**

Implémenter une fonction diviser qui prend en paramètre une liste et qui renvoie  

```pyton
liste1 = [1, 3, 10, 12, 5, 7, 9, 11, 13, 15, 17, 19]
diviser(liste1)
((([1], ([3], [10])), ([12], ([5], [7]))), (([9], ([11], [13])), ([15], ([17], [19]))))
```

***Correction***

```python
def diviser(liste):
    longueur_liste = len(liste)
    if longueur_liste < 2:
        return liste
    else:
        T1 = diviser(liste[:longueur_liste//2])
        T2 = diviser(liste[longueur_liste//2:])
        return (T1,T2)
```

-----



**Trie fusion**

```python
def trifusion(T):
    if len(T)<=1 :
        return T
    T1=trifusion([T[x] for x in range(len(T)//2)])
    T2=trifusion([T[x] for x in range(len(T)//2,len(T))])
    return fusion(T1,T2)
```

