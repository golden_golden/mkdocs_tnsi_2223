---
title: "Documentation des modules"

---

# Information sur un module

## 1. Obtenir la liste des éléments d’un module

Souvent, on importera des modules standards ou créés par d’autres développeurs. Dans ce cas là, il peut être intéressant d’obtenir rapidement la liste des éléments du module afin de voir rapidement ce qui va pouvoir nous être utile.

<br/>

Pour faire cela, on peut utiliser la fonction `dir()` qui renvoie la liste de toutes les fonctions et variables d’un module.

```python
    >>> import fonctions_usuelles
    >>> dir(fonction_usuelles)
    ['__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'somme', 'division','carre']
```

Comme vous pouvez le voir, tout fichier Python possède par défaut des éléments de configuration.

<br/>

## 2. Documentation d’un module

Lorsqu'on écrit un module, il est important de créer de la documentation pour expliquer ce que fait le module et comment utiliser chaque fonction. On documente ainsi l’API, du module.

<br/>

La documentation des modules se fait comme celle des fonctions, par l'intermédiaire de *docstrings* (« chaînes de documentation » en français).
On place du texte entre triple cote au début du fichier.
Ces *docstrings* permettent de fournir de l'aide lorsqu'on invoque la commande `help()` :

```python
    >>> print(help(fonctions_usuelles))

    help on module fonctions_usuelles:

    NAME
        fonctions_usuelles

    DESCRIPTION
        Module fonctions_usuelles
        Version : 0.1
        Auteur : toto
        Remarque : Mon permier module

    FUNCTIONS
        carre(x)
            Docuentation de la fonction carré
        
        division(numerateur, denominateur)
            Documentation de la fonction division
        
        somme(x, y)
                additionne de deux nombre et renvoie le résultat.
            
            :param a: Le permier nombre à additionner.
            :param b: Le second nombre à additionner.
            :type a: int
            :type b: int
            :return: Le résultat de l'addition
            :rtype: int
            
            :Example:
            
            >>> add(1, 2)
            3

    FILE
        c:\users\Toto\pycharmprojects\venv\fonctions_usuelles.py
```

<br/>

Vous remarquez que Python a généré automatiquement cette page d'aide,  tout comme il est capable de le faire pour les modules internes à Python (*random*, *math*, etc.).

Notez que l'on peut aussi appeler l'aide pour une seule fonction du module :

```python
    >>> help(fonctions_usuelles.division)

    Help on function division in module fonctions_usuelles:

    division(numerateur, denominateur)
        Documentation de la fonction division
```

<br/>


*Pour en savoir plus sur les `docstrings` et comment les écrire, lire le document `Documenter son code` dans la partie `Thème5  Mise au point des programmes`.*
