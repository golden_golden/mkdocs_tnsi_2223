'''Le module stats met à disposition des fonctions permettent d'effectuer 
des calculs de paramètres statistiques classiques'''

def somme(liste: list) :
    '''
    somme renvoie la somme des nombres de la liste

    Entrée
    --------
    liste : une liste non vide de nombres

    Sortie
    --------
    un nombre
    '''
    somme_partielle = 0
    for nombre in liste:
        somme_partielle+=nombre
    return somme_partielle

def moyenne(liste: list):
    ''''
    moyenne renvoie la moyenne des nombres de la liste

    Entrée
    --------
    liste : une liste non vide de nombres

    Sortie
    --------
    un nombre
    '''
    return somme(liste)/len(liste)