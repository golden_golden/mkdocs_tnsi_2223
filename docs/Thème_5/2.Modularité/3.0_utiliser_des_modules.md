---
title: "Utiliser les modules"

---

# Comment utiliser les modules

## 1. Comment utiliser le module que nous venons de créer

Désormais nous savons créer un module, mais comment doit on faire pour l’utiliser ?

- Pour appeler une fonction ou une variable de ce module, il faut que le fichier du module `fonctions_usuelles.py` soit dans le répertoire courant *(dans lequel on travaille)* ou bien dans un répertoire listé par la variable d'environnement `PYTHONPATH` de votre système d'exploitation.


- Ensuite, il suffit d'importer le module et toutes ses fonctions (et constantes) vous sont alors accessibles.

<br/>

### 1.1 Importer son module

Pour importer un module, on utilise la syntaxe `import` suivit du nom du module.


!!! example "Exemple"
    Voyons comment importer le module *fonctions_usuelles.py* que nous avons créer dans un nouveau fichier.

    ```python
        # -*- coding: utf-8 -*-
        # Un nouveau fichier qui nécéssite l'utilisation de fonction présente dans
        #le module fonction_usuelles que nous avons précédament créé.

        import fonctions_usuelles
    ```



Notez que le fichier du module est bien enregistré avec une extension `.py` et pourtant on ne la précise pas lorsqu'on importe le module.


Maintenant que le module est importer, on va pouvoirs utiliser les fonctions et les constantes qu’il contient dans notre fichier.

<br/>

Mais avant ça regardons quelques conventions pour l’importation de plusieurs modules.

<br/>

### 1.2 Conventions pour l’import de plusieurs modules

####  A. Convention pour les imports

Toujours situés en haut du fichier, juste après d’éventuels commentaires et *docstrings* relatifs au modules, on trouve les imports des modules nécessaires.

<br/>

Les imports doivent être faits sur des lignes séparées, exemple :


=== "Bien"
    ```python
        import os
        import sys
    ```

=== "Mauvais"
    ```python
        import sys , os
    ```

<br/>

#### B. Structurer les imports

Les imports doivent être groupés dans cette ordre :

- imports de la bibliothèque standard
- imports de modules tiers
- imports spécifiques à l’application/bibliothèque locale *(vos créations !)*

   

!!! note "Remarque"
    Vous pouvez mettre une ligne vide entre chaque groupe d’imports.

<br/>

## 2. Utilisation de son propre module

Pour utiliser les éléments du module, il faudra préfixer le nom de ces éléments par le nom du module et un point.

```python
    # -*- coding: utf-8 -*-
    import fonctions_usuelles

    nombre = 3
    nombre_au_carre = fonction_usuelles.carre(nombre)
    print(nombre_au_carre)
```

```python
    9
```


!!! note "Remarque"
    Cette façon de faire permet d’éviter les conflits dans le cas où on aurait défini des éléments de même nom que ceux disponibles dans le module.
    De plus, cela permet de savoir en permanence s'il s’agit de notre code ou du code présent dans un module.

<br/>

## 3. Importation d’un module externe

Certains modules et packages, ne sont pas inclus avec python, il est alors nécessaire de les télécharger sur votre machine avant de pouvoir les importer dans votre programme. Le dépôt Pypi (Python Package Index [https://pypi.org/](https://pypi.org/)) référence la plupart des modules tiers.

<br/>

Pour les ajouter vous pouvez utiliser le gestionnaires de paquet Python `pip` ou utiliser le gestionnaire présent dans votre IDE.


??? Warning "Avertissement"

    Les fichiers des modules ou des packages doivent être au bon endroit !

    <br/>

    Lorsque l’interprète rencontre une instruction `import`, il importe le module s’il est présent dans le path (le chemin de  recherche). Pour rappel, le path ou chemin de recherche est une liste de répertoires dans lesquels l’interpréteur cherche avant d’importer un  module. Pour être tout à fait précis, lorsqu’on importe un module, l’interpréteur Python le recherche dans différents répertoires selon  l’ordre suivant :

    - Le répertoire courant ;
    - Si le module est introuvable, Python recherche ensuite chaque répertoire listé dans la variable `shell PYTHONPATH` ;
    - Si tout échoue, Python vérifie le chemin par défaut. Sous UNIX, ce chemin par défaut est normalement `/usr/local/lib/python/`.


<br/>

## 4. Importation partielle d’un module

### 4.1 Importation d’une seule fonction

Parfois, nous n’aurons besoin que de certains éléments précis dans un module. On va alors pouvoir se contenter d’importer ces éléments en particulier.  Pou cela, on va utiliser l’instruction `from nom-du-module import un-element`.

<br/>

On va par exemple pouvoir choisir de n’importer que la fonction  `carre()` ou que la fonction `somme()` depuis le module `fonctions_usuelles.py`.

```python
    # -*- coding: utf-8 -*-
    from fonctions_usuelles import carre
```

<br/>

Dans le cas où on importe que certains éléments depuis un module, il ne faudra pas ensuite préfixer le nom des éléments par le nom du module  pour les utiliser dans notre script principal.


```python
    # -*- coding: utf-8 -*-
    from fonctions_usuelles import carre

    nombre = 3
    nombre_au_carre = carre(nombre)
    print(nombre_au_carre)
```
```python
    9
```

<br/>

### 4.2 Utilisation d’un alias

Si le nom de la fonction que l’on souhaite importer est un peu long ou pour d’autres raisons,  il est possible d’utiliser un alias.

<br/>

*Mais comment faire ?*


```python
    from puissance import carre as ca

    a = 5
    u = ca(a)
    print("le carre vaut", u)
```

<br/>

### 4.3 import de deux ou trois fonctions

Il est possible d’importer deux ou trois fonction seulement en utilisant:

 `from nom_du_module import nom_fonction1,nom_fonction2`


!!! example "Exemple"
    ```python
        from puissance import carre, cube

        a = 5
        u = carre(a)
        print("le carre vaut", u)
    ```

<br/>

### 4.4 À connaître, mais à ne pas faire !

Il est possible d’importer toutes les fonctions contenue dans un module en utilisant `from nom_du_module import *`

!!! example "Exemple"
    ```python
        from puissance import *

        a = 5
        u = carre(a)
        print("le carre vaut", u)
    ```



!!! Warning "Avertissement"

    L’importation de toutes les fonctions avec `*` est fortement déconseillée ! En effet, elle ne permet pas d’avoir une  vision claire des fonctions qui ont été importées. Ceci est donc une source potentielle d’erreurs.

<br/>

## 5. Complément sur l’importation d'une module

### 5.1 La variable globale `__name__`

À l’intérieur d’un module, le nom du module (en tant que chaîne de caractères) peut être obtenu grâce à la variable globale `__name__`. Par exemple, si on utilise le module `fonctions_usuelles.py` défini précédemment, on a dans l’interpréteur :

```python
    # -*- coding: utf-8 -*-
    import fonctions_usuelles as fu

    print(fu.__name__)
```

```python
    fonctions_usuelles
```

<br/>

### 5.2 Utiliser son programme en tant que module

Un même fichier `.py` peut jouer le rôle de script ou de module suivant son utilisation. Quand on utilise un fichier en tant que *script*, la variable globale `__name__` prend pour valeur `__main__`. 

<br/>

Ceci permet d’avoir dans le fichier un bloc d’instructions qui sera exécuté uniquement lorsque le fichier est lancé en tant que script.




!!! exemple "Exemple"
    Fichier : `test_de_mon_programme.py`

    ```python
        import mon_programme as mp

        def test_carre():
            assert mp.carre(3)==9 ; "Erreur, le carre de 3 est 9 !"
            assert mp.carre(2)==4 ; "Erreur, le carre de 2 est 4 !"
            print("La fonction carre passe tout les tests !")


        test_carre()
    ```

    <br/>

    **Version 1**

    Fichier version 1 : `mon_programme.py` 

    ```python
        def carre(valeur):
            return valeur**2

        nombre_utilisateur = int(input("Saisir un nombre entier :"))
        print("Le carre vaut", carre(nombre_utilisateur))
    ``` 
    Si on exécute notre programme de test, le programme nous demande :

    ```python
        Saisir un nombre entier :
    ``` 
    et les tests ne se lance pas !

    <br/> 

    **Version 2**
       
    Fichier version 2 : `mon_programme.py`

    ```python
        def carre(valeur):
            return valeur**2

        if __name__ == "__main__":
            nombre_utilisateur = int(input("Saisir un nombre entier :"))
            print("le carre vaut", carre(nombre_utilisateur))
    ``` 
    Si on exécute notre programme de test, on obtient :

    ```python
        La fonction carre passe tout les tests !
    ``` 
    Les tests on bien pu être passé ! La condition  `__name__ == __main__` est évolué à `False`, le bloc qui nous gêné n’est plus exécutée. 




