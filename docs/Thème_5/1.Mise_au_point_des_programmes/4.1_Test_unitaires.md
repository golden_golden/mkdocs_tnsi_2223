---
title: "Les tests unitaires"

---

# Les tests unitaires


## 1. À propos des tests unitaires

Les tests unitaires ont pour but de valider, de manière isolée, chaque unité élémentaire de notre code, C'est-à-dire chaque fonction, méthode ou procédure.

Les tests unitaires doivent permettent de déterminer si l'élément testée fonctionne conformément aux attentes, c'est à dire à sa spécification (test "boite noire"), mais également si l'implémentation ne posera pas de problème (test "boite blanche").

Les tests unitaires doivent être lancer dès que l'on touche à une fonction, pour vérifier son intégrité !


Il est donc absolument nécessaire qu'ils soient automatisés. Finalement un test unitaire, ne serra qu'un morceau de code permettant d’en soumettre un autre et d’analyser le résultat obtenu.


## 2. En route vers une réalisation de test unitaire satisfaisante
Nous allons voir que, la réalisation de test est juste une amélioration de ce que vous faites déjà !

Reprenons notre exemple, avec la fonction `puissance(x,n)`. Il vous arrive peut être de taper dans la console, des appels pour vérifier les valeurs de retour :

``` Python
    >>> puissance(2, 3)
    8
    >>> puissance(0, 2)
    0
    >>> puissance(5, 0)
    1
```

Cependant, dès que vous voulez refaire un test, il faut tout retaper ! Une possibilité est de faire les mêmes appels mais dans le programme lui-même.

```python
    def puissance(x, n: int):
        # ici l'implémentation de la fonction qui retourne x^n, avec n entier.

    print(puissance(2, 3))
    print(puissance(0, 2))
    print(puissance(5, 0))
    print(puissance(-2, 5))
    print(puissance(0.5, 2))
```

Vous pouvez alors lire dans la console :

```python
    8
    0
    1
    -32
    0.25
```

L'avantage est que vous pouvez relancer vos tests autant que vous voulez. Cela permet déjà d'écrire plus de tests et de vérifier s'ils couvrent tous les cas potentiellement problématique, mais on ne peut pas dire que la sortie soit très lisible, surtout s'il y a beaucoup de tests.
Une solution pourrait être de placer les valeurs de retour dans le programme et d'indiquer uniquement si le test est passé ou non.


```python
    def puissance(x, n: int):
        # ici l'implémentation de la fonction qui retourne x^n, avec n entier.


    if puissance(2,3) == 8 :
                print("ok")
            else:
                print("erreur")

    if puissance(0, 2) == 0 :
                    print("ok")
                else:
                    print("erreur")
        
    if puissance(5, 0) == 1 :
                    print("ok")
                else:
                    print("erreur")

    if puissance(-2, 5) == -32 :
                    print("ok")
                else:
                    print("erreur")
        
    if puissance(0.5, 2) == 0.25 :
                    print("ok")
                else:
                    print("erreur")

```

L'affichage dans la console est plus lisible, mais l'écriture du test est un peu lourde !
Une solution est d'utiliser l'instruction `assert`.

```python
    def puissance(x, n: int):
        # ici l'implémentation de la fonction qui retourne x^n, avec n entier.

    assert puissance(2, 3) == 8
    assert puissance(0, 2) == 0
    assert puissance(5, 0) == 1
    assert puissance(-2, 5) == -32
    assert puissance(0.5, 2) == 0.25
```

La syntaxe est considérablement allégée, cependant la sortie n'est pas la même.
Avec `assert`, si la condition est vérifiée, il n'y a aucun affichage dans la console et dans le cas contraire une exception et levé (qui arrête le programme et dont les tests !).

```python
File "/run/…/nom_du_fichier.py", line 27, in test_puissance
    assert puissance(-2, 5) == -32
AssertionError:
```

!!! note "à supprimer ?"
    
    Il est possible mais pas nécéssairement indipensable d'écrire un commentaire pour les `assert`.

    ```python
        def puissance(x, n: int):
            # ici l'implémentation de la fonction qui retourne x^n, avec n entier.

        assert puissance(2, 3) == 8, "cas quelconque"
        assert puissance(0, 2) == 0, "cas ou la valeur est nulle."
        assert puissance(5, 0) == 1, "cas ou la puissance est nulle."
        assert puissance(-2, 5) == -32, "cas ou la valeur est négative"
        assert puissance(0.5, 2) == 0.25, "cas ou la valeur n'est pas un entier"
    ```

Si on écrit, les tests de cette façon l'ensemble des tests de l'ensemble de fonction va se lancer à chaque exécution du programme ! On va donc regrouper les tests dans une fonction. Par convention, le nom de la fonction de test commencera par `test_` suivie du nom de la fonction testée. Soit ici :


```python
def test_puissance():
    assert puissance(2, 3) == 8, "cas quelconque"
    assert puissance(0, 2) == 0, "cas ou la valeur est nulle."
    assert puissance(5, 0) == 1, "cas ou la puissance est nulle."
    assert puissance(-2, 5) == -32, "cas ou la valeur est négative"
    assert puissance(0.5, 2) == 0.25, "cas ou la valeur n'est pas un entier"
```
On ferra alors l'appel dans la console :

```python
>>>test_puissance()
File "/run/…/nom_du_fichier.py", line 27, in test_puissance
    assert puissance(-2, 5) == -32
AssertionError: cas ou la valeur est négative
```

On pourra également pour gagner en lisibilité, placer l'ensemble des fonctions de test dans un fichier séparée !

*Nous verrons comment faire dans le chapitre suivant sur la modularité !*



## 3. Comment et ou écrire no tests unitaires

Pour réaliser nos tests, nous utiliserons des instructions `assert`, placé dans une fonctions en veillant à respecter la convention de nommage : `test_`nom_de_la_fonction_testé

```python
def puissance(x, n: int):
    # ici l'implémentation de la fonction qui retourne x^n, avec n entier.


def test_puissance():
    assert puissance(2, 3) == 8, "cas quelconque"
    assert puissance(0, 2) == 0, "cas ou la valeur est nulle."
    assert puissance(5, 0) == 1, "cas ou la puissance est nulle."
    assert puissance(-2, 5) == -32, "cas ou la valeur est négative"
    assert puissance(0.5, 2) == 0.25, "cas ou la valeur n'est pas un entier"
```

C'est fonction pourront être regroupé dans une fichier.

!!! note "exercice"

    Écrire une fonction `maxi(liste)` qui renvoie le plus grand élément de la liste `liste` passée en paramètre (évidament sans utiliser la fonction `max()`…). 
    Vous écrirez **d'abord** une fonction `test_maxi()` avant d'écrire la fonction `maxi(liste)` 


## 4. Les modules de test
À fin d'aider les développeurs dans leur travail avec les tests unitaires sous Python, il existe plusieurs 
frameworks/modules, parmi lesquels les principaux sont :

- [Unittest](../../z_Divers/2_Modules_python/Unitest.md)
- [Doctest](../../z_Divers/2_Modules_python/Doctest.md)
- [Pytest](../../z_Divers/2_Modules_python/Pytest.md)

Bien qu'une présentation de ces modules est présente dans le cours *(en cliquant sur les liens au dessus)*, il faut noter qu'ils ne sont pas au programme, de fait rien ne sera demander sur ces modules lors des évaluations !

<br/>

*Ps : je vous recommande d'aller voir ces modules après le cours sur la modularité et pour certains après le cours sur la POO.*

<br/>

Cette année, nous nous contenterons dans la plus grande majorité des cas de ce que nous venons de faire dans la partie précédente *(qui elle fait bien partie des attendu du programme !)*.


## 5. Bonnes pratiques, lors de la création de tests unitaires
### 5.1 Sur les tests

Comme vous allez vous en rendre compte, il n'est pas simple de construire un bon "jeu de tests". Pour y parvenir, on veillera à ce que les tests soient :

<br/>

**Justes :**

Un test est un outils dans lequel on doit pouvoir avoir confiance, il faut donc qu'il soit juste ! Pour cela on veillera a ce que les tests soient le plus simple possible, on utilisera juste les données nécessaires. On n'utilise jamais de valeur calculée dans un test, on utilise des constantes (on écrit les valeur en dur) !

=== "Bien"
    ``` python
        assert addition_liste([1, 2, 5, 8, 12]) == 28
    ```
=== "Mal"
    ``` python
        assert addition_liste([1, 2, 5, 8, 12]) == sum([1, 2, 5, 8, 12])
    ```

<br/>

**Rapides :**

Un test doit être rapide à exécuter, pour qu’il puisse être exécuté régulièrement. Plus un bug est détecté tôt, plus il est facile de le corriger.

<br/>


**Pertinants :**

Il n’y a pas besoin de créer des cas qui ne sont pas utiles pour le test en question.
Le test doit toujours avoir une marnière d'échouer.
Le test doit échouer pour une seule raison.

<br/>


**Auto validants :**

Le résultat d’un test doit être direct. On doit savoir avec certitude, si le test a réussi ou s'il a échoué, la réponse est de nature booléen.

<br/>


**Déterministes :**

Appliqué à un code donnée, le résultat doit être toujours le même.

<br/>


**Répétables :**

Un test unitaire doit pouvoir être répété peu importe l’environnement de test. S’il échoue une fois, il doit échouer systématiquement sans modification. Un test qui échoue en production doit donc aussi échouer en développement.

<br/>


**Indépendants :**

- *Indépendant dans l’exécution :* Un test ne doit pas avoir besoin qu’un autre test réussisse pour pouvoir s’exécuter.
- *Indépendant dans l’évolution :* Un test doit être facile à modifier sans avoir à toucher à d’autres tests.
- *Indépendant dans les résultats :* Deux tests ne doivent pas pouvoir échouer pour les mêmes raisons.

<br/>


**Uniques :**

On ne teste pas plusieurs fois un même comportement. Par exemple, un test d’intégration ne doit pas tester ce qui a déjà été testé avec des tests unitaires.

<br/>


**Quand :** 

Quand doit-on écrire les tests ? La réponse dépend du type de test.
Pour les tests fonctionnel (boite noire), écrire les tests avant de coder est une bonne pratique (démarche TDD), cela permet d'avoir la spécification bien en tête avant de commencer, et de vérifier que les tests ne passe pas, ce qui améliore la confiance dans les tests.
Pour les tests structuraux (boite blanche), il est recommandé d'écrire les tests en parallèle du code.

<br/>

**Ou :**

Séparer le code des tests du code de l’application, dans des fonctions et si possible dans des modules, correctement nommé !

<br/>

**Qui :**

Cela n'est pas toujours possible, mais une bonne pratique consiste à faire en sorte que la personne qui code, n'est pas la personne qui écrit les tests (l'avantage est que cela peut rendre les tests un peu plus ludique ! Et ça c'est super parce qu'il faut l'avouer l'écriture des tests n'est pas le truc le plus fun en programmation.)

<br/>

??? note "vidéo"
    - [https://www.youtube.com/watch?v=odT2vpyqVN8](https://www.youtube.com/watch?v=odT2vpyqVN8)
  

### 5.2 Sur ce que l'on teste
De manière générale, voici quelques règles que l'on peut appliquer pour savoir quoi tester :

- **Test boite noire** :
    * si la spécification mentionne plusieurs cas, il faut s'assurer de tous les tester ;
    * si la fonction renvoie un booléen, il faut s'assurer de tester les deux résultats possibles ;
    * si la fonction s'applique à un nombre, il faut s'assurer de tester des cas où le nombre est positif, où le nombre négatif et où le nombre vaut zéro ;
    * si la fonction s'applique à un nombre appartenant à un intervalle, il faut s'assurer de tester les cas où le nombre est égal aux bornes de l'intervalle.
  
<br/>

- **Test boite blanche** :
    * si la fonction s'applique à un tableau, il faut tester le cas où le tableau est vide ;
    * si la fonction doit parcourir un tableau en entier, il faut s'assurer que celui-ci est parcouru entièrement ;
    * si la fonction possède des structures conditionnelles, il faut s'assurer que l'ensemble des conditions soient testées ;






----- à faire

??? note "Sources"
    - [https://glassus.github.io/terminale_nsi/T2_Programmation/2.4_Pratiques_de_programmation/cours/](https://glassus.github.io/terminale_nsi/T2_Programmation/2.4_Pratiques_de_programmation/cours/)
    - [https://info-mounier.fr/premiere_nsi/langages_programmation/documentation_mise_au_point_de_programmes.php](https://info-mounier.fr/premiere_nsi/langages_programmation/documentation_mise_au_point_de_programmes.php)

??? note "Lien test unitaires"

    - [https://www.youtube.com/watch?v=apgReCCAQr4](https://www.youtube.com/watch?v=apgReCCAQr4)
    - [https://www.youtube.com/watch?v=gCT1z-0aq7c](https://www.youtube.com/watch?v=gCT1z-0aq7c)
  
    - [https://www.youtube.com/watch?v=1Lfv5tUGsn8](https://www.youtube.com/watch?v=1Lfv5tUGsn8)

    

