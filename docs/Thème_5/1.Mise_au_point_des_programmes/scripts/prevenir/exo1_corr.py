def correspond(mot, mot_a_trous):
    if len(mot) != len(mot_a_trous):
        return False
    for i in range(len(mot)):
        if mot_a_trous[i]!="*" and mot[i] != mot_a_trous[i]:
            return False
    return True


# Voici une solution envisageable, mais il existe certainement d'autres solutions !
