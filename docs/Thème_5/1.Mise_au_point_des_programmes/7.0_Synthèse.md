
# Résumé du cours


## Types d'erreurs

Il peut exister dans un programme trois types d'erreurs assez différentes :

- **Les erreurs de syntaxe :** elles surviennent lorsque le code source du programme est mal formé.


- **Les erreurs d'exécution :** elles surviennent lorsqu'un programme, syntaxiquement correct, effectue une opération interdite.

- **Les erreurs logiques :** elles surviennent lorsqu'un programme, sans erreur de syntaxe ni d'exécution, ne produit pas le résultat correct attendu.


## Coder beau
L'aspect «esthétique» d'un code ne doit pas être négligé. Standardiser le code permet de le rendre plus lisible et cohérent.

Le premier aspect de cette standardisation est le respect des conventions syntaxiques. L'ensemble des règles est à connaître (cf cours).



### 1. Quelques bonnes pratiques
Il est également utile d'adopter quelques bonnes pratiques, pour mieux se repérer dans le code et réduire les erreurs.

- Pour **les fonctions**, donner un nom de fonction *pour ce qu’elle renvoie* et non pour ce qu’elle fait :
```python
moyenne()
```


- Pour **les procédures**, donner le nom de procédure *pour ce que la procédure fait* de manière explicite :
```python
affiche_moyenne()
```

- Utiliser **les formes plurielles, pour les collections** d'objets et **la forme singulière, pour ses éléments**.
```python
for point in points
```

- Utiliser le préfixe `est_` pour les variables et les méthodes qui **renvoient un booléen**.<br/>`est_visible, est_fini, est_trouve, est_ouvert…`

<br/>

- Évitez d'utiliser des valeurs constantes codées en dur au milieu du code, **utilisez des constantes définies au niveau global**, et nommées **en majuscule**.
  
```python
MAX_TAILLE = 200
…
if taille > MAX_TAILLE :
    …
```

<br/>

### 2. Organiser son code
> Afin de rendre le code plus lisible et de gagner du temps lors de sa modification, il est nécessaire de l'organiser. Généralement l’organisation d'un script python respecte le plan suivant :

    Docstring du programme
        - Cette docstring décrit globalement le script et donne quelques informations sur la version du script, les auteurs,…

    Importation des modules
        - un module par ligne.

    Définition des constantes
        - Le nom des constantes est en majuscule.

    Définition de fonction
        - La docstring indique ce que retourne la fonction et décrit les paramètres, le retour, les préconditions et les postconditions

    Programme principal
        - c'est celui qui est lancé lorsqu'on exécute le script. Il est composé d'appels des fonctions définies précédemment.

<br/>


## Renseigner son code
### 1. Commenter

```python
    # mon magnifique commentaire
    x = x + 1   
```
Les commentaires doivent donner des explications claires sur l'utilité du code et doivent être synchronisés avec le code, c'est-à-dire que si le code est modifié, les commentaires doivent l'être aussi (le cas échéant).

<br/>

De bons commentaires peuvent vous faire gagner du temps, car :

- ils facilitent l'examen du code par une personne tierce (dans le cadre d'un travail collaboratif par exemple)

- ils vous seront utiles si vous replongez dans votre code après un certain temps d'interruption


Cependant, avant d'écrire un commentaire, il faut se rappeler que :

- Si vous avez besoin d’expliquer votre code avec du texte, c’est probablement que votre code est améliorable : nommages explicites, découpage du code en éléments logiques simples …. (Attention, ce n'est pas toujours le cas) .

- Ne commenter pas du code qui n'a pas besoin d'être commenté, car il est suffisamment explicite.

- On préfère de loin pas de commentaire du tout à des commentaires erronés !

<br/>

### 2. Docstring
#### 2.1 À qui s'adresse les `Docstrings`
Les `docstrings` s'adressent aux programmeurs qui veulent utiliser une fonction ou un module.

<br/>

#### 2.2 Consulter les Docstring, la fonction d'aide help
Pour consulter la docstring d'une fonction `f()` par exemple, il suffit d'utiliser dans la console la commande `help(f)`.

<br/>

#### 2.3 Comment écrire ses propres Docstrings
Pour écrire la docstring associée à une fonction, une classe… Il suffit pour cela de commencer le code de la fonction à documenter par une ou plusieurs phrases entre triple guillemets """ :

```python
    def produit_de_nombres(nombre1, nombre2):
        """
        Multiplication de deux nombres entiers.
        Cette fonction ne sert pas à grand chose.

        Paramètres
        ----------
        nombre1 : int Le premier nombre
        nombre2 : int Le second nombre

        Retour
        -------
        int : Le produit des deux nombres.
        """
        return nombre1 * nombre2
```

<br/>

L'objectif d'une chaîne de documentation est d'être courte, mais aussi précise.

<br/>

De manière générale, la chaîne de documentation d'une fonction doit contenir sa spécification, c'est-à-dire :

- son rôle, ce que fait la fonction ou la méthode,
- la (les) valeur(s) accepté(es) en paramètre(s) 
- la (les) valeur(s) qu'elle renvoie
- On peut ajouter des informations générales sur leur fonctionnement.

<br/>

## Prévenir certaines erreurs
### 1. La programmation défensive
Dans ce type de programmation, on suppose que les fonctions/procédures sont appelées comme il faut, dans le respect de leurs préconditions, rappelons qu'il s'agit de code sur lequel on a le contrôle. Cependant, on prévoit néanmoins un garde-fou.


<br/>

Au début de la fonction/procédures, on effectue des tests pour vérifier les préconditions à l'aide d'un système d'assertions : l’interpréteur vérifie qu’une assertion (test logique) est correcte et déclenche une exception (erreur provoquant l’arrêt du programme et l’affichage d’un message d’erreur) si ce n’est pas le cas.


On évite ainsi le pire scénario qui serait d’avoir un comportement inattendu du programme, qui déclenche des erreurs longtemps après l'exécution incorrecte de la fonction, ce qui serait très difficile à debugger.
Attention, les assertions ne doivent pas faire partie du code fonctionnel !

<br/>

### 2. Les outils de la programmations défensive⚓︎
#### 2.1 Annotation de type des fonctions
Les annotations de type de Python constituent une indication pour le développeur. Attention ils n'interdisent rien, Python reste permissif !

```python
def ont_meme_parite(n: int, m: int) -> bool:
        return n % 2 == m % 2
```
<br/>


#### 2.2 Lever des assertions avec assert
Les assertions permettent de vérifier une condition en cours d'exécution d'un programme.

- Si la condition est vérifiée, le programme continue normalement.
- Quand l’assertion est violée, c'est-à-dire que la condition n'est pas vérifiée, alors une exception est émise avec le message demandé par l’utilisateur.

<br/>

Une exception est chargée de signaler un comportement exceptionnel (mais prévu) d’une partie spécifique d’un logiciel. Le mécanisme d’exceptions permet aussi d’interrompre proprement un programme.

```python
    def est_voyelle(lettre: str) -> bool:
        ''' La docstring
        '''
        assert isinstance(lettre, str), "lettre doit être de type str"
        assert len(
            lettre) == 1, "La chaîne `lettre` ne devrait contenir qu'un seul caractère"

        return lettre in "AEIOUYaeiouy"
```

<br/>

##### A. Connaître le type d'une variable
Pour tester le type d'une variable, nous utiliserons deux instructions : `type()` qui renvoie le type sous la forme d'une *chaîne de caractère*, et `isinstance()` qui renvoie *un booléen*. Exemple d'utilisation :

```python
    def est_strictement_plus_grand(a: int, b: int) -> float:
        ''' La docstring …
        '''
        assert type(a) is int, "a doit être de type int"
        assert isinstance(b,int), "b doit être de type int"
        return a > b
```

<br/>

## Les tests
### 1. Généralités sur les tests
Lors des différentes étapes du développement d'un logiciel, on rencontre différents tests, parmi lequel on trouve généralement : *Les tests unitaires, les tests d'intégration, les tests de système complet et les tests d'acceptation*.

<br/>

### 2. Les différents types de test
#### A. Les tests fonctionnels
**Les tests fonctionnels vérifient qu'un programme ou une partie de programme** (par exemple une fonction) **est bien conforme à sa spécification**. La réalisation des tests fonctionnels ne nécessite pas de connaître l'implémentation de la fonction testée, c'est pour cela que ce type de test est souvent appelé test **"Boite Noire"**.

<br/>

#### B. Les tests structurels
**Les tests structurels vérifient le fonctionnement interne d'un programme** ou d'une partie de programme. Ils sont donc dépendants des choix réalisés lors de l'implémentation de la fonction ou du programme, c'est pour cela que ce type de test est souvent appelé test **"Boite Blanche"**.


Lors de ce type de test, on cherche à couvrir l'ensemble des différents chemins d'exécution : toutes les branches conditionnelles, les cas limites d'une boucle, etc.

<br/>

#### C. Les tests de performance
On réalise également des tests de performance. Généralement ceux-ci portent sur la rapidité du programme, mais également sur les besoins en ressources.

<br/>

### 3. À propos des tests
Une fonction qui passe *les tests que vous avez écrits ne vous assurera pas que cette fonction aura toujours le comportement souhaité*. Elle l'aura pour les valeurs des tests, mais pas forcément pour les autres.


En revanche, *si une fonction ne passe pas un des tests, vous avez la certitude qu'il y a un problème* à régler quelque part.


En réalité, les tests ne permettront jamais de conclure qu'un programme est juste !


Il est possible de garantir qu'un programme fonctionnera bien correctement, mais il faut le prouver !

<br/>

### 4. Les tests unitaires

!!! note "Les tests unitaires"
    Un test unitaire est un type de test dans lequel des unités ou des composants, sont testés de manière indépendante, avec des données réduites.

    <br/>
    
    L’objectif principal des tests unitaires est de valider chaque unité du code isolément et de déterminer si elle fonctionne conformément aux attentes, c'est-à-dire à sa spécification.


Pour réaliser des tests unitaires, nous utiliserons des instructions assert, placées dans une fonction en veillant à respecter la convention de nommage : `test_nom_de_la_fonction_testé`

```python

def puissance(x, n: int):
    # ici l'implémentation de la fonction qui retourne x^n, avec n entier.


def test_puissance():
    assert puissance(2, 3) == 8, "cas quelconque"
    assert puissance(0, 2) == 0, "cas ou la valeur est nulle."
    assert puissance(5, 0) == 1, "cas ou la puissance est nulle."
    assert puissance(-2, 5) == -32, "cas ou la valeur est négative"
    assert puissance(0.5, 2) == 0.25, "cas ou la valeur n'est pas un entier"
```

Les tests unitaires doivent êtres : justes, rapides, pertinents, répétables, indépendants, uniques, auto validants.


<br/>


## Débogage

- Identifier le type d'erreur, au bas du message.
- Lire le message qui accompagne l'erreur, ce message vous indique souvent comment corriger votre erreur.
- Trouver la position de notre erreur, en déchiffrant la partie supérieure du message.
