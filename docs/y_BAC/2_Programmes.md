---
title : "Programme"

---

# Programme

## Le programme de l'année

!!! Abstract "Textes réglementaires"
    [https://eduscol.education.fr/document/30010/download](hhttps://eduscol.education.fr/document/30010/download)



## Aménagement pour les épreuves du bac

Lors de l'épreuve terminale dans l'enseignement de spécialité numérique et sciences informatiques, les candidats peuvent être évalués sur les parties suivantes du programme de la classe de terminale :

<br/>

Rubrique « **Structures de données** », uniquement les items suivants :

- Structures de données, interface et implémentation
- Vocabulaire de la programmation objet : classes, attributs, méthodes, objets
- Listes, piles, files : structures linéaires. Dictionnaires, index et clé
- Arbres : structures hiérarchiques. Arbres binaires : nœuds, racines, feuilles, sous-arbres gauches, sous-arbres droits


<br/>

Rubrique « **Bases de données** », uniquement les items suivants :

- Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel
- Base de données relationnelle
- Langage SQL : requêtes d'interrogation et de mise à jour d'une base de données

<br/>

Rubrique « **Architectures matérielles, systèmes d'exploitation et réseaux** », uniquement les items suivants :

- Gestion des processus et des ressources par un système d'exploitation
- Protocoles de routage

<br/>

Rubrique « **Langages et programmation** », uniquement les items suivants :

- Récursivité
- Modularité
- Mise au point des programmes. Gestion des bugs

<br/>

Rubrique « **Algorithmique** », uniquement les items suivants :

- Algorithmes sur les arbres binaires et sur les arbres binaires de recherche
- Méthode « diviser pour régner »



!!! Abstract "Textes réglementaires"
    - [https://www.education.gouv.fr/bo/22/Hebdo36/MENE2227884N.htm](https://www.education.gouv.fr/bo/22/Hebdo36/MENE2227884N.htm)
