---
title : "Calendrier"

---

# Calendrier


## Période de notation trimestrielle :

- **Trimestre 1 :** Du 5 septembre au 2 décembre *(11 semaines)*
- **Trimestre 2 :** Du 5 décembre au 10 mars *(10 semaines)*
- **Trimestre 3 :** Du 13 mars au 2 juin *(10 semaines)*

## Épreuves du Bac :

- **Épreuve écrite :** 20-21 mars
- **Épreuve pratique :** la semaine suivant les écrits !
- **Grand Oral :** lundi 20 juin au vendredi 1er juillet *(dates de 2022 à titre indicatif !)*