---
title : "Sujets et Corrigés"

---


# Sujets de Baccalauréat

## 2022

- [Polynésie](pdf/2022/2022_Polynesie.pdf)

<br/>

## 2021

- [Amérique du Nord](pdf/2021/21_Amérique_du_Nord.pdf)
- [Centres Étrangers 1](pdf/2021/21_Centres_Etrangers_1.pdf)
- [Centres Étrangers 2](pdf/2021/21_Centres_Etrangers_2.pdf)
- [Métropole 1](pdf/2021/21_Metropole_1.pdf)
- [Métropole 2](pdf/2021/21_Metropole_2.pdf)
- [Métropole Candidats Libres 1](pdf/2021/21_Metropole_Candidats_libres_1.pdf)
- [Métropole Candidats Libres 2](pdf/2021/21_Metropole_candidats_libres_2.pdf)
- [Polynésie](pdf/2021/21_Polynesie.pdf)
- [Septembre](pdf/2021/21_Septembre.pdf)

<br/>

## 2020

- [sujet 0](pdf/2020/sujet_0.pdf)



!!! note ""
    - [https://pixees.fr/informatiquelycee/term/suj_bac/](https://pixees.fr/informatiquelycee/term/suj_bac/)

    - Une autre site que à l'avantage de proposer un rangement par thème, mais il n'y a pas que des sujets de bac : [https://e-nsi.gitlab.io/ecrit/2-struct/](https://e-nsi.gitlab.io/ecrit/2-struct/)
