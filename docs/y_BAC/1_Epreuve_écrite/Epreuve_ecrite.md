---
title : "Épreuve écrite"

---



# Épreuve écrite


**Durée :** *3 heures 30*

## Modalités de l'épreuve écrite

La partie écrite consiste en la résolution de trois exercices permettant d'évaluer les connaissances et les capacités attendues conformément aux programmes de première et de terminale de la spécialité. Chaque exercice est noté sur **4 points**.

<br/>

Le sujet **propose cinq exercices**, parmi lesquels **le candidat choisit les trois qu'il traitera**. Ces cinq exercices permettent d'aborder les différentes rubriques du programme, sans obligation d'exhaustivité.

<br/>

Le sujet comprend obligatoirement **au moins un exercice relatif à chacune des trois rubriques** suivantes : *traitement de données en tables et bases de données ; architectures matérielles, systèmes d'exploitation et réseaux ; algorithmique, langages et programmation*.


!!! Abstract "Textes réglementaires"
    - [https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm?cid_bo=149244](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm?cid_bo=149244)



## Modification officielle pour le BAC 2022

**3. Numérique et sciences informatiques**

La note de service du 11 février 2020, complétée par la note de service du 12 juillet 2021 et relative à l'épreuve terminale de l'enseignement de spécialité numérique et sciences informatiques de la classe de terminale de la voie générale, est modifiée comme suit :

<br/>

Dans le titre « 1. Partie écrite », le paragraphe « Modalités » rédigé comme suit :

<br/>

« La partie écrite consiste en la résolution de trois exercices permettant d'évaluer les connaissances et les capacités attendues conformément aux programmes de première et de terminale de la spécialité. Chaque exercice est noté sur 4 points.

<br/>

« Le sujet propose cinq exercices, parmi lesquels le candidat choisit les trois qu'il traitera. Ces cinq exercices permettent d'aborder les différentes rubriques du programme, sans obligation d'exhaustivité. Le sujet comprend obligatoirement au moins un exercice relatif à chacune des trois rubriques suivantes : traitement de données en tables et bases de données ; architectures matérielles, systèmes d'exploitation et réseaux ; algorithmique, langages et programmation. »

<br/>

est remplacé par la phrase ainsi rédigée :

<br/>

« Le sujet comporte trois exercices indépendants les uns des autres, qui permettent d'évaluer les connaissances et compétences des candidats. ».






!!! Abstract "Textes réglementaires"
    - [https://www.education.gouv.fr/bo/22/Hebdo36/MENE2226770N.htm](https://www.education.gouv.fr/bo/22/Hebdo36/MENE2226770N.htm)