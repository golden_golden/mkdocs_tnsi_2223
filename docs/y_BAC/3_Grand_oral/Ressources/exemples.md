# Exemples de sujets qui ont été vue en grand oral


-----

Préparé par nos élèves :

- Comment l’informatique permet-elle de sécuriser les transactions des cryptomonaies ?
- Qu’est-ce qui fait qu’une voiture est autonome ?
- Comment les robots militaires sont-ils contrôlés ?
- Quels sont les impacts de l’obsolescence programmée dans la société ?
- Comment le machine learning a-t-il révolutionné le domaine du médical
- En quoi les IA et les robots ont-ils permit une hausse de production dans les entreprises ?
- Comment l’informatique permet d’améliorer l’aide humanitaire?
- En quoi les programmes écrits en langage en python permettent-ils d’effectuer des calculs scientifiques complexes ?
- En quoi les bases de données permettent la recherche d’information ?
- En quoi la cryptographie peut-elle optimiser la sécurisation des informations confidentielles?
- Par quelles mesures la technologie Blockchain va t-elle remplacer les transactions classique
- En quoi la miniaturisation a permis l’évolution des ordinateurs ?
- Quels sont les impacts de l’avancée des systèmes informatiques sur les emplois ?
- En quoi les bases de données ont permis une évolution de l’organisation du travail ?
- En quoi les nouvelles technologies ont impactés l’engagement politique ?
- De quelle manière sont cryptées les données informatiques ?
- Quel est le role de l’informatique dans la sécurité intérieure ?
- En quoi les bases de données sont-elles importantes dans la prise de décision ?
- Comment améliorer ses performances sportive grâce à l’informatique ?
- La 5G: quels sont ses avantages et ses inconvénients ?
- Comment fonctionne un logiciel de modélisation tridimentionnelle ?
- Quel impact les IA ont elles eu dans le monde des échecs ?


Rencontrés au grand oral :

- Quels sont les impacts et la dangerosité des bugs dans notre société ?
- Comment un ordinateur peut-il apprendre de ses erreurs et pourquoi ?
- En quoi la notion de récurrence intervient-elle dans l’élaboration et le fonctionnement d’un programme récursif
- Quelle est l’importance des graphes de nos jours ?
- En quoi le raisonnement par récurrence est-il utile en mathématiques et informatiques ?
- A quoi servent les bases de données ?
- Comment les bases de données ont-elles révolutionné notre quotidien ?
- Pourquoi chiffrer les communications ?
- Comment le cryptage nous permet-il d’utiliser les outils informatiques de manière sûre ?
- Quelles sont les différentes étapes pour parvenir à un site web complet ?
- Quels sont les impacts et la dangerosité des bugs dans notre société
- Comment les transactions des cryptomonnaies sont-elles sécurisés ?
- Comment calculer le coût d’exécution en temps d’un algorithme ?
- Quels sont les les enjeux du cyber espace et quel est le rôle de la cryptologie dans cette nouvelle plateforme ?
- Pourquoi utilise ton les nombres premiers dans l’algorithme RSA ? 

-----
Bonjour !
Préparés par mes élèves :

- Comment s’adapter à la cyberguerre ?
- Comment casser le chiffrement de Vigenère (Maths / NSI, l’élève en question y parle de chiffrement à clé puis pour la partie maths, introduit la notion d’indice de coïncidence d’un texte et l’utilise pour expliquer comment trouver la longueur de la clé dans un chiffrement de Vigenère)
- Comment les cryptomonnaies sont-elles sécurisées ?
- Comment l’art de couper en deux permet-il de résoudre des problèmes en mathématiques et informatique (Maths/NSI, sujet autour de la dichotomie, parle de la recherche dichotomique, des algorithmes de dichotomie pour résoudre des équations en maths, voire des arbres binaires de recherche …)
- Les tours de Hanoï , entre récurrence et récursivité (Maths/NSI)
- Comment les caractères de nos claviers sont-ils encodés ?
- Deep Blue, un algorithme imbattable ? (parle d’IA et de bases de données)
- Que se cache-t-il derrière une page web ?
- La suite de Fibonacci, quels algorithmes pour calculer ses termes? (Maths/NSI)
- Comment les données peuvent elle aider les entreprises à se développer ?
- Comment les systèmes d’exploitation permettent de rendre aisée l’utilisation de notre ordinateur ?

-----
J’ajoute ma pierre à l’édifice. Bon, c’est une pierre mal taillée car je fais cela vaguement de tête mais l’idée des sujets est là :

- Comment l’algèbre booléenne a-t-elle permis le développement de l’informatique ?
- Plusieurs questions autour de la cryptographie : un sujet axé sur le HTTPS, un sur l’authentification, un sur quelques grands principes de la crypto.
- Plusieurs questions autour des femmes en informatique : certains sujets avaient un axe historique d’autres plus sociologiques (plusieurs élèves ayant un profil SES-NSI)
- Récursivité et récurrence : similarités et différences.
- La sonde Perseverance et le binaire
- Bug de rover martien et système d’exploitation multitâche
- Encoder toutes les écritures du monde avec des 1 et des 0
- Fonctionnement et enjeux des BDD-


Sujets rencontrés au GO :

- Combien de fois retrouve-t-on le mot « Harry » dans la série de romans Harry Potter ?
- Le bitcoin est-il une monnaie comme les autres ? (sujet SES-NSI)
- Comment Mathématiques et Informatiques imitent-elles la nature ? (sujet Math-NSI sur les fractales)
- Les risques de la mise en réseau des industries. (Sujet SI-NSI)
- Quels sont les enjeux démocratiques des NFT ?
- L’informatique, une prison dorée ? (ne cherchez pas de lien avec le programme, c’était un sujet fourre-tout avec assez peu de contenu)
- Comment stocker le code pénal sur un petit espace de stockage ? (codage de Huffmann)
- Comment Shazam fonctionne-t-il ? (sujet Math-NSI)
- Pourquoi le codage de Fibonacci est-il une méthode simple de compression des données ?

[https://mooc-forums.inria.fr/moocnsi/t/liste-de-sujets-pour-le-grand-oral/2721/4](https://mooc-forums.inria.fr/moocnsi/t/liste-de-sujets-pour-le-grand-oral/2721/4)

----- fin fil 1













----- début fil 2

- Le logiciel libre a-t-il un avenir ?
- Les cyberattaques incarnent-elles les nouvelles menaces du monde d’aujourd’hui?
- Le langage PHP compromet-il la sécurité du web ?
- Quelle est la place des réseaux sociaux dans la politique ? (aucun lien avec le programme)
- Femmes et numérique : quelle histoire ? Et où en sommes nous aujourd’hui?
- L’ordinateur quantique va-t-il tuer les cryptomonnaies ?
- La compression de données est-elle encore nécessaire ? Peut-elle progresser ?
- Les VPN nous protègent-ils des hackers ?
- Quelles sont les promesses du métaverse ?
- Base de données : la vie privée sur internet
- L’IA peut-elle remplacer l’être humain (sujet peu en lien avec le programme …)
- Peut-on développer une BDD importante sans avoir recours à une base de donnée répartie ?
- Les IA, le renouveau de l’art ? (sujet très peu en lien avec le programme)
- Nos données personnelles sont-elles toujours personnelles ?
- Deepfake, la fin de la preuve par image ? (sujet peu en lien avec le programme)
- En quoi la machine ENIGMA montre-t-elle le début de l’ère informatique ?

Remarques : 
De manière générale, les sujets qui parlent de la blockchain ne sont pas maitrisés du tout.

Voici les sujets que mes élèves NSI ont préparé (mais ils ne sont pas tombés sur ceux-ci puisqu’il n’y avait pas de membre NSI dans leur jury).

- Comment cacher une image dans une autre image ?
- Comment ranger ses données de façon efficace ?
- Pourquoi existe-t-il tant de langages informatiques ?
- Comment les données importantes sont-elles sécurisées sur Internet ?
- Comment le chiffrement RSA sécurise-t-il nos données ?
- Comment sont structurés les sites web ?
- Quelles sont les méthodes d’attaque du chiffrement par substitution ?


??? note "sources"
    - [https://mooc-forums.inria.fr/moocnsi/t/exemples-sujets-grand-oral-2022/6412](https://mooc-forums.inria.fr/moocnsi/t/exemples-sujets-grand-oral-2022/6412)



----- fin fil 2




??? note " forum non traité 1" 
    L’an dernier et cette année encore, j’ai conseillé à tous mes élèves de choisir leur question de GO en relation avec leur projet de terminale, et si je n’ai rien imposé ils ont tous suivi ma recommandation. Par exemple une élève qui avait programmé plusieurs fractales en projet pendant l’année (fougère de Barnsley, arbre de Pythagore, etc.) a choisi pour son GO « Comment les fractales sont présentes dans la nature ?»; ou un autre élève qui a developpé une appli de coach sportif avec tkinter a choisi une question qui ressemble ≈ à « Comment l’informatique peut aider les gens à se remettre au sport ?».

    Je me rends compte qu’aucun des candidats que je viens d’interroger n’a suivi cette approche et je me demande si finalement c’est une bonne idée ou pas d’essayer de coller à son projet pour le grand oral, ou si je devrais plutôt laisser tomber et les orienter vers autre chose.
    
    sources forum [https://mooc-forums.inria.fr/moocnsi/t/question-de-grand-oral-et-projet-de-terminale/6382](https://mooc-forums.inria.fr/moocnsi/t/question-de-grand-oral-et-projet-de-terminale/6382)


??? note " forum non traité 2"
    [https://mooc-forums.inria.fr/moocnsi/t/un-peu-daide-sur-un-contenu-de-go/6063/2](https://mooc-forums.inria.fr/moocnsi/t/un-peu-daide-sur-un-contenu-de-go/6063/2)
    relire le fil et résumer 

    5 minutes c'est court


    L’essentiel est que le sujet se rattache à un thème étudié en classe.
    Ensuite, les 2 éventualités sont possibles :

    soit elle présente des généralités pendant l’exposé et elle présentera un exemple pendant les questions.
    soit elle présente un exemple pendant l’exposé.

    Le plus important est que le naïf comprenne bien l’exposé et soit intéressé par le sujet. À mon avis, c’est plus facile avec un exemple concret .

    Ensuite, je leur conseille de laisser volontairement des zones d’ombre à certains endroits pour susciter des questions (de préférence les passages difficiles pour un naïf, donc par exemple l’algorithme).

    sources [https://mooc-forums.inria.fr/moocnsi/t/un-peu-daide-sur-un-contenu-de-go/6063/2](https://mooc-forums.inria.fr/moocnsi/t/un-peu-daide-sur-un-contenu-de-go/6063/2)



