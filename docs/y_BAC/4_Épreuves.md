---
title : "Épreuves "

---

# Épreuves de spécialité

**Objectifs**

L'épreuve porte sur les notions et contenus, capacités et compétences du programme de l'enseignement de spécialité numérique et sciences informatique de la classe de terminale (cf. arrêté du 17 juillet 2019 paru au BOEN spécial n° 8 du 25 juillet 2019). Les notions rencontrées en classe de première (cf. arrêté du 17 janvier 2019 paru au BOEN spécial n° 1 du 22 janvier 2019) mais non approfondies en classe de terminale, doivent être connues et mobilisables. Elles ne peuvent cependant pas constituer un ressort essentiel du sujet.

<br/>

**Nature de l'épreuve**

L'épreuve terminale obligatoire de spécialité est composée de deux parties :

- [**une partie écrite**](./1_Épreuve_écrite/Epreuve_ecrite.md), comptant pour **12 points** sur 20.
- et [**une partie pratique**](./2_Épreuve_pratique/Epreuve_pratiques.md), comptant pour **8 points** sur 20.

<br/>

La note globale de l'épreuve est donnée sur **20 points**.

<br/>

!!! Abstract "Textes réglementaires"
    *Source : [https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm?cid_bo=149244](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm?cid_bo=149244)*


