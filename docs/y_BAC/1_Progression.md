---
title : "Progression"

---


# Progression

??? warning "Attention"
    Cette progression, n'est ici qu'à titre indicatif, il est tout à fait possible qu'elle soit modifiée durant l'année.



- Mise au points des programmes

- Modularité
  
- Programmation orienté objets (POO)
  
- La récursivité

---
### Vacances d'octobre

---

- Les bases de données relationnelles (thème complet)
  

- Les structures de données linéaires

    - liste, piles, files, tableau…


- Les structures de données non linéaires
  
    - Les arbres + Algorithmes 
    - Graphes + Algorithmes 

- Routages 

-----

# Épreuves du BAC 

----


- Graphe et leurs algorithmes
  
- La programmation dynamiques

- sécurisation des communications
  
- Les systèmes sur puces (soc)

- Paradigme de programmation

- Programme en tant que donnée
  
- Calculabilité, décidabilité
  
- Recherche textuelle
  
-----

# BAC : Grand Oral

----