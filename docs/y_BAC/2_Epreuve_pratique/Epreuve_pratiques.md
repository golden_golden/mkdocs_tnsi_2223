---
title : "Épreuve pratique"

---
# Épreuve pratique

**Durée** : *1 heure*
  
## Modalités de l'épreuve pratique

L'épreuve pratique donne lieu à une note sur **8 points**, qui s'ajouteront aux 12 points de l'épreuve écrite.

<br/>

La partie pratique consiste en la résolution de **deux exercices sur ordinateur**, chacun étant noté sur **4 points**.

<br/>

Le candidat est évalué sur la base d’un dialogue avec un professeur-examinateur. Un examinateur évalue au maximum quatre élèves. L’examinateur ne peut pas évaluer un élève qu’il a eu en classe durant l’année en cours.
L’évaluation de cette partie se déroule au cours du deuxième trimestre pendant la période de l’épreuve écrite de spécialité.

<br/>

**Premier exercice :**

Le premier exercice consiste à programmer un algorithme figurant explicitement au programme, ne présentant pas de difficulté particulière, dont on fournit une spécification. Il s’agit donc de restituer un algorithme rencontré et travaillé à plusieurs reprises en cours de formation. Le sujet peut proposer un jeu de test avec les réponses attendues pour permettre au candidat de vérifier son travail.

<br/>

**Deuxième exercice :**

Pour le second exercice, un programme est fourni au candidat. Cet exercice ne demande pas l’écriture complète d’un programme, mais permet de valider des compétences de programmation suivant des modalités variées : le candidat doit, par exemple, compléter un programme « à trous » afin de répondre à une spécification donnée, ou encore compléter un programme pour le documenter, ou encore compléter un programme en ajoutant des assertions, etc.


!!! Abstract "Textes réglementaires"
    - [https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm?cid_bo=149244](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm?cid_bo=149244)




## Banque d'exercices

!!! Abstract "Banque de sujets officielle"
    - [https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi)

