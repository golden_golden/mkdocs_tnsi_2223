---
title: "Sujet 19"

---

# **Sujet 19**

<br/>

*Version originale, en pdf :* [ici](pdf2022/22-NSI-19.pdf){. target = "_blank"}

<br/>

### **Exercice 1**

=== "Énoncé" 
    --8<-- 'docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/19_1/enonce.md'


=== "Source Markdown"
        --8<-- 'docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/19_1/enonce.md'

    <br/>



???+ example "S'exercer"

    {{IDE('fichiers/19_1/exo', MAX = 10, SANS = 'eval,max')}}


<br/>


### **Exercice 2**

**Énoncé**

=== "Énoncé" 
    --8<-- 'docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/19_2/enonce.md'

=== "Source Markdown"
        --8<-- 'docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/19_2/enonce.md'

<br/>

???+ example "S'exercer"

    {{IDE('fichiers/19_2/exo', MAX = 10, SANS = 'eval,max')}}

<br/>