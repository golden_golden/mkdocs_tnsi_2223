---
title: "Sujet 02"

---

# **Sujet 02**

<br/>

*Version originale, en pdf :* [ici](pdf2022/22-NSI-02.pdf){. target = "_blank"}

<br/>

### **Exercice 1**

=== "Énoncé" 
    --8<-- 'docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/02_1/enonce.md'


=== "Source Markdown"
        --8<-- 'docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/02_1/enonce.md'

    <br/>



???+ example "S'exercer"

    {{IDE('fichiers/02_1/exo', MAX = 10, SANS = 'eval,max')}}


<br/>


### **Exercice 2**

**Énoncé**

=== "Énoncé" 
    --8<-- 'docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/02_2/enonce.md'

=== "Source Markdown"
        --8<-- 'docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/02_2/enonce.md'

<br/>

???+ example "S'exercer"

    {{IDE('fichiers/02_2/exo', MAX = 10, SANS = 'eval,max')}}

<br/>