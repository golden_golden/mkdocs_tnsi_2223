---
title: "Sujet 01"

---

# **Sujet 01**

<br/>

*Version originale, en pdf :* [ici](pdf2022/22-NSI-01.pdf){. target = "_blank"}

<br/>

### **Exercice 1**
=== "Énoncé" 
    --8<-- "docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/01_1/enonce.md"

=== "Source Markdown"
        --8<-- "docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/01_1/enonce.md"


!!! note "s'exercer"
    {{IDE('fichiers/01_1/exo', MAX = 10, SANS = 'eval,max')}}

<br/>




### **Exercice 2**

=== "Énoncé" 
    --8<-- "docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/01_2/enonce.md"

=== "Sources Markdown"
    ```md
        --8<-- "docs/y_BAC/2_Epreuve_pratique/Annales/2022/fichiers/01_2/enonce.md"
    ```


{{IDE('fichiers/01_2/exo', MAX = 10, SANS = 'eval,max')}}


