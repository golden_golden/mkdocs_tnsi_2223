import os

DOSSIERS_CHEMIN = "/run/media/arnaud/TravailD1/Cours/Lycée/Informatique/30. T_NSI/mkdocs_tnsi_2223_prof/docs/y_BAC/2_Epreuve_pratique/Annales/fichiers/"


def numero(indice: int) -> str:
    if indice < 9:
        texte = "0" + str(indice+1)
    else:
        texte = str(indice+1)
    return (texte)


def creer_dossiers():
    for i in range(40):
        texte = numero(i)
        os.mkdir(texte + "_1")
        os.mkdir(texte + "_2")


def creer_enonce(chemin_fichier):
    nom_fichier = chemin_fichier + "/enonce.md"
    with open(nom_fichier, "w") as file:
        file.write("")


def creer_exo(chemin_fichier):
    nom_fichier = chemin_fichier + "/exo.py"
    with open(nom_fichier, "w") as file:
        if chemin_fichier[-1] == "1":
            file.write("# Exercice 1: Écrire votre fonction ici !")
        else:
            file.write("# Exercice 2: Écrire votre fonction ici !")


def creer_test(chemin_fichier):
    nom_fichier = chemin_fichier + "/exo_test.py"
    with open(nom_fichier, "w") as file:
        file.write("")


def creer_correction(chemin_fichier):
    nom_fichier = chemin_fichier + "/exo_corr.py"
    with open(nom_fichier, "w") as file:
        file.write("")

# ne pas utiliser, cette procédure fonctionne mais les consoles bug lorsque REM est présent !


def creer_remarque(chemin_fichier):
    nom_fichier = chemin_fichier + "/exo_REM.md"
    with open(nom_fichier, "w") as file:
        file.write("**Remarque sur la solution**:")


def creer_fichiers():
    for (chemin_dossier, dir, name) in os.walk(DOSSIERS_CHEMIN):
        if chemin_dossier[-1] in ["1", "2"]:
            creer_enonce(chemin_dossier)
            creer_exo(chemin_dossier)
            creer_test(chemin_dossier)
            creer_correction(chemin_dossier)


# creer_dossiers()
creer_fichiers()
