Programmer une fonction `renverse`, prenant en paramètre une chaîne de caractères non vide
`mot` et renvoie une chaîne de caractères en inversant ceux de la chaîne `mot`.

<br/>

**Exemple :**

```python
>>> renverse("informatique")
"euqitamrofni"
```
