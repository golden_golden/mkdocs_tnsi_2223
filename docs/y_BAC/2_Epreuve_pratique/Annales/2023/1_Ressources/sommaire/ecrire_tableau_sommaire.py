
ANNEE = 2023
FNAME = f"./docs/officiels/Annales/EP/{ANNEE}/{ANNEE}.txt"
ICONES = {"N": ":star:", "B": "<span class='rouge'>:material-bug:</span>", "D": "<span class='navy'>:material-bomb:</span>",
          "M": ":fontawesome-solid-square-root-variable:", "W": "<span class='orange'>:fontawesome-solid-triangle-exclamation:</span>"}


def entete():
    texte_entete = "\n"
    texte_entete += "|Numéro | Lien de téléchargement| Thème exercice 1 | Thème exercice 2  | Code fourni |Correction|\n"
    texte_entete += "|-------|-----------------------|------------------|-------------------|-------------|----------|\n"
    return texte_entete


def code_icones(id_icones):
    texte_icones = ""
    for lettre in ICONES:
        if lettre in id_icones:
            texte_icones += ICONES[lettre]
    return texte_icones


def lien_correction(statue_correction,nums):
    if '0' in statue_correction:
        lien = f"[Sur Pixees](https://pixees.fr/informatiquelycee/term/ep/s{nums}.html)" + \
            "{target=_blank}"
    else:
        # lien à adapter !
        lien = f"[{ANNEE}-S{str(nums).zfill(2)}](../../Corriges/{ANNEE}-S{str(nums).zfill(2)}/)"
    return lien


def lignes():

    with open(FNAME, "r", encoding="utf-8") as fichier_texte:

        lignes_tableau = ""

        nums = 1
        for ligne in fichier_texte:

            lf = ligne.split(",")

            # Création des données
            icones = code_icones(id_icones=lf[4])
            lien_sujet = f"../../../officiels/Annales/EP/{ANNEE}/{lf[0]}/{lf[0]}.pdf"
            theme_exo1 = lf[1]
            theme_exo2 = lf[2]
            lien_code_python = f"../../../officiels/Annales/EP/{ANNEE}/{lf[0]}/{lf[0]}.py"
            correction = lien_correction(statue_correction=lf[3],nums=nums)

            # Mise en forme de la ligne
            lignes_tableau += f"|**{nums}**{icones}|[Sujet N°{nums}]({lien_sujet}}) | {theme_exo1} | {theme_exo2} | [:material-download: Code]({lien_code_python}) | {correction} |\n"

            nums += 1

    return lignes_tableau


def tableau():
    tableau = entete()
    tableau += lignes()
    return tableau
