import os




ANNEE=2023

# création des dossiers de destinations
os.system(f"mkdir pdf{ANNEE}")
os.system(f"mkdir scripts")


# Déplacement des fichiers
for num  in range(1,46):
    
    # la méthode zfill permet de mettre de 0 devant jusqua ce que le nomber de carractére total soit égal au paramètre
    snum=str(num).zfill(2)
    
    os.system(f"mv ./23-NSI-{snum}/23-NSI-{snum}.pdf ./pdf{ANNEE}/23-NSI-{snum}.pdf")
    os.system(f"mv ./23-NSI-{snum}/23-NSI-{snum}.py ./scripts/23-NSI-{snum}.py")