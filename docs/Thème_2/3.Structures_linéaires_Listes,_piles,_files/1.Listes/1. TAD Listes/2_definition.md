---
title: "Définition"

---

# Le TAD Liste

## 2. Définition du TAD liste (itérative) : 

***Type abstrait*** : Liste

<br/>

***Utile :*** Entier, Elément, Booléen

<br/>

***Opérations*** : Voici les primitives communément utilisées pour manipuler des  listes ; il n'existe pas de normalisation pour les primitives de  manipulation de listes, leurs noms respectifs sont donc indiqués de manière informelle.

<br/>

Primitives de base :

$$
  creer\_liste :\space → Liste
$$

$$
  longueur : Liste → Entier
$$

$$
inserer : Liste × Entier × Element → Liste
$$

$$
  supprimer : Liste × Entier → Liste
$$

$$
est vide : Liste → Booleen
$$

$$
contenu : Liste × Entier → Element
$$

<br/>

Primitives auxiliaires fréquemment rencontrées :

$$
Premier : Liste  → Element
$$

$$
Dernier : Liste  → Element
$$

$$
Prochain : Liste × Entier → Element
$$

$$
Precedent : Liste × Entier → Element
$$

$$
Cherche : Liste × Element →  Booleen
$$

<br/>


***Préconditions*** :

Soient l une liste, e un élément et i un entier.

$$
inserer(l,i,e) \space \text{est defini si et seulement si} \space 0 ≤ i≤ longueur(l)
$$

$$
supprimer(l,i)\space \text{est defini si et seulement si} \space 0 ≤ i≤ longueur(l)
$$

$$
contenu(l,i)\space \text{est defini si et seulement si} \space 0 ≤ i≤ longueur(l)
$$


<br/>

***Les axiomes*** :

Soient l une liste, e un élément et i,j deux entiers.

$$
  longueur(creer\_ liste) = 0
$$

$$
longueur(inserer(l,i,e)) = longueur(l) +1
$$

$$
longueur(supprimer(l,i)) = longueur(l) - 1
$$

$$
contenu(inserer(l,i,e),j) =\begin{cases} e \space \text{si} \space i=j \\ contenu(l,j) \space \text{si} \space   i > j \\ contenu(l,j-1) \space \text{si} \space   i < j  \end{cases}
$$

$$
contenu(supprimer(l,i),j) =\begin{cases} contenu(l,j) \space \text{si} \space   i > j \\ contenu(l,j+1) \space \text{si} \space   i ≤ j  \end{cases}
$$



!!! note "Remarques"
    * La définition du type abstrait `liste` **n'est PAS normalisée**, notamment pour les opérations/**primitives** des listes : il peut donc y avoir quelques variations sur ce sujet. Néanmoins, le principe reste fondamentalement le même.
    * Généralement, les éléments seront de même type de données (dans les langages à **typage statique**: Java/C++,), mais la structure de données liste ne l'impose pas (donc certains langages à **typage dynamique** : Python, .. autorisent des éléments de types différents)
    * **Positions Tête /Queue** : Selons les contextes et les auteurs, la **Tête** et la **Queue** peuvent être inversées.
    * La modélisation **linéaire** d'une Liste se prête bien à la représentation d'éléments qui arrivent et/ou partent dans le temps. Dans ce contexte :
        * **La Tête représente souvent le Premier élément arrivé**
        * **La Queue représente souvent le Dernier élément arrivé** (comme quand on on arrive dernier pour accéder à une ressource, et qu'on se place à la queue)
    * La structure abstraite de liste est à la base de nombreuses autres structures, notamment les **Piles** et les **Files**, les **Files de priorité**, etc..
