---
title: "Qu'est ce qu'une Liste"

---

# Le TAD Liste

## 1. Description

Une liste est une  structure abstraite de données permettant de regrouper des données sous forme séquentielle (collection linéaire d’éléments).

Elle est constituée d’éléments d’un même type, chacun possédant un rang (aussi appelé indice).
La longueur d’une liste correspond au nombre de ses éléments.
Il est possible d’accéder à n’importe quel ième élément de la liste à partir de son rang.
Enfin, une liste est évolutive : on peut ajouter ou supprimer n’importe lequel des ses éléments.

<br/>

Le langage de programmation Lisp (inventé par John McCarthy en 1958) a été l’un des premiers langages de programmation à introduire cette notion de liste (Lisp signifie «list processing»).



!!! def "Listes"
    Une <bred>Liste</bred> :fr: ou <bred>List</bred> :gb:, est un **Type Abstrait de Données (TAD)** représentant un **conteneur/collection fini** d'<bred>éléments</bred>/objets/données :
    
    * Tous accessibles (contrairement aux **Piles** et aux **Files**), mais de plusieurs manières, selon l'implémentation choisie :

        * soit par **Accès Séquentiel** (l'un derrière l'autre, avec la notion de ***successeur*** ou ***suivant***), au minimum.
        * soit par **Accès Direct** / **Accès Aléatoire** :fr:, ou **Random Access** :gb:, c'est -à-dire **via un indice**, lorsqu'il y en a un
        
    L'**ordre** des éléments a donc une importance


