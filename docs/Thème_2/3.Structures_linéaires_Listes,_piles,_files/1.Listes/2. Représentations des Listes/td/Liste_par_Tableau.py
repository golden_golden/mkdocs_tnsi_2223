# Implémentation d'une Liste avec un Tableau (type 'list' de Python)

class Liste:
    def __init__(self):
        self.data = []

    def est_vide(self):
        return len(self.data) == 0 
  
    def ajouter_queue(self,x): # en O(1)
        self.data.append(x)

    def retirer_queue(self):
        if self.est_vide() == True :
            raise IndexError("Vous avez essayé de retirer la tête dans une liste vide !")
        else :
            return self.data.pop()

    def ajouter_tete(self,x): # en O(n)
        self.data.insert(0,x)

    def retirer_tete(self):
        if self.est_vide() == True :
            return []
            # raise IndexError("Vous avez essayé de retirer la queue dans une liste vide !")
        else :
            return self.data.pop(0)

    def renverser(self):
        renvListe = Liste()
        n=len(self.data)
        for i in range(n):
            renvListe.data.append(self.data[n-1-i])
        return renvListe

    def concatener(self,l2):
        """concatène self avec l2 dans cet ordre"""
        nouvelle = Liste()
        nouvelle.data = self.data+l2.data
        return nouvelle

    def __repr__(self):       # Hors-Programme : pour afficher 
        s = "|•|"              # convenablement la pile avec p
        for k in self.data :
            s = s + str(k) + "|"
        s += "⟂|"
        return s

    # def __str__(self):       # Hors-Programme : pour afficher 
    #     s = "|"              # convenablement la liste avec print(p)
    #     for k in self.data :
    #         s = s + str(k) + "|"
    #     s += "⟂|"
    #     return s

    # def __add__(self,l2):
    #     return self

if __name__ == "__main__":
    # l=Liste()
    # l.ajouter_tete(2)
    # l.ajouter_tete(3)
    # l.ajouter_queue(4)
    # l.ajouter_queue(5)
    # print(l)
    l1 = Liste()
    l1.ajouter_tete(3)
    l1.ajouter_tete(4)
    l1.ajouter_tete(5)
    print("l1 =", l1)

    l2 = l1.renverser()
    print("l2 = ", l2)
    # l2 = Liste()
    # # print("l2 =", l2)
    # l2.ajouter_tete(6)
    # l2.ajouter_tete(7)
    # l2.ajouter_tete(8)
    # print("l2 =", l2)

    # l3  = l1.concatener(l2)
    # print("l3 =", l3)