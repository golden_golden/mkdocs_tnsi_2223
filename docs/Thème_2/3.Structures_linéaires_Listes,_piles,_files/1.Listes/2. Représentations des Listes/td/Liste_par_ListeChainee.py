# Implémentation avec Liste Chaînée

class Cellule:
    def __init__(self, valeur=None, suivante=None):
        self.valeur = valeur
        self.suivante = suivante

    def get_valeur(self) -> int:
        return self.valeur

    def get_suivante(self):
        return self.suivante

    def set_suivante(self, cellule) -> None: # 'cellule' est définie comme la suivante de self (de la cellule courante)
        if cellule is None or (type(cellule.valeur) == type(self.valeur)):
            self.suivante = cellule
        else:
            raise TypeError
    
    def get_queue(self):
        if self.get_suivante() is not None:
            return self.get_suivante().get_queue()
        else:
            return self

    def __getitem__(self, i):  # en O(n)
        if self is None:
            return None
        if i == 0:
            return self.get_valeur()
        else:
            return self.get_suivante().__getitem__(i-1)

    def __repr__(self) -> str:
        # Pour implémentation de Liste Chaînée par classe 'Cellule'
        cell = self
        mylist = f"|•|{cell.valeur}|"
        while cell.get_suivante() is not None: # en O(n)
            cell = cell.get_suivante()
            mylist += str(cell.get_valeur())+"|"
        mylist += "⟂|"
        return mylist
        # Pour implémentation de Liste Chaînée par classe 'Liste'
        # return f"{self.valeur}"

    def __len__(self):
        if self.get_suivante() is None:
            return 1
        else:
            return 1 + len(self.get_suivante())

# l = Cellule(3, Cellule(8, Cellule(5, Cellule(7, None))))
# l = Cellule(3, None)
# l = Cellule(3, Cellule(8, None))
# print("l = ", l)
# print("l.get_valeur() = ", l.get_valeur())
# print("l[0] = ", l[0])
# print("l[1] = ", l[1])
# print("l[2] = ", l[2])
# print("longueur = ", len(l))
# print("queue = ", l.get_queue())

class Liste:
    def __init__(self) -> None:
        self.tete = None
    
    def est_vide(self) -> bool:
        return self.tete is None
    
    def ajouter_tete(self, valeur) -> None: # en O(1)
        """Ajoute 'valeur' en tête de liste"""
        print("Ajoute Tête", valeur)
        self.tete = Cellule(valeur, self.tete)

    def get_tete(self) -> None: # en O(1)
        return self.tete

    def retirer_tete(self) -> None: # en O(1)
        """retire le premier élément de la liste"""
        print("Retirer Tête")
        if self.tete is not None: # pour ne pas renvoyer d'erreurs si self. est vide (queue)
            self.tete = self.tete.get_suivante()

    def get_queue(self) -> None: # en O(n)
        print("Get Queue")
        cell = self.tete
        while (cell is not None) and (cell.get_suivante() is not None):
            cell = cell.get_suivante()
        return cell

    def retirer_queue(self): # en O(n)
        """renvoie la liste courante, privée du dernier élément (queue)"""
        print("Retirer Queue")
        cell = self.tete
        if (cell is None) or (cell.get_suivante() is None): # en O(1)
            self.tete = None
        else: # en O(n)
            while (cell is not None) and (cell.get_suivante().get_suivante() is not None):
                cell = cell.get_suivante()
            cell.set_suivante(None)

    def ajouter_queue(self, valeur): # en O(n)
        """ajoute 'valeur' en queue de liste"""
        print("Ajoute Queue", valeur)
        if self.tete is None:
            self.tete = Cellule(valeur, None)
        else:
            cell = self.tete
            while (cell is not None) and (cell.get_suivante() is not None):
                cell = cell.get_suivante()
            cell = cell.set_suivante(Cellule(valeur, None))

    def insert(self, i, valeur):
        cell = self.tete
        assert type(i) is int, "L'indice doit être entier"
        assert i>=0, "L'indice d'insertion doit être >= 0"  # comme le insert de Python :accepte lorsque i> len(liste)
        print("Insert", valeur,"en position ",i)
        if (self.tete is None) and i==0:
            self.ajouter_tete(valeur)
        else: # Liste d'au moins 1 élément
            premiereCelluleSuivante = self[i]
            if i==0: # on insère en 1ère position
                cell = Cellule(valeur)
                cell.set_suivante(self[0])
                self.tete = cell
            else: # insertion PAS en 1ère position
                if self[i-1] is None: # insertion en position dépassée
                    # recalcule le bon 'i' en cas de dépassement
                    # pour faire comme le type 'list' de Python ...
                    i = len(self)
                if self[i-1].get_suivante() is None: # insertion en Dernière position
                    cell = Cellule(valeur)
                    self[i-1].set_suivante(cell)
                else: # insertion PAS en dernière position
                    cell = Cellule(valeur)
                    self[i-1].set_suivante(cell)
                    self[i].set_suivante(premiereCelluleSuivante)

    def renverser(self):
        nouvelle = Liste()
        if self is None:
            return None
        else:
            cell = self.tete
            while cell is not None:
                nouvelle.ajouter_tete(cell.get_valeur())
                cell = cell.get_suivante()
            return nouvelle

    def concatener(self,l2):
        """concatène self avec l2 dans cet ordre"""
        if self.est_vide():
            return l2
        else:
            queue = self.get_queue()
            queue.set_suivante(l2.tete)
            return self

    def __getitem__(self, i): # en O(n)
        if self.tete is None: # Liste vide
            return None
        else:
            j = 0
            cell = self.tete
            if (cell is None) or (cell.get_suivante() is None): # Liste à 1 seul élément
                return self.tete if i==0 else None
            else: # liste à plus d'un élément
                while (cell.get_suivante() is not None) and (cell.get_suivante().get_valeur() != self.tete.get_valeur()) :
                    cell = cell.get_suivante()
                    j += 1
                    if j==i:
                        return cell
                # On peut retourner None, ou lever une erreur : Choix Perso
                return self.tete if i==0 else None
                # if i==0:
                #     return self.tete
                # else:
                #     raise IndexError("list index "+str(i)+" out of range")

    def __repr__(self) -> str:  # en O(n)
        if self.tete is None:
            return "|•|⟂|"
        else:
            return f"{self.tete}"

    def __len__(self) -> int: # en O(n)
        cell = self.tete
        if cell is None:
            return 0
        else:
            length = 0
            while cell.get_suivante() is not None:
                length += 1
                cell = cell.get_suivante()
            return length + 1

        # TRÈS MAUVAISE IDÉE car vide la liste en cas de demande de longueur !!
        # if self.tete is None:
        #     return 0
        # else:
        #     self.retirer_tete()
        #     return 1 + self.longueur()


l1 = Liste()
# l1.ajouter_queue(3)
l1.ajouter_tete(3)
l1.ajouter_tete(4)
l1.ajouter_tete(5)
print("l1 =", l1)

# l2 = Liste()
# l2.ajouter_tete(6)
# l2.ajouter_tete(7)
# l2.ajouter_tete(8)
# print("l2 =", l2)

l2  = l1.renverser()
print("l2 =", l2)

# l = Liste()
# print("l =",l)
# l.ajouter_tete(5)
# print("l =",l)
# l.ajouter_tete(6)
# print("l =",l)
# l.ajouter_tete(7)
# print("l =",l)
# l.ajouter_tete(8)
# print("l =",l)
# # print("l[3]=",l[3])
# # print("l[5]=",l[5])
# l.insert(3,10)
# print("l =",l)
# l.retirer_tete()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.ajouter_queue(9)
# print("l =",l)
# print("GET QUEUE = ", l.get_queue())
# for i in range(len(l)+10):
#     print("l[",i,"] =",l[i])
# print("LENGTH = ", len(l))
# print("l =",l)

# l.retirer_tete()
# print("l =",l)
# l.retirer_tete()
# print("l =",l)
# l.retirer_tete()
# print("l =",l)
# l.retirer_tete()
# print("l =",l)
# l.retirer_tete()
# print("l =",l)


# print("Longueur = ", len(l))

# l.insert(5,9)
# print("l =",l)
# print("Longueur = ", len(l))

# print("l.longueur() =",l.longueur())
# l.ajouter_queue(4)
# print("l =",l)
# l.ajouter_queue(3)
# print("l =",l)
# l.ajouter_queue(2)
# print("l =",l)
# l.ajouter_queue(1)
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)









