---
title: "Listes chaînées (Simple)"

---


# La Structure de Données *Listes*


## 3. Avec une Liste Chaînée (Simple)

!!! def "Listes Chaînées"
    Une <bred>Liste Chaînée (Simple)</bred>, ou <bred>Liste Simplement Chaînée</bred>, ou tout simplement une <bred>Liste</bred>, est une implémentation d'une liste dans laquelle :

    * Tous les éléments sont appelés des <bred>Cellules</bred> :fr:, ou <bred>Maillons</bred> :fr:, ou <bred>Noeuds</bred> :fr:, ou <bred>Nodes</bred> :gb:, et se composent de deux choses :
        * une **valeur**
        * un **lien**, en fait un **pointeur/référence**, vers l'élément **suivant** / **successeur**
    * On suppose également connue la référence vers la **première Cellule** appelée <bred>Tête</bred> (et seulement cette référence)

Une **Liste Chaînée** est une autre implémentation possible pour une liste. on dit que les Cellules sont **chaînées (entre elles)**

<center>
```dot
digraph listeChainee {
        rankdir=LR;
        node [shape=record];
        start [label="{ <data> }", width=0.5]
        a [label="{ <data> 1  | <ref>  }"]
        b [label="{ <data> 2 | <ref>  }"];
        c [label="{ <data> 3 | <ref>  }"];
        d [label="{ <data> 4 | <ref> &#x27c2; }"];
        start:data -> a:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false, arrowsize=1.2];
        a:ref:c -> b:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false, arrowsize=1.2];
        b:ref:c -> c:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
        c:ref:c -> d      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
}
```
</center>

<br/>

Le symbole <enc>**&#x27c2;**</enc> $\,$ dont le nom officiel est "***taquet vers le haut***", représente la fin de la liste (`queue`), lorsque le *suivant* n'existe pas.
En Python, on utilise souvent `None` pour implémenter ce symbole.

<br/>

Le code Python-OOP suivant implémente une Classe `Cellule` d'une Liste Chaînée :

```python
class Cellule:
"""Une Cellule d'une Liste Chaînée"""
  def __init__(self,v,s):
    self.valeur = v
    self.suivante = s
```
<br/>

On peut alors créer une instance de la liste `|•|1|2|3|4|⟂|` précédente grâce à :

```python
uneListe = Cellule(1, Cellule(2, Cellule(3, Cellule(4,None))))
```

<br/>

Plus précisément, on a créé ici 4 objets de la Classe Cellule, que l'on peut visualiser comme suit :


<center>
```dot
digraph listeChainee {
  rankdir=LR
  node [shape=record];
  a [label="{ <ref>  }", width=0.5];
  b [label="<data> Cellule | <ref> 1 | <next>"];
  c [label="<data> Cellule | <ref> 2 | <next>"];
  d [label="<data> Cellule | <ref> 3 | <next>"];
  e [label="<data> Cellule | <ref> 4 | None"];

  a:ref:c -> b:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false, arrowsize=1.2];
  b:next:c -> c:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  c:next:c -> d:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  d:next:c -> e:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];

}
```
</center>

<br/>


Par la suite on s'autorisera le dessin simplifié suivant :

<center>
```dot
digraph listeChainee {
  rankdir=LR
  node [shape=record];
  a [label="{ <ref>  }", width=0.4];
  b [label="{<data> 1 | <next>}"];
  c [label="{<data> 2 | <next>}"];
  d [label="{<data> 3 | <next>}"];
  e [label="{<data> 4 | <next> &#x27c2;}"];

  a:ref:c -> b:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false, arrowsize=1.2];
  b:next:c -> c:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  c:next:c -> d:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  d:next:c -> e:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];

}
```
</center>