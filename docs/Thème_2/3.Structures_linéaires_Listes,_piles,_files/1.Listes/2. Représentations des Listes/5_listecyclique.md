---
title: "Liste cyclique"

---


# La Structure de Données *Listes*



## 5. Listes cycliques

<env>**Listes Cycliques**</env> Le tout dernier élément est relié au tout premier


<center>
```dot
digraph listeCyclique {
  rankdir=LR
  //splines=polyline;
  node [shape=record];
  a [label="{<data> 1 | <next>}"];
  b [label="{<data> 2 | <next>}"];
  c [label="{<data> 3 | <next> }"];

  a:next:sc -> b:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  b:next:c -> c:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  c:next:sc -> a:data:s      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
}
```
</center>