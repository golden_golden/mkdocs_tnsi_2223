---
title: "Liste chaînées doubles"

---


# La Structure de Données *Listes*



## 4. Listes Chaînées Doubles

!!! def "Définition : Listes Chaînées Doubles"
    Une <bred>Liste Chaînée Double (comprendre *à Double Extrémité*)</bred> :fr:, ou <bred>Double Ended Queue</bred> :gb:, est une Liste Chaînée contenant des références :

    * vers le **premier** élément (la `tête`) de la liste (de même que les Listes Chaînées Simples)
    * **et aussi**, vers le **dernier** élément (la `queue`) de la liste


