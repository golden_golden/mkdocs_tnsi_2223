---
title: "Introduction"

---


# La Structure de Données *Listes*

## 1. Implémentations des Listes

Il existe plusieurs implémentations d'une Liste, avec :

- Un tableau dynamique
- Une liste chaînée
- Une liste chaînée double
- Une liste cyclique
- Une liste doublement chaînée
- …

<br/><br/>

Cependant, quelque soit l'implémentations choisi, l'interface
 dispose usuellement des opérations *(primitives)* suivantes :

<br/>

**Primitives de Base** :
  
* `creerListeVide()` / `createEmptyList` : crée une Liste Vide
* `estVide()` / `isEmpty()` : renvoie si la liste est vide, ou pas
* `tete()` / `head()` / `shift()` : récupére l'élément en **tête** de la liste, en le supprimant de la liste
* `ajouteTete()` / `unShift()` : ajoute un élément en **tête** de la liste
* `retirerQueue()` / `dequeue()` / `pop()` / `tail()` : récupére l'élément situé en **queue** de la Liste
* `ajouteQueue()` / `enqueue()` / `push()` : ajoute un élément en **queue** de la Liste
* `longueur()` / `length()` : renvoie la longueur de la liste
  
<br/>
  
**Primitives auxilliaires**, fréquemment rencontrées :
  
* `inserer(L,e,i)`/`Create(L,e,i)` : insère un élément `e` dans la Liste `L` à la position `i`
* `supprimer(L,i)`/`Delete(L,i)` : supprime l'élément en position `i` de la liste `L`
* `modifier(L,e,i)`/`Update(L,e,i)` : modifie l'élément à la position `i` dans la Liste `L`, en le nouvel `e`

<br/>

<center>
```dot
digraph liste {
  rankdir=LR
  node [shape=Mrecord, color=grey, fontcolor=red, fontsize=18, width=0.6, fixedsize=true];
  headerValues [label="<header>"];
  ranksep=0

  node [shape=record, color=blue, fontcolor=black, fontsize=14, width=2, fixedsize=true];
  values [label="{<header> 4 | 8 | <footer> 7 }"];

  node [shape=Mrecord, color=grey, fontcolor=red, fontsize=18, width=0.6, fixedsize=true];
  footerValues [label="<footer>"];

  headerValues:header:se -> values:header:sw [label="unShift / ajouteQueue"];
  values:header:nw -> headerValues:header [label="Shift / Queue / Tail"];

  footerValues:footer:sw -> values:footer:c [label="Push / ajouteTête"];
  values:footer:ne -> footerValues:footer [label="Pop / Tête / Head"];
}
```
</center>


