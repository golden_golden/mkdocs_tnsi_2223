---
title: "Listes doublement chaînées"

---

# La Structure de Données *Listes*


## 6. Listes doublement chaînées

<env>**Listes Doublement Chaînées**</env> Chaque élément est relié au précédent et au suivant


<center>

```dot
digraph listeChaineeDouble {
  rankdir=LR
  node [shape=record];
  a [label="{<prev> | <data> 1 | <next>}"];
  b [label="{<prev> | <data> 2 | <next>}"];
  c [label="{<prev> | <data> 3 | <next> }"];

  a:next:nc -> b:prev:nw [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  b:prev:sc -> a:next:s [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  b:next:nc -> c:prev:nw [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  c:prev:sc -> b:next:s [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
}
```
</center>

<br/>

<env>**Listes Cycliques Doublement Chaînées**</env> Combine les deux variantes prédentes.