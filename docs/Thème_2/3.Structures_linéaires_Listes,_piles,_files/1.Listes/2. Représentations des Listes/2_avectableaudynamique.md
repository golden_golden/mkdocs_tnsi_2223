---
title: "Tableau Dynamique"

---


# La Structure de Données *Listes*



## 2. Avec un Tableau Dynamique
### 2.1 Présentation

On peut représenter une liste de $n$ éléments par un Tableau Dynamique $L[0..(n-1)]$, lui même implémenté par la donnée de :

* **la taille $n$ du tableau** (un entier)
On ne détaillera pas ici son stockage en mémoire, mais on pourrait par exemple imaginer, très simplement, de placer l'entier $n$ en début du tableau dynamique, et décaler les $n$ cellules vers la droite (ce qui donnerait un tableau à $n+1$ valeurs)

* **$n$ Cellules Contiguës**, de `L[0]` à `L[n-1]`, contenant chacune l'**adresse mémoire** de l'élément contenu dans la cellule. Chaque Cellule étant de plus référencée par son indice.

<center>

```dot
digraph {
  node [shape=plaintext, fontcolor=red, fontsize=18, width=1.5, fixedsize=true];
  headerValeurs [label="Valeurs :"]
  headerIndices [label="Indices :"]
  headerValeurs -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=14, width=4, fixedsize=true];
  valeurs [label="4 | 8 | 3 | 5 | 7 |  |  "];
  indices [label="0 | 1 | 2 | 3 | 4 | 5 | 6", color=none];

  { rank=same; headerValeurs; valeurs }
  { rank=same; headerIndices; indices }
}
```
</center>

<br/>

* Pour **lire** l’élément d’indice $i$, il suffit donc d’aller chercher son adresse dans la $(i+1)$-ème cellule et pour cela d’ajouter $i$ à l’adresse de début de tableau. **Cela se fait en temps constant $O(1)$**. 
* Pour **modifier** l'élément d'indice $i$, il suffit de faire pointer la cellule correspondante sur un autre objet ce qui se fait aussi **en temps constant  $O(1)$**. 
* Pour **insérer** un élément à l'indice $i$ à la liste, il faut modifier $n$ (ce dont nous ne parlerons pas) et ajouter une cellule au tableau (à la fin):
  
    * Si la cellule qui suit directement les $n$ cellules consécutives est "*libre*", pas de problème, on y met le pointeur qui désigne l’élément ajouté, ce qui se fait **en $O(1)$**.
    * Si ce n’est pas le cas, on doit alors déplacer tout le tableau vers une zone libre plus grande, où on pourra le prolonger, ce qui nécessite la recopie intégrale des $n$ cellules et se fait **en $O(n)$**.
  
    ??? note "Remarque"

        Pour que cela arrive le moins souvent possible, on utilise d’habitude le procédé suivant : 

        - au départ, on prévoit en général plus de place que les $n$ cases initiales, au cas où on voudrait prolonger la liste. 
        - Puis lors de chaque déplacement forcé, on s’arrange pour doubler la place mémoire disponible et la réserver. Cela réduit le nombre de déplacements et le coût d’ajout de $p$ éléments est alors en $O(max(n,p))$. Par exemple, si on part d’une liste vide et on ajoute un à un,  $n$ éléments à la liste, cela fait un nombre maximal de recopies de cases $\displaystyle \approx n+\frac n2+ \frac n4··· = O(n)$, et la complexité del’opération globale a le même ordre de grandeur que s’il n’y avait pas eu de déplacement ! En pratique, on **lisse** le coût des déplacements et on fait l’approximation que les ajouts se font en temps constant.
    
* pour **supprimer** un élément de la liste, on doit modifier $n$, et déplacer d'un cran vers la gauche tous les éléments situés à droite de l'élément à supprimer. Cela se fait **en $O(n)$** dans le pire des cas.

<br/>

<env><b>CAS DE PYTHON : ATTENTION, FAUX AMI</b></env>

Le langage Python est un langage de Programmation, mais il en existe plusieurs implémentations :

L'Implémentation par défaut de **Python** est [<bred>CPython</bred>](https://fr.wikipedia.org/wiki/CPython "CPython est l'implémentation de référence du langage Python. C'est un interpréteur de bytecode écrit en langage C. C'est un logiciel libre. ") qui est un interpréteur de [<bred>bytecode</bred>](https://fr.wikipedia.org/wiki/Bytecode "Un bytecode est un code intermédiaire entre les instructions machines et le code source, qui n'est pas directement exécutable. Le bytecode (également appelé code portable ou p-code) peut être créé à la volée et résider en mémoire (compilation à la volée, ou JIT Just-In-Time en anglais) ou bien résider dans un fichier, généralement binaire qui représente le programme, tout comme un fichier de code objet produit par un compilateur. ") écrit en langage C.



??? info "Information"
    Il existe néanmoins d'autres implémentations de Python:

    * [IronPython](https://ironpython.net/) (pour dévelopement avec/pour le framework .NET), 
    * [Jython](https://www.jython.org/) (écrit en Java, exécutables dans la Jython Virtual Machine), 
    * [PyPy](https://www.pypy.org/) (écrit en Python, qui peut donc s'auto-interpréter!)

    En pratique, cela veut dire qu'une même notion du langage Python peut être implémentée différemment, selon l'implémentation de Python choisie.


!!! note "À noter"
    * Les types de données Python peuvent prêter à confusion : Le type de données `list` est implémenté (**en CPython**) par un **Tableau Dynamique**, et **non PAS par une Structure de Données Liste (Chaînée)**, comme le nom `list` pourrait le laisser croire trompeusement.
    * Il est donc normal que les résultats trouvés ci-dessus soient en cohérence totale avec la Documentation Officielle de Python concernant la Complexité pour les `list`, que l'on trouvera sur [cette page de la Documentation officielle](https://wiki.python.org/moin/TimeComplexity).
  
    <span style="font-size:0.85em;">Cf. le [ticket Stackoverflow suivant](https://stackoverrun.com/fr/q/9104972) qui justifie pourquoi, sur la Documentation Officielle, la complexité de l'ajout est **toujours en $O(1)$** dans le *pire des cas **amortis*** (mais PAS en $O(1)$).</span>

### 2.2 Exercices

Quelques exemples d'implémentation par un tableau

a) la liste vide : ([] , 0)     
b) la liste composée du seul élément 2 : ([2] , 1)   
c) la liste composée des éléments 2 et 3 où 3 est en tête de liste : ([2,3] , 2) 


!!! faq "Exercice"
    1) Implémenter le type abstrait liste avec l'implémentation décrite ci-dessus.
    
    2) Etendez l'interface de cette implémentation en créant les opérations suivantes :
    
    - taille(Liste) : accesseur qui retourne le nombre d'éléments de la liste.
    - ième(Liste, i) : accesseur qui retourne le i-ème élément de la liste.
    - ajouter_fin(Liste, e) : opérateur qui ajoute e à la fin de la liste.
    - insérer_ième(Liste , i , e) : opérateur qui insère l'élement  e en i-ème position.
    - remplacer_ième(Liste , i , e) : opérateur qui remplace le i-ème élément par e.
    - supprimer_ième(Liste, i) : opérateur qui supprime le i-ème élément de la liste.

    3) a) Ecrire une fonction enleve_valeur(v) qui retourne une liste privée des éléments d'une valeur donnée v en utilisant uniquement les opérations de l'interface.


    b) Ecrire une fonction separe_liste(v) qui sépare les éléments d'une liste de nombres réels en deux listes selon qu'ils sont inférieurs ou égaux à v ou strictement supérieurs à v.