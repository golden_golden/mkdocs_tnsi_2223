
## 3. Implémentation d'une Pile par un Tableau

Dans cette partie, un **Tableau** sera implémenté par le type de données `list` de Python

!!! ex "Implémentation d'une Pile par un Tableau"

    1. Implémenter une classe `Pile`, disposant des méthodes suivantes :

        * `Pile()` : crée une pile vide. Syntaxe : `p = Pile()` crée une Pile vide
        * `est_vide()->bool` : indique si la pile est vide.
        * `empile(x:int)->None` ou `push(x:int)->None` : insère un élément en haut (/à droite) de la pile. Ce sera le **dernier arrivé**
        * `depile()` ou `pop()` : renvoie la valeur de l'élément en haut /(à droite/premier à sortir) de la pile ET le supprime de la pile.
        * `get_sommet()` qui renvoie la valeur du sommet de la Pile, **sans le dépiler**, ou `None` si la Pile est vide
        * une **méthode magique** `__repr__()` : permet d'afficher la pile sous forme agréable lorsque :
            * l'on tape `>>> p` ou `>>> print(p)` dans un Interpréteur Python 
            * l'on utilise l'instruction `print(p)` dans un Script Python 
            Exemple d'affichage : `|•|3|6|2|5|` où `3` est le **premier arrivé**, et `5` est le `sommet`/**dernier arrivé**)
        * Modifier le constructeur `__init__()` de sorte que l'on puisse maintenant instancier la classe `Pile` en lui passant en argument un objet de type `list` de Python:

            ```python linenums="0"
            # Créer une Pile `p` initialisée par la liste Python [1,2,3], 
            # qui sera représentée dans un Terminal par `|•|1|2|3|`
            p = Pile([1,2,3])
            ```

        * `vider()` : vide la Pile. Syntaxe : `p.vider()` vide `p`
        * `copy()` renvoie une **copie profonde** de la Pile (la modification de l'une n'entraine pas la modification de l'autre). Syntaxe : `p1 = p.copy()`
        * `renverse()` renverse la Pile. Syntaxe : `p.renverse()` renverse `p`
        * `concatene()` : concatène deux Piles. Syntaxe : `p1.concatene(p2)` concatène `p2` au dessus de `p1`, et `p2` est vidée

    2. Quelle est la complexité en temps des opérations `empile()` et `depile()` ?
    Conclusion : Le type `list` de Python est-il parfaitement adapté pour implémenter une Pile?
    3. Précisément, quelle méthode dans notre classe `Pile` est le ***facteur limitant***, c'est à dire ralentit nos opérations? et pourquoi ?

