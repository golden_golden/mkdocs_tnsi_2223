```python
"""
Représentation d'une pile
Utilise: utilise list de python
Contraintes: Aucunes contraintes, utilisation de pop et append autorisé
"""

def creer_pile():
    """
        creer une pile vide

        :return: une liste python vide
        :rtype: list
    """
    return []


def est_vide(pile):
    """
        permet de savoir si une pile est vide.

        :param pile: une pile
        :type pile: list
        :return: True si la pile est vide, False sinon
        :rtype: bool

        :Example:

        >>> est_vide([4])
        False

        >>> est_vide([])
        True
    """
    return len(pile) == 0


def empiler(pile, element):
    """
       permet d'ajouter un élément à la pile.

       :param pile: une pile
       :param element: un element
       :type pile: list
       :type elemen: element
       :return: une pile avec un élement en plus
       :rtype: list

       :Example:

       >>> empiler([1,2,3],4)
       [1,2,3,4]
   """
    pile.append(element)


def depiler(pile):
    """
        renvoie la pile sans l'élément au sommet de la pile

        :param pile: une pile
        :type pile: list
        :return: une pile plus petite d'une element
        :rtype: list

        :Example:

        >>> depiler([1,2,3])
        [1,2]
    """
    assert not est_vide(pile), "Attention, la pile est vide !"
    pile.pop()


def sommet(pile):
    """
        renvoie la valeur au sommet de la pile.

        :param pile: une pile
        :type pile: list
        :return: Element au sommet de la pile
        :rtype: element

        :Example:

        >>> sommet([1,2,3])
        3
    """
    assert not est_vide(pile), "Attention, la pile est vide !"
    return pile[-1]

```

