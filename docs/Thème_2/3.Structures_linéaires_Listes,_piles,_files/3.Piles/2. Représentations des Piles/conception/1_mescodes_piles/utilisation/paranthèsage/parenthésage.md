```python
"""
Exemple d'utilisation d'une pile.
Proposer un algorithme qui vérifie le bon parenthésage d'une expressino mathématique
"""
import Pile as p

def est_parantheses_valides(expression):
    """
        Permet de vérifier le paranthésage d'une expression mathématique

        :param expression: une expression mathématique
        :type expression: str
        :return: renvoie True si le parenthésage de l'expression est valide, et false sinon
        :rtype: bool

        :Example:

        >>> est_parantheses_valides("(())")
        True

        >>> est_parantheses_valides("(()")
        False
    """
    pile = p.creer_pile()
    for element in expression:
        if element == "(":
            pile = p.empiler(pile,element)
        elif element == ")":
            if p.est_vide(pile):
                return False
            else:
                pile = p.depiler(pile)
    if p.est_vide(pile):
        return True
    else:
        return False




### Procédure de test
def  test_est_parantheses_valides():
    assert est_parantheses_valides("(((())(()))())"),"(((())(()))()),cette expression est pourtant bien valide !"
    assert not est_parantheses_valides("(()()"),"(()(),cette expression n'est pas valide, il y a plus de paranthése ouvrantes que fermantes !"
    assert not est_parantheses_valides("(()))"),"(())),cette expression n'est pas valide, il y a plus de paranthése fermantes qu'ouvrantes !"
    assert not est_parantheses_valides(")"),"),cette expression n'est pas valide !"
    print("Bravo, la fonction est_paranthese_valides passe tout les tests !")


### Programme principal
if __name__ == "__main__":
    test_est_parantheses_valides()
```

