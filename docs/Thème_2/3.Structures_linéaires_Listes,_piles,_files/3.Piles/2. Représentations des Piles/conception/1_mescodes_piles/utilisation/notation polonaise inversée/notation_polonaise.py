"""
Exemple d'utilisation d'une pile.
Proposer un algorithme qui permet l'évaluation d'une expression mathématique utilisant la notation polonaise
"""
import Pile as p


def notation_polonaise(expression):
    """
        Permet d'évaluer une expression mathématique écrite en notation polonaise

        :param expression: une expression mathématique
        :type expression: str
        :return: renvoie la valeur de l'expression
        :rtype: int

        :Example:

        >>> notation_polonaise("23+4*")
        20

    """
    pile = p.creer_pile()
    nombres = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    operateurs = {"+": "a+b", "-": "a-b", "*": "a*b", "/": "a/b"}
    for element in expression:
        if element in nombres:
            pile = p.empiler(pile, int(element))
        elif element in operateurs.keys():
            a = p.sommet(pile)
            pile = p.depiler(pile)
            b = p.sommet(pile)
            pile = p.depiler(pile)
            pile = p.empiler(pile, eval(operateurs[element]))
    return p.sommet(pile)


def test_notation_polonaise():
    assert notation_polonaise("23+4*") == 20, "23+4* doit renvoyer 20 !"
    print("Bravo, la fonction notation_polonaise passe les tests !")


if __name__ == "__main__":
    test_notation_polonaise()
