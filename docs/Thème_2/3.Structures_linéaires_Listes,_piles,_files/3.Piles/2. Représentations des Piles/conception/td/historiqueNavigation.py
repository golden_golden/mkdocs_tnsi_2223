# Implémentation avec un Tableau (type 'list' de Python)

class Pile:
    def __init__(self):
        self.data = []
      
    def est_vide(self):
        return len(self.data) == 0 
    
  
    def empile(self,x):
        self.data.append(x)

    def depile(self):
        if self.est_vide() == True :
            raise IndexError("Vous avez essayé de dépiler une pile vide !")
        else :
            return self.data.pop() 

    def __str__(self):       # Hors-Programme : pour afficher 
        s = "|"              # convenablement la pile avec print(p)
        for k in self.data :
            s = s + str(k) + "|"
        return s

    def __repr__(self):       # Hors-Programme : pour afficher 
        s = "|"              # convenablement la pile avec p
        for k in self.data :
            s = s + str(k) + "|"
        return s

######################################################################################
####      HISTORIQUE DE NAVIGATION
######################################################################################

adresses = Pile()
adresse_courante = ""

def go_to(nouvelle_adresse) :
    global adresse_courante
    adresses.empile(nouvelle_adresse)
    adresse_courante = nouvelle_adresse

def back():
    global adresse_courante
    adresses.depile()
    adresse_courante = adresses.depile()
