---
title: "Définition"

---


# Le TAD Pile



## 2. Définition du TAD Pile



***Type abstrait*** :  Pile

<br/>

***Utile :***  Entier, Elément, Booléen

<br/>

***Opérations*** : Voici les primitives communément utilisées pour manipuler des piles ; il n'existe pas de normalisation pour les primitives de manipulation de pile, leurs noms respectifs sont donc indiqués de manière informelle.

<br/>

Primitives de base :

$$
creer\_Pile :\space → Pile
$$

$$
empiler : Pile × Element → Pile
$$

$$
depiler : Pile  → Pile
$$

$$
est vide : Pile → Booleen
$$

$$
sommet : Pile → Element
$$

<br/>

***Préconditions*** :

Soient p une Pile, e un élément.

$$
sommet(p) \space \text{est defini si et seulement si} \space estvide(p) = Faux
$$

$$
depiler(p)\space \text{est defini si et seulement si} \space estvide(p)= Faux
$$


<br/>


***Les axiomes*** :

Soient p une Pile, e un élément.

$$
estvide(creer\_Pile) = Vrai
$$

$$
estvide(empiler(p,e)) = Faux
$$

$$
sommer(empiler(p,e)) = e
$$

$$
depiler(empiler(p,e)) = p
$$



