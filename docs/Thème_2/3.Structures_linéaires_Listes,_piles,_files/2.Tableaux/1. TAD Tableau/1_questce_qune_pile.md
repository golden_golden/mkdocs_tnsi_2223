---
title: "Qu'est ce qu'une Pile"

---



# Le TAD Pile



## 1. Qu'est ce qu'une pile ?


Dans une <bred>Pile</bred> :fr:, ou <bred>Stack</bred> :gb:, les éléments sont empilés les uns après les autres. Si la pile n’est pas vide, il est possible de dépiler l’élément au sommet, c’est à dire, l’élément le plus récent. 

On dit que la pile fonctionne suivant le principe **LIFO (Last In, First Out)** :gb: / **Dernier Arrivé, Premier Sorti** :fr:., les ajouts et suppressions n'ont lieu que sur une même extremité : le **Sommet** de la Pile.



<br/>


<center><img src="https://notebooks.lecluse.fr/images/copied_from_nb/my_icons/pile1.png" alt="pile" style="zoom:80%;" /><center/>

*illustration : Une pile de vaisselle*
