

## 2. Interface d'une Pile

!!! pte
    L'**interface** (c'est-à-dire les opérations disponibles) d'une Pile contient a minima :

    * la création d'une **pile vide**
    * l'**ajout** d'un élément **au sommet / dernier arrivé** de la pile (qui sera forcément celui **du dessus** dans une représentation verticale, ou celui **de droite** dans une représentation horizontale). On dira qu'on <bred>empile</bred> :fr: ou <bred>push</bred> :gb:, <bgreen>idéalement en temps constant</bgreen>.
    * le **retrait** d'un élément **du sommet** de la pile (qui sera forcément celui **du dessus** dans une représentation verticale, ou celui **de droite** dans une représentation horizontale), et le renvoi de sa valeur. On dira qu'on <bred>dépile</bred> :fr: ou <bred>pop</bred> :gb:, <bgreen>idéalement en temps constant</bgreen>.

!!! ex
    On considère l'enchaînement d'opérations ci-dessous. 
    Écrire à chaque étape l'état de la pile `p` et la valeur éventuellement renvoyée.

    Bien comprendre que la classe `Pile()` et ses méthodes n'ont pas encore été **Implémentées** (c'est-à-dire n'ont pas été concrètement écrites). 
    Nous utilisons simplement son **Interface**.

    === "Énoncé"
        ```python
        # On suppose que la classe Pile est déjà implémentée :
        >>> p = Pile()
        >>> p.empile(3)   # ou p.push(3)
        >>> p.empile(5)
        >>> p.est_vide()
        >>> p.empile(1)
        >>> p.depile()    # ou p.pop()
        >>> p.depile()
        >>> p.empile(9)
        >>> p.depile()
        >>> p.depile()
        >>> p.est_vide()
        ```

    === "Corrigé"
        ```python
        # On suppose que la classe Pile est déjà implémentée :
        >>> p = Pile()    # p=None
        >>> p.empile(3)   # p= 3
        >>> p.empile(5)   # p= 3 5 par convention
        >>> p.est_vide()  #  False
        >>> p.empile(1)   # p= 3 5 1
        >>> p.depile()    # p= 3 5     valeur renvoyée : 1
        >>> p.depile()    # p= 3       valeur renvoyée : 5
        >>> p.empile(9)   # p= 3 9
        >>> p.depile()    # p= 3       valeur renvoyée :9
        >>> p.depile()    # p= None    valeur renvoyée : 3
        >>> p.est_vide()  # True
        ```
