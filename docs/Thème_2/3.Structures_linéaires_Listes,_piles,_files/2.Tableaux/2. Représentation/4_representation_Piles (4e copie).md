

## 5. Exemples d'Utilisation des Piles

!!! ex "Historique de Navigation"
    À l'aide de deux variables `adresses` et `adresse_courante`, et de la classe `Pile` créée plus haut, simulez une gestion de l'historique de navigation internet.
    Seules deux fonctions `go_to(nouvelle_adresse)` et `back()` sont à créer.

!!! ex
    Chercher sur internet, des exemples classiques d'utilisation des Piles en informatique, voire dans toute autre discipline.

