"""
Représentation d'une pile
Utilise: utilise list de python et le POO
Contraintes: ne pas utiliser pop, append et les slices
"""

class Pile:
    """

    """
    compteur = 0

    # Constructeur
    def __init__(self):
        """
            creer une pile vide
        """
        Pile.compteur += 1
        self.__nom = "Pile "+str(Pile.compteur)
        self.__pile = []
        self.__taille = 0

    # Assesseurs
    def get_nom(self):
        return self.__nom

    # Méthodes classiques
    def est_vide(self):
        """
            permet de savoir si une pile est vide.
        """
        return self.__taille == 0

    def empiler(self, element):
        """
            permet d'ajouter un élément à la pile.

            :param element: un element
            :type elemen: element
        """
        nouvelle_pile = [None for i in range(self.__taille + 1)]
        for i in range(self.__taille):
            nouvelle_pile[i] = self.__pile[i]
        nouvelle_pile[-1] = element
        self.__pile = nouvelle_pile
        self.__taille += 1

    def depiler(self):
        """
         enlève le sommet élément de la pile

        """
        assert not self.est_vide(), "la pile est vide !"
        self.__pile = [self.__pile[i] for i in range(self.__taille - 1)]
        self.__taille -= 1

    def sommet(self):
        """
            renvoie la valeur au sommet de la pile.
        """
        assert not self.est_vide(), "la pile est vide !"
        return self.__pile[-1]

    # Méthodes spéciales
    def __str__(self):
        texte =""
        for element in self.__pile:
	       	texte = "|\t" + str(element) + "\t|" + "\n" + texte
        texte = "\nEtat de la " + self.__nom + ":\n" + texte
        return texte



print("on créer une pile")
p = Pile()


print("on empile 9")
p.empiler(9)
print(p)


p.empiler(10)
print("on empile 10")
print(p)
print("le sommet :",p.sommet())


p.depiler()
print("on dépile")
print(p)
print("le sommet :",p.sommet())


print("La pile est elle vide ? ",p.est_vide())

p.depiler()
print("on dépile")

print(p)
print("La pile est elle vide ? ",p.est_vide())


