"""
Représentation d'une pile
Utilise: utilise de la représentation liste chainée et de la POO
Contraintes: ne pas utiliser pop, append et les slices
"""

class Maillon:
    """

    """
    def __init__(self,valeur,precedent):
        self.valeur = valeur
        self.precedent = precedent



class Pile:
    """

    """

    # Constructeur
    def __init__(self):
        """
            creer une pile vide
        """
        self.__sommet = None

    # Méthodes classiques
    def est_vide(self):
        """
            permet de savoir si une pile est vide.
        """
        return self.__sommet == None

    def empiler(self, element):
        """
            permet d'ajouter un élément à la pile.

            :param element: un element
            :type elemen: element
        """
        self.__sommet = Maillon(element,self.__sommet)

    def depiler(self):
        """
         enlève le sommet élément de la pile

        """
        assert not self.est_vide(), "la pile est vide !"
        self.__sommet = self.__sommet.precedent

    def sommet(self):
        """
            renvoie la valeur au sommet de la pile.
        """
        assert not self.est_vide(), "la pile est vide !"
        return self.__sommet.valeur

    # Méthodes spéciales
    def __str__(self):
        texte =""
        parcour = self.__sommet
        while parcour != None:
            texte = "|\t" + str(self.__sommet.valeur) + "\t|" + "\n" + texte
            parcour = self.__sommet.precedent
        texte = "\nEtat de la pile:\n" + texte
        return texte


if __name__ == "__main__":
    print("on créer une pile")
    p = Pile()


    print("on empile 9")
    p.empiler(9)
    print(p)


    p.empiler(10)
    print("on empile 10")
    #print(p)
    print("le sommet :",p.sommet())


    p.depiler()
    print("on dépile")
    #print(p)
    print("le sommet :",p.sommet())


    print("La pile est elle vide ? ",p.est_vide())

    p.depiler()
    print("on dépile")

    #print(p)
    print("La pile est elle vide ? ",p.est_vide())
