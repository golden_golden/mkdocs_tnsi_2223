"""
Exemple d'utilisation d'une pile.
Proposer un algorithme qui permet l'évaluation d'une expression mathématique utilisant la notation polonaise
Contraintes: ne pas utiliser pop, append, isinstance, isdigits,…
"""
import Pile as p
import file as f

CHIFFRES = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
OPERATEURS = {"+": "a+b", "-": "a-b", "*": "a*b", "/": "a/b"}



def convertir(expression):
    """
        Permet de placer les éléments de l'expression dans une liste

        :param expression: une expression mathématique
        :type expression: str
        :return: renvoie la liste des elements constitutifs de l'expression
        :rtype: list

        :Example:

        >>> convertir("23 5 + 4 *")
        ['23', '5', '+', '4', '*']

    """
    File = f.creer_file()
    nombre = ""
    for element in expression:
        if element in CHIFFRES:
            nombre += element
        elif element == " ":
            if nombre != "":
                File = f.enfiler(File,nombre)
                nombre = ""
        else:
            File = f.enfiler(File,element)
    return File



def notation_polonaise(expression):
    """
        Permet d'évaluer une expression mathématique écrite en notation polonaise

        :param expression: une expression mathématique
        :type expression: str
        :return: renvoie la valeur de l'expression
        :rtype: int

        :Example:

        >>> notation_polonaise("23 + 4 *")
        20
    """
    file_expression = convertir(expression)
    pile = p.creer_pile()

    for element in file_expression:
        if element[-1] in CHIFFRES:
            pile = p.empiler(pile, int(element))
        if element in OPERATEURS.keys():
            a = p.sommet(pile)
            pile = p.depiler(pile)
            b = p.sommet(pile)
            pile = p.depiler(pile)
            pile = p.empiler(pile, eval(OPERATEURS[element]))
    return p.sommet(pile)




def test_notation_polonaise():
    assert notation_polonaise("2 3 + 4 *") == 20, "23+4* doit renvoyer 20 !"
    print("Bravo, la fonction notation_polonaise passe les tests !")


if __name__ == "__main__":
    print(convertir("23 5 + 4 *"))
    print(notation_polonaise("23 5 + 4 *"))
    test_notation_polonaise()
