class Maillon:
    """ un maillon pour la liste chainée """

    def __init__(self, donnee=None, lien=None):
        self.donnee = donnee
        self.lien = lien


class Liste_Chainee:
    def __init__(self):
        self.liste = None  # Pointeur sur le Premier maillon de la liste
        self.fin_liste = None  # Pointeur sur le Dernier maillon de la liste
        self.longueur = 0  # Mémorise la longueur de la liste

    def __len__(self):
        """ Renvoie la longueur de la liste """
        return self.longueur

    def __str__(self):
        """ Renvoie une représentation affichable de la liste """
        if self.est_vide():
            return ">[vide]<"
        else:
            m = self.liste
            resultat = "["
            while m.lien is not None:
                resultat += str(m.donnee) + "]->["
                m = m.lien
            resultat += str(m.donnee) + "]"
            return resultat

    def _aller_en_position(self, position) -> Maillon:
        """ Renvoie le maillon situé à 'position' """
        pos = 0
        maillon = self.liste
        while pos < position:
            maillon = maillon.lien
            pos += 1
        return maillon

    def est_vide(self) -> bool:
        """ Renvoie 'True' si la liste est vide """
        return self.liste == None

    def ajouter(self, donnee):
        """ ajoute un élément en fin de liste """
        dernier_maillon = self.fin_liste
        m = Maillon(donnee, None)
        if dernier_maillon is None:  # liste vide ?
            self.liste = m
        else:
            dernier_maillon.lien = m
        self.fin_liste = m
        self.longueur += 1

    def inserer(self, position, donnee) -> bool:
        """ Insère un élément à 'position'. Renvoie 'True' si réussi """
        if position < 0:
            return False
        elif position == 0:  # insertion en tête
            nouv_maillon = Maillon(donnee, None)
            nouv_maillon.lien = self.liste
            self.liste = nouv_maillon
            self.longueur += 1
            return True
        elif position > self.longueur - 1:
            return False
        else:  # insertion dans la liste ou à la fin
            nouv_maillon = Maillon(donnee, None)
            maillon_precedent = self._aller_en_position(position - 1)
            nouv_maillon.lien = maillon_precedent.lien
            maillon_precedent.lien = nouv_maillon
            self.longueur += 1
            return True

    def supprimer(self, position) -> bool:
        """ Supprime l'élément situé à 'position'. Renvoie 'True' si réussi. """
        if position < 0 or position > self.longueur - 1:
            return False
        else:
            if position == 0: # suppression en tête de liste
                self.liste = self.liste.lien
            else:
                maillon_precedent = self._aller_en_position(position - 1)
                if position == self.longueur - 1: # suppression en fin de liste.
                    maillon_precedent.lien = None
                    self.fin_liste = maillon_precedent
                else:
                    maillon_precedent.lien = maillon_precedent.lien.lien
            self.longueur -= 1
            if self.longueur == 0:
                self.liste = None
                self.fin_liste = None # pour la cohérence de l'objet
            return True

    def element(self, position):
        """ Renvoie l'élément situé à 'position' ou 'None' si pas trouvé """
        if position < 0 or position > self.longueur - 1:
            return None
        else:
            maillon = self._aller_en_position(position)
            return maillon.donnee

    def modifier(self, position, donnee) -> bool:
        """ Modifie l'élément situé à 'position' avec 'donnee'. 
            Renvoie 'True' si réussi """
        if position < 0 or position > self.longueur - 1:
            return False
        else:
            maillon = self._aller_en_position(position)
            maillon.donnee = donnee
            return True
