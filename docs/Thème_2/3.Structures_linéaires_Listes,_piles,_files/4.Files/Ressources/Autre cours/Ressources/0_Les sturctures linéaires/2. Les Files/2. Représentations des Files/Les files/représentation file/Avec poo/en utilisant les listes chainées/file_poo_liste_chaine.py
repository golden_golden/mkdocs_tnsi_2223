"""
Correction de l'exercice 6
    Représentation d'une pile
    Utilise: utilise de la représentation liste chainée et de la POO
    Contraintes: ne pas utiliser pop, append et les slices
"""


class Maillon:
    def __init__(self, valeur, suivant):
        self.valeur = valeur
        self.suivant = suivant


class File:

    # Constructeur
    def __init__(self):
        self.__premier = None

    # Méthodes classiques
    def est_vide(self):
        return self.__premier == None

    def enfiler(self, element):
        """
            permet d'ajouter un élément à la file.

            :param element: un element
            :type elemen: element
        """
        if self.est_vide():
            self.__premier = Maillon(element, self.__premier)
        else:
            dernier_maillon = self.__premier
            while dernier_maillon.suivant != None:
                dernier_maillon = dernier_maillon.suivant
            dernier_maillon.suivant = Maillon(element, dernier_maillon.suivant)


    def defiler(self):
        """
         enlève le premier élément de la pile

        """
        assert not self.est_vide(), "la file est vide !"
        self.__premier = self.__premier.suivant


    def premier(self):
        """
            renvoie la valeur au sommet de la pile.
        """
        assert not self.est_vide(), "la file est vide !"
        return self.__premier.valeur


    # Méthodes spéciales
    def __str__(self):
        titre = "\nEtat de la file:\n"
        bordure = ""
        texte_element = ""

        parcour = self.__premier
        while parcour != None:
            element = str(parcour.valeur)
            texte_element = element + " " + texte_element
            for caractere in element:
                bordure += "_"
            bordure += "_"
            parcour = parcour.suivant

        return titre + bordure +"\n" + texte_element + "\n"+ bordure +"\n"


if __name__ == "__main__":
    print("on créer une File")
    p = File()
    print(p)

    print("on enfile 6")
    p.enfiler(6)
    print(p)

    p.enfiler(10)
    print("on enfile 10")
    print(p)
    print("le premier :", p.premier())

    p.enfiler(12)
    print("on enfile 12")
    print(p)
    print("le premier :", p.premier())


    p.defiler()
    print("on défile")
    print(p)
    print("le premier :", p.premier())

    print("La file est elle vide ? ", p.est_vide())

    p.defiler()
    print("on défile")

    print(p)
    print("La file est elle vide ? ", p.est_vide())

    p.defiler()
    print("on défile")

    print(p)
    print("La file est elle vide ? ", p.est_vide())
