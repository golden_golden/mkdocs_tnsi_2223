from list import List


def length(xs: List) -> int:
    """ Renvoie le nombre d'éléments de la liste
    """
    if xs == List():
        return 0
    else:
        return 1 + length(xs.tail)


xs = 5 ** 5 ** 2 ** 8 ** List(int)
length(xs)
# 4


def fmap(f, xs: List) -> List:
    """ Renvoie la liste [|f(x1), f(x2), ..., f(xn)|].
    """
    if xs == List():
        return List()
    else:
        return f(xs.head) ** fmap(f, xs.tail)


xs = 4 ** 5 ** 3 ** List(int)
fmap(lambda x: x ** 2, xs)
# [|16, 25, 9|]


def pfilter(p, xs: List) -> List:
    """ Renvoie la liste formée des éléments x qui vérifient
        le prédicat p(x).
    """
    if xs == List():
        return List()
    elif p(xs.head):
        return xs.head ** pfilter(p, xs.tail)
    else:
        return pfilter(p, xs.tail)


xs = 3 ** 5 ** 4 ** 2 ** 7 ** 8 ** List(int)
pfilter(lambda x: x % 2 == 0,
        xs)
# [|4, 2, 8|]


def foldr(xs: List, f, acc: object) -> object:
    """ Renvoie x1 `f` x2 `f` x3 `f` ... `f` xn
        où x `f` y  `f` z  == f(x, f(y, z))
    """
    if xs == List():
        return acc
    else:
        return f(xs.head, foldr(xs.tail, f, acc))


xs = 2 ** 3 ** 4 ** List(int)
foldr(xs,
      lambda x, acc: x * acc,
      1)
# 24


def foldl(acc: object, f, xs: List) -> object:
    """ Renvoie x1 `f` x2 `f` x3 `f` ... `f` xn
        où x `f` y  `f` z  == f(f(x, y), z)
    """
    if xs == List():
        return acc
    else:
        return foldl(f(acc, xs.head), f, xs.tail)


xs = 2 ** 3 ** 4 ** List(int)
foldl(1,
      lambda acc, x: acc + x,
      xs)
# 10


def concat(xs, ys):
    if xs == List():
        return ys
    else:
        return xs.head ** concat(xs.tail, ys)


xs = 1 ** 2 ** 3 ** 4 ** List(int)
ys = 5 ** 6 ** 7 ** 8 ** List(int)
concat(xs, ys)
# [|1, 2, 3, 4, 5, 6, 7, 8|]


def zip2(xs, ys):
    if xs == List() or ys == List():
        return List()
    else:
        return (xs.head, ys.head) ** zip2(xs.tail, ys.tail)


xs = 'Alice' ** 'Bob' ** 'Carl' ** List()
ys = 12 ** 8 ** 16 ** List()
zip2(xs, ys)
#  [|('Alice', 12), ('Bob', 8), ('Carl', 16)|]


def zipWith(f, xs, ys):
    if xs == List() or ys == List():
        return List()
    else:
        return f(xs.head, ys.head) ** zipWith(f, xs.tail, ys.tail)


xs = 'Alice' ** 'Bob' ** 'Carl' ** List()
ys = 'Durand' ** 'Dupond' ** 'Martin' ** List()
zipWith(lambda x, y : x + ' ' + y, xs, ys)
# [|'Alice Durand', 'Bob Dupond', 'Carl Martin'|]


def reverse(xs):
    def loop(xs, ys):
        if xs == List():
            return ys
        else:
            return loop(xs.tail, xs.head ** ys)
    return loop(xs, List())


xs = 1 ** 2 ** 3 ** 4 ** 5 ** List()
reverse(xs)
# [|5, 4, 3, 2, 1|]


def reverse(xs):
    return foldl(List(),
                 lambda acc, x: x ** acc,
                 xs)


xs = 1 ** 2 ** 3 ** 4 ** 5 ** List()
reverse(xs)
# [|5, 4, 3, 2, 1|]


def insertion(x, xs):
    if xs == List():
        return x ** List()
    elif x <= xs.head:
        return x ** xs
    else:
        return xs.head ** insertion(x, xs.tail)


def insertionSort(xs):
    if xs == List():
        return xs
    else:
        return insertion(xs.head, insertionSort(xs.tail))



xs = 7 ** 8 ** 5 ** 7 ** List()
insertionSort(xs)
# [|5, 7, 7, 8|]


def quickSort(xs):
    if xs == List():
        return xs
    else:
        u = xs.head
        ys = quickSort(pfilter(lambda x: x < u, xs.tail))
        zs = quickSort(pfilter(lambda x: x >= u, xs.tail))
        return concat(ys, xs.head ** zs)


xs = 7 ** 8 ** 5 ** 7 ** List()
quickSort(xs)
# [|5, 7, 7, 8|]
