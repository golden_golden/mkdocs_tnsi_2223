from __future__ import annotations


class List(tuple):
    def __new__(cls, *args):
        if len(args) <= 1:
            return super().__new__(cls, ())
        elif len(args) == 2:
            return super().__new__(cls, args)
        else:
            raise ValueError('Arguments incorrects')

    def __init__(self, *args):
        if len(args) == 0:
            self.type = None
        elif len(args) == 1:
            if isinstance(args[0], type):
                self.type = args[0]
            else:
                raise TypeError(f"{repr(args[0])} n'est pas un type")
        elif args[1].type is not None and type(args[0]) != args[1].type:
            raise TypeError(f"{repr(args[0])} n'est pas du type {args[1].type.__name__}")
        else:
            self.type = args[1].type

    @property
    def head(self) -> object:
        """ Renvoie la tête de la liste.
        """
        if self == List():
            raise ValueError('Liste vide')
        return self[0]

    @property
    def tail(self) -> 'List':
        """ Renvoie la queue de la liste.
        """
        if self == List():
            raise ValueError('Liste vide')
        return self[1]

    def __rpow__(self, x: object) -> 'List':
        """ Constructeur d'une nouvelle liste.
        """
        return List(x, self)

    def _tolist(self) -> list:
        L = []
        xs = self
        while xs != List():
            L.append(xs.head)
            xs = xs.tail
        return L

    def __repr__(self) -> str:
        L = [repr(x) for x in self._tolist()]
        return '[|' + ', '.join(L) + '|]'
