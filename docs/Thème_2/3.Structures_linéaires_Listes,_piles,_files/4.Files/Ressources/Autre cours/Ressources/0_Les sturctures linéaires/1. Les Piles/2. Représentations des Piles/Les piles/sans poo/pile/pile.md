```python
"""
Représentation d'une pile
Utilise: utilise list de python
Contraintes: ne pas utiliser pop,append et les slices
"""


def creer_pile():
    """
        creer une pile vide

        :return: une liste python vide
        :rtype: list
    """
    return []


def est_vide(pile):
    """
        permet de savoir si une pile est vide.

        :param pile: une pile
        :type pile: list
        :return: True si la pile est vide, False sinon
        :rtype: bool

        :Example:

        >>> est_vide([4])
        False

        >>> est_vide([])
        True
    """
    return len(pile) == 0


def empiler(pile, element):
    """
        permet d'ajouter un élément a la pile.

        :param pile: une pile
        :param element: un element
        :type pile: list
        :type elemen: element
        :return: une pile avec un élement en plus
        :rtype: list

        :Example:

        >>> empiler([1,2,3],4)
        [1,2,3,4]
    """
    longueur_pile = len(pile)
    nouvelle_pile = [None for i in range(longueur_pile + 1)]
    for i in range(longueur_pile):
        nouvelle_pile[i] = pile[i]
    nouvelle_pile[-1] = element
    return nouvelle_pile


def depiler(pile):
    """
        renvoie la pile avec l'élément au sommet de la pile en moins

        :param pile: une pile
        :type pile: list
        :return: une pile plus petite d'une element
        :rtype: list

        :Example:

        >>> depiler([1,2,3])
        [1,2]
    """
    assert not est_vide(pile), "la pile est vide !"
    return [pile[i] for i in range(len(pile) - 1)]


def sommet(pile):
    """
        renvoie la valeur au sommet de la pile.

        :param pile: une pile
        :type pile: list
        :return: Element au sommet de la pile
        :rtype: element

        :Example:

        >>> sommet([1,2,3])
        3
    """
    assert not est_vide(pile), "la pile est vide !"
    return pile[-1]
```

