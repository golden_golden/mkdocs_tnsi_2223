## Le TAD Pile

#### Description :

Dans une pile, les éléments sont empilés les uns après les autres. Si la pile n’est pas vide, il est possible de dépiler l’élément au sommet (c’est à dire, l’élément le plus récent), on dit que la pile fonctionne suivant le principe LIFO (Last In, First Out).

*illustration : Une pile de vaisselle*

<img src="https://notebooks.lecluse.fr/images/copied_from_nb/my_icons/pile1.png" alt="pile" style="zoom:80%;" />



#### Définition du Pile : 

***Type abstrait*** : Pile

***Utile :*** Entier, Elément, Booléen

***Opérations*** : Voici les primitives communément utilisées pour manipuler des piles ; il n'existe pas de normalisation pour les primitives de manipulation de pile, leurs noms respectifs sont donc indiqués de manière informelle.

Primitives de base :
$$
creer\_Pile :\space → Pile
$$

$$
empiler : Pile × Element → Pile
$$

$$
depiler : Pile  → Pile
$$

$$
est vide : Pile → Booleen
$$

$$
sommet : Pile → Element
$$

***Préconditions*** :

Soient p une Pile, e un élément.
$$
sommet(p) \space \text{est defini si et seulement si} \space estvide(p) = Faux
$$

$$
depiler(p)\space \text{est defini si et seulement si} \space estvide(p)= Faux
$$



***Les axiomes*** :

Soient p une Pile, e un élément.

$$
estvide(creer\_Pile) = Vrai
$$

$$
estvide(empiler(p,e)) = Faux
$$

$$
sommer(empiler(p,e)) = e
$$

$$
depiler(empiler(p,e)) = p
$$













#### Exemple d’utilisation :









### Remarque

------

Bonjour,

J'aimerais écrire une fonction qui effectue  des tests sur une pile, incluant des opérations "dépiler", sans qu'à la  sortie de la fonction la pile soit modifiée. L'un de vous sait-il/elle  comment faire ?

Ainsi par exemple dans la  fonction suivante qui renvoie la somme des premiers nombres successifs  croissants d'une pile, le problème est que cette fonction vide  partiellement (ou totalement si la pile est "triée") la pile :

def sommeC(pile):
  P = pile
  if est_vide(P):
    return 0
  x = depiler(P)
  somme = x
  while not est_vide(P) and consulter(P) >= x:
    x = depiler(P)
    somme += x
  return somme

Merci de votre aide et bon courage à tous,

Sylvie Genre



------

Bonjour,  

L'idée d'une pile, c'est qu'on stocke      les éléments avant de pouvoir les traiter. On ne fait pas, en      général, des parcours des valeurs. Mais quand j'ai besoin de faire      cela, je trouve assez logique d'empiler ce que je dépile dans une      autre pile temporaire, puis de dépiler cette seconde pile pour      tout remettre sur la première.

Par exemple pour les tours de Hanoï,      j'ai demandé à mes élèves de dépiler la valeur du dessus pour la      connaître, avant de remettre cet élément sur la pile. Comme si la      valeur était écrite sur le dos du disque et qu'il fallait      forcément l'enlever pour le voir.

Romain

-----

Bonjour,

une solution consiste à travailler sur une copie de la pile. Mais      cela requiert d'ajouter à la classe Pile une méthode copie.

Bonne journée.

Bruno

----

