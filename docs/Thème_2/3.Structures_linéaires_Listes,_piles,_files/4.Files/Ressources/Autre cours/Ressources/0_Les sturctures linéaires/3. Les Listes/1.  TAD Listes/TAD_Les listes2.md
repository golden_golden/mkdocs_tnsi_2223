

### Le TAD Liste

#### Description

Une liste est une collection linéaire d’éléments, chacun occupant un rang dans la liste (aussi appelé indice). La longueur d’une liste correspond au nombre de ses éléments. Il est possible d’accéder à n’importe quel ième élément de la liste à partir de son rang. Enfin, une liste est extensible : nous pouvons insérer ou supprimer des éléments en n’importe quel point de la liste.

----

Une liste est une  structure abstraite de données permettant de regrouper des données sous forme séquentielle. Elle est constituée d’éléments d’un même type, chacun possédant un rang. Une liste évolutive : on peut ajouter ou supprimer n’importe lequel des ses éléments. 

Le langage de programmation Lisp (inventé par John McCarthy en 1958) a été l’un des premiers langages de programmation à introduire cette notion de liste (Lisp signifie «list processing»).

----



----

On trouve généralement de définition de TAD de type liste, une définition itérative et une définition récursive.



Dans la définition récursive,une liste L est composée de deux parties : 

- Sa tête (souvent noté *car*), qui correspond au dernier élément ajouté à la liste;
- Sa queue (souvent noté *cdr*) que correspond au reste de la liste;

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20201112231423929.png" alt="image-20201112231423929" style="zoom:50%;" />

------





















---

#### Définition du Type liste (itérative) : 

***Type abstrait*** : Liste

***Utile :*** Entier, Elément, Booléen

-----

***Opérations*** : Voici les primitives communément utilisées pour manipuler des  listes ; il n'existe pas de normalisation pour les primitives de  manipulation de listes, leurs noms respectifs sont donc indiqués de manière informelle.



Primitives de base :
$$
  creer\_liste :\space → Liste
$$

$$
  longueur : Liste → Entier
$$

$$
inserer : Liste × Entier × Element → Liste
$$

$$
  supprimer : Liste × Entier → Liste
$$

$$
est vide : Liste → Booleen
$$

$$
contenu : Liste × Entier → Element
$$

  

Primitives auxiliaires fréquemment rencontrées :

$$
Premier : Liste  → Element
$$
$$
Dernier : Liste  → Element
$$
$$
Prochain : Liste × Entier → Element
$$
$$
Precedent : Liste × Entier → Element
$$
$$
Cherche : Liste × Element →  Booleen
$$
-----

***Préconditions*** :

Soient l une liste, e un élément et i un entier.
$$
inserer(l,i,e) \space \text{est defini si et seulement si} \space 0 ≤ i≤ longueur(l)
$$

$$
supprimer(l,i)\space \text{est defini si et seulement si} \space 0 ≤ i≤ longueur(l)
$$

$$
contenu(l,i)\space \text{est defini si et seulement si} \space 0 ≤ i≤ longueur(l)
$$
----

***Les axiomes*** :

Soient l une liste, e un élément et i,j deux entiers.

$$
  longueur(creer\_ liste) = 0
$$

$$
longueur(inserer(l,i,e)) = longueur(l) +1
$$

$$
longueur(supprimer(l,i)) = longueur(l) - 1
$$

$$
contenu(inserer(l,i,e),j) =\begin{cases} e \space \text{si} \space i=j \\ contenu(l,j) \space \text{si} \space   i > j \\ contenu(l,j-1) \space \text{si} \space   i < j  \end{cases}
$$

$$
contenu(supprimer(l,i),j) =\begin{cases} contenu(l,j) \space \text{si} \space   i > j \\ contenu(l,j+1) \space \text{si} \space   i ≤ j  \end{cases}
$$

-----



---

**Exercice**

![image-20201115150800783](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20201115150800783.png)

---
















