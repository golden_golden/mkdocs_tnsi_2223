### A vous de jouer





----

Vous allez devoir implémenter les fonctions

- ```
  p_valeur(pile)
  ```

  - prend en paramètre une pile `pile`
  - renvoie le sommet de la pile ou `None` si la pile est vide

- ```
  p_depile(pile)
  ```

  - prend en paramètre une pile `pile`
  - dépile le dernier élément saisi
  - renvoie la valeur dépilée ou déclenche une exception si la pile est vide

- ```
  p_empile(pile, v)
  ```

  - prend en paramètre une pile `pile` et une valeur `v`
  - empile la valeur `v`
  - ne renvoie rien

----





-----

```python
def p_valeur(pile):
    """- prend en paramètre une pile pile
    - renvoie le sommet de la pile
    Exemple : 
    >>> p_valeur([2, 3, 5])
    >>> 5
    >>> p_valeur([])
    >>> None
    """
    # YOUR CODE HERE
    raise NotImplementedError()
```



```python
assert p_valeur([]) is None
assert p_valeur([2, 3, 5]) == 5
```



```python
def p_depile(pile):
    """- prend en paramètre une pile pile
    - dépile le dernier élément saisi
    - renvoie le sommet de la pile
    Exemple : 
    >>> p_valeur([2, 3, 5])
    >>> 5
    >>> p_valeur([])
    >>> None
    """
    # YOUR CODE HERE
    raise NotImplementedError()
```



```python
p=[2, 3, 5]
assert p_depile(p) == 5
assert p == [2, 3]
assert p_depile([]) is None
```



```python
def p_empile(pile, v):
    """- prend en paramètre une pile et une valeur v
    - empile la valeur v
    Exemple : 
    >>> pile = [2, 3]
    >>> p_empile(pile, 5)
    >>> pile
    >>> [2, 3, 5]
    """
    # YOUR CODE HERE
    raise NotImplementedError()
```

```python
pile = [2, 3]
p_empile(pile, 5)
assert pile == [2, 3, 5]
```