# -*- coding: utf-8 -*-

"""PROGRAMME DE SIMULATION DU TRI PAR PILE Decades flush pour reprendre l'idée d'ALAIN BUSSER :
https://alainbusser.frama.io/NSI-IREMI-974/queuesortable.html

Merci à Vincent JARC pour avoir codé toute la partie tkinter et l'aide apportée.
Merci à Alain pour m'avoir illuminé sur le codage du mélange secret afin que le jeu fonctionne à tous les coups.
Il manque l'affichage d'un message de victoire mais là je n'ai plus la foi...
Gael balduini apprenti codeur
"""

from tkinter import *
from random import shuffle,random

###     FONCTIONS POUR L'AFFICHAGE DANS TKINTER   ###
POSITION_PACK1 = (10, 10)
POSITION_PACK2 = (10, 400)
POSITION_PACK3 = (200, 400)
POSITION_PACK4 = (400, 400)
POSITION_PACK5 = (300, 200)

def drawCard(can, x, y, card):
    '''Création d'une image utilisable le canevas tkinter'''
    can.create_image(
                x,
                y,
                anchor=NW,
                image=card["photo"])

def drawPack(can, pos, style, pack):
    '''Génération de l'affichage d'un paquet de carte dans le canevas tkinter
    - pos : coordonnées de la première carte à afficher)
    - Style : choix de l'affichage en "ligne" ou en "colonne"
    '''
    x,y = pos
    if style == "ligne":
        a, b = 125,1
    elif style == "colonne":
        a, b = 1, +10
    for i in range(len(pack)):
        drawCard(can, x+a*i, y+b*i, pack[i])

def updateScreen(can, pack1, pack2, pack3, pack4, pack5):
    '''Fonction pour afficher les 3 paquets de cartes !!! A ADAPTER POUR L'EXO 2'''
    global POSITION_PACK1, POSITION_PACK2, POSITION_PACK3, POSITION_PACK4, POSITION_PACK5
    for item in can.find_all():
        can.delete(item)
    drawPack(can, POSITION_PACK1, "ligne",   pack1)
    drawPack(can, POSITION_PACK2, "colonne", pack2)
    drawPack(can, POSITION_PACK3, "colonne", pack3)
    drawPack(can, POSITION_PACK4, "colonne", pack4)
    drawPack(can, POSITION_PACK5, "ligne",   pack5)

def on_shuffle_click(can, pack1, pack2, pack3, pack4, pack5):
    '''Fonction lors du click sur le bouton mélange'''
    pack1.clear()
    pack2.clear()
    pack3.clear()
    pack4.clear()
    pack5.clear()
    pack1.extend(images_paquet(paquet_depart()))
    updateScreen(can, pack1, pack2, pack3, pack4, pack5)

def on_pop_click1(can, pack1, pack2, pack3, pack4, pack5):
    if len(pack1)>0:
        empile(pack2,defile(pack1))
        updateScreen(can, pack1, pack2, pack3, pack4, pack5)

def on_push_click1(can, pack1, pack2, pack3, pack4, pack5):
    if len(pack2)>0:
        enfile(pack5,depile(pack2))
        updateScreen(can, pack1, pack2, pack3, pack4, pack5)

def on_pop_click2(can, pack1, pack2, pack3, pack4, pack5):
    if len(pack1)>0:
        empile(pack3,defile(pack1))
        updateScreen(can, pack1, pack2, pack3, pack4, pack5)

def on_push_click2(can, pack1, pack2, pack3, pack4, pack5):
    if len(pack3)>0:
        enfile(pack5,depile(pack3))
        updateScreen(can, pack1, pack2, pack3, pack4, pack5)

def on_pop_click3(can, pack1, pack2, pack3, pack4, pack5):
    if len(pack1)>0:
        empile(pack4,defile(pack1))
        updateScreen(can, pack1, pack2, pack3, pack4, pack5)

def on_push_click3(can, pack1, pack2, pack3, pack4, pack5):
    if len(pack4)>0:
        enfile(pack5,depile(pack4))
        updateScreen(can, pack1, pack2, pack3, pack4, pack5)

def nom_image(carte):
    """Génération du nom du fichier image 'enseigne_couleur_valeur.png' de la carte """
    nom=''
    if(carte[0]=='♠'):
        nom='pique_noir_'
    if(carte[0]=='♥'):
        nom='coeur_rouge_'
    if(carte[0]=='♦'):
        nom='carreau_rouge_'
    if(carte[0]=='♣'):
        nom='trefle_noir_'
    if len(carte)<3:
        valeur=carte[1]
    else:
        valeur=carte[1:3]
    return nom+valeur+'.png'

def images_paquet(paquet):
    '''génération d'une liste de dictionnaires des cartes avec leurs images
        carte = { couleur': , 'valeur': , 'nomFichier' : , 'photo' : }
    '''
    images=[]
    for i in paquet:
        if len(i)<3:
            valeur=i[1]
        else:
            valeur=i[1:3]
        carte = {
                'couleur': i[0],
                'valeur': valeur,
                'nomFichier' : nom_image(i),
                'photo' : None   #Impossible de définir directement l'image ici
                }
        carte['photo'] = PhotoImage(file=carte['nomFichier'])
        images.append(carte)
    return images


###     FONCTIONS POUR LA DEFINITION DU JEU DE CARTE   ###
def paquet_depart():
    '''Fonction de génération de la première liste de cartes'''
    return  melange_secret_decades(['♠A','♠2','♠3','♠4','♠5','♠6','♠7','♠8','♠9','♠10'])#,'♠V','♠D','♠R'])

def melange_aleatoire(paquet):
    '''Fonction de mélange en mode aléatoire d'un paquet de carte'''
    melange=paquet.copy()
    shuffle(melange)
    return melange

def melange_secret_decades(paquet):
    '''Fonction de mélange en mode aléatoire d'un paquet de carte'''
    L1 = []
    L2 = paquet.copy() #Nécessaire pour éviter les effets de bords...
    p1 = []
    p2 = []
    p3 = []
    while len(L2)>0 or len(p1)>0 or len(p2)>0 or len(p3)>0:
        if (random()<0.5):
            if len(L2)>0:
                file = [p1,p2,p3][int(random()*3)]
                file.insert(0,L2.pop(0))
        else:
            file = [p1,p2,p3][int(random()*3)]
            if len(file)>0:
                L1.append(file.pop(0))
    return L1

def defile(paquet):          #ATTENTION ICI ON UTILISE VOLONTAIREMENT L'EFFET DE BORD !!!
    '''Fonction pour défiler la dernière carte de la file'''
    return paquet.pop()

def empile(paquet,valeur):   #ATTENTION ICI ON UTILISE VOLONTAIREMENT L'EFFET DE BORD !!!
    '''Fonction pour empiler la valeur en haut de la pile'''
    paquet.append(valeur)    #Le haut de la pile correspond à la fin de la liste
    return paquet

def depile(paquet):          #ATTENTION ICI ON UTILISE VOLONTAIREMENT L'EFFET DE BORD !!!
    '''Fonction pour dépiler la valeur du haut de la pile'''
    return paquet.pop()      #Le haut de la pile correspond à la fin de la liste

def enfile(paquet,valeur):   #ATTENTION ICI ON UTILISE VOLONTAIREMENT L'EFFET DE BORD !!!
    '''Fonction pour enfiler la valeur en début de file'''
    paquet.insert(0,valeur)
    return paquet            #Ici il serait préférable d'utilise la fonction queue.appendleft() si la liste était grande...


###     PROGRAMME PRINCIPAL   ###
if __name__ == '__main__':
    #FENETRE
    fenetre = Tk()
    fenetre.geometry("1800x874")

    #CANVAS
    caneva = Canvas(fenetre)
    caneva.config(width = 1800,
            height = 800,
            bg="#4183D7",
            highlightbackground="#4183D7")
    caneva.pack(side="top")

    #CARTES
    paquet1=images_paquet(paquet_depart())
    paquet2 = []
    paquet3 = []
    paquet4 = []
    paquet5 = []
    updateScreen(caneva, paquet1, paquet2, paquet3, paquet4, paquet5)

    #BOUTONS (MELANGE | EMPILE | DEPILE | SORTIE)
    photo0 = PhotoImage(file="ButtonShuffle64.png")
    photo1 = PhotoImage(file="ButtonGB64.png")
    photo2 = PhotoImage(file="ButtonBD64.png")
    photo3 = PhotoImage(file="ButtonClose64.png")
    button_melange = Button(fenetre, image=photo0, command=lambda:on_shuffle_click(caneva,paquet1,paquet2,paquet3,paquet4,paquet5))
    button_empile1  = Button(fenetre, image=photo1, command=lambda:on_pop_click1(caneva,paquet1,paquet2,paquet3,paquet4,paquet5))
    button_depile1  = Button(fenetre, image=photo2, command=lambda:on_push_click1(caneva, paquet1, paquet2, paquet3,paquet4,paquet5))

    button_empile2  = Button(fenetre, image=photo1, command=lambda:on_pop_click2(caneva,paquet1,paquet2,paquet3,paquet4,paquet5))
    button_depile2  = Button(fenetre, image=photo2, command=lambda:on_push_click2(caneva,paquet1,paquet2,paquet3,paquet4,paquet5))
    button_empile3  = Button(fenetre, image=photo1, command=lambda:on_pop_click3(caneva,paquet1,paquet2,paquet3,paquet4,paquet5))
    button_depile3  = Button(fenetre, image=photo2, command=lambda:on_push_click3(caneva,paquet1,paquet2,paquet3,paquet4,paquet5))

    button_sortie  = Button(fenetre, image=photo3, command=fenetre.destroy)
    button_melange.pack(side="left", fill=X, expand=YES)
    button_empile1 .pack(side="left", fill=X, expand=YES)
    button_depile1 .pack(side="left", fill=X, expand=YES)
    button_empile2 .pack(side="left", fill=X, expand=YES)
    button_depile2 .pack(side="left", fill=X, expand=YES)
    button_empile3 .pack(side="left", fill=X, expand=YES)
    button_depile3 .pack(side="left", fill=X, expand=YES)
    button_sortie .pack(side="left", fill=X, expand=YES)

    #MAINLOOP
    fenetre.mainloop()
