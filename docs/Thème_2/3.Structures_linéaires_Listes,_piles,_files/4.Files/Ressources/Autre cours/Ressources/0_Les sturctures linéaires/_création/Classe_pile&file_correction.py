# -*- coding: utf-8 -*-

### Classe CELLULE
class Cellule:
    
    def __init__(self, valeur, suivant):
        self._valeur = valeur
        self._suivant = suivant

### Classe PILE
class Pile:
    
    def __init__(self, c = None):
        self.cellule = c
        self.taille = 0
    
    def empiler(self, valeur):
        self.cellule = Cellule(valeur, self.cellule)
        self.taille+=1
    
    def depiler(self):
        if self.est_vide():
            return None
        else:
            valeur = self.cellule._valeur
            self.cellule = self.cellule._suivant
            self.taille-=1
            return valeur
    
    def est_vide(self):
        return self.cellule is None
    
    def taille(self):
        return self.taille()
    
    def __str__(self):
        cell = self.cellule
        txt = "{ \n"
        if cell is not None:
            txt += str(cell._valeur) + " ; \n"
            cell = cell._suivant
        else:
            return " Pile Vide "
        while cell is not None:
            txt +=  str(cell._valeur) + " ; \n"
            cell = cell._suivant
        return txt + "}"
    
    def __repr__(self):
        l=[]
        cell=self.cellule
        for i in range(self.taille):
            l.append(cell._valeur)
            cell=cell._suivant
        return str(l)

### Classe FILE
class File:
    
    def __init__(self, c = None):
        self.cellule = c
        self.taille = 0
    
    def enfiler(self, valeur):
        if self.est_vide():
            self.cellule = Cellule(valeur, self.cellule)
        else:
            cel = self.cellule
            while cel._suivant != None:
                cel = cel._suivant
            cel._suivant = Cellule(valeur, cel._suivant)
        self.taille+=1
    
    def defiler(self):
        if self.est_vide():
            return None
        else:
            valeur = self.cellule._valeur
            self.cellule = self.cellule._suivant
            self.taille-=1
            return valeur
    
    def est_vide(self):
        return self.cellule == None
    
    def taille(self):
        return self.taille()
    
    def __str__(self):
        txt = '{ '
        cell = self.cellule
        while cell!=None:
            txt += str(cell._valeur)
            if cell._suivant != None:
                txt += " ; "
            cell = cell._suivant
        txt += ' }'
        return txt
    
    def __repr__(self):
        l=[]
        cell=self.cellule
        for i in range(self.taille):
            l.append(cell._valeur)
            cell=cell._suivant
        l=l[::-1]
        return str(l)
