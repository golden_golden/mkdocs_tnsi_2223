# -*- coding: utf-8 -*-
"""
Chapitre: NSIT_04_Strucutres lineaires
Activité: Spool d'impression / Spool

Test de 3 implémentations différentes du type File

@author: eric.buonocore
"""

import NSIT_e04d_spool as sp
import NSIT_e04d_file as fl
from collections import deque
import time
from pylab import *

REPETITIONS = 1000
TAILLE_MAX = 100

# ***** ***** ***** ***** ***** ***** ***** *****
# Test de l'implémentation de la File avec Spool
# ***** ***** ***** ***** ***** ***** ***** *****
a = sp.Spool(TAILLE_MAX) # Instancie a en tant qu'objet de type Spool défini dans NSIT_e04d_spool.py

# ***** ***** ***** ***** ***** ***** ***** *****
# Test de l'implémentation de la File avec File
# ***** ***** ***** ***** ***** ***** ***** *****
f = fl.File() # Instancie a en tant qu'objet de type File défini dans NSIT_e04d_spool.py

# ***** ***** ***** ***** ***** ***** ***** *****
# Test de l'implémentation de la File avec deque
# ***** ***** ***** ***** ***** ***** ***** *****
d = deque([]) # Crée une collection vide

tailles = []
tps_Spool = []
tps_File = []
tps_deque = []

for taille in range(10, TAILLE_MAX, TAILLE_MAX//10):
    tailles.append(taille)
    tps_debut = time.time() # Relève le chronomètre à ce moment

    # Remplie et vide la file 10.000 fois de suite
    for n in range(REPETITIONS):
        # Ajoute 10 éléments: Remplie entièrement la file
        for i in range(taille):
            a.ajoute("un nouveau document")

        # Extrait 10 éléments: Vide la file
        for i in range(taille):
            a.extrait()

    tps_fin = time.time() # Relève la nouvelle caleur du chronomètre
    tps_Spool.append(tps_fin - tps_debut)

    tps_debut = time.time() # Relève le chronomètre à ce moment

    # Remplie et vide la file 10.000 fois de suite
    for n in range(REPETITIONS):
        # Ajoute 10 éléments: Remplie entièrement la file
        for i in range(taille):
            f.ajoute("un nouveau document")

        # Extrait 10 éléments: Vide la file
        for i in range(taille):
            f.extrait()

    tps_fin = time.time() # Relève la nouvelle caleur du chronomètre
    tps_File.append(tps_fin - tps_debut)


    tps_debut = time.time() # Relève le chronomètre à ce moment

    # Remplie et vide la file 10.000 fois de suite
    for n in range(REPETITIONS):
        # Ajoute 10 éléments: Remplie entièrement la file
        for i in range(taille):
            d.appendleft("un nouveau document")

        # Extrait 10 éléments: Vide la file
        for i in range(taille):
            d.popleft()

    tps_fin = time.time() # Relève la nouvelle caleur du chronomètre
    tps_deque.append(tps_fin - tps_debut)

plot(tailles, tps_Spool, label="Spool")
plot(tailles, tps_File, label="File") 
plot(tailles, tps_deque, label="deque") 
xlabel("Taille des files")
ylabel("Temps")
legend()

show() # Affiche la figure à l'ecran

