## Le TAD File

#### Description :

Dans une file, les éléments sont enfilés à une extrémité et défilés à l’autre. Si la file n’est pas vide, il est possible de défiler son premier élément (c’est à dire, l’élément le plus ancien), on dit que la file fonctionne suivant le principe FIFO (First In, First Out).

*Illustration : File d’attente en caisse*

<img src="https://notebooks.lecluse.fr/images/copied_from_nb/my_icons/file1.png" alt="file" style="zoom:80%;" />



#### Définition du File : 

***Type abstrait*** : File

***Utile :*** Entier, Elément, Booléen

***Opérations*** : Voici les primitives communément utilisées pour manipuler des files ; il n'existe pas de normalisation pour les primitives de manipulation de file, leurs noms respectifs sont donc indiqués de manière informelle.

Primitives de base :
$$
creer\_File :\space → File
$$

$$
emfiler : File × Element → File
$$

$$
defiler : File  → File
$$

$$
est vide : File → Booleen
$$

$$
premier : File → Element
$$

***Préconditions*** :

Soient f une File, e un élément.
$$
premier(f) \space \text{est defini si et seulement si} \space estvide(f) = Faux
$$

$$
defiler(f)\space \text{est defini si et seulement si} \space estvide(f)= Faux
$$

***Les axiomes*** :

Soient f une File, e un élément.

$$
estvide(creer\_File) = Vrai
$$

$$
estvide(emfiler(f,e)) = Faux
$$

$$
premier(emfiler(f,e)) = premier(f)
$$

$$
defiler(emfiler(f,e)) = enfiler(defiler(f),e)
$$







#### Exemples d’utilisations :





