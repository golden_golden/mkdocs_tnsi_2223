"""
Représentation d'une file
Utilise: deux piles
Contraintes: ne pas utiliser pop, append et les slices
"""


##################################################################################################
### La class Pile  ###############################################################################
##################################################################################################

class Maillon:
    def __init__(self, valeur, precedent):
        self.valeur = valeur
        self.precedent = precedent


class Pile:
    # Constructeur
    def __init__(self):
        """
            creer une pile vide
        """
        self.__sommet = None

    # Méthodes classiques
    def est_vide(self):
        """
            permet de savoir si une pile est vide.
        """
        return self.__sommet == None

    def empiler(self, element):
        """
            permet d'ajouter un élément à la pile.

            :param element: un element
            :type elemen: element
        """
        self.__sommet = Maillon(element, self.__sommet)

    def depiler(self):
        """
         enlève le sommet élément de la pile

        """
        assert not self.est_vide(), "la pile est vide !"
        self.__sommet = self.__sommet.precedent

    def sommet(self):
        """
            renvoie la valeur au sommet de la pile.
        """
        assert not self.est_vide(), "la pile est vide !"
        return self.__sommet.valeur

    # Méthodes spéciales
    def __str__(self):
        texte = ""
        parcour = self.__sommet
        while parcour != None:
            texte = "|\t" + str(self.__sommet.valeur) + "\t|" + "\n" + texte
            parcour = self.__sommet.precedent
        texte = "\nEtat de la pile:\n" + texte
        return texte





###########################################################################################################
### la classe file  #######################################################################################
###########################################################################################################

class File:
    def __init__(self):
        self.__pile1 = Pile()
        self.__pile2 = Pile()

    def est_vide(self):
        return self.__pile1.est_vide() and self.__pile2.est_vide()

    def enfiler(self, element):
        self.__pile1.empiler(element)

    def __basucler_pile1_dans_pile2(self):
        while not self.__pile1.est_vide():
            self.__pile2.empiler(self.__pile1.sommet())
            self.__pile1.depiler()

    def defiler(self):
        assert not self.est_vide(), "Attention,pour defiler la file ne doit pas être vide !"
        if self.__pile2.est_vide():
            self.__basucler_pile1_dans_pile2()
        self.__pile2.depiler()

    def premier(self):
        assert not self.est_vide(), "Attention,pour obtenir premier la file ne doit pas être vide !"
        if self.__pile2.est_vide():
            self.__basucler_pile1_dans_pile2()
        return self.__pile2.sommet()

    # Méthodes spéciales (partie à terminer)
    def __str__(self):
        titre = "\nEtat de la file:\n"
        bordure = ""
        texte_element = ""


        # Parcours de la file et stoquage des éléments dans un file de stockage
        file_de_stoquage = File()
        while not self.est_vide():
            element = str(self.premier())
            texte_element = element + " " + texte_element

            # création d'une bordure de la bonne dimmension
            for caractere in element:
                bordure += "_"
            bordure += "_"

            file_de_stoquage.enfiler(element)
            self.defiler()

        # on vide la file de stoquage pour remmetre les éléments dans la file de départ, ni vu ni connu
        while not file_de_stoquage.est_vide():
            self.enfiler(file_de_stoquage.premier())
            file_de_stoquage.defiler()


        return titre + bordure + "\n" + texte_element + "\n" + bordure + "\n"


##########################################################################################################
## Test de la file #######################################################################################
##########################################################################################################

if __name__ == "__main__":
    print("On créer une File")
    p = File()
    print("La file est elle vide ?",p.est_vide())

    print("On enfile 9")
    p.enfiler(9)
    print(p)
    print("La file est elle vide ?",p.est_vide())
    print("Le premier :", p.premier())

    print("\n \n")

    p.enfiler(10)
    print("On enfile 10")
    print(p)
    print("Le premier :", p.premier())
    print("La file est elle vide ?",p.est_vide())

    print("\n \n")

    p.enfiler(12)
    print("On enfile 12")
    print(p)
    print("Le premier :", p.premier())
    print("La file est elle vide ?",p.est_vide())


    print("\n \n")

    p.defiler()
    print("On dépile")
    print(p)
    print("Le premier :", p.premier())

    print("\n \n")

    p.defiler()
    print("on dépile")
    print(p)
    print("Le premier :", p.premier())

    print("\n \n")

    p.defiler()
    print("on dépile")
    print(p)
    print("La file est elle vide ?",p.est_vide())


