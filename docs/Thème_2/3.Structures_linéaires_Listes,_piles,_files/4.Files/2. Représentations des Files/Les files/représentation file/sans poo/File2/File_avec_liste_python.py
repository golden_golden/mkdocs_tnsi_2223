"""
Correction de l'exercice 1
    Représentation d'une pile
    Utilise: utilise list de python
    Contraintes: Aucunes contraintes, utilisation de pop et append autorisé
"""

def creer_file():
    """
        creer une file vide

        :return: une liste python vide
        :rtype: list
    """
    return []


def est_vide(file):
    """
        permet de savoir si une file est vide.

        :param file: une file
        :type file: list
        :return: True si la file est vide, False sinon
        :rtype: bool

        :Example:

        >>> est_vide([4])
        False

        >>> est_vide([])
        True
    """
    return len(file) == 0


def enfiler(file, element):
    """
       permet d'ajouter un élément à la file.

       :param file: une file
       :param element: un element
       :type file: list
       :type element: element
       :return: une file avec un element en plus
       :rtype: list

   """
    file.append(element)


def defiler(file):
    """
        renvoie la file sans l'élément au sommet de la file

        :param file: une file
        :type file: list
        :return: une file plus petite d'une element
        :rtype: list

    """
    assert not est_vide(file), "Attention, la file est vide !"
    file.pop(0)


def premier(file):
    """
        renvoie la valeur au sommet de la file.

        :param file: une file
        :type file: list
        :return: Element au sommet de la file
        :rtype: element

        :Example:

        >>> premier([1,2,3])
        1
    """
    assert not est_vide(file), "Attention, la file est vide !"
    return file[0]


# Test de la file
if __name__ == "__main__":
    ma_file = creer_file()
    print(ma_file)
    print("ma file est elle vide ?", est_vide(ma_file))
    print("\n")

    enfiler(ma_file, 5)
    enfiler(ma_file, 6)
    print(ma_file)
    print("ma file est elle vide ?", est_vide(ma_file))
    print("le premier element de ma file est ", premier(ma_file))
    print("\n")

    defiler(ma_file)
    print("On défile")
    print(ma_file)
    print("le premier element de ma file est ", premier(ma_file))
    print("\n")