"""
Correction de l'exercice 2:
    Représentation d'une pile
    Utilise: utilise list de python
    Contraintes: ne pas utiliser pop,append et les slices
"""


def creer_file():
    """
        creer une file vide

        :return: une liste python vide
        :rtype: list
    """
    return []


def est_vide(file):
    """
        permet de savoir si une file est vide.

        :param file: une file
        :type file: list
        :return: True si la file est vide, False sinon
        :rtype: bool

        :Example:

        >>> est_vide([4])
        False

        >>> est_vide([])
        True
    """
    return len(file) == 0


def enfiler(file, element):
    """
        permet d'ajouter un élément à la file.

        :param file: une file
        :param element: un element
        :type file: list
        :type elemen: element
        :return: une file avec un élement en plus
        :rtype: list

        :Example:

        >>> enfiler([1,2,3],4)
        [1, 2, 3, 4]
    """
    longueur_file = len(file)
    nouvelle_file = [None for i in range(longueur_file + 1)]
    for i in range(longueur_file):
        nouvelle_file[i] = file[i]
    nouvelle_file[-1] = element
    return nouvelle_file


def defiler(file):
    """
        renvoie la file sans l'élément au sommet de la file

        :param file: une file
        :type file: list
        :return: une file plus petite d'une element
        :rtype: list

        :Example:

        >>> defiler([1,2,3])
        [2, 3]
    """
    assert not est_vide(file), "la file est vide !"
    return [file[i] for i in range(1, len(file))]


def premier(file):
    """
        renvoie la valeur au sommet de la file.

        :param file: une file
        :type file: list
        :return: Element au sommet de la file
        :rtype: element

        :Example:

        >>> premier([1,2,3])
        1
    """
    assert not est_vide(file), "la file est vide !"
    return file[0]


# Test de la file
if __name__ == "__main__":
    ma_file = creer_file()
    print(ma_file)
    print("ma file est elle vide ?", est_vide(ma_file))
    print("\n")

    ma_file = enfiler(ma_file, 5)
    ma_file = enfiler(ma_file, 6)
    print(ma_file)
    print("ma file est elle vide ?", est_vide(ma_file))
    print("le premier element de ma file est ", premier(ma_file))
    print("\n")

    ma_file = defiler(ma_file)
    print("On défile")
    print(ma_file)
    print("le premier element de ma file est ", premier(ma_file))
    print("\n")
