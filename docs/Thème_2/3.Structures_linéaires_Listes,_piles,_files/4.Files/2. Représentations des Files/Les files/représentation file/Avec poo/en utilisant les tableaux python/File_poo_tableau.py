"""
Correction de l'exercice
    Représentation d'une pile
    Utilise: utilise list de python et le POO
    Contraintes: ne pas utiliser pop, append et les slices
"""

class File:
    """

    """

    # Constructeur
    def __init__(self):
        """
            creer une pile vide
        """
        self.__file = []
        self.__taille = 0


    # Méthodes classiques
    def est_vide(self):
        """
            permet de savoir si une pile est vide.
        """
        return self.__taille == 0

    def enfiler(self, element):
        """
            permet d'ajouter un élément à la file.

            :param element: un element
            :type elemen: element
        """
        nouvelle_file = [None for i in range(self.__taille + 1)]
        for i in range(self.__taille):
            nouvelle_file[i] = self.__file[i]
        nouvelle_file[-1] = element
        self.__file = nouvelle_file
        self.__taille += 1

    def defiler(self):
        """
         enlève le sommet élément de la pile

        """
        assert not self.est_vide(), "la file est vide !"
        self.__file = [self.__file[i] for i in range(1, self.__taille)]
        self.__taille -= 1

    def premier(self):
        """
            renvoie la valeur du premier de la file.
        """
        assert not self.est_vide(), "La file est vide !"
        return self.__file[0]


   # Méthodes spéciales
    def __str__(self):
        titre = "\nEtat de la file:\n"
        bordure = ""
        texte_element = ""

        for element in self.__file:
            element_texte = str(element)
            texte_element =  element_texte + " " + texte_element
            for caractere in element_texte:
                bordure += "_"
            bordure += "_"

        return titre + bordure +"\n" + texte_element + "\n"+ bordure +"\n"



# test de la file
if __name__=="__main__":

    print("on créer une file")
    p = File()


    print("on enfile 9")
    p.enfiler(9)
    print(p)


    p.enfiler(10)
    print("on enfile 10")
    print(p)
    print("le premier :",p.premier())


    p.enfiler(12)
    print("on enfile 12")
    print(p)
    print("le premier :",p.premier())


    p.defiler()
    print("on défile")
    print(p)
    print("le premier :",p.premier())


    print("La pile est elle vide ? ",p.est_vide())

    p.defiler()
    print("on défile")
    print(p)
    print("La file est elle vide ? ",p.est_vide())


    p.defiler()
    print("on défile")
    print(p)
    print("La file est elle vide ? ",p.est_vide())
