```python
"""
Représentation d'une file
Utilise: utilise list de python
Contraintes: ne pas utiliser pop,append et les slices
"""


def creer_file():
    """
        creer une file vide

        :return: une liste python vide
        :rtype: list
    """
    return []


def est_vide(file):
    """
        permet de savoir si une file est vide.

        :param file: une file
        :type file: list
        :return: True si la file est vide, False sinon
        :rtype: bool

        :Example:

        >>> est_vide([4])
        False

        >>> est_vide([])
        True
    """
    return len(file) == 0


def enfiler(file, element):
    """
        permet d'ajouter un élément à la file.

        :param file: une file
        :param element: un element
        :type file: list
        :type element: element
        :return: une file avec un élement en plus
        :rtype: list

        :Example:

        >>> enfiler([1,2],3)
        [1,2,3]
    """
    longueur_file = len(file)
    nouvelle_file = [None for i in range(longueur_file + 1)]

    for i in range(longueur_file):
        nouvelle_file[i] = file[i]
    nouvelle_file[-1] = element
    return nouvelle_file


def defiler(file):
    """
        renvoie la file sans l'élément au sommet de la file

        :param file: une pile
        :type file: list
        :return: une file plus petite d'une element
        :rtype: list

        :Example:

        >>> defiler([1,2,3])
        [2,3]
    """
    assert not est_vide(file), "la file est vide !"
    return [file[i] for i in range(len(file)) if i >= 1]


def premier(file):
    """
        renvoie la première valeur de la file.

        :param file: une file
        :type file: list
        :return: premier element de la file
        :rtype: element

        :Example:

        >>> premier([1,2,3])
        1
    """
    assert not est_vide(file), "la file est vide !"
    return file[0]


```



```python
# test rapide
if __name__ == "__main__":
    a = creer_file()
    print(est_vide(a))
    a = enfiler(a, 1)
    a = enfiler(a, 2)
    a = enfiler(a, 3)
    print(a)
    print(premier(a))
    a = defiler(a)
    print(a)
```

