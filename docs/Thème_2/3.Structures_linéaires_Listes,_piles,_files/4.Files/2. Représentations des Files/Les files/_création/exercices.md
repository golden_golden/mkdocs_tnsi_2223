### A vous de jouer

Vous allez devoir implémenter les fonctions

- ```
  f_valeur(file)
  ```

  - prend en paramètre une file
  - renvoie la valeur à l'avant de la file ou `None` si la file est vide

- ```
  f_defile(file)
  ```

  - prend en paramètre une file
  - défile l'élément situé à l'avant de la file
  - renvoie la valeur défilée ou `None` si la file est vide

- ```
  f_emfile(file, v)
  ```

  - prend en paramètre une file et une valeur `v`
  - emfile la valeur `v` à l'arrière de la file
  - ne renvoie rien





```python
def f_valeur(file):
    """- prend en paramètre une file
    - renvoie la valeur à l'avant de la file ou None si la file est vide
    Exemple : 
    >>> f_valeur([2, 3, 5])
    >>> 5
    >>> f_valeur([])
    >>> None
    """
    # YOUR CODE HERE
    raise NotImplementedError()
```


```python
assert f_valeur([]) is None
assert f_valeur([2, 3, 5]) == 5
```


```python
def f_defile(file):
    """- prend en paramètre une file
    - défile l'élément situé à l'avant de la file
    - renvoie la valeur défilée ou None si la file est vide
    Exemple : 
    >>> file = [2, 3, 5, 8]
    >>> f_defile(file)
    >>> 8
    >>> file
    >>> [2, 3, 5]
    """
    # YOUR CODE HERE
    raise NotImplementedError()
```


```python    
file = [2, 3, 5, 8]
assert f_defile(file) == 8
assert file == [2, 3, 5]
```


```python
def f_enfile(file, v):
    """- prend en paramètre une file et une valeur v
    - enfile la valeur v à l'arrière de la file
    Exemple :
    >>> file = [2, 3, 5, 8]
    >>> f_enfile(file, 1)
    >>> file
    >>> [1, 2, 3, 5, 8]
    """
    # YOUR CODE HERE
    raise NotImplementedError()
```

```python   
file = [2, 3, 5, 8]
f_enfile(file, 1)
assert file == [1, 2, 3, 5, 8]
```