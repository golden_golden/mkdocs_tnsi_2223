---
title: "Qu'est ce qu'une File"

---


# Le TAD File

## 1. Qu'est ce qu'une File

Dans une file, les éléments sont enfilés à une extrémité et défilés à l’autre. Si la file n’est pas vide, il est possible de défiler son premier élément (c’est à dire, l’élément le plus ancien), on dit que la file fonctionne suivant le principe FIFO (First In, First Out).

<br/>

<center>
<img src="https://notebooks.lecluse.fr/images/copied_from_nb/my_icons/file1.png" alt="file" style="zoom:80%;" />
<center/>

*Illustration : File d’attente en caisse*