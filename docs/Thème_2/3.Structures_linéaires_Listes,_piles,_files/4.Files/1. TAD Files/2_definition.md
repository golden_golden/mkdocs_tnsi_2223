---
title: "Définition"

---


# Le TAD File



## 2. Définition du TAD File

***Type abstrait*** : File

<br/>

***Utile :*** Entier, Elément, Booléen

<br/>

***Opérations*** : Voici les primitives communément utilisées pour manipuler des files ; il n'existe pas de normalisation pour les primitives de manipulation de file, leurs noms respectifs sont donc indiqués de manière informelle.

<br/>

Primitives de base :

$$
creer\_File :\space → File
$$

$$
emfiler : File × Element → File
$$

$$
defiler : File  → File
$$

$$
est vide : File → Booleen
$$

$$
premier : File → Element
$$

<br/>

***Préconditions*** :

Soient f une File, e un élément.

$$
premier(f) \space \text{est defini si et seulement si} \space estvide(f) = Faux
$$

$$
defiler(f)\space \text{est defini si et seulement si} \space estvide(f)= Faux
$$


<br/>


***Les axiomes*** :

Soient f une File, e un élément.

$$
estvide(creer\_File) = Vrai
$$

$$
estvide(emfiler(f,e)) = Faux
$$

$$
premier(emfiler(f,e)) = premier(f)
$$

$$
defiler(emfiler(f,e)) = enfiler(defiler(f),e)
$$
