def creer_liste():
    return ()

def liste_vide(Liste):
    return Liste == ()

def inserer(Liste,e):
    return (e, Liste)

def tete(Liste):
    assert not liste_vide(Liste)
    tete, queue = Liste
    return tete

def queue(Liste):
    assert not liste_vide(Liste)
    tete, queue = Liste
    return queue

def element_liste(Liste):
    tableau = []
    while not liste_vide(Liste):
        tableau.append(tete(Liste))
        Liste = queue(Liste)
    return tableau

liste = creer_liste()
liste = inserer(liste,"Alice")
liste = inserer(liste,"Bob")
liste = inserer(liste,"Charlie")
print(tete(liste))
print(element_liste(liste))


