﻿Chapitre 6 - Feuille d'exercice sur les types abstraits de données                   Terminale NSI

******    Exercices sur les listes    ******
Une liste est une séquence ordonnée d'éléments. Son interface minimale est à suivante :
    • creer liste() : constructeur qui retourne une liste vide.
    • liste_vide(L) : accesseur qui retourne Vrai si la liste L est vide
    • inserer(L, e) : opérateur qui insère l'élément e en tête de la liste L et retourne la nouvelle liste
    • tete(L) : accesseur qui retourne l'élément en tête de liste (celle-ci n'est pas vide)
    • queue(L) : accesseur qui retourne la liste privée de son premier élément (la liste n'est pas vide)
    • elements_liste(L) : itérateur qui retourne un tableau contenant les éléments de la liste, que l'on peut ensuite énumérer avec une boucle for.
Exercice 1- Implémentation par des tuples
On peut implémenter une liste en Python en utilisant des tuples .Comme les tuples Python sont immuables, la liste elle-même sera immuable, ce qui peut être intéressant dans certains cas.
Une liste est représentée par un tuple (tête, queue) où tête est le premier élément de la liste et queue est le reste de la liste, qui est elle-même une liste (donc un tuple). 
Exemples : 
a) la liste vide : ()     
b) la liste composée du seul élément 2 : (2, ())   
c) la liste composée des éléments 2 et 3 où 3 est en tête de liste : (3, (2, ())) 
Compéter l'implémentation suivante : 
def creer_liste():
    return ()

def liste_vide(Liste):
    return Liste == ()

def inserer(Liste,e):
    return (e, Liste)

def tete(Liste):
    assert not liste_vide(Liste)
    tete, queue = Liste
    return tete

def queue(Liste):
    


def element_liste(Liste):






Testez l'utilisation suivante de l'implémentation des listes :
liste = creer_liste()
liste = inserer(liste,"Alice")
liste = inserer(liste,"Bob")
liste = inserer(liste,"Charlie")
print(tete(liste))
print(element_liste(liste))


Exercice 3– Les listes chaînées
https://info.blaisepascal.fr/nsi-listes-chainees




******    Exercices sur les piles    ******
Exercice 4
Donnez  des exemples classiques d'utilisation des Piles en informatique.
Exercice 5
On suppose que la classe Pile est déjà implémentée. On considère l'enchaînement d'opérations ci-dessous. Écrire à chaque étape l'état de la pile p et la valeur éventuellement renvoyée. 
>>> p = Pile()
>>> p.empile(3)   # ou p.push(3)
>>> p.empile(5)
>>> p.est_vide()
>>> p.empile(1)
>>> p.depile()    # ou p.pop()
>>> p.depile()
>>> p.empile(9)
>>> p.depile()
>>> p.depile()
>>> p.est_vide()

Exercice 6 - Implémentation d'une Pile par un tableau
Dans cette partie, un tableau sera implémenté par le type de données list de Python. Le sommet de la pile sera l'élément d'indice le plus grand du tableau (donc à droite).
1) Implémenter une classe Pile, disposant des méthodes suivantes :
    a) Pile() : constructeur qui crée une pile vide avec la syntaxe : p = Pile().
    b) est_vide() -> bool : accesseur qui indique si la pile est vide.
    c) empile(x : int) -> None ou push(x : int) -> None : opérateur qui insère un élément en haut (/à droite) de la pile. Ce sera le dernier arrivé.
    d) depile() ou pop() : accesseur et opérateur qui renvoie la valeur de l'élément en haut /(à droite/premier à sortir) de la pile ET le supprime de la pile.
    e) get_sommet() accesseur qui renvoie la valeur du sommet de la Pile, sans le dépiler, ou None si la Pile est vide.
    f) une méthode magique __repr__() : permet d'afficher la pile sous forme agréable lorsque :
    • l'on tape  >>> p ou  >>> print(p) dans un Interpréteur Python
    • l'on utilise l'instruction print(p) dans un Script Python 
Exemple d'affichage : |•|3|6|2|5| où 3 est le premier arrivé, et 5 est le sommet/dernier arrivé)
    g) Modifier le constructeur __init__() de sorte que l'on puisse maintenant instancier la classe Pile en lui passant en argument un objet de type list de Python:
# Créer une Pile `p` initialisée par la liste Python [1,2,3], 
# qui sera représentée dans un Terminal par `|•|1|2|3|`
p = Pile([1,2,3])

    h) vider() : opérateur qui vide la Pile. Syntaxe : p.vider() vide p
    i) copy() : renvoie une copie profonde de la Pile (la modification de l'une n'entraine pas la modification de l'autre). Syntaxe : p1 = p.copy()
    j) renverse() : opérateur qui renverse la Pile. Syntaxe : p.renverse() renverse p
    k) concatene() : opérateur qui concatène deux Piles. Syntaxe : p1.concatene(p2) concatène p2 au dessus de p1, et p2 est vidée
2) Quelle est la complexité en temps des opérations empile() et depile() ? 
Conclusion : Le type list de Python est-il parfaitement adapté pour implémenter une Pile?
3) Précisément, quelle méthode dans notre classe Pile est le facteur limitant, c'est à dire ralentit nos opérations? et pourquoi ?
Exercice 7 - Historique de Navigation
À l'aide de deux variables adresses et adresse_courante, et de la classe Pile créée plus haut, simulez une gestion de l'historique de navigation internet. Seules deux fonctions go_to(nouvelle_adresse) et back() sont à créer.
******    Exercices sur les files    ******
Exercice 8
Donnez des exemples d'utilisation des Files en informatique.
Exercice 9
On suppose que la classe File est déjà implémentée. On considère l'enchaînement d'opérations ci-dessous. Écrire à chaque étape l'état de la file f et la valeur éventuellement renvoyée. 
f = File()
f.enfile(3)       # ou f.enqueue(3)
f.enfile(5)
f.est_vide()
f.enfile(1)
f.defile()        # ou f.dequeue()
f.defile() 
f.enfile(9)
f.defile() 
f.defile()  
f.est_vide()

Exercice 10- Implémentation d'une File par un Tableau
Dans cette partie, un tableau sera implémenté par le type de données list de Python. 
1) Implémenter une classe File, disposant des méthodes suivantes :
    • File() : constructeur qui crée une file vide, et initialise le constructeur __init__() avec un attribut data = [] initialisé à un objet Python vide, de type list
    • est_vide() : accesseur qui indique si la file est vide.
    • enfile(x : int) -> None ou enqueue(x : int) -> None : opérateur qui insère un élément en bas (ou à droite) de la file, en temps constant
    • defile() ou dequeue() : accesseur et opérateur qui renvoie la valeur de l'élément en haut (ou à gauche) de la file ET le supprime de la file.
    • get_tete() : accesseur qui renvoie la valeur de la tete, sans la supprimer de la File
    • une méthode magique __repr__() : permet d'afficher la file sous forme agréable lorsque :
- > l'on tape >>> f ou >>> print(f) dans un Interpréteur Python 
- > l'on utilise l'instruction print(f) dans un Script Python (Exemple d'affichage : `|<--|3|2|6|5|<--|`où 3 est la tête/premier arrivé/premier à sortir, et 5 est la queue/dernier arrivé/dernier à sortir, de la file).
    • Modifier le constructeur __init__() de sorte que l'on puisse maintenant instancier la classe File en lui passant en argument un objet de type list de Python:
# Créer une File `f` initialisée par la liste Python [1,2,3], 
# qui sera représentée dans un Terminal par `|<--|1|2|3|<--|`
f = File([1,2,3])

    • vider() : opérateur qui vide la File. Syntaxe : f.vider() vide f
    • copy() renvoie une copie profonde de la File (la modification de l'une n'entraine pas la modification de l'autre). Syntaxe : f1 = f.copy()
    • renverse() : opérateur qui renverse la File. Syntaxe : f.renverse() renverse f
    • concatene() : opérateur qui concatène deux Files. Syntaxe : f1.concatene(f2) concatène f2 à la queue/au dessous/à droite de f1, et f2 est vidée.
2) Quelle est la complexité en temps des opérations enfile()/enqueue() et defile()/dequeue() ?
Conclusion : Le type list de Python est-il parfaitement adapté pour implémenter une File?
3) Précisément, quelle méthode dans notre classe File est le facteur limitant, c'est-à-dire ralentit nos opérations? et pourquoi ?
Exercice 11 - Implémentation d'une File avec deux Piles
Il est possible d'implémenter une File avec 2 Piles, l'idée est la suivante : on crée une Pile d'Entrée et une Pile de Sortie.
    • quand on veut enfiler, on empile sur la pile d'entrée.
    • quand on veut défiler, on dépile sur la pile de sortie.
    • Lorsque la pile de sortie est vide, on dépile entièrement la pile d'entrée dans la pile de sortie.
1) Implémenter une classe File avec deux Piles :
a) le constructeur __init__() définit une Pile d'Entrée, et une Pile de Sortie
b) disposant des méthodes suivantes :
    • est_vide() -> bool
    • enfile(x : int) -> None ou enqueue(x : int) -> None
    • defile() ou dequeue()
    • get_tete() : renvoie la valeur de la tête, sans la supprimer de la File
    • une méthode magique __repr__() : permet d'afficher la file sous forme agréable lorsque :
->  l'on tape >>> f ou >>> print(f) dans un Interpréteur Python
  	 ->  l'on utilise l'instruction print(f) dans un Script Python (Exemple d'affichage : `|<--|3|2|6|5|<--|`où 3 est la tête/premier arrivé/premier à sortir, et 5 est la queue/dernier arrivé/dernier à sortir, de la file).
    • Modifier le constructeur __init__() de sorte que l'on puisse maintenant instancier la classe File en lui passant en argument un objet de type list de Python:
# Créer une File `f` initialisée par la liste Python [1,2,3], 
# qui sera représentée dans un Terminal par `|<--|1|2|3|<--|`
f = File([1,2,3])
    • vider() : vide la File. Syntaxe : f.vider() vide la file f
    • copy() renvoie une copie profonde de la File (la modification de l'une n'entraine pas la modification de l'autre). Syntaxe : f1 = f.copy()
    • renverse() renverse la File. Syntaxe : f.renverse() renverse f.
    • concatene() : concatène deux Files. Syntaxe : f1.concatene(f2) concatène f2 au dessous/à droite de f1, et f2 est vidée
c) la Classe File doit pouvoir être utilisée par exemple comme suit:
f = File()
f.enfile(5)     # ou f.enqueue(5)
f.enfile(8)
f.defile()      # ou f.dequeue()
2) Quelle est la complexité des méthodes enfile() et defile() ? 
Conclusion : Une Double Pile est-elle parfaitement adaptée pour implémenter une File ?
******    Annales    ******
Exercice 12







Exercice  13





Exercice 14








