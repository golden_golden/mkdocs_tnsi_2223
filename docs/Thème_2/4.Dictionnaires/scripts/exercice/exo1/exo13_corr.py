def ajout(habit:str):
    '''
    ajoute un vêtement dans le dictionnaire dressing

    Entrée
    --------
    habit : type sring

    Sortie
    --------
    None
    '''
    if habit in dressing.keys() :
        dressing[habit] += 1
    else:
        dressing[habit] = 1


