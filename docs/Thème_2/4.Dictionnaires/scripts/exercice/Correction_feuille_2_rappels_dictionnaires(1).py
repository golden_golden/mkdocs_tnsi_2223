########    Exercice 2    ########
exemple_pokemons = {
'Bulbizarre': (70, 7),
'Herbizarre': (100, 13),
'Abo': (200, 7),
'Jungko': (170, 52)}

exemple_pokemons['Goupix'] = (60, 10)

def plus_grand(pokemons: dict) -> tuple:
    '''
    renvoie le nom et la taille du plus grand pokémon

    Entrée
    --------
    pokemons : type dict

    Sortie
    --------
    un tuple renvoyant le nom du plus grand pokemon et sa taille
    '''
    grand = ''
    taille_max = 0
    for nom_pokemon, (taille, poids) in pokemons.items() :
         if  taille > taille_max:
             grand = nom_pokemon
             taille_max = taille
    return (grand, taille_max)    

print(plus_grand(exemple_pokemons))

########    Exercice 3    ########
Capitales = ['France', 'Paris', 'Angleterre', 'Londres', 'Espagne', 'Madrid','Allemagne', 'Berlin']

def liste_en_dico(liste: list) -> dict:
    '''
    transforme une liste de la corme [clé1, valeur1, clé2, valeur2...] en dictionnaire

    Entrée
    --------
    une liste de type list

    Sortie
    --------
    un dictionnaire de type dict
    '''
    dictionnaire = {}
    n = len(liste)
    for i in range(0, n, 2):
        dictionnaire[liste[i]] = liste[i+1]
    return dictionnaire

print(liste_en_dico(Capitales))


########    Exercice 4    ########
dates = {"Alan": (23, 6, 1912),
"Grace": (9, 12, 1906),
"Linus": (28, 12, 1969),
"Guido": (31, 1, 1956),
"Ada": (10, 12, 1815),
"Tim": (8, 6, 1955),
"Dennis": (9, 9, 1941),
"Hedy": (9, 11, 1914),
"Steve": (24, 2, 1955)
}

dates['Margaret'] = (17, 8, 1936)
dates['John'] = (28, 12, 1903)


LISTE_MOIS = ['jan', 'fev', 'mars', 'avril', 'mai', 'juin', 'juil', 'aout', 'sept', 'oct', 'nov', 'dec']


def calendrier(dictionnaire: dict)-> dict:
    '''
    prend en entrée un dictionnaire de types dates et renvoit un dictionnaire 
    ayant pour clé les mois et valeurs les noms des personnes

    Entrée
    --------
    un dictionnaire

    Sortie
    --------
    un dictionnaire
    '''
    dictionnaire_mois = {}
    i = 0 # i est le rang du mois
    for mois in LISTE_MOIS:
        i = i+1
        dictionnaire_mois[mois] = []
        for nom, date in dictionnaire.items():
            if date[1] == i:
                dictionnaire_mois[mois].append(nom)
    return dictionnaire_mois

print(calendrier(dates))


assert calendrier({"Alan": (23, 1, 1912), "Grace": (9, 1, 1906)}) == {'jan': ['Alan','Grace'], 'fev': [], 'mars': [], 'avril': [], 'mai':[], 'juin': [], 'juil': [], 'aout': [], 'sept': [], 'oct': [], 'nov': [], 'dec': []} ; "l 5"
assert calendrier({"Alan": (23, 1, 1912), "Grace": (9, 2, 1906)}) == {'jan': ['Alan'], 'fev': ['Grace'], 'mars': [], 'avril': [], 'mai':[], 'juin': [], 'juil': [], 'aout': [], 'sept': [], 'oct': [], 'nov': [], 'dec': []} ; "lde 5"
assert calendrier({"Alan": (23, 6, 1912), "Grace": (9, 12, 1906), "Linus": (28, 12, 1969), "Guido": (31, 1, 1956), "Ada": (10, 12, 1815), "Tim": (8, 6, 1955), "Dennis": (9, 9, 1941), "Hedy": (9, 11, 1914), "Steve": (24, 2, 1955),"Margaret": (17, 8, 1936), "John": (28, 12, 1903)}) == {'jan': ['Guido'], 'fev': ['Steve'], 'mars': [], 'avril': [], 'mai': [], 'juin': ['Alan', 'Tim'], 'juil': [], 'aout': ['Margaret'], 'sept': ['Dennis'], 'oct': [], 'nov': ['Hedy'], 'dec': ['Grace', 'Linus', 'Ada', 'John']} ; "le nombre de chemise doit être de 5"


def plus_jeune(dictionnaire: dict)-> str:
    '''
    renvoie la personne la plus jeune du dictionnaire
    Entrée
    --------
    un dictionnaire

    Sortie
    --------
    le nom de la personne de type str
    ''' 
    benjamin = ''
    date_max = (1,1,0)

    for nom, date in dictionnaire.items():
        if date[2] > date_max[2]:
            date_max = date
            benjamin = nom
        elif date[2] == date_max[2]:
            if date[1] > date_max[1]:
                date_max = date
                benjamin = nom
            elif date[1] == date_max[1]:
                if date[0] > date_max[0]:
                    date_max = date
                    benjamin = nom
    return benjamin

print(plus_jeune(dates))

dates = {"Alan": (23, 6, 1912), "Grace": (9, 12, 1906), "Linus": (28, 12, 1969), "Guido": (31, 1, 1956),
         "Ada": (10, 12, 1815), "Tim": (8, 6, 1955), "Dennis": (9, 9, 1941), "Hedy": (9, 11, 1914), "Steve": (24, 2, 1955),
         "Margaret": (17, 8, 1936), "John": (28, 12, 1903)}

assert plus_jeune({"Alan": (23, 6, 1912)}) == "Alan" ; "dictionnaire ne comportant qu'un élément"
assert plus_jeune({"Alan": (23, 6, 1912),"Margaut": (23, 6, 2000)}) == "Margaut" ; "deux élément qui ne différent que par l'année"
assert plus_jeune({"Alan": (23, 1, 2000),"Margaut": (23, 2, 2000)}) == "Margaut" ; "deux élément qui ne différent que par le mois"
assert plus_jeune({"Alan": (1, 1, 2000),"Margaut": (10, 1, 2000)}) == "Margaut" ; "deux élément qui ne différent que par le jour"
assert plus_jeune(dates) == "Linus" ; "dictionnaire dates"