def liste_en_dico(liste: list) -> dict:
    '''
    transforme une liste de la corme [clé1, valeur1, clé2, valeur2...] en dictionnaire

    Entrée
    --------
    une liste de type list

    Sortie
    --------
    un dictionnaire de type dict
    '''
    dictionnaire = {}
    n = len(liste)
    for i in range(0, n, 2):
        dictionnaire[liste[i]] = liste[i+1]
    return dictionnaire



