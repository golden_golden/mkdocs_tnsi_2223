LISTE_MOIS = ['jan', 'fev', 'mars', 'avril', 'mai'\
    'juin', 'juil', 'aout', 'sept', 'oct', 'nov', 'dec']


def calendrier(dictionnaire: dict)-> dict:
    '''
    prend en entrée un dictionnaire de types dates et renvoit un dictionnaire 
    ayant pour clé les mois et valeurs les noms des personnes

    Entrée
    --------
    un dictionnaire

    Sortie
    --------
    un dictionnaire
    '''
    dictionnaire_mois = {}
    i = 0 # i est le rang du mois
    for mois in LISTE_MOIS:
        i = i+1
        dictionnaire_mois[mois] = []
        for nom, date in dictionnaire.items():
            if date[1] == i:
                dictionnaire_mois[mois].append(nom)
    return dictionnaire_mois





