dates = {"Alan": (23, 6, 1912), "Grace": (9, 12, 1906), "Linus": (28, 12, 1969), "Guido": (31, 1, 1956),
         "Ada": (10, 12, 1815), "Tim": (8, 6, 1955), "Dennis": (9, 9, 1941), "Hedy": (9, 11, 1914), "Steve": (24, 2, 1955),
         "Margaret": (17, 8, 1936), "John": (28, 12, 1903)}

assert plus_jeune({"Alan": (23, 6, 1912)}) == "Alan" ; "dictionnaire ne comportant qu'un élément"
assert plus_jeune({"Alan": (23, 6, 1912),"Margaut": (23, 6, 2000)}) == "Margaut" ; "deux élément qui ne différent que par l'année"
assert plus_jeune({"Alan": (23, 1, 2000),"Margaut": (23, 2, 2000)}) == "Margaut" ; "deux élément qui ne différent que par le mois"
assert plus_jeune({"Alan": (1, 1, 2000),"Margaut": (10, 1, 2000)}) == "Margaut" ; "deux élément qui ne différent que par le jour"
assert plus_jeune(dates) == "Linus" ; "dictionnaire dates"
