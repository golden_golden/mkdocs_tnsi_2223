
def plus_jeune(dictionnaire: dict)-> str:
    '''
    renvoie la personne la plus jeune du dictionnaire
    Entrée
    --------
    un dictionnaire

    Sortie
    --------
    le nom de la personne de type str
    ''' 
    benjamin = ''
    date_max = (1,1,0)

    for nom, date in dictionnaire.items():
        if date[2] > date_max[2]:
            date_max = date
            benjamin = nom
        elif date[2] == date_max[2]:
            if date[1] > date_max[1]:
                date_max = date
                benjamin = nom
            elif date[1] == date_max[1]:
                if date[0] > date_max[0]:
                    date_max = date
                    benjamin = nom
    return benjamin



