def plus_grand(pokemons: dict) -> tuple:
    '''
    renvoie le nom et la taille du plus grand pokémon

    Entrée
    --------
    pokemons : type dict

    Sortie
    --------
    un tuple renvoyant le nom du plus grand pokemon et sa taille
    '''
    grand = ''
    taille_max = 0
    for nom_pokemon, (taille, poids) in pokemons.items() :
         if  taille > taille_max:
             grand = nom_pokemon
             taille_max = taille
    return (grand, taille_max)    





