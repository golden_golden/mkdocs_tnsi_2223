---
title: "Qu'est ce qu'un Arbre"

---



# Cours sur les arbres



## 1. Qu’est ce qu’un arbre

Les arbres sont des types abstraits très utilisés en informatique, notamment quand on a besoin d’une **structure hiérarchique** des données. Contrairement aux listes, piles, files, les arbre permettent de représenter des relations non séquentielles.


!!! note "Définition : Arbre"
    Un arbre est un ensemble de nœuds, reliés par des arcs.


<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-49-06.png)
</center>






----

Nous rencontrons souvent des schémas qui permettent de mettre en évidence une structure arborescente des données.


<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-49-21.png)
</center>
----









## 2. Un peu de vocabulaire

Un arbre est un ensemble organisé de nœuds, reliés entre eux par des arêtes selon une relations *parent - enfant*.

<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-49-28.png)
</center>



### 2.1 Racine

Lorsqu’un sommet se distingue des autres, on le nomme racine de l’arbre et celui-ci devient alors une arborescence *(par la suite on utilisera le mot arbre pour une arborescence)*.

<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-49-33.png)
</center>

Les 3 arbres ci-dessus représentent la même structure, cependant pour deux d’entre eux, un sommet peut être désigné comme racine.


!!! note "Définition : Racine"
    La racine de l’arbre est l’unique nœud ne possédant pas de parent.



On a l’habitude, lorsqu’on dessine un arbre, de le représenter avec la tête en bas, c’est-à-dire que la racine est tout en haut, et les nœuds fils sont représentés en-dessous du nœud père.





### 2.2 Nœuds *(ou sommets)*

!!! note "Définition : Nœuds"
    Les nœuds sont des éléments possédant des fils (sous-branches).





### 2.3 Feuille

Un nœud qui n’est le point de départ d’aucun arc est appelé feuille.

!!! note "Définition : Feuille"
    Les feuilles (ou *nœuds externes*) sont des éléments ne possédant pas d‘enfant dans l’arbre.





### 2.4 Branche *(arcs ou arêtes)*


!!! note "Définition : Branche"
    Les nœuds sont reliés par des branches.


<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-49-40.png)
</center>




### 2.5 Vocabulaire pour décrire la relation entre les nœuds

Le vocabulaire de lien entre des nœuds reliés entres eux est emprunté à la généalogie : 

<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-49-48.png)
</center>


- 8 est le parent de 9 et de 10
- 6 est un enfant de 4
- 6 et 7 sont des nœuds frères
- 5 est un ancêtre de 9
- 10 est un descendant de 5









-----

### 2.6 Étiquette

Les arbres peuvent être étiquetés. Dans ce cas, chaque nœud possède une étiquette, qui est en quelque sorte le « contenu » du nœud. L'étiquette peut être très simple (un nombre entier, par exemple) ou plus complexe : un objet, une instance d'une structure de données, etc. 


!!! note "Définition :Étiquette *(ou nom du sommet)*"
    L’étiquette d’un nœud représente directement la valeur du nœud ou bien une clé associée à une donnée.


!!! note "Définition : Arbre étiqueté"
    Un arbre dont tous les nœuds sont nommés est dit étiqueté. 


Voici, un arbre étiqueté avec les entiers entre 1 et 10.


<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-49-54.png)
</center>






## 3. Mesures relatives aux nœuds

### 3.1 Chemin d’un nœud


!!! note "Définition : Chemin d’un nœud"
    Le chemin d’un nœud est la liste des nœuds qu’il faut parcourir depuis la racine jusqu’au nœud considéré.


<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-49-59.png)
</center>






### 3.2 La Hauteur *(ou Profondeur)* d’un nœud


!!! note "Définition : Hauteur d’un nœud"
    La hauteur *(ou profondeur ou niveau)* d’un nœud X est égale au nombre d’arêtes qu’il faut parcourir à partir de la racine pour aller jusqu’au nœud X.



- Si l’on suit la définition ci-dessus, on en déduit que la hauteur de la racine est égale à 0 .
- La profondeur d’un nœud est égale à la profondeur de son père plus 1. Si un nœud est à une profondeur p, tous ses fils sont à une profondeur p +1.


<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-50-07.png)
</center>



Dans l’exemple ci-dessous la hauteur du nœud 9 est de 3 et celle du nœud 7 est de 2. Tous les nœuds d’un arbre, de même profondeur sont au même niveau, ainsi les nœud 9 et 10 sont au même niveau car ils ont tous les deux une hauteur de 3.


!!! note "Remarque importante"
    La définition de la hauteur d’un nœud varie en fonction des auteurs. Pour certains la racine a une hauteur de 1.





### 3.3 Degré d’un nœud

!!! note "Définition : Degré d’un nœud"
    Le degré d’un nœud est égal au nombre de ses descendants *(enfants)*.

<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-50-13.png)
</center>



## 4. Mesures relatives à un arbre
### 4.1 Taille d’un arbre


!!! note "Définition : Taille d’un arbre"
    La taille d’un arbre est égale au nombre de nœuds de l’arbre.



Voici, un arbre de taille 10.


<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-50-23.png)
</center>

### 4.2 Hauteur d’un arbre

!!! note "Définition : Hauteur d’un arbre"
    La hauteur *(profondeur)* de l’arbre est la hauteur *(profondeur)* du nœud le plus haut *(profond)*.

<center>
![](../images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-50-31.png)
</center>


Dans notre exemple, le nœud le plus haut possède une hauteur de  3, donc l’arbre a une hauteur de 3.



!!! note "Remarque importante"
    Il n’existe pas de définition universelle pour la hauteur d’un arbre et la profondeur d’un nœud dans un arbre.

    Dans certains cas la profondeur des nœuds est comptée à partir de 1, ainsi L’arbre vide a une hauteur 0, et l’arbre réduit à une racine étiqueté a une hauteur 1.

    Parfois également, la taille d’un arbre ne tient pas compte des feuilles !





### 4.3 Degré d’un arbre

!!! note "Définition : Degré d’un arbre"
    Le degré d’un arbre est égal au plus grand des degrés de ses nœuds.

<center>
![](/images/Capture%20d%E2%80%99%C3%A9cran%20du%202022-12-28%2018-50-39.png)
</center>

