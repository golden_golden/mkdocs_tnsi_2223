---
title : "Exemples d'usages"

---


# Cours sur les arbres


## 4. Quelques exemples d’utilisation des arbres

Les arbres sont très utilisés en informatique, d’une part parce que les informations sont souvent hiérarchisées et peuvent se représenter naturellement sous une forme arborescente, et d’autre part, parce que les structures de données arborescentes permettent de stocker des données volumineuses de façon que leur accès soit efficace.



### 4.1 Arborescence de fichiers

L’organisation des fichiers et des dossiers dans un système de fichiers d‘un OS peut également être représenté par un arbre étiqueté avec les noms des fichiers et dossiers :

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210109191207049.png" alt="image-20210109191207049" style="zoom: 67%;" />



<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210109180217225.png" alt="image-20210109180217225" style="zoom:50%;" />


### 4.2 Arbre représentant le DOM d'un document html



### 4.2 Arbre syntaxique

Un arbre syntaxique représente l’analyse d’une phrase à partir de règles (la grammaire).

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210109175719620.png" alt="image-20210109175719620" style="zoom:50%;" />





### 4.3 Arbre lexicographique

Un arbre lexicographique, ou arbre en parties communes, ou dictionnaire, représente un ensemble de mots. Les préfixes communs à plusieurs mots apparaissent une seule fois dans l’arbre.

![image-20210109173955595](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210109173955595.png)





### 4.4 Évaluation d’expression arithmétique

**expressions arithmétiques** : elles peuvent être représentées par des arbres étiquetés par des **opérateurs**, des **constantes** et des **variables**. La structure de l’arbre rend compte de la priorité des opérateurs et rend inutile tout parenthésage.


On peut également représenter les expressions arithmétiques par des arbres étiquetés par des opérateurs,des constantes et des variables. La structure de l’arbre rend compte de la priorité des opérateurs et rend inutile tout parenthésage. Pour l’expression : (y 2 − t)(75 + z) cela donne :

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210109174037185.png" alt="image-20210109174037185" style="zoom:50%;" />





### 4.5 Compression par codage de Huffman

Le codage de Huffman est un algorithme de compression de données sans perte. Le codage de Huffman utilise un code à longueur variable pour représenter un symbole de la source (par exemple un caractère dans un fichier). Le code est déterminé à partir d'une estimation des probabilités d'apparition des symboles de source, un code court étant associé aux symboles de source les plus fréquents. Un code de Huffman est optimal au sens de la plus courte longueur pour un codage par symbole, et une distribution de probabilité connue. Des méthodes plus complexes réalisant une modélisation probabiliste de la source permettent d'obtenir de meilleurs ratios de compression. Il a été inventé par David Albert Huffman, et publié en 1952. Le principe du codage de Huffman repose sur la création d'une structure d'arbre composée de nœuds. 

Source : https://fr.wikipedia.org/wiki/Codage_de_Huffman




![image-20210109183240705](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210109183240705.png)



### 4.6 Théorie des jeux

Certaines stratégies nécessitent l’exploration (partielle) d’arbres de jeu (voir morpion ou puissance 4)
