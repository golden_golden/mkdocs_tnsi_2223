# Les arbres binaires de recherches ABR





### 1. Qu’est ce qu’un arbre binaire de recherche

**Arbre  binaire  de  recherche  (ABR)**

> Un  arbre  binaire  de  recherche  (ABR)  est  une  structure  de  donnée  composée  de  nœuds. Chaque nœud a au plus 2 enfants ordonnés d’une manière particulière :
>
> - Les enfants à gauche d’un nœud ont des valeurs inférieures à lui.
> - Les enfants à droite d’un nœud ont des valeurs supérieures à lui.
>
>  Et cela doit être vrai pour chaque nœud de l’arbre

-----

> Un arbre binaire de recherche est un arbre binaire dont l’ensemble des nœuds vérifient les propriétés suivantes : 
>
> - Les valeurs du sous-arbre gauche sont inférieures ou égales à la valeur du nœud. 
>
> - Les valeurs du sous-arbre droit sont strictement supérieures à la valeur du nœud.

![image-20210120195549758](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120195549758.png)

### 2. TAD ABR

Le TAD Arbre Binaire de Recherche (ABR) est une extension de l’ArbreBinaire. Il possède donc les mêmes opérations/préconditions/axiomes auxquels on ajoute :

**Opérations **

- ajouter : ABR[E] × E → ABR[E] 
- retirer : ABR[E] × E → ABR[E] 
- rechercher : ABR[E] × E → Booléen



Détaillons la mise en œuvre de ces différentes opérations. 

**Remarque :** La recherche et l’ajout d’une clé dans un arbre sont explicitement au programme. Ce n’est pas le cas du retrait d’une clé d’un arbre, qui est effectivement plus complexe.



### 3. Les opérations sur les ABR

#### 3.1 Insertion dans un ABR

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120143259208.png" alt="image-20210120143259208" style="zoom: 80%;" />



----

L’ajout d’un élément dans un ABR se fait au niveau des feuilles.

![image-20210120195837747](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120195837747.png)

Nous allons donc naviguer dans l’arbre, en partant de la racine jusqu’à trouver un emplacement qui permettra de conserver la caractérisation d’un ABR. Nous obtenons alors l’algorithme suivant : 



> Ajouter(A,x) : 
> ​	Si x > racine(A) : 
> ​		Si estVide(sad(A)) alors sad(A) = assembler(x,creerArbre,creerArbre) 
> ​		Sinon Ajouter(sad(A),x) 
> ​	Sinon 
> ​		Si estVide(sag(A)) alors sag(A) = assembler(x,creerArbre, creerArbre) 
> ​		Sinon Ajouter(sag(A),x)



#### 3.2 Rechercher dans un ABR

**Algorithme**

La recherche dans un arbre binaire de recherche est équivalent à une recherche par dichotomie dans une liste triée. 



> Recherche(A,x) : 
> ​	Si estVide(A) alors faux 
> ​	Si x = racine(A) alors Vrai 
> ​	Si x < racine(A) alors Recherche(sag(A),x) 
> ​	Sinon Recherche(sad(A),x)



**Performances**

> En moyenne, la complexité de la recherche dans un ABR est en Θ(lnn).



**Remarque :** Rappelons que rechercher un élément dans une liste quelconque a pour complexité Θ(n). La recherche dans un ABR est donc plus rapide. 



**Attention !** Ces performances dépendent de l’équilibre de l’ABR.

Cet arbre n’est pas du tout équilibré, chaque nœud n’a qu’un fils droit. La recherche dans cet arbre s’apparente donc à une recherche dans une liste, est à pour complexité Θ(n) ! 

**Pour aller plus loin :** Il existe des rotations qui permettent d’équilibrer un arbre



![image-20210120200407892](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120200407892.png)



##### 3.2.1 Rechercher une valeur

##### 3.2.2 Rechercher un extrémum



#### 3.3 Supprimer un nœud dans un ABR

Supprimer une valeur d’un arbre binaire de recherche est beaucoup plus technique et nous nous contenterons d’observer, sur des exemples, les différents cas de figure. 

**Cas de figure n°1 : La valeur à supprimer est une feuille**

![image-20210120200629373](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120200629373.png)



**Cas de figure n°2 : Le nœud possède un seul fils**

![image-20210120200914532](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120200914532.png)

Je supprime alors le nœud et je fais "remonter" son unique fils.



**Cas de figure n°3 : Le nœud possède deux fils**

![image-20210120200934353](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120200934353.png)

Je vais rechercher le nœud qui a la valeur maximale parmi les nœuds de valeur inférieure à la valeur du nœud à supprimer. Je remplace le nœud à supprimer par ce nouveau nœud.



### 4. Implémentation d’un ABR

```python
classNoeud:
    #Le constructeur
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right
        self.parent =None
        
     def__str__(self):
        return str(self.value)
    
    def estFeuille(self):
        if not self.left and not self.right:
            return True
        else:
            return False
```



**Insertion**

```python
def insert(self, valeur):
	if valeur <self.value:
		if self.left is None:
			self.left = Noeud(valeur)
			self.left.parent = self
		else:
			self.left.insert(valeur)
	elif valeur >self.value:
		if self.right is None:
			self.right = Noeud(valeur)
			self.right.parent =self
		else:
			self.right.insert(valeur)
```

-----

![image-20210120201104968](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120201104968.png)

![image-20210120201120266](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210120201120266.png)