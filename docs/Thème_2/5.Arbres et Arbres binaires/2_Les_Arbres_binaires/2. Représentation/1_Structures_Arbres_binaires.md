---
title : "Structure de données arbres binaires"

---



# Cours sur les arbres binaires


## 5. Représentation d’un arbre binaire

###  5.1. Représentation 1 liste 

-----

La représentation d’un arbre binaire par un tableau repose sur la relation suivante entre les indices d’un père et de ses fils :

 ( if ilsGauche = 2× i père +1 

if ilsDroit = 2×(i père +1)

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110191144473.png" alt="image-20210110191144473" style="zoom:50%;" />

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110191331336.png" alt="image-20210110191331336" style="zoom:50%;" />



----



![image-20210110165415845](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110165415845.png)

![image-20210110165443089](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110165443089.png)

-----

Les arbres binaires peuvent aussi être rangés dans des tableaux, et si l'arbre est un arbre binaire complet, cette méthode ne gaspille pas de place, et la donnée structurée résultante est appelée un tas. 



**Avantages : **

- Implémentation facile à réaliser.

- Possibilité d'accès direct à un nœud de l'arbre (un seul accès en mémoire). 

  

**Inconvénients :** 

- Conçu pour contenir un nombre fixe de nœuds.
- Si l'arbre est profond mais contient peu de nœuds, il se produit un gaspillage important de mémoire.

------



#### A. Implémentation en python

![image-20210110165511126](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110165511126.png)

#### B. Implémentation des mesures



#### C. Implémentation des différents parcours



###  5.2. Représentation 1 liste de liste

-----



<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110204658321.png" alt="image-20210110204658321" style="zoom:50%;" />

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110205047370.png" alt="image-20210110205047370" style="zoom:50%;" />

------





![image-20210110165546626](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110165546626.png)

#### A. Implémentation en python

![image-20210110165606304](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110165606304.png)

#### B. Implémentation des mesures

![image-20210110165642016](C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110165642016.png)

#### C. Implémentation des différents parcours

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110205137892.png" alt="image-20210110205137892" style="zoom:67%;" />



###  5.3. Représentation 2 poo avec une seule classe

------

https://info.blaisepascal.fr/nsi-arbres

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110190522364.png" alt="image-20210110190522364" style="zoom:67%;" />

-----



Nous allons créer une classe Nœud dont les attributs d’instances seront : 

- Le nom (ou valeur) de la racine.
- on fils gauche (vide par défaut).
- Son fils droit (vide par défaut). 

De plus on rajoute une méthode spécifique, qui permet d’afficher la racine du nœud avec la fonction print() 

Ci-dessous la classe Nœud et la représentation de l’arbre.(La racine possède deux fils qui sont eux mêmes des nœuds possédant deux fils qui.....)



#### A. Implémentation en python

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110170445277.png" alt="image-20210110170445277" style="zoom:67%;" />

#### B. Implémentation des mesures

Écrire une méthode est Feuille qui renvoie vrai si le nœud est une feuille et faux sinon.

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110170602954.png" alt="image-20210110170602954" style="zoom:67%;" />



#### C. Implémentation des différents parcours

<img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20210110170650246.png" alt="image-20210110170650246" style="zoom:67%;" />



###  5.4. Représentation 3 poo avec deux classe

#### A. Implémentation en python

Python ne propose pas de façon native l’implémentation des arbres binaires.

Il est cependant très aisé de définir un **arbre binaire** en définissant des classes Arbre et  Noeud :

```python
class Arbre:
    def __init__(self, racine = None):
        self.racine = racine # type : Noeud
        
class Noeud:
    def __init__(self, v, g = None, d = None):
        self.g = g # type Noeud
        self.d = d # type Noeud
        self.v = v
```



Et on implémente un arbre ainsi :

```python
arbre = Arbre(Noeud("A"))
```





#### B. Implémentation des mesures

#### C. Implémentation des différents parcours




