---
title : "TAD"

---


----
À reprendre

----




## 4. TAD d’un arbre binaire

**Opérations**

- creerArbreVide : → ArbreBinaire[E] 
- estVide : ArbreBinaire[E] → Booléen 
- racine : ArbreBinaire[E] → E 
- sag, sad : ArbreBinaire[E] → ArbreBinaire[E] 
- assembler : E × ArbreBinaire[E] × ArbreBinaire[E] → ArbreBinaire[E]



**Préconditions **

Soit a un ArbreBinaire. racine(a), sag(a) et sad(a) si et seulement si non estVide(a)



**Axiomes** 

Soient a, g,d des ArbreBinaire[E], r un E.

- estVide(arbreVide)= Vrai 
- estVide(assembler(r, g,d)) = Faux 
- racine(assembler(r, g,d)) = r 
- sag(assembler(r, g,d)) = g 
- sad(assembler(r, g,d)) = d



