# Cours sur les arbres binaires

## 1. Qu’est ce qu’un arbre binaire



!!! note "Arbre binaire"

    Un arbre binaire est un type spécifique d'arbre qui se caractérise par le fait que chaque *nœud* ne peut avoir qu'au plus deux enfants. Un arbre binaire est donc un arbre de degré 2 *(dont les nœuds sont de degré 2 au plus)*.



!!! example "Exemple d'arbre binaire"
    L'arbre binaire de l'expression $a×b + c-(d+e)$ est un arbre binaire :

    <br/>

    <center>
    <img src="../images/arbre_binaire.png" alt="exemple arbre gauche et arbre droit" />
    </center>


<br/>

**Vocabulaire :**


Les descendants (enfants) d'un noeud sont lus de gauche à droite et sont appelés respectivement *fils gauche (descendant gauche)* et *fils droit (descendant droit)* de ce noeud. Attention, ils ne sont pas intervertibles !

<br/>

<center>
<img src="../images/arbre_gd.png" alt="exemple arbre gauche et arbre droit" />
</center>




<br/>

!!! note "Remarque :"

    Les arbres binaires forment une structure de données qui peut se définir de façon récursive. 

    Un arbre binaire est : 

    - Soit vide. 

    - Soit composé d’une racine portant une étiquette (clé) et d’une paire d’arbres binaires, appelés sous-arbre gauche et sous-arbre droit.



## 2. Caractéristique des arbres binaires

### 2.1. Un peu de vocabulaire descriptif
Il est possible d’avoir des arbres binaires de même taille mais de « forme » très différentes.


!!! note "Arbre parfait"
    C'est un arbre binaire dont tous les nœuds de chaque niveau sont présents sauf éventuellement au dernier niveau où il peut manquer des nœuds (nœuds terminaux  = feuilles), dans ce cas l'arbre parfait est un arbre binaire incomplet et les feuilles du dernier niveau doivent être regroupées à partir de la gauche de l'arbre.


??? example "Arbre parfait complet"
    <center>
    <img src="../images/arbre_parfait_complet.png" alt="exemple arbre_parfait_complet" style="zoom:50%;" />
    </center>



??? example "Arbre parfait incomplet"
    <center>
    <img src="../images/arbre_parfait_incomplet.png" alt="exemple arbre_parfait_incomplet" style="zoom:50%;" />
    </center>

??? example "Arbre non parfait"
    <center>
    <img src="../images/arbre_non_parfait.png" alt="exemple arbre_non_parfait" style="zoom:50%;" />
    </center>

??? example "Autre Arbre non parfait"
    <center>
    <img src="../images/arbre_non_parfait2.png" alt="exemple arbre_non_parfait2" style="zoom:50%;" />
    </center>


<br/>

!!! note "Arbre équilibré"
    Un arbre est qualifié d’équilibré si toutes ses feuilles sont à la même profondeur.

<center>
<img src="./images/arbre_binaire.png" alt="exemple arbre equilibré" style="zoom:50%;" />
</center>


<br/>

!!! note "Arbre filiforme *(ou dégénéré, on parle aussi de peigne)*"
    Un arbre est qualifié de filiforme si tous ses nœuds possèdent un unique fils. 

<center>
<img src="../images/arbre_filiforme.png" alt="exemple arbre gauche et arbre droit" style="zoom:50%;" />
</center>

On pourra aussi dire que l’arbre filiforme est déséquilibré alors que l’arbre complet est équilibré.



### 2.2. Hauteur d’un arbre binaire
#### A. Hauteur d’un arbre binaire filiforme

La hauteur d’un arbre filiforme de taille *n* est égale à :

$$
n−1
$$


#### B. Hauteur d’un arbre binaire complet

La hauteur d’un arbre complet de taille *n* est égale à :

$$
log_2(n)
$$


!!! example "Exemples des arbres précédents :"

    $$
    log_2(7)=2,8
    $$

    La hauteur de l’arbre complet de taille 7 est égale à 2.


#### C. Encadrement de la hauteur d’un arbre binaire
!!! note "Hauteur d'un arbre binaire"
    Un arbre filiforme et un arbre complet étant deux cas extrêmes, on peut encadrer la hauteur *h* d’un arbre binaire quelconque de taille *n* par :

    $$
    log_2(n)≤h≤n−1
    $$


### 2.3. Encadrement de la taille d’un arbre binaire

!!! note "Taille d'un arbre binaire"
    De la même manière, on peut encadrer la taille *n* d’un arbre binaire que l’on peut obtenir pour une hauteur *h* donnée :

    $$
    h+1≤n≤2^{h+1}−1
    $$


!!! example "Exemple"
    La taille d’un arbre binaire de hauteur 3 est comprise entre :

    $$
    3+1≤n≤2^{3+1}−1
    $$
    
    $$
    4≤n≤15
    $$

<center>
<img src="./images/arbre_binaire.png" alt="Image  taille d'un arbre binaire" style="zoom:50%;" />
</center>

## 3. Parcours d’un arbre binaires


Parcourir un arbre, c’est ordonner la liste des noeuds et feuilles de celui-ci en vue d’effectuer un certain traitement (par exemple imprimer la liste des étiquettes de cet arbre). On distingue au moins quatre types de parcours :

- parcours en largeur (ou parcours hiérarchique)
- parcours en profondeur :
  
    - Le parcours préfixe 
    - Le parcours suffixe 
    - Le parcours infixe 





###  3.1. Parcours en largeur

Le parcours en largeur correspond à un parcours par niveau de noeuds de l'arbre et en général de la gauche vers la droite pour une profondeur donnée. Un niveau est un ensemble de noeuds ou de feuilles situés à la même profondeur.
Ainsi, si l'arbre de l'exemple est utilisé, le parcours sera A - B - C - D - E - F - G. 

<br/>

<center>
<img src="../images/parcour_largeur.jpg" alt="image parcour" />
</center>



###  3.2. Parcours en profondeurs

#### A. Ballade autour de l'arbre
On se balade autour de l’arbre en suivant les pointillés dans l’ordre des numéros indiqués :

<center>
<img src="../images/arbre_parcour_profondeur.jpg" alt="image parcour en largeur" style="zoom:50%;" />
</center>

A partir de ce contour, on définit trois parcours des sommets de l’arbre :

- l’ordre préfixe : on liste chaque sommet la première fois qu’on le rencontre dans la balade.
- l’ordre suffixe : on liste chaque sommet la dernière fois qu’on le rencontre.
- l’ordre infixe : on liste chaque sommet ayant un fils gauche la seconde fois qu’on le voit et chaque sommet sans fils gauche la première fois qu’on le voit.


#### B. Le parcours préfixe
Le parcours préfixe consiste à parcourir l’arbre suivant l’ordre : inspection -→ fils g -→ fils d . On descend de la racine vers une feuille, puis on remonte à la racine, plusieurs fois, afin de passer par tous les nœuds de l’arbre.

<br/>

<center>
<img src="../images/parcour_largeur.jpg" alt="image parcour" />
</center>

Dans l’exemple ci-dessus, le parcour préfixe correspond au parcours de l’arbre suivant l’ordre : **A - B - D - E - C - F - G**



#### C. Le parcours suffixe
Dans ce mode de parcours, le nœud courant est traité après le traitement des nœuds gauche et droit. Soit parcourir l’arbre suivant l’ordre : fils g. −→ fils d. −→ Inspection.

<br/>

<center>
<img src="../images/parcour_largeur.jpg" alt="image parcour" />
</center>

Ainsi, le parcour suffixe correspond au parcours de l’arbre suivant l’ordre : **D, E, B, F, G, C, A.**




#### D. Le parcours infixe

Le parcours infixe (ou parcours symétrique) consiste à parcourir l’arbre suivant l’ordre : fils g. −→ Inspection −→ fils d. Le noeud courant est traité entre le traitement des fils gauche et droit.

<center>
<img src="../images/parcour_largeur.jpg" alt="image parcour" />
</center>

Dans l’exemple précédent, le parcours infixe sera : **D - B - E - A - F - C - G** 


### 4. Vidéos
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Wz5KTtG66mg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/wHWHXP-uSTA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/hzeMclzpgfk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>