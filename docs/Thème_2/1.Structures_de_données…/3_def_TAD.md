---
title: "Types Abstraits de Données (TAD)"

---


# Types Abstraits de Données (TAD)

## 3. Types Abstraits de Données (TDA ou TAD)
### 3.1 Qu'est ce qu'un TDA

!!! note "Définition : Type abstrait de donnée"
    Un type abstrait ou une structure de données abstraite (en anglais, *abstract data type* ou *ADT*)  est une spécification mathématique d’un ensemble de données et de l’ensemble des opérations qu'on peut effectuer sur elles.



Les types abstraits, correspondent à un cahier des charge, se sont des entités purement théoriques utilisés principalement pour simplifier la description des algorithmes.


### 3.2 Description d'un TDA
Un TDA est une entité constituée d’une signature, représenté par quatre champs et d’un ensemble d’axiomes.

#### A. La signature

!!! note "Type abstrait :"
    contient le nom du type que l'on est en train de décrire et précise éventuellement si celui-ci n'est pas une extension d'un autre type abstrait.
!!! note "Utilise :"
    contient les types abstraits que l'on va utiliser dans celui que l'on est en train de décrire.

!!! note "Opérations :"
    contient le prototypage de toutes les opérations, c’est à dire une description des opérations par leur nom, leurs arguments et leur retour.

    <br/>

    Les opérations sont divisées en plusieurs types :

    - *les constructeurs* : permettent de créer un objet du type que l'on est en train de définir ;
    - *les transformateurs* : permettent de modifier les objets et leur contenu ;
    - *les observateurs* : fonction donnant des informations sur l'état de l'objet

!!! note "Préconditions :"
    contient les conditions à respecter sur les arguments des opérations pour que celles-ci puissent avoir un comportement normal. On peut parler ici d'ensemble de définition des opérations.


#### B. Les axiomes

!!! note "Les axiomes :"
    contient une série d’axiomes pour décrire le comportement de chaque opération d'un type abstrait. Chaque axiome est une proposition logique vraie.


Lorsqu’un TDA est défini, la partie axiomes pose plusieurs problèmes fondamentaux :  la complétude (tous les comportements sont ils modélisés) et la consistance des axiomes (pas de contradiction entre les axiomes).

<br/><br/>

??? note "Remarque :"
    L'approche TAD est à l'origine de l'approche objets.
