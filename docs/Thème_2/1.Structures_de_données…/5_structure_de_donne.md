---
title: "Implémentation"

---


# Types Abstraits de Données (TAD)



## 5. Implémentation
### 5.1 Structure de données construite (TDC)
Pour qu’un programme puisse être exécuter sur une machine, il est nécessaire d’implémenter les algorithmes et les structures de données qu’ils utilisent. On parle de représentation des types abstraits de données ou bien encore de structure de données construite (TDC).


!!! def "Structure de Données"
    Une <bred>Structure de Données</bred> est en charge d'**organiser**, de **stocker** et de **gérer** les données.

    Une Structure de données est une **implémentation** d'un type de abstrait de données, donc en particulier elle est en charge de :

    * la **Représentation concrète** des données en mémoire
    * l'**implémentation des opérations/primitives** du type abstrait de données

<br/>

Une **structure de données** correspond au point de vue de l'implémenteur. Un type de données abstrait peut donc être implémenté de plusieurs manières distinctes en des structures de données différentes.


??? exp "en Python"
    En Python, on pourra penser par exemple aux structures de données (appelés types de données en Python) : `list` et `dict`.


    :warning: En Python, la structure de données `list` est un faux-ami : il implémente le type abstrait de données **Tableau Dynamique**.


Nous étudierons plusieurs représentation (structures de donnée concrète) correspondant aux types abstraits de données du programme.




### 5.2 Opérations de base d'une Structure de Données : `CRUD`

Classiquement, une Structure de Données fournit normalement au minimum les opérations de base suivantes, usuellement notées <env>**CRUD**</env> (<b>C</b>reate, <b>R</b>ead, <b>U</b>pdate, <b>D</b>elete :gb:) :

* <b>C</b>reate / Insertion : Créer une donnée éventuellement vide, en utilisant ce qu'on appelle un constructeur.
* <b>R</b>ead / Lecture : Accéder à un élément.
* <b>U</b>pdate / Modification : Ajouter, retirer un élément, en précisant comment il s'intègre dans l'organisation globale de la collection.
* <b>D</b>elete / Suppression

<br/>

Éventuellement, on trouvera également des opérations plus avancées comme : *rechercher un élément, trier la collection, fusionner deux collections…*

<br/>

Chaque opération doit être bien spécifiée (entrée, sorties, précondition) : c'est ce que l'on fait dans l'interface.




### 5.3 Complexité des Structures de données

C'est la Structure de Données, et non pas le type de Données Abstrait, qui détermine la complexité (en temps mais aussi en espace) de chacune des opérations de la Structure.


On cherchera évidemment l'implémentation la plus efficace, en temps et en espace, pour le traitement considéré.

<br/>


??? exp "Complexité des Types de Python"

    $n$ désigne la taille du conteneur courant.
    $k$ désigne soit la taille d'un paramètre, soit le nombre d'éléments dans ce paramètre.

    <center>

    |Opérations sur les `list`|Cas Moyen|Pire des Cas (amorti)|
    |:-:|:-:|:-:|
    |Copy|$O(n)$|$O(n)$|
    |Append|$O(1)$|$O(1)$|
    |Pop (le dernier)|$O(1)$|$O(1)$|
    |Pop (au milieu)|$O(n)$|$O(n)$|
    |Insert|$O(n)$|$O(n)$|
    |Lire item|$O(1)$|$O(1)$|
    |Modifier item|$O(1)$|$O(1)$|
    |Supprimer item|$O(n)$|$O(n)$|
    |Itération|$O(n)$|$O(n)$|
    |Lire Slice/Tranche|$O(k)$|$O(k)$|
    |Supprimer Slice/Tranche|$O(n)$|$O(n)$|
    |Modifier Slice/Tranche|$O(k+n)$|$O(k+n)$|
    |Extend|$O(k)$|$O(k)$|
    |Sort|$O(n.log(n))$|$O(n.log(n))$|
    |Multiplication|$O(nk)$|$O(nk)$|
    |$x$ in $lst$|$O(n)$|$\,$|
    |min($lst$), max($lst$)|$O(n)$|$\,$|
    |Lire longueur|$O(1)$|$O(1)$|

    <br/>

    |Opérations sur les `dict`|Cas Moyen|Pire des Cas (amorti)|
    |:-:|:-:|:-:|
    |$k$ in $d$|<green>$O(1)$</green>|$O(n)$|
    |Copy|$O(n)$|$O(n)$|
    |Lire item|$O(1)$|<red>$O(n)$</red>|
    |Modifier item|$O(1)$|<red>$O(n)$</red>|
    |Supprimer item|<green>$O(1)$</green>|$O(n)$|
    |Itération|$O(n)$|$O(n)$|

    </center>

    <env>**ALLER PLUS LOIN**</env> [La page suivante du wiki de Python,](https://wiki.python.org/moin/TimeComplexity) liste de manière plus complète, les complexités de certains autres Types de données Python.


<br/>

**Accéder à / Rechercher une donnée** dans la structure de données est un problème totalement indépendant du reste des opérations (e.g. CRUD): C'est pourquoi on étudie / compte séparément ce que l'on appelle la **Complexité d'Accès**.


