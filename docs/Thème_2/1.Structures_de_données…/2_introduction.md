---
title: "Introduction"

---


# Types Abstraits de Données (TAD)



## 2. Introduction

### 2.1 Pourquoi les Type abstraits de données

Lors de la conception d'un algorithme compliqué, ils est intéressant d'appliquer une démarche descendante. La conception de l’algorithme doit être indépendant de l'implémentation. *La conception est alors plus simple, puisqu’on n’a pas à prendre en compte des détails de programmation ; la conception sera faite une fois pour toutes, car ainsi l'algorithme sera facilement portable d'un language à un autre…*

<br/>

Pour y parvenir, il est nécessaire de considérées les données de manière abstraites en leurs attribuant une spécification. On parle alors de type abstrait de données (TAD).

<br/>

Cette approche pose le problème de la définition d’un type abstrait. Cette définition doit être précise et non ambiguë. Globalement, cela consiste à donner une notation pour le décrire, ainsi que décrire l’ensemble des opérations qu’on peut appliquer et les propriétés de ces opérations.

<br/>

L'implémentation de l'algorithme, dans un langage de programmation donné, nécessitera alors la réalisation d'une représentation concrète des types et des opérations, on parle alors de structure de donnée.

<br/>

Dans la suite de ce cours, nous allons voir comment définir un type abstrait de donnée, puis nous étudierons quels types abstrait couramment rencontré. Pour chacun de ces types abstraits, nous réaliserons des implémentations de représentation concrète en Python.





### 2.2 Différence entre type abstrait de données (TAD) et implémentation

Nous avons vu que l'on pouvait classiquement différencier deux niveaux de représentation des données :


#### A. niveau ***Abstrait***

*Le niveau Abstrait, ou niveau Logique, soit le Type Abstrait de Données (TAD).*

* Ce niveau regroupe tout un ensemble de *routines* (procédures ou fonctions) abstraites, définissant certaines *opérations* de manipulation des données. 
* L'ensemble de toutes ces routines est appelée l'<red><b>*INTERFACE*</b></red> (abstraite) de la structure de données abstraites.
     
!!! def "Interface"
    <bred>L'interface</bred> décrit :

    - **Quelles sont les valeurs possibles ?**
    - **Quelles sont les opérations avec lesquels on peut la manipuler ?**
    - **Quel est le résultat qu'ils produisent ?**

* Utiliser un objet ou une fonctionnalité par son interface abstraite permet de raisonner indépendamment des détails de son implémentation, ce qui est une tâche qui revient à la *Structure de Données*.



<br/>


#### B. niveau <b>IMPLÉMENTATION</b>.
*Le niveau <b>implémemtation</b>, soit la Structure de Données.*


C'est la **Structure de données** qui est en charge de l'implémentation du type abstrait de données, en particulier :

* de la représentation concrète des données en mémoire
* de l'implémentation des opérations de manipulation de données.(contient **le code et la manière précis** permettant de réaliser l'opération spécifiée par l'interface.)

<br/>

En résumé, <env>IMPLÉMENTATION = Comment ils fonctionnent ?</env>

<br/>

!!! note "Remarque"
    Il n'est pas nécessaire de connaître l'implémentation, pour manipuler une structure de données. (cf API)

