---
title: "Classification des TAD"

---


# Types Abstraits de Données (TAD)


## 4. Classification des Types Abstraits de Données (TAD)

Les structures de données abstraites que nous étudierons cette année peuvent être classées selon la nature de l'organisation de la collection de données :


### 4.1 Les structures linéaires / séquentielles
Il y a un premier élément et un dernier ; chaque élément a un prédécesseur (sauf le premier) et un successeur (sauf le dernier).

- *Les listes*
- *Les piles* 
- *Les files*
- *(Files de Priorité, etc…)*




  
### 4.2 Structures associatives :
les éléments sont repérés par une clé ; ils n'ont pas de lien entre eux.

- Tableaux (Statiques vs Dynamiques)
- Dictionnaires, ou Tableaux Associatifs, ou Table d'Association
- ensembles.




### 4.3 Les structures structures Récursives / Hiérarchiques / Arborescentes :
Il y a un (parfois plusieurs) élément racine ; chaque élément dépend d'un antécédent (sauf la/les racine/s) et a des descendants (sauf les feuilles).

- *Arbres planaires généraux*
- *Arbres binaires*



### 4.4 Les structures relationnelles
Chaque élément est en relation directe avec des voisins, ou bien a des prédécesseurs et des successeurs.

- *Les graphes*


!!! note ""
    Ces structures de données sont parfois implémentées nativement dans les langages de programmation comme type de données mais ce n'est pas toujours le cas. En Python, les listes (type list), les dictionnaires (type dict) et les ensembles (type set) le sont mais pas les autres.

