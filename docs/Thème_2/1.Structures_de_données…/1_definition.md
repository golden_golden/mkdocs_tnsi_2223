---
title: "Définition et rappels"

---



## 1. Définition et rappels

!!! note "Type de données"
    Un type de données, ou type, définit le genre du contenu d’une donnée et les opérations possibles sur la variable correspondante.
   
    Les types les plus communs : entier (Int), réel (float, double), chaîne de caractère (char), booléen (boolean),…



!!! note "Structures de données"
    Une structure de données est une organisation d'une collection de données en vue de leur exploitation efficace (accès, modification…). Elle regroupe des données à gérer et un ensemble d'opérations qu'on peut leur appliquer.

    Différents types de structures de données existent pour répondre à des problèmes très précis.



!!! note "Interface (ou Type abstrait de données)"
    Vue « logique » de la structure de données. Elle spécifie la nature des données ainsi que l'ensemble des opérations permises sur la structure.


!!! note "Implémentation"
    Vue « physique » de la structure de données. Il s'agit de la programmation effective des opérations définies dans l'interface, en utilisant des types de données existants.


!!! note "Remarque importante"
    L'interface est la partie visible pour qui veut utiliser ce type de données. Elle précise comment utiliser la structure de données sans se préoccuper de la façon dont les choses ont été programmées (son implémentation).