- cours de permière [https://dav74.github.io/site_nsi_prem/c16c/](https://dav74.github.io/site_nsi_prem/c16c/) [dépot](https://github.com/dav74/site_nsi_prem)
- 
- cours de terminale [https://dav74.github.io/site_nsi_term/c11c/](https://dav74.github.io/site_nsi_term/c11c/) [dépot](https://github.com/dav74/site_nsi_term)


Autres cours terminale (dépot existant cf lien nsi)):
- [https://eskool.gitlab.io/tnsi/reseaux/protocoles/ospf/](https://eskool.gitlab.io/tnsi/reseaux/protocoles/ospf/)
- [https://glassus.github.io/terminale_nsi/T5_Architecture_materielle/5.3_Protocoles_de_routage/cours/#2-le-protocole-rip](https://glassus.github.io/terminale_nsi/T5_Architecture_materielle/5.3_Protocoles_de_routage/cours/#2-le-protocole-rip)


autre lien bien : 
- [https://nc-lycees.netocentre.fr/s/DRy7b2tWY6MXJmC](https://nc-lycees.netocentre.fr/s/DRy7b2tWY6MXJmC)
- [https://nsi4noobs.fr/IMG/pdf/e2_1nsi_reseaux_modele_osi.pdf](https://nsi4noobs.fr/IMG/pdf/e2_1nsi_reseaux_modele_osi.pdf)
- [https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html](https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html)
- [https://www.monlyceenumerique.fr/nsi_terminale/arse/a3_protocoles_routages.php](https://www.monlyceenumerique.fr/nsi_terminale/arse/a3_protocoles_routages.php)
- [https://frederic-junier.gitlab.io/parc-nsi/chapitre25/reseau-cours-git/](https://frederic-junier.gitlab.io/parc-nsi/chapitre25/reseau-cours-git/)
- [https://www.lyceum.fr/1g/nsi/6-architectures-materielles-et-systemes-dexploitation/5-reseaux/](https://www.lyceum.fr/1g/nsi/6-architectures-materielles-et-systemes-dexploitation/5-reseaux/)
- [http://gisele.bareux.free.fr/NSITerm/NSI_Ter_S6.pdf](http://gisele.bareux.free.fr/NSITerm/NSI_Ter_S6.pdf)


-----

[http://www.monlyceenumerique.fr/nsi_terminale/arse/a3_protocoles_routages.php](http://www.monlyceenumerique.fr/nsi_terminale/arse/a3_protocoles_routages.php)