---
title : "Réseau"

---

Révision première

Histoire
Les réseaux informatiques
    - voc

Les différents types de réseau
    - réseaux interconnecté
    - topologie des réseaux

Les routeurs 
Les switchs
Les hubs



L'adressage réseau : adresses MAC et IP
    - L'adresse MAC
    - Adressage IP
Principe du routage IP

Le protocole DNS : Adresse IP et nom de domaine


Modèle OSI
    - modèle en 7 couches
    - encapsulation


protocole TCP/IP
protocole TCP et UDP
protocole du bit alterné
    - 

Protocole de routage
    - Le protocole RIP
        - Définition et principe
        - Construction d'une table de routage
    - Le protocole OSPF
        - Définition et principe
        - L’algorithme de Dijkstra
        - Applications
        - exo bac

Travail terminale
