---
title : "Type de réseau"

---


# Introduction aux réseaux informatiques




<br/>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/FZcQlcO-ZFM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<br/>

## 1. Vocabulaire

!!! note "Définition"

    Un réseau informatique *(Data Communication Network ou DCN)* est un ensemble d'équipements reliés entre eux et qui échangent des informations.



Dans les grandes lignes, un réseau est intégralement composé d'équipements informatiques *(ordinateurs , tablette, mobiles … et d’autres équipements réseau : routeur, hub, switch …)* et de liaisons point-à-point *(câbles, wifi… )* qui relient deux équipements entre eux.


<br/>

Les réseau peuvent se différencier selon : *leur tailles, leur mode de fonctionnement, leur topologie …*



## 2. Différentes tailles de réseaux

Certains réseaux sont limités à une salle, voire un bâtiment. D'autres font la taille d'une ville ou d'un quartier, quand d'autres ont une étendue nationale ou mondiale.

<br/>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/c0Xj09s5hYA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<br/>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/_D4bObLSyjE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>



### 2.1 Réseaux locaux

Le plus petit réseaux qu'il est possible de faire, consiste à connecter deux ordinateurs via leur carte réseau *(Figure - 1c)*, à l'aide de câbles réseau *Ethernet (RJ45)*.

<br/>


<center>
<img src="../../../images/carte_reseau.png">
</center>

<br/>


Pour relier plus de deux ordinateurs, il est nécessaire d’utiliser du matériel réseau supplémentaire, comme un hub ou un switch. 

<br/>


Le SWITCH est un un boîtier disposant de plusieurs ports RJ45 (entre 4 et plusieurs centaines).

<br/>


Pour réaliser le réseau de la figure 2b, chaque cartes réseaux des ordinateurs doit être branchée à une des prises du switch à l’aide d’un câble RJ45.

<br/>



<center>
<img width="800" src="../../../images/2b.png">
</center>


<br/>


Les petits réseaux, contenant entre deux et cent ordinateurs sont appelés des réseaux locaux ou encore des réseaux : 

- **PAN** *(Personnal Area Network)*  ou  **LAN** *( Local Area Network)*
  
Les réseaux internes aux entreprises ou aux écoles sont généralement de ce type.




### 2.3 Réseaux interconnectés


Ces réseaux vus précédemment peuvent être connectés ensemble et forment des réseaux de réseaux. En voici un exemple dans la figure 3, deux réseaux sont connectés de la manière suivante.

<br/>

<center>
<img width="800" src="../../../images/reseau_interconecte.png">
</center>

<br/>



Les ordinateurs 1 et 2 sont connectés par un switch, ils sont en réseau. C’est la même chose pour les ordinateurs 3, 4 et 5. Et comme les deux switchs sont connectés, les ordinateurs 1, 2, 3, 4 et 5 sont dans un même réseau. Tout cela est un peu rapide, il restera à configurer correctement tout ce matériel pour que le réseau soit opérationnel !

<br/>

À coté de ces réseaux assez petits, on trouve les réseaux de taille moyenne, qui permettent d’interconnecter des réseaux locaux proches :


- les **MAN**, pour *Metropolitan Area Network* ont généralement la taille d’une ville.


- les **WAN**, pour *Wide Area Network*, permettent de relier entre eux des réseaux locaux dans des villes différentes.




### 2.4 Réseaux mondiaux

**Internet** est une interconnexion de réseaux à l'échelle mondiale. C’est d’ailleurs ce qui lui a valu son nom : internet est l’abréviation de *interconnections of networks*. Environ 47000 réseaux de grande envergure, des réseaux autonomes, sont interconnectés pour former internet. Ces 47000 réseaux sont souvent des réseaux appartenant à des fournisseurs d’accès. Les informations qui transitent sur internet passent par des câbles en fibre optique. Les câbles qui relient ces réseaux parcourent le monde entier et traversent les différents océans : pour donner un exemple, il y a environ 6 à 7 câbles qui traversent l’Atlantique qui permettent aux réseaux américains de communiquer avec les réseaux européens.

??? note "Représentation d'internet"
        
    <center>
    <img src="../../../images/opteProject.gif">
    </center>

    <center>
    [Opte Project](https://www.opte.org/) : [the Internet $1997$ - $2021$](https://www.opte.org/the-internet)  
    (gif animé, accéléré $\approx 8$x, $\approx 17$ sec)
    </center>

    <br/>
    
    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/-L1Zs_1VPXA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    
    [Opte Project](https://www.opte.org/) : la vidéo complète ($2$ min $18$: [the Internet $1997$ - $2021$](https://www.opte.org/the-internet)
    </center>

    <br/>
    

    Des regroupements *cohérents* entre certains réseaux, que l'on peut envisager comme des *réseaux de réseaux*, ont donc été mis en place, pour en faciliter la gestion technique et leur donner du sens.
    



??? note "Remarque"
    Attention de ne pas confondre internet et le web !



## 3. Mode de fonctionnement
Pour les réseaux de petites tailles (PAN et LAN) on peut envoyer un message à tous les équipements (broadcast), ou certains seulement (multicast).

<br/>


Pour les grands réseau tel qu’internet par exemple, on communique avec un seul équipement (unicast) via de nombreux supports. Il doit exister au moins un chemin entre deux équipements.

<br/>

<center>
<img width="600" src="../../../images/unicast_broadcast.png">
</center>

<br/>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Iob624Nz5fY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>




## 4. Topologie

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Iob624Nz5fY?start=173" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<br/>


!!! note "Définition : nœud"
    Une machine connectée au réseau se nomme un nœud. 


!!! note "Définition : Topologie"
    La topologie est la façon dont les nœud sont interconnectées.


**Remarque :** Cela ne concerne pas les réseaux sans fils !

<br/>

Il existe tout un ensemble de topologie de réseau possible : en bus, en anneau, en étoile, en étoile, en maillage.


<br/>


<center>
<img width="800" src="../../../images/topologie-de-reseau.jpg">
</center>





## 5. Vocabulaire

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Iob624Nz5fY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>


jusqu'a 2.54


i
??? note "Ressources"
    - [https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html](https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html)

    - [https://nc-lycees.netocentre.fr/s/DRy7b2tWY6MXJmC](https://nc-lycees.netocentre.fr/s/DRy7b2tWY6MXJmC)

    - [https://nsi4noobs.fr/IMG/pdf/e2_1nsi_reseaux_modele_osi.pdf](https://nsi4noobs.fr/IMG/pdf/e2_1nsi_reseaux_modele_osi.pdf)