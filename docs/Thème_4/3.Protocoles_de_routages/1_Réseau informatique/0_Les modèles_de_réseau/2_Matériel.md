---
title : "Matériel"

---

# Le matériel réseaux


## 1. Cartes réseaux
### A. Carte Ethernet
La carte Ethernet (NAC : Network Adapter Card) constitue un composant indispensable pour la création d'un réseau informatique. Celle-ci permet la connexion d'un câble réseau établissant la liaison entre différents ordinateurs. Il existe différents types de carte Ethernet, mais ils possèdent tous la même vocation, à savoir, l'envoie, la réception et le contrôle des données sur un réseau.

<center>
<img width="800" src="../../../images/Carte_ethernet.gif">

</center>




### B. Carte wifi
Depuis le début nous avons uniquement parlé de réseaux filaires (les différents composants du réseau sont reliés par des câbles), il est aussi possible de mettre plusieurs machines en réseau grâce à des technologies sans fil (utilisation des ondes radio pour transmettre l’information entre les différents composants du réseau), par exemple, le wifi (il existe d’autres technologies sans fil que le wifi, mais elles ne seront abordées ici). Chaque ordinateur appartenant au réseau sans fil devra posséder une carte réseau wifi (aujourd’hui tous les ordinateurs portables vendus sont par défaut équipés d’une telle carte). Il sera nécessaire d’utiliser un concentrateur wifi (équivalent du switch en filaire) si l’on désire mettre en réseau plus de deux ordinateurs.

<center>
<img width="300" src="../../../images/carte-wifi.png">

</center>



## 2. Les hubs


Le Hub (concentrateur en français) est l’équivalent de la multiprise en électricité, il répète les données reçues en les transmettant à toutes les machines connectées, ce qui réduit considérablement la bande passante.

Le HUB agit seulement au niveau de la couche 1 du modèle OSI, c’est-à-dire qu’il ne voit que des bits (10010111..).

Si au moins deux machines transmettent en même temps il y a collision.

Le Hub pose d’importants problèmes de sécurité en facilitant l’écoute sur le réseau.

----
----


Un HUB fera la même chose qu’un switch mais une seule des machines connectées peut y transmettre à la fois.
Contrairement à un HUB, un SWITCH ne reproduit pas sur tous les ports chaque trame qu’il reçoit : il sait déterminer sur quel port il doit envoyer une trame, en fonction de l’adresse de destination de cette trame.
    



Un hub est un dispositif de réseau de base qui permet de connecter plusieurs périphériques ensemble en utilisant une technique de diffusion de paquets. Il agit en recevant les paquets de données envoyés par un périphérique connecté à l'un de ses ports et en les transmettant à tous les autres ports connectés. Les hubs sont souvent utilisés pour étendre simplement un réseau local (LAN) en permettant à plusieurs périphériques de communiquer entre eux, mais ils sont moins avancés que les switches en termes de fonctionnalités et de performance. Les hubs sont considérés comme obsolètes pour de nombreux scénarios de réseau, en raison de leur manque de capacité à gérer efficacement le trafic réseau et à minimiser les collisions de paquets.





## 3. Les switchs

<center>
<img width="300" src="../../../images/switch.png">

<img width="800" src="../../../images/switch_pro.png">

</center>

----
----

À la différence du HUB, le switch (commutateur en français) distribue les données à la machine destinataire, il travaille donc sur les deux premières couches du modèle OSI. Le switch va réussir à "décoder" les entêtes de la trame pour trouver l'adresse MAC de destination. Il a une table de correspondance (adresse mac = port x) et il renvoie la trame uniquement à ce destinataire.

Le switch permet également d’éviter les collisions, si la machine que l’on tente de joindre est occupée, un nouvel essai sera effectué plus tard.

Il n’y a donc plus de partage de bande passante et la sécurité est accrue.


-----
-----




Le SWITCH est un un boîtier disposant de plusieurs ports RJ45 (entre 4 et plusieurs centaines).

Un switch est un dispositif de réseau qui permet de connecter plusieurs ordinateurs et/ou périphériques à un réseau local (LAN) ou étendu (WAN). Il joue le rôle de concentrateur de trafic en transmettant les données entre les différents périphériques connectés sur le réseau. Les switches sont capables de déterminer l'adresse MAC (Media Access Control) de chaque dispositif connecté et de transmettre les paquets de données uniquement à l'appareil cible, ce qui améliore les performances du réseau en réduisant les collisions et en augmentant la bande passante disponible. Les switches sont souvent utilisés pour des applications telles que les réseaux d'entreprise, les centres de données et les réseaux domestiques.

Un switch est un dispositif de réseau qui permet de connecter plusieurs périphériques ensemble en utilisant des techniques de commutation de paquets. Il agit en recevant les paquets de données envoyés par les périphériques connectés à ses ports et en les transmettant à leur destination appropriée en utilisant l'adresse MAC associée à chaque paquet. Les switches sont souvent utilisés pour connecter des ordinateurs, des imprimantes, des serveurs et d'autres équipements réseau, et sont plus avancés que les commutateurs de hub en termes de fonctionnalités et de performance.





## 4. Les routeurs

###  A. Qu'est-ce qu'un Routeur / Relais ?


Un <bred>Routeur</bred> / <bred>Relais</bred> :fr: / <bred>Router</bred> / <bred>Relay</bred> :gb: est un matériel réseau (une sorte d'ordinateur), qui a vocation a **relayer** / **router** les paquets d'information reçus en entrée, vers leur prochaine destination (le prochain routeur), voire leur destination finale (lorsqu'ils sont arrivés). 
Un routeur dispose de (au moins) $2$ <bred>ports</bred> / <bred>Interfaces</bred> (réseau) / <bred>NIC - Network Interface Card</bred> / <bred>Cartes Réseau</bred> :

* Chaque interface réseau (NIC) d'un routeur dispose d'une adresse IP distincte
* Chaque interface réseau (NIC) d'un routeur définit un réseau différent

### B. À quoi sert un Routeur / Relais ?

- Un routeur sert d'**intermédiaire** dans la transmission d'un message.  
- Plus précisément, son rôle est de relayer / router des informations entre des **hôtes** distincts **situés dans des réseaux différents** (càd dans un inter-réseau). 
- Chaque routeur reçoit des données et c'est lui qui décide/calcule à qui les transmettre, en déterminant la meilleure route, grâce à des **protocoles/algorithmes de routage** qu'il contient.
- Lorsqu'il joue le rôle de passerelle (c'est le cas usuellement au sens réseau), un routeur détermine les limites d'un réseau.



### C. Exemples de Routeurs

#### Votre Box : un routeur domestique

Votre box domestique est également un routeur qui possède plusieurs interfaces réseau :

* une interface est connectée au réseau de votre opérateur / Fournisseur d'Accès (FAI) (ici WAN - Wide Area Network: en rouge)
* une ou plusieurs interfaces filaires (ethernet) connectées à votre réseau local
* une interface Wifi

!!! col __40 center
    ![Bbox Fibre](./../img/bbox-fibre.png)
    <figcaption>
    Bbox Fibre
    </figcaption>

!!! col __60 center clear
    ![Bbox Fibre Arrière](./../img/bbox-fibre-arriere.png)
    <figcaption>
    Bbox Fibre - Arrière
    </figcaption>



??? note "Remarque"
    En fait, une box familiale joue plusieurs rôles :

    - Le rôle de **routeur** pour déterminer les meilleurs routes par lesquelles acheminer/router les paquets de données
    - Le rôle de **passerelle** / **gateway** pour sortir du réseau local (ici : l'interface filaire ethernet rouge)
    - le rôle de **commutateur/switch** pour permettre la communication entre eux des hôtes d'un même réseau (ici: les interfaces filaires ethernet jaunes, et l'interface wifi)

</br>

#### Routeurs Professionnels

Un routeur sur internet est un peu plus sophistiqué, possède souvent plus de ports et ressemble d'extérieur à un switch. Il dispose d'un logiciel interne bien plus sophistiqué afin de lui permettre de communiquer avec ses routeurs voisins pour l'aider à déterminer les meilleures routes à emprunter pour acheminer ses paquets.

!!! col _2 center
    ![Routeur Cisco](./../img/routeur-cisco.png)
    <figcaption>Routeur Cisco - WS-C2960X-48FPD-L - Catalyst 2960-X</figcaption>

!!! col _2 center clear
    ![Routeur Cisco](./../img/routeur-cisco-arriere.png)
    <figcaption>Routeur Cisco - Arrière - WS-C2960X-48FPD-L - Catalyst 2960-X</figcaption>







-----
----


## D. Rôle d'un Routeur

Essentiellement, le rôle d'un routeur est de:

### Relayer / Router les paquets de données

Chaque routeur reçoit des données en entrée, sous forme de paquets, et doit décider à qui les <bred>relayer</bred> / rediriger / <bred>router</bred> :

* vers le **prochain routeur** / <bred>passerelle</bred> / <bred>gateway</bred>, également surnommé le <bred>saut suivant</bred> :fr: / <bred>next hop</bred> :gb:. En pratique, de manière équivalente, le routeur doit déterminer vers laquelle de ses propres <bred>interfaces de sortie</bred> / <bred>ports</bred> il doit relayer / router / rediriger le paquet.  
Pour prendre une telle décision, le routeur dispose de :
    * **tables de routage** qui sont des tableaux d'informations stockés sur chaque routeur, composés de plusieurs lignes / <bred>entrées</bred>, comprenant classiquement, a minima:
        * les adresses IP des réseaux connus (les destinations possibles),
        * les adresses IP des passerelles / gateways correspondant au saut suivant (instructions pour rejoindre ces destinations)
        * le Port / Interface de sortie (qui mène vers le saut suivant)
        * quelquefois d'autres indications (variables selon les protocoles implémentés par le routeur: distance des prochains sauts, coût de la route, etc.)
    * des **protocoles de routage**, qui sont des algorithmes d'aide à la décision : en charge de déterminer le saut suivant sur chaque routeur
* vers la **machine de destination**, si le paquet est arrivé au routeur final, directement connecté réseau de destination




-----
-----






!!! note "Ressources"
    - à exploiter : [https://eskool.gitlab.io/tnsi/reseaux/routage/](https://eskool.gitlab.io/tnsi/reseaux/routage/)
    - [https://nc-lycees.netocentre.fr/s/DRy7b2tWY6MXJmC](https://nc-lycees.netocentre.fr/s/DRy7b2tWY6MXJmC)

    - [https://www.monlyceenumerique.fr/nsi_terminale/arse/a3_protocoles_routages.php](https://www.monlyceenumerique.fr/nsi_terminale/arse/a3_protocoles_routages.php)