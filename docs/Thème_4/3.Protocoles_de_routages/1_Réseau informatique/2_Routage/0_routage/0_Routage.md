---
title : "Routage"

---


# Routage

## 1. Qu'est-ce que le Routage?

!!! def "Routage"
    Le <bred>Routage</bred> :fr: / <bred>Routing</bred> :gb: est le mécanisme général qui implémente la communication (par commutation de paquets) dans un inter-réseau, entre un **hôte source** (émetteur, dans un réseau) et un (ou plusieurs) **hôtes de destination** (récepteur, dans un autre réseau). Le routage est basé sur:

    * un ensemble de **matériels** : les routeurs, qui correspondent aux noeuds/nodes de l'inter-réseau. (Les commutateurs/switchs sont utilisés dans un même réseau local). 
    * un ensemble de **logiciels** : les protocoles/algorithmes (de routage), contenus dans chaque routeur

C'est cet ensemble **matériel** et **logiciel** qui permet d'acheminer / relayer / router les paquets de données depuis l'hôte source (expéditeur) vers le ou les hôtes de destination.

<br/>

<center>
<img width="600" src="./../img/commutation-paquets.gif">
copyright: CC-BY-SA 3.0 [Wikipedia](https://commons.wikimedia.org/wiki/File:Packet_Switching.gif)
</center>


## 2. Rôle d'un Routeur

Chaque routeur reçoit des données en entrée, sous forme de paquets, et doit décider à qui les <bred>relayer</bred> / rediriger / <bred>router</bred> :

* vers le **prochain routeur** / <bred>passerelle</bred> / <bred>gateway</bred>, également surnommé le <bred>saut suivant</bred> :fr: / <bred>next hop</bred> :gb:. 

<br/>

En pratique, de manière équivalente, le routeur doit déterminer vers laquelle de ses propres <bred>interfaces de sortie</bred> / <bred>ports</bred> il doit relayer / router / rediriger le paquet.

<br/>

Pour prendre une telle décision, le routeur dispose de :

- Tables de routage
- De protocoles de routages *(dans certain cas )*







    * **tables de routage** qui sont des tableaux d'informations stockés sur chaque routeur, composés de plusieurs lignes / <bred>entrées</bred>, comprenant classiquement, a minima:
        * les adresses IP des réseaux connus (les destinations possibles),
        * les adresses IP des passerelles / gateways correspondant au saut suivant (instructions pour rejoindre ces destinations)
        * le Port / Interface de sortie (qui mène vers le saut suivant)
        * quelquefois d'autres indications (variables selon les protocoles implémentés par le routeur: distance des prochains sauts, coût de la route, etc.)

    * des **protocoles de routage**, qui sont des algorithmes d'aide à la décision : en charge de déterminer le saut suivant sur chaque routeur vers la **machine de destination**, si le paquet est arrivé au routeur final, directement connecté réseau de destination

