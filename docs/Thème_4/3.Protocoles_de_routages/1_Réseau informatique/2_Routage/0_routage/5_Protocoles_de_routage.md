---
title : "Protocoles de routages"

---




<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/friTvHagVCw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>


# Les protocoles de routages






------
------




### 3. Maintenir à jour ses Tables de Routage

Les réseaux sont soumis à des évolutions constantes, prévues ou pas, souhaitées ou pas, ce qui oblige la mise à jour/le maintien réguliers des tables de routage dans les routeurs:

* ajout de nouveaux matériels
* suppression de matériels
* remplacement de matériel (vieillissant, défectueux, etc.)
* pannes imprévues
* etc.

Plusieurs méthodes pour maintenir à jour les tables de routage existent, et dépendent des protocoles de routage choisis. On distignue deux grandes familles de protocoles de routage:

* **statiques** ou **centralisés** (par un humain): on parle dans ce cas de (protocoles de) <bred>routage statique</bred>
* **dynamiques** (par un programme): on parle dans ce cas de (protocoles de) <bred>routage dynamique</bred>





-----
-----




## Routage Statique vs Routage Dynamique

Les tables de routage peuvent être construites et maintenues :

* soit **statiquement**, par un humain : on parle de (protocoles de) <bred>routage statique</bred>
* soit **dynamiquement**, par un programme/algorithme/protocole de routage: on parle de (protocoles de) <bred>routage dynamique</bred>

!!! info
    Certains routeurs (e.g. Cisco, Oracle Solaris [^2], etc..) autorisent la configuration des deux routages (statique et dynamique) simultanément dans une même table de routage.



-----
-----







## 1. Routage statique
Le routage statique, correspondant à défénir définir **statiquement**, càd manuellement, les **tables de routages**, est utilisé dans certains cas. Il a le double avantage d’être sécurisé et de ne pas consommer de bande passante (aucune information sur la structure du réseau ne circule). Cependant dès que la taille du réseau augmente, ou que sa configuration est amenée à varier souvent, ce type de routage devient inadapté.

<br/>

## 2. Routage dynamique
Il est impossible au niveau d'internet, ou même de gros réseaux d'entreprise de définir **statiquement**, les **tables de routages** de milliers, voire de milliards, de routeurs. Des algorithmes/protocoles dits de **routage dynamique** ont été inventés pour automatiser cette tâche. Nous allons étudier plus en détail deux grandes familles d'algorithmes dynamiques utilisé dans le cas de réseaux de taille moyenne, qui sont spécifiquement au programme de TNSI:


* Le Protocole <bred>RIP - Routing Information Procotol</bred>
* Le Protocole <bred>OSPF - Open Shortest Path First</bred>



Ces deux protocoles utilisent chacun des paramètres supplémentaires appelés des **métriques**.


!!! def "Métrique (de Routage)"
    Une <bred>métrique</bred> ou <bred>métrique de routage</bred> est un paramètre supplémentaire (numérique) stocké dans les tables de routages, qui **mesure un certain coût** de la route:

    - les distances entre un routeur et un sous-réseau de destination
    - la bande-passante
    - la charge du tronçon, 
    - etc…

    Cette métrique dépend du protocole utilisé, et est en pratique utilisée par chaque routeur pour déterminer les (meilleures) routes de sortie des paquets qu'il reçoit en entrée.  


Ainsi : 

* Le Protocole <bred>RIP - est un routage à vecteur de distance.
* Le Protocole <bred>OSPF - est un routage à état de lien.

<br/>

La première famille consiste à compter le nombre d'étapes nécessaires pour atteindre l'objectif alors que la seconde famille d’algorithmes va permettre de prendre en compte la qualité des liaisons pour optimiser le débit ou la latence de la liaison.



-----
-----




## Distance Administrative

Dans la vraie vie, un routeur peut disposer simultanément de plusieurs protocoles de routage distincts. Dans le cas où un routeur dispose d'informations de routage vers la même destination pour plusieurs protocoles, la première chose qu'il doit faire c'est déterminer quel est le **protocole le plus fiable** parmi tous ceux à sa disposition.

Pour cela, il utilise une métrique appelée **Distance Administrative** (AD) censée mesurer la fiabilité de chaque protocole. Chaque protocole dispose de **Distances Administratives par défaut**, mais ces valeurs sont paramétrables (donc personnalisables). En outre, cette valeur n'a de signification que locale (elle n'est pas annoncée dans les mises à jour de routage).



!!! def "Distance Administrative"
    - La <bred>Distance Administrative (AD)</bred>  est une métrique qui mesure la **fiabilité de chaque protocole**.
    - Chaque protocole admet une <bred>Distance Administrative par défaut</bred>

Pour information, voici quelques valeurs usuelles de distances administratives par défaut, par protocole (supportés par les routeurs Cisco [^3]):

<center>

| Protocole/Route | Valeurs de <br/>Distance Administrative<br/>par défaut |
|:-:|:-:|
| Interface Connectée | 0 |
| Route Statique | 1 |
| BGP externe | 20 |
| IGRP | 100 |
| OSPF | 110 |
| RIP | 120 |
| BGP interne | 200 |
| Inconnu | 255 |

</center>

!!! pte
    Plus la Distance Administrative est petite, et plus le protocole/la route est fiable, donc mieux c'est.


-----
-----