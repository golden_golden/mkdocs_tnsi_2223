---
title : "Protocole RIP"

---




# Routage à vecteur de distance : protocole RIP *(Routing Information Protocol)*


<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/o5gm5aoweGQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>


## 1. Définition et principe

## 2. Construction d'une table de routage
## 3. Application
## 4. Exercices BAC



-------
-------

Le principe simplifié de ce protocole est de chercher à minimiser le nombre de routeurs
à traverser pour atteindre la destination (on minimise le nombre de sauts).

## 1. Principe général de l'algorithme

Le principe du protocole RIP (Protocole d'Information de Routage, ou Routing Information
Protocol en anglais) est le suivant : chaque routeur transmet à ses voisins les adresses de ses propres voisins ou celles qu'il a reçues par d’autres routeurs.
En plus des adresses, le routeur indique la distance, exprimée en nombre de sauts, qui le
sépare d’une machine donnée, c'est-à-dire combien de routeurs il faut traverser pour atteindre cette machine en passant par lui.

<br/>

Ce sont donc des couples (adresse, distance), appelés vecteurs de distance, qui sont
échangés avec ce protocole. C’est grâce à ces indications de distance qu’un routeur va pouvoir choisir la meilleure route, c’est-à-dire celle qui traverse le moins de routeurs pour atteindre une machine.

<br/>

Chaque routeur reçoit en permanence (toutes les 30 secondes, configurable) de ses voisins
les informations de routage qu'ils possèdent. Il va alors exploiter ces informations pour se
construire lui-même sa table de routage en ne retenant que les informations les plus
pertinentes : une simple comparaison permet de ne garder que le chemin le plus avantageux.
Il transmettra à son tour ces informations à ses voisins et ainsi de suite.

<br/>

**Remarque :** C'est l'algorithme de [Belman-Ford](https://fr.wikipedia.org/wiki/Algorithme_de_Bellman-Ford) : un des algorithmes de recherche de plus
court chemin dans un graphe.

<br/>

À l'issue de quelques étapes, les tables se stabilisent et le routage est pleinement
opérationnel.

<br/>

Le temps nécessaire à la stabilisation des tables (appelé délai de convergence) est
proportionnel au nombre maximal d'étapes nécessaires pour relier deux points quelconques du
réseau (diamètre du graphe modélisant le réseau).


## 2. Exemple
Pour illustrer le fonctionnement du protocole RIP, voyons comment évoluent les tables de
routage des routeurs R1 et R3 en utilisant ce protocole sur l’exemple ci-dessous :



### a. Phase d'initialisation


### b. Demandes RIP


### c. Détection des pannes
Le protocole RIP est en mesure de détecter des pannes et de déterminer si une liaison
est en panne : si un routeur ne reçoit pas d'information de la part d'un de ses voisins au
bout d'un certain laps de temps (par défaut, 3 minutes (configurable)), il va considérer que ce lien est mort et en informer ses voisins en indiquant un nombre de sauts égal à 16. En effet, puisque RIP ne gère que 15 sauts au maximum, 16 peut être considéré comme une distance infinie.

<br/>

De cette manière, les voisins vont pouvoir recalculer leurs routes en conséquence en
évitant le lien qui est tombé.

### d. Boucles de routage
Le protocole RIP est conçu de manière à ce que ces boucles de routage ne puissent pas
être créées lors de la mise à jour des tables et la limite imposée sur les distances des
routes est une première technique. Mais ce protocole implémente également d'autres
mécanismes pour éviter ceci comme par exemple :

- la règle du split horizon qui impose qu’un routeur ne renvoie pas une information à
un autre routeur s’il a appris cette information par ce même routeur,
- la règle du temporisateur de retenue (hold down en anglais) qui impose à un routeur
d’ignorer, pendant une certaine durée, toutes informations concernant des routes vers un sous-réseau dont il aurait appris l’indisponibilité (utilisation d'un temporisateur).



### e. Limites
La notion de distance utilisée par le protocole RIP ne garantit pas que les routes soient les meilleures en terme de débit puisque la nature des liaisons (fibre optique, satellite, sans fil, etc.) n’est pas intégrée dans les messages d’information échangés par ce protocole.

<br/>

Par ailleurs, RIP n’est pas adapté aux grands réseaux puisqu'il ignore les routes de plus de
15 sauts (afin de limiter son délai de convergence et pour éviter des boucles de routage).


-----
-----

[https://eskool.gitlab.io/tnsi/reseaux/protocoles/rip/](https://eskool.gitlab.io/tnsi/reseaux/protocoles/rip/)