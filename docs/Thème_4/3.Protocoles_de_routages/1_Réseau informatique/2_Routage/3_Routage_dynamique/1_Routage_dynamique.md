---
title : "Routage Dynamique"

---



# Routage Dynamique


<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/bslQLLzzxcg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<br/>


-----

<br/>

<center>
vecteur de distance
<iframe width="560" height="315" src="https://www.youtube.com/embed/kzablGaqUXM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></iframe>
</center>

<br/>

<center>
Le routage à état de liens
<iframe width="560" height="315" src="https://www.youtube.com/embed/-utHPKREZV8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<br/>

<center>
performance d'un protocole de routage
<iframe width="560" height="315" src="https://www.youtube.com/embed/nwr_MsYBBe8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<br/>

<center>
Open Shortest Path First (OSPF)
<iframe width="560" height="315" src="https://www.youtube.com/embed/FeZI3Xl7j84" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<br/>

<center>
Conclusion
<iframe width="560" height="315" src="https://www.youtube.com/embed/h-OIdNwtH3U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>