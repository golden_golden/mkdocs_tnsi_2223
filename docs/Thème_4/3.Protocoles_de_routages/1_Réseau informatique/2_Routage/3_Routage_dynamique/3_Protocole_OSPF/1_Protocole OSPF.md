---
title : "Protocole OSPF"

---


# Le protocole OSPF



<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/5NbsGRKm6fY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>



## 1. Définition et principe
## 2. L’algorithme de Dijkstra
## 3. Applications
## 4. exo bac


??? note "Ressources"
    [https://eskool.gitlab.io/tnsi/reseaux/protocoles/ospf/](https://eskool.gitlab.io/tnsi/reseaux/protocoles/ospf/)