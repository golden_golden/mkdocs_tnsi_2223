---
title : "L'algorithme de Dijkstra"

---




# L'Algorithme de Dijkstra


L'algorithme de Dijkstra datant de $1959$ permet de trouver le chemin le plus court sur un graphe.


<br/>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/JPeCmKFrKio" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


<br/>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/8Lrad0a1Hjk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

