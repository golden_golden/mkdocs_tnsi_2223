---
title : "Protocoles"

---



# Notion de protocole de communication

Pour communiquer, deux interlocuteurs doivent respecter des règles communes :

<br/>

<center>
<img width="800" src="../../../../images/protocole.png">
</center>

<br/>

- **Même niveau de compréhension**: le sujet traité doit être accessible par les deux interlocuteurs (un enfant de 3 ans ne pourra certainement pas comprendre un exposé de mécanique quantique)
- **Même règle de communication** : le sujet doit être exposé avec des règles de communication communes (même langue et règles d'usage communes)
- **Même support de communication** : la méthode d'expression doit être la même (une personne sourde ne pourra pas comprendre un message parlé par exemple).

<br/>

Dans l'exemple précédent chaque niveau constitue une couche de communication. En informatique, on appelle **Couche Réseau** une entité qui fournit les moyens électriques et/ou fonctionnels nécessaires à l'activation, au maintien et à la désactivation des connexions destinées à la transmission de données numériques (ensemble de bits) entre deux entités de liaison de données.


