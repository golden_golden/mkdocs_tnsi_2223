---
title : "Modèle TCP IP"

---

# Modèle TCP/IP

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/26jazyc7VNk?start=372" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>


<br/>


??? note "Autre vidéo"
    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/GxDYWSo6Tjo?start=311" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></iframe>
    </center>

    <br/>

    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/_0thnFumSdA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </center>

<br/>

## 1. Qu'est ce que le modèle TCP/IP

Initialement le protocole TCP/IP fut créé lorsque Bob Kahn, qui travaillait alors pour la DARPA, a dut créer un protocole pour un réseau de commutation de paquets par radio.

<br/>

Le réseau Arpanet adoptera la suite de protocoles TCP/IP, le 1er janvier 1983. Ce modèle représente la base d'Internet.

<br/>

Le protocole TCP/IP est également un modèle par couche, mais à la différence du modèle OSI, il ne comporte que quatre couches.

<br/>


<center>
<img width="800" src="../../../../images/TCPIP.png">
</center>

<br/>

??? note "Remarque"
    Le modèle OSI est postérieur au protocole TCP/IP. 
    Alors que le modèle TCP/IP à été établis pour la construction pratique d'internet, le modèle OSI est plus général et théorique.

    <br/>
    
    Le modèle OSI peut être utilisé pour décrire la suite de protocoles Internet, notamment car il est plus facile à comprendre, mais il faut noter qu'il n'y a pas toujours une correspondance directe entre les sept couches du protocoles OSI et les quatre couches du protocoles TCP/IP.
    *(malgré ce que laisse penser les représentations souvent utilisé)*


## 2. Fonctionnement du modèle TCP/IP
Le fonctionnement du modèle TCP/IP correspond à celui que nous avons vu lors de l'étude du modèle OSI, il utilise également l'encapsulation.



## 3. Les différentes couches du modèle TCP/IP



<center>
<img width="800" src="../../../../images/TCPIP.png">
</center>

<br/>


### 3.1 Couche 4 : Appilcation
### 3.2 Couche 3 : Transport
### 3.3 Couche 2 : Internet
### 3.1 Couche 1 : Accés réseau



!!! note "Ressources"
    [https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet](https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet)