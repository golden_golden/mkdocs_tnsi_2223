---
title : "Modèle OSI"

---


# Modèle OSI

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/26jazyc7VNk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>


<br/>


??? note "Autre vidéo"
    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/GxDYWSo6Tjo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </center>



## 1. Qu'est ce que le modèle OSI
Créer en 1984, le modèle OSI (Open Systems Interconnection) est un modèle conceptuel qui caractérise en normalisent la façon dont différents composants logiciel et matériels impliqués dans une communication réseau doivent se diviser le travail et interagir les uns avec les autres.


Ainsi, il identifie et organise tous les éléments et fonctionnalités nécessaires à la communication pour garantir une facilité d'évolutivité et d'interopérabilité entre les équipements des différents constructeurs par exemple.

<br/>

Le modèle OSI est un modèle en couche. Il est composé de sept couches :

<br/>

<center>
<img width="800" src="../../../../images/OSI.png">
</center>

<br/>


??? note "Remarque"
    Le modèle OSI est postérieur au modèle TCP/IP que nous verrons par la suite. <br/>

    Le modèles TCP/IP est un modèle qui a été crée afin de répondre à un problème pratique lors de la création d'internet. <br/>

    Le modèle OSI correspond à une approche plus théorique et permet de modéliser les réseaux (pas seulement internet), il comporte davantage de couche est sépare d'avantage les fonctionnalités, il est de fait souvent plus facile à comprendre, d'où son utilisation.



## 2. Principes de fonctionnement du modèle

Chaque couche à un rôle bien définit et assure donc une fonctionnalité bien spécifique dans la communication.

<br/>

Pour chaque couche, il existe des protocoles qui définissent comment sont réalisées les fonctionnalités de la couche.

<br/>

Les messages échangées par les protocoles sont appelés PDU (pour protocole data unit) et un PDU représente l'unité de données manipulées ou produite au niveau de la couche.

<br/>

<center>
<img width="800" src="../../../../images/OSI.png">
</center>

<br/>

Dans une telle architecture les données sont transmises de couches en couche depuis la couches applications où elles sont produites jusqu'à la couches physique. Pendant la transmission chaque couche ajoute un en-tête au PDU provenant de la couche supérieure qui dirige et identifie le paquet, on parle alors d'encapsulation.

<br/>

L'en-tête est les données forment alors de PDU pour la couches suivante. Le processus se poursuit ainsi jusqu'à atteindre la couche du plus bas niveau, qui est la couche physique à partir de laquelle les données sont transmises sur le réseau jusqu'au dispositif de réception.

<br/>

Le dispositif de réception inverse le processus en désencapsulant les données à chaque couche avec els informations d'entête et du pied de page dirigeant les opérations.
 
<br/>

<center>
<img width="800" src="../../../../images/OSI.png">
</center>



## 3. Rôle de chaque couches du modèle OSI


[https://www.youtube.com/watch?v=GxDYWSo6Tjo](https://www.youtube.com/watch?v=GxDYWSo6Tjo)

### 2.1 La couche : physique
La couche physique définit les spécifications électriques et physiques de la connexion des données. Par exemple, la disposition des broches du connecteur, les tensions de fonctionnement d'un câble électrique, les spécifications des câbles à fibres optiques et la fréquence des appareils sans fil. Elle est responsable de la transmission et de la réception de données brutes non structurées sur un support physique. Le contrôle du débit binaire est effectué au niveau de la couche physique. Il s'agit de la couche des équipements de réseau de bas niveau et n'est jamais concerné par les protocoles ou autres éléments de couche supérieure.

-----
-----

Comme son nom l'indique, la couche physique est responsable de l'équipement qui facilite le transfert des données, comme les câbles et les routeurs installés sur le réseau.

<br/>

Cette couche est l'un des aspects de la transmission réseau où les normes sont essentielles. Sans normes, la transmission entre les appareils de différents fabricants est impossible.

<br/>


### 2.2 La couche : laisons de données
La couche de liaison de données assure le transfert entre nœuds - un lien entre deux nœuds directement connectés. Elle gère l'encapsulation et la décapsulation des données dans les trames. Elle définit le protocole permettant d'établir et de terminer une connexion entre deux dispositifs physiquement connectés, tel que le protocole point à point (PPP). La couche de liaison de données est généralement divisée en deux sous-couches : la couche de contrôle d'accès aux médias (MAC) et la couche de contrôle de liaison logique (LLC). La couche MAC est chargée de contrôler la façon dont les appareils d'un réseau accèdent à un support et sont autorisés à transmettre des données. La couche LLC est responsable de l'identification et de l'encapsulation des protocoles de la couche réseau, et contrôle la vérification des erreurs et la synchronisation des trames.

-----
-----

La couche réseau facilite la communication entre différents réseaux, mais la couche liaison de données est responsable du transfert des informations sur le même réseau.

<br/>

La couche liaison de données transforme les paquets reçus de la couche réseau en trames. Tout comme la couche réseau, la couche liaison de données est responsable du contrôle des erreurs et du flux pour garantir la réussite de la transmission.



### 2.3 La couche : réseau
La couche réseau gère le routage des paquets via des fonctions d'adressage et de commutation logiques. Un réseau est un support sur lequel de nombreux nœuds peuvent être connectés. Chaque nœud a une adresse. Lorsqu'un nœud a besoin de transférer un message à d'autres nœuds, il peut simplement fournir le contenu du message et l'adresse du nœud de destination, puis le réseau trouvera le moyen de livrer le message au nœud de destination, éventuellement en l'acheminant par d'autres nœuds. Si le message est trop long, le réseau peut le diviser en plusieurs segments au niveau d'un nœud, les envoyer séparément et réassembler les fragments au niveau d'un autre nœud.

-----
-----

La couche réseau est chargée de décomposer les données sur l'appareil de l'expéditeur et de les réassembler sur l'appareil du destinataire lorsque la transmission s'effectue sur deux réseaux différents.

<br/>

Lorsque l'on communique au sein d'un même réseau, la couche réseau est inutile, mais la plupart des utilisateurs se connectent à d'autres réseaux, tels que les réseaux dans le cloud.

<br/>

Lorsque les données traversent différents réseaux, la couche réseau est chargée de créer de petits paquets de données acheminés vers leur destination, puis reconstruits sur l'appareil du destinataire.


<br/>


### 2.4 La couche : transport
La couche transport fournit les fonctions et les moyens de transférer des séquences de données d'une source à un hôte de destination via un ou plusieurs réseaux, tout en conservant les fonctions de qualité de service (QoS) et en assurant la livraison complète des données. L'intégrité des données peut être garantie par la correction des erreurs et des fonctions similaires. Elle peut également assurer une fonction explicite de contrôle des flux. Bien qu'ils ne correspondent pas strictement au modèle OSI, les protocoles TCP et UDP (User Datagram Protocols) sont des protocoles essentiels de la couche 4.

-----
-----

La couche transport est chargée de prendre les données et de les décomposer en petits morceaux.

<br/>

Lorsque des données sont transférées sur un réseau, elles ne sont pas transférées en un seul paquet.

<br/>

Pour rendre les transferts plus efficaces et plus rapides, la couche transport divise les données en segments plus petits. Ces petits segments contiennent des informations d'en-tête qui peuvent être réassemblées sur le périphérique cible.

<br/>

Les données segmentées sont également dotées d'un contrôle d'erreur qui indique à la couche session de rétablir une connexion si les paquets ne sont pas entièrement transférés au destinataire cible.

<br/>


### 2.5 La couche : session
La couche session contrôle les dialogues (connexions) entre les ordinateurs. Elle établit, gère, entretient et, finalement, met fin aux connexions entre l'application locale et l'application distante. Les logiciels de la couche 5 gèrent également les fonctions d'authentification et d'autorisation. Elle vérifie également que les données sont bien fournies. La couche session est généralement appliquée explicitement dans les environnements d'application qui utilisent des appels de procédure à distance.

-----
-----

Pour communiquer entre deux appareils, une application doit d'abord créer une session, qui est unique à l'utilisateur et l'identifie sur le serveur distant.

<br/>

La session doit être ouverte suffisamment longtemps pour que les données soient transférées, mais elle doit être fermée une fois le transfert terminé. Lorsque de gros volumes de données sont transférés, la session est chargée de s'assurer que le fichier est transféré dans son intégralité et que la retransmission est établie si les données sont incomplètes.

<br/>

Par exemple, si 10 Mo de données sont transférés et que seuls 5 Mo sont complets, la couche session s'assure que seuls 5 Mo sont retransmis. Ce transfert rend la communication sur un réseau plus efficace au lieu de gaspiller des ressources et de retransférer l'intégralité du fichier.


<br/>


### 2.6 La couche : présentation
La couche de présentation vérifie les données pour s'assurer qu'elles sont compatibles avec les ressources de communication. Elle traduit les données dans un format que la couche d’application et les couches inférieures acceptent. Tout formatage de données ou conversion de code nécessaire est également pris en charge par la sixième couche, comme la conversion d'un fichier de texte codé en EBCDIC (Extended Binary Coded Decimal Interchange Code) en un fichier de texte codé en ASCII (American Standard Code for Information Interchange). Elle fonctionne également pour la compression et le cryptage des données. Par exemple, les appels vidéo seront compressés pendant la transmission afin qu'ils puissent être transmis plus rapidement, et les données seront récupérées du côté récepteur. Pour les données qui ont des exigences de sécurité élevées, telles qu'un message texte contenant votre mot de passe, elles seront cryptées sur cette couche.

-----
-----

Nous avons mentionné que la couche application affiche les informations aux utilisateurs, mais la couche présentation du modèle OSI est celle qui prépare les données pour qu'elles puissent être affichées à l'utilisateur.

<br/>

Il est courant que deux applications différentes utilisent l’encodage.

<br/>

Par exemple, la communication avec un serveur Web via HTTPS utilise des informations chiffrées. La couche de présentation est responsable de l’encodage et du décodage des informations afin qu'elles puissent être affichées en clair.

<br/>

La couche de présentation est également responsable de la compression et de la décompression des données lorsqu'elles passent d'un appareil à un autre.

<br/>

### 2.7 La couche : Application
La couche application du modèle OSI interagit directement avec les applications logicielles pour fournir des fonctions de communication selon les besoins. Cette couche est la plus proche des utilisateurs finaux. La fonction de la couche d'application consiste généralement à vérifier la disponibilité des partenaires de communication et des ressources pour prendre en charge tout transfert de données. Cette couche définit également des protocoles pour les applications finales, tels que le système de noms de domaine (DNS), le protocole de transfert de fichiers (FTP), le protocole de transfert hypertexte (HTTP), le protocole d'accès aux messages Internet (IMAP), le protocole de bureau de poste (POP), le protocole de transfert de courrier simple (SMTP), le protocole de gestion de réseau simple (SNMP) et Telnet (une émulation de terminal).

-----
-----

La couche 7 est connue de la plupart des gens car elle communique directement avec l'utilisateur.

<br/>

Une application qui s'exécute sur un appareil peut communiquer avec d'autres couches OSI, mais l'interface fonctionne sur la couche 7.

<br/>

Par exemple, un client de messagerie qui transfère des messages entre le client et le serveur fonctionne sur la couche 7. Lorsqu'un message est reçu sur le logiciel client, c'est la couche application qui le présente à l'utilisateur.

<br/>

Les protocoles d'application comprennent le SMTP (Simple Mail Transfer Protocol) et le HTTP, qui est le protocole de communication entre les navigateurs et les serveurs Web.


<br/>


??? note "Ressources"
    - [https://www.proofpoint.com/fr/threat-reference/osi-model](https://www.proofpoint.com/fr/threat-reference/osi-model)