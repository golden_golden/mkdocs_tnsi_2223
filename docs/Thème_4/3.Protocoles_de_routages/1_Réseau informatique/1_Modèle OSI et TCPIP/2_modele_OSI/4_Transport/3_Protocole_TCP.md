---
title : "Protocole TCP"

---

# Protocole TCP


<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/JpihtQ30Me8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

-----

<br/>

Avec l’adresse physique (MAC), deux hôtes sur le même réseau peuvent communiquer.

<br/>

Avec l’adresse logique (IP), deux hôtes sur des réseaux différents peuvent communiquer.

<br/>

Imaginons maintenant que plusieurs programmes qui fonctionnent en même temps sur le même ordinateur :

- un navigateur ;
- un logiciel d'Email ;
- un logiciel pour écouter la radio sur Internet.

<br/>

Si l'ordinateur reçoit un paquet IP, comment savoir à quel logiciel donner ce paquet IP ? Le système d’exploitation attribue aléatoirement un numéro de port supérieure à 1024 à chaque logiciel dans l'ordinateur. Ce numéro de Port est encapsulé dans un segment TCP ou un datagramme UDP dans chaque paquet IP pour pouvoir s'adresser à tel ou tel logiciel. Les ports réservés sont compris entre 0 et 1023 : [https://fr.wikipedia.org/wiki/Liste_de_ports_logiciels](https://fr.wikipedia.org/wiki/Liste_de_ports_logiciels)

<br/>

[https://nsi4noobs.fr/IMG/pdf/e2_1nsi_reseaux_modele_osi.pdf](https://nsi4noobs.fr/IMG/pdf/e2_1nsi_reseaux_modele_osi.pdf)

<br/>

TCP et UDP sont les 2 principaux protocoles de la couche transport. La différence entre TCP et UDP sont
fondamentales. Ces deux protocoles servent à échanger des paquets d'information entre 2 machines en utilisant leur adresse IP et un numéro de port.

<br/>

**Protocole TCP (Transmission Control Protocol)**
TCP fonctionne un peu comme le téléphone : il faut d'abord établir une connexion TCP entre les 2 machines, ce qu'on pourrait comparer à composer le numéro de téléphone. Une fois que la communication est établie, les 2 machines peuvent dialoguer de manière bidirectionnelle (vous pouvez parler à votre interlocuteur, et c'est réciproque). Et vous pouvez communiquer de cette manière autant que vous voulez, tant que vous ne fermez pas la connexion TCP (i.e.
tant que vous ne raccrochez pas le combiné téléphonique).TCP sert de socle à de nombreux protocoles de la couche application :

- HTTP, qui sert à accéder aux sites internet (autrement dit : le web) ;
- FTP, qui sert à échanger des fichiers entre 2 ordinateurs
- POP3 et IMAP qui sert à lire ses emails
- SMTP qui sert quant à lui à envoyer des emails