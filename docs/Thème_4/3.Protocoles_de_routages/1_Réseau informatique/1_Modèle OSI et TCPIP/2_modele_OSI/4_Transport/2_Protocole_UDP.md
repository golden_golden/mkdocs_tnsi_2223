---
title : "Protocole UDP"

---



# Protocole UDP (User Datagram Protocol)


<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/o6SMyuds0yA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>




----

On peut le comparer au courrier : vous placez le message à envoyer dans une enveloppe qui contient toutes les informations nécessaires au routage : l'adresse IP et le port (i.e. les coordonnées du destinataire), puis vous envoyez l'enveloppe.


<br/>

Utilisation d'UDP : DNS, le protocole de résolution des noms de domaines qui permet de connaître l'adresse IP d'un serveur à partir de son nom de domaine (exemple: [www.google.fr]()).

<br/>

[https://nsi4noobs.fr/IMG/pdf/e2_1nsi_reseaux_modele_osi.pdf](https://nsi4noobs.fr/IMG/pdf/e2_1nsi_reseaux_modele_osi.pdf)

<br/>

Chaque couche (UDP et IP) va ajouter ses informations. Les informations de IP vont permettre d'acheminer le paquet à destination du bon ordinateur. Une fois arrivé à l'ordinateur en question, la couche UDP va délivrer le paquet au bon logiciel (ici: au serveur HTTP). Les deux logiciels se contentent d'émettre et de recevoir des données ("Hello !"). Les couches UDP et IP en dessous s'occupent de tout.