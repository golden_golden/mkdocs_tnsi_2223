---
title : "Protocole IPv4"

---

------
------
a reprendre 


-----
-----


# Protocole IPv4

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/y5iEhgQPXTU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>




## 1. Les adresses IP
### 1.1 Introduction

Après avoir reliés les ordinateurs par l'intermédiaire d'un switch (ou d'un concentrateur wifi), imaginons que l'ordinateur A "souhaite" entrer en communication avec l'ordinateur C.

</br>

Quand vous désirez communiquer avec quelqu'un par voie postale, il est nécessaire d'écrire l'adresse de cette personne sur une enveloppe, à chaque habitation correspond donc une adresse postale. Et bien, c'est un peu la même chose pour les ordinateurs en réseau, chaque machine possède une adresse.

</br>

Pendant très longtemps, différentes technologies de réseau, et donc différents types d'adresse, ont coexisté. Aujourd’hui, on trouve presque exclusivement qu'un seul type d'adresse : les adresses IP.


!!! note "Adresse IP"
    Une adresse IP *(Internet Protocol)* est un numéro d'identification qui est attribué de façon permanente ou provisoire à chaque périphérique relié à un réseau informatique qui utilise l'Internet Protocol.



### 1.2 Adresse IP

Il existe des adresses IP de version 4 sur 32 bits (IPV4), et de version 6 sur 128 bits (IPV6). La version 4 est actuellement la plus utilisée (90% en 2016) mais est progressivement remplacée.

</br>

Les adresses IPV4 sont de la forme : "a.b.c.d", avec a, b, c et d compris entre 0 et 255 (a, b, c et d sont codés sur 1 octet).
Voici un exemple d'adresse IPV4 : **172 . 16 . 254 . 1**


<br/>

<center>
<img width="600" src="../../../images/ipv4.png">

</center>

<br/>
<br/>

Toute adresse IP est composée de deux parties distinctes :

- Une partie nommée Identificateur (ID) du réseau : net-ID située à gauche, elle désigne le réseau contenant les ordinateurs
- Une autre partie nommée identificateur de l’hôte : host-ID située à droite et désignant les ordinateurs de ce réseau.

<br/>

<center>
<img width="800" src="../../../images/net_hoste_id.png">
</center>

<br/>

Pour savoir où se trouve la limite entre le net-ID et le host-ID, il faut connaître le masque de sous-réseau.


    


### 1.2 Les masques de sous-réseaux

#### A. Principes

Pour que le réseau Internet puisse router (acheminer) les paquets de données, il faut qu’il connaisse l’adresse du réseau de destination. Pour déterminer cette adresse réseau à partir de l’adresse IP de destination, à chaque adresse IP on associe un masque de sous-réseau.

</br>
Comme les adresses IP, un masque sous-réseau, ou Netmask, est noté en utilisant quatre nombres a.b.c.d. compris entre 0 et 255. (a,b,c et d sont codé sur un octet, ainsi le masque de réseau est constitué de 32 bits). Voici un exemple de masque de sous-réseau : 255.255.255.0 .

</br>

Pour comprendre comment fonctionne un masque sous réseau, il est nécessaire de travailler sur les représentations binaires des adresses IP et des masques de sous-réseau.

??? note "Remarque"
    Conversion base 10 - base 2.

</br>

Un masque de sous-réseau est composé de sorte que tout les bits à « 1 » sont à gauche  et tout les bits à  « 0 » sont à droite. On dit que les bits à « 1 » sont contigus (c’est-à-dire collés).


</br>

Voici quelques exemples de masques de sous réseau :

- 255.255.255.0 → 11111111.11111111.11111111.00000000
- 255.0.0.0 → 11111111.00000000.00000000.00000000
- 255.224.0.0 → 11111111.11100000.00000000.00000000
  
- 255.225.0.0 → 11111111.11100001.00000000.00000000 Les bits à "1" ne sont pas tous contigus (à gauche), ce n’est donc pas un masque de sous réseau valide.

</br>



!!! note "Méthode"
    Pour déterminer l'adresse réseau correspondant à l'adresse IP 192.168.2.1, il suffit de faire un "et logique bit à bit" entre l'adresse IP (en binaire) et le masque de sous-réseau (en binaire) :

    - pour chaque bit, si le bit de l'adresse IP est 1 et si le bit du masque de sous-réseau est 1 alors le bit correspondant de l'adresse réseau sera 1. 
    - Dans tous les autres cas, le bit du résultat sera à 0 :

    </br>

    ![](img/c16c_7.png)


</br>



!!! example "exemple"

     Intitulé | Décimal | Binaire
    ---------|----------|---------
     l'adresse IP | 192.168.2.1 | 11000000.10101000.00000010.00000001
     masque de sous-réseau | 255.255.255.0 | 11111111.11111111.11111111.00000000
     adresse réseau (net-ID) | 192.168.2.0 | 11000000.10101000.00000010.00000000
     identificateur de l’hôte (host-ID) | 0.0.0.1 | 00000000.00000000.00000000.00000001

    Comme vous pouvez le constater, une adresse IP 11000000.10101000.00000010.00000001 associée à une masque de sous-réseau 11111111.11111111.11111111.00000000 donne une adresse réseau 11000000.10101000.00000010.00000000. Ce qui traduit en base 10 donne : une adresse IP 192.168.2.1 associée à un masque de sous-réseau 255.255.255.0 donne une adresse réseau 192.168.2.0 (exactement le résultat déjà obtenu ci-dessus dans le 2a).


??? example "exemple"


    l'adresse IP                        : 172.16.28.44   → 10101100.00010000.00011100.00101100
    le masque de sous-réseau            : 255.255.0.0    → 11111111.11111111.00000000.00000000
    adresse réseau (net-ID)             : 172.16.0.0     → 10101100.00010000.00000000.00000000
    identificateur de l’hôte (host-ID)  : 0.0.28.44      → 00000000.00000000.00011100.00101100


    Envisageons un autre cas : adresse IP 172.16.28.44 avec un masque de sous-réseau 255.255.0.0. Ce qui nous donne en binaire : adresse IP 10101100.00010000.00011100.00101100 avec un masque de sous-réseau 11111111.11111111.000000000.00000000. En appliquant un "et logique bit à bit", nous obtenons l'adresse réseau suivante : 10101100.00010000.00000000.00000000, soit en base 10 : 172.16.0.0


??? example "exemple"


    l'adresse IP                        : 10.5.23.247   → 00001010.00000101.00010111.11110111
    le masque de sous-réseau            : 255.0.0.0     → 11111111.00000000.00000000.00000000
    adresse réseau (net-ID)             : 10.0.0.0      → 00001010.00000000.00000000.00000000
    identificateur de l’hôte (host-ID)  : 0.5.23.247    → 00000000.00000101.00010111.11110111

    Un dernier cas : adresse IP 10.5.23.247 avec un masque de sous-réseau 255.0.0.0. Ce qui nous donne en binaire : adresse IP 00001010.00000101.00010111.11110111 avec un masque de sous-réseau 11111111.000000000.000000000.00000000. En appliquant un "et logique bit à bit", nous obtenons l'adresse réseau suivante : 00001010.00000000.00000000.00000000, soit en base 10 : 10.0.0.0


</br>

Nous pouvons résumer les exemples ci-dessus comme suit :

- quand le masque de sous-réseau est 255.255.255.0 pour une adresse IP a.b.c.d, les parties a, b et c sont consacrées à l’identification du réseau et la partie d est consacrée à l’identification des machines sur le réseau (l'adresse réseau sera a.b.c.0)
- quand le masque de sous-réseau est 255.255.0.0 pour une adresse IP a.b.c.d, les parties a, b sont consacrées à l’identification du réseau et les parties c et d sont consacrées à l’identification des machines sur le réseau (l'adresse réseau sera a.b.0.0)
- quand le masque de sous-réseau est 255.0.0.0 pour une adresse IP a.b.c.d, la partie a est consacrée à l’identification du réseau et les parties b, c et d sont consacrées à l’identification des machines sur le réseau (l'adresse réseau sera a.0.0.0)

</br>

Bien que les masques sous réseau soient le plus souvent parmi les trois présenté en exemple précédemment, bien d'autre masque de sous réseau sont possible !


!!! example "exemple"

    - l'adresse IP : 192.168.2.1 → 11000000.10101000.00000010.00000001
    - le masque de sous-réseau : 255.255.224.0. → 11111111.11111111.11100000.00000000

    l'adresse IP                        : 192.168.200.1  → 11000000.10101000.11001000.00000001
    le masque de sous-réseau            : 255.255.224.0  → 11111111.11111111.11100000.00000000
    adresse réseau (net-ID)             : 192.168.192.0  → 11000000.10101000.11000000.00000000
    identificateur de l’hôte (host-ID)  : 0.0.8.1        → 00000000.00000000.00001000.00000001
    
    </br>




### d) Connecter des machines :  des Ip et masque sous réseau cohérent

Deux machines physiquement reliées pourront communiquer ensemble uniquement si elles appartiennent aux mêmes réseaux.
Pour que cela soit bien le cas, les adresses IP et le masque sous réseau doivent être choisie avec cohérence.

<br/>

<center>
<img width="800" src="../../../images/resau_masque.png">
</center>

<br/>

Dans la figure ci-dessus le portable A pourra communiquer avec le portable B ,car ils sont dans le même réseau. Mais la machine A et la machine C ne pourront pas communiquer, elles ne sont pas dans le même réseau, alors qu’elles sont bien connectées ! 




-----
-----


### d) Obtenir les adresses ip de son réseau

[https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html](https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html)

#### Sur Windows
Sur Windows, cliquez sur le bouton “Démarrer”, puis tapez **“cmd”** dans **“Rechercher”** pour atteindre *l’Invité de Commande*.
Saisissez la fonction **ipconfig /all** (ou **getmac /v**)
    

[image cmd window](https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html)

#### Sur Linux
Sous Linux, ouvrir un *terminal* et tapper : (**ip a**)
[image terminal linux](https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html)
[https://www.linuxtricks.fr/wiki/reseau-gerez-les-interfaces-reseaux-avec-la-commande-ip-correspondances-avec-ifconfig](https://www.linuxtricks.fr/wiki/reseau-gerez-les-interfaces-reseaux-avec-la-commande-ip-correspondances-avec-ifconfig)


#### Sur MAC
Sur MAC, sélectionnez le **“Menu Pomme”** puis **“A propos de ce Mac”**.
Choisissez le menu **“Réseau”** puis **“Configurations”**.
[image mac](https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html)


------
--------
----
----


#### B. Notation CIDR

Il peut être parfois un peu long d'écrire les masques de sous-réseau sous forme de 4 octets (même en base 10). La notation CIDR (Classless Inter-Domain Routing) permet de raccourcir cette notation :

- au lieu d'écrire adresse IP 192.168.2.1 associée à un masque de sous-réseau 255.255.255.0, on pourra directement écrire adresse IP 192.168.2.1/24
- au lieu d'écrire adresse IP 172.16.28.44 associée à un masque de sous-réseau 255.255.0.0, on pourra directement écrire adresse IP 172.16.28.44/16
- au lieu d'écrire adresse IP 10.5.23.247 associée à un masque de sous-réseau 255.0.0.0, on pourra directement écrire adresse IP 10.5.23.247/8

</br>

Comme vous l'avez sans doute déjà remarqué, le nombre situé après le / correspond au nombre de bits à 1 dans le masque de sous-réseau :

- pour le masque de sous-réseau 255.255.255.0 qui correspond à 11111111.11111111.11111111.00000000 en binaire, on a 24 bits à 1, d'où le /24
- pour le masque de sous-réseau 255.255.0.0 qui correspond à 11111111.11111111.000000000.00000000 en binaire, on a 16 bits à 1, d'où le /16
- pour le masque de sous-réseau 255.0.0.0 qui correspond à 11111111.000000000.000000000.00000000 en binaire, on a 8 bits à 1, d'où le /8


#### C. Des cas plus complexes

Dans la plupart des cas, vous rencontrerez des cas "simples" avec des /8, /16 ou /24, mais vous devez aussi savoir que dans certaines situations il est possible de rencontrer des cas plus complexes.

</br>

Il est, par exemple, possible de voir des /18 ! Comment faire dans ce cas ?

</br>

Imaginons l'adresse IP 172.24.82.47/18 quelle est l'adresse réseau ?

Nous devons obligatoirement travailler en binaire :

- adresse IP 172.24.82.47 en binaire : 10101100.00011000.01010010.00101111
- masque de sous-réseau /18 : 11111111.11111111.11000000.00000000 (on a bien 18 bits à 1) ce qui donne en base 10 : 255.255.192.0

</br>

Le "et logique bit à bit" donne :

</br>

![](img/c16c_8.png)

</br>

D'où une adresse réseau : 10101100.00011000.01000000.00000000 soit en base 10 : 172.24.64.0

</br>

Dans le cas d'un masque sous réseau 255.255.192.0 (/18), les 18 bits les plus à gauche permettront d'identifier le réseau, le reste (32-18 = 14 bits) permettra d'identifier la machine. Ici, impossible de raisonner sur les octets (comme en /8, /16 ou /24), pas le choix, il faut raisonner sur les bits.


------
------

### c) L'adresse de broadcast

Il existe une adresse IP un peu particulière dans un réseau puisque cette adresse IP permet de contacter toutes les machines du réseau en même temps : c'est l'adresse de broadcast.

</br>

Obtenir une adresse de broadcast est relativement simple : il suffit de mettre tous les bits de la partie machine à 1.

</br>

Commençons par des exemples simples :

- 192.168.22.33/24 ; la partie machine correspond au dernier octet (puisque 32-24 = 8 bits = 1 octet), pour obtenir l'adresse de broadcast, il suffit donc de mettre tous les bits du dernier octet à 1 : 192.168.22.255
- 172.43.28.12/16 ; la partie machine correspond aux 2 derniers octets (puisque 32-16 = 16 bits = 2 octets), pour obtenir l'adresse de broadcast, il suffit donc de mettre tous les bits des deux derniers octets à 1 : 172.43.255.255
- 19.68.221.23/8 ; la partie machine correspond aux derniers octets (puisque 32-8 = 24 bits = 3 octets), pour obtenir l'adresse de broadcast, il suffit donc de mettre tous les bits des trois derniers octets à 1 : 19.255.255.255

</br>

Évidemment, la situation se complique un peu avec un masque de sous-réseau un peu plus exotique :

</br>

Prenons un exemple avec le masque 255.255.255.240 qui donne en binaire 11111111.11111111.11111111.11110000, soit un /28 (28 bits à 1) et l'adresse IP 195.32.12.22/28. Quelle est dans ce cas l'adresse de broadcast (et pendant que nous y sommes, nous rechercherons aussi l'adresse réseau) ?

</br>

L'adresse IP donne en binaire : 11000011.00100000.00001100.00010110, dans cette adresse, seuls les 4 bits les plus à droite sont consacrés à la partie machine de l'adresse IP.

</br>

Pour avoir l'adresse de broadcast, il faut mettre à 1 les 4 bits les plus à droite : 11000011.00100000.00001100.0001**1111**, soit en décimal : 195.32.12.31. L'adresse de broadcast est donc de 195.32.12.31

</br>

Pour avoir l'adresse réseau, il faut mettre à 0 les 4 bits les plus à droite : 11000011.00100000.00001100.0001**0000**, soit en décimal : 195.32.12.16. L'adresse de réseau est donc de 195.32.12.16

</br>

Conclusion : Si une machine M1 d'adresse IP 195.32.12.22/28 appartient à un réseau R1, le réseau R1 a une adresse réseau 195.32.12.16 et une adresse de broadcast 195.32.12.31





## 3) Nombre de machines adressables dans un réseau



------
------

<br/>

L'IPV4 permet de générer 232 adresses soit plus de 4 milliards.

<br/>

*Pour rappel 1 octet = 8 bits permet de représenter 28 valeurs de 0 à 255.*



------
-----




Selon le masque de sous-réseau, le nombre de machines qui peuvent appartenir à un réseau varie grandement :

</br>

Avec un masque 255.255.255.0, nous avons à notre disposition un octet pour la partie machine. Avec un octet, il est possible de coder 256 valeurs (2<sup>8</sup> = 256). Mais, il ne faut pas oublier que l'adresse réseau (tous les bits de la partie machine à 0) ne peut pas être utilisée par une machine. De la même façon, l'adresse de broadcast (tous les bits de la partie machine à 1) ne peut pas non plus être utilisé par une machine. En conclusion, dans le cas où nous utilisons un masque 255.255.255.0 nous pouvons adresser 256 - 2 = 254 machines

</br>

Avec un masque de 255.255.0.0, nous avons 2 octets (16 bits) à consacrer à la partie machine. Avec 2 octets, il est possible de coder 2<sup>16</sup> = 65536 valeurs. Nous devons ensuite enlever l'adresse réseau et l'adresse de broadcast, d'où 65536 - 2 = 65534 machines

</br>

Avec un masque de 255.0.0.0, nous avons 3 octets (24 bits) à consacrer à la partie machine. Avec 3 octets, il est possible de coder 2<sup>24</sup> = 16777216 valeurs. Nous devons ensuite enlever l'adresse réseau et l'adresse de broadcast, d'où 16777216 - 2 = 16777214 machines

</br>

Un dernier exemple avec un masque de sous-réseau un peu "exotique" : reprenons notre exemple avec le masque 255.255.255.240. En convertissant ce masque en binaire (11111111.11111111.11111111.11110000), nous pouvons constater qu'il y a uniquement 4 bits réservés à la machine. Avec 4 bits, il est possible de coder 2<sup>4</sup> = 16 valeurs. Il nous faut ensuite retrancher l'adresse réseau (195.32.12.16) et l'adresse de broadcast (195.32.12.31), d'où 16 - 2 = 14 machines. Comme dans ce cas il n'y a pas trop d'adresses possibles, nous pouvons toutes les énumérer : 195.32.12.17, 195.32.12.18, 195.32.12.19, 195.32.12.20, 195.32.12.21, 195.32.12.22, 195.32.12.23, 195.32.12.24, 195.32.12.25, 195.32.12.26, 195.32.12.27, 195.32.12.28, 195.32.12.29, 195.32.12.30, comme vous pouvez le constater, nous avons bien 14 adresses IP.






??? note "Ressources"
    - [https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/010-nsi-numerique-et-sciences-informatiques-reseaux.html](https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/010-nsi-numerique-et-sciences-informatiques-reseaux.html)
    - [images](https://nc-lycees.netocentre.fr/s/DRy7b2tWY6MXJmC)