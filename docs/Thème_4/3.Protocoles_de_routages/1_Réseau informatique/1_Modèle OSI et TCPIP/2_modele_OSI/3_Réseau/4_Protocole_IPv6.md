---
title : "Protocole IPv6"

---

# Protocole IPv6



## 3. Les adresses logiques (IPV6)

Jusqu'à présent nous avons uniquement étudiées les adresses appelées *adresse IPv4* (adresse IP version 4). Cependant devant le nombre d'adresse insuffisant proposé par les adresses IPV4, une nouvelle norme est en train de la remplacer progressivement, les adresses IPv6 (adresse IP version 6). 

<br/>

Bien que ce type d'adresse ne soit pas au programme, voici quelques informations sur les adresses IPV6 pour votre culture générale.

<br/>

L'IPV6 est constituée de **128 bits (16 octets)** et est représentée en écriture hexadécimale, où les 8 groupes de 2 octets (16 bits par groupe donc de 0000 à ffff) sont séparés par un signe deux-points : Elle permet de générer 2128 adresses soit plus de 3,4. 1038.

<br/>

<center>
<img width="600" src="../../../images/ipv6.png">

</center>

<br/>

??? note "Pour en savoir plus "

    *Hors programme !*

    <br/>

    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/Qz3sA6y3eZ4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </center>
