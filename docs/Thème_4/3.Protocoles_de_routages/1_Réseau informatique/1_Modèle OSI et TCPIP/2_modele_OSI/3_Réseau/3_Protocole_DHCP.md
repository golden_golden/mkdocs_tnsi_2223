---
title : "Protocole DHCP"

---

# Protocole DHCP

## 2. Qu'est ce que le protocole DHCP

Dynamic Host Configuration Protocol (DHCP, protocole de configuration dynamique des hôtes) est un protocole réseau dont le rôle est d’assurer la configuration automatique des paramètres IP d’une station ou d'une machine, notamment en lui attribuant automatiquement une adresse IP et un masque de sous-réseau. 

<br/>


??? note "Pour en savoir plus"
    *Hors programme !*
    <br/>

    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/yH9UvkeAz-I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </center>

    <br/>

    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/VlbUh-kzrH8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </center>


<br/>

