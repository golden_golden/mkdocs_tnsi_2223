---
title : "modele osi routeur switch"

---


## Routeurs et Modèle OSI

### Modèle OSI vs Modèle TCP/IP

La communication entre deux hôtes, qu'elle se fasse par connexion directe, ou bien à distance via un réseau, est soumise aux modèles de communications par couches. Il en existe principalement deux :

* Le <bred>modèle OSI - Open Systems Interconnection</bred>, est une **norme ISO - International Standards Organisation** ou Organisation Internationale de Normalisation. C'est un **modèle Académique et Abstrait** (sans implémentation pratique), mais qui admet néanmoins une utilité réelle. **Le modèle OSI contient $7$ couches**.
* Le <bred>modèle TCP/IP</bred> est une **implémentation pratique**, et un standard qui s'est imposé de fait. c'est ce modèle qui est utilisé en pratique. **Le modèle TCP/IP contient $4$ couches**.

<center>
<img src="./../img/OSI-vs-TCP-IP.svg">
<figcaption>Modèle OSI vs Modèle TCP/IP
</figcaption>
</center>

<env>Aller Plus Loin</env> : Relire le [Cours de 1ère NSI - Réseaux](https://eskool.gitlab.io/1nsi/reseaux/)

### Communication par Couches, via un inter-Réseau

!!! pte "Routeurs & Modèle OSI"
    Les <bred>routeurs</bred> sont des matériels de **niveau 3 du Modèle OSI**.  
    En particulier:  

    * Les routeurs manipulent uniquement des identifiants de la couche $3$ (des adresses IP),
    * Les routeurs ne manipulent jamais aucun identifiant de la couche $2$ (PAS d'adresse MAC)

!!! info "Switchs & Modèle OSI"
    Les <bred>switchs</bred>, par contre, sont des matériels de niveau $2$ du Modèle OSI (à l'exception notable des **switchs** dits **administrables** qui, par défnition, sont de niveau 3). Les switchs sont donc utilisés dans le cadre d'un même réseau local (LAN).

<center>
<img src="./../img/osi-switch-routeur.svg" style="width:80%;">
<figcaption>
SW1=Switch Numéro 1, SW2 = Switch Numéro 2<br/>
R1 = Routeur Numéro 1
</figcaption>
</center>

### Traversée des Couches OSI dans un Routeur

!!! mth "Traversée des couches d'un Routeur"

    * Une trame arrive sur la Couche $1$ du routeur
    * elle est traduite logiquement sous la forme d'une trame de Couche $2$ :

    | MAC<br/>dest | MAC<br/>source | . | IP<br/>source | IP<br/>dest | Données | . |
    |:--- |:---|:---|:---:|:---|:---|:---:|

    * Le routeur lit l'adresse MAC du destinataire : 

        * Si ce n'est pas la sienne, il rejette la trame,
        * sinon, la trame lui est destinée, donc il envoie le paquet IP à sa couche $3$. Le protocole IP lit alors l'adresse IP du destinataire :
            * Si elle est dans son réseau local, il l'envoie à la machine concernée,
            * sinon, il regarde dans sa table de routage à  quel nouveau routeur il va l'adresser (passerelle - *gateway* en anglais).

## Notes et Références

[^1]: [Zeste de Savoir, Les réseaux de Zéro, Dans les basses couches du modèle OSI: la couche 3 - Le Réseau](https://zestedesavoir.com/tutoriels/2789/les-reseaux-de-zero/dans-les-basses-couches-du-modele-osi/la-couche-3-le-reseau/)

