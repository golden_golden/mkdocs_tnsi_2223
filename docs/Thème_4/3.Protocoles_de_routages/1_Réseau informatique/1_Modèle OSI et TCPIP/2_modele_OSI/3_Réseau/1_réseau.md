---
title : 'Couche réseau'
---

# La couche réseau



<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/y5iEhgQPXTU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

??? note "Autre vidéo"
    <center><iframe width="560" height="315" src="https://www.youtube.com/embed/s5ccduDBY8w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></center>