---
title : "Adresse MAC"

---

# Les adresses physiques (MAC)


## 1. Qu'est ce qu'une adresse MAC

Une adresse **MAC** *(de l'anglais Media Access Control)*, parfois nommée adresse physique, est un identifiant physique stocké dans une carte réseau ou une interface réseau similaire *(carte Ethernet, carte wifi…)*. 

<br/>

Chaque adresse MAC est sensée être unique au monde, à moins qu'elle n'ait été modifiée au niveau logiciel. On peut donc considérer qu’elle constitue une sorte de plaque d’immatriculation des appareils électroniques.

<br/>

Toutes les cartes réseau ont une adresse MAC, même celles contenues dans les PC et autres appareils connectés (tablette tactile, smartphone, consoles de jeux, réfrigérateurs, montres, etc.).

<br/>

MAC constitue la partie inférieure de la couche de liaison (couche 2 du modèle OSI). Elle insère et traite ces adresses au sein des trames transmises. Elle est parfois appelée adresse Ethernet, UAA pour (Universally Administered Address), BIA (pour Burned-In Address), MAC-48 ou EUI-48. 


## 2. Structure

Une adresse MAC-48 est constituée de 48 bits (6 octets) et est généralement représentée sous la forme hexadécimale en séparant les octets par un double point. 

<br/>

Par exemple **5E:FF:56:A2:AF:15**


??? note "Rappel"
    Pour rappel 1 octet = 8 bits permet de représenter 28 valeurs de 0 à 255 soit en hexadécimal de 00 à FF.


<br/>



<center>
<img width="600" src="../../../images/mac.png">

</center>

<br/>

Ces 48 bits sont répartis de la façon suivante :

- **1 bit I/G** : indique si l'adresse est individuelle, auquel cas le bit sera à 0 (pour une machine unique, unicast) ou de groupe (multicast ou broadcast), en passant le bit à 1 ;
- **1 bit U/L** : 0 indique si l'adresse est universelle (conforme au format de l'IEEE) ou locale, 1 pour une adresse administrée localement ;
- **22 bits réservés** : tous les bits sont à zéro pour une adresse locale, sinon ils contiennent l'adresse du constructeur ;
- **24 bits** : adresse unique (pour différencier les différentes cartes réseaux d'un même constructeur).


<br/>

Les concepteurs d'Ethernet ayant utilisé un adressage de 48 bits, il existe potentiellement 248 (environ 281 000 milliards) d'adresses MAC possibles. L'IEEE donne des préfixes de 24 bits (appelés Organizationally Unique Identifier - OUI) aux fabricants, ce qui offre 224 (environ 16 millions) d'adresses MAC disponibles par préfixe.

## 3. Adresses particulières

<center>
<img width="400" src="../../../images/mac_adresses.png">

</center>

## 4. Pourquoi avoir deux adresses : MAC et IP ?
        
Une telle séparation a son utilité : elle permet le remplacement d'un ordinateur sans pour autant changer son adresse internet.

<br/>

Une adresse IP permet d’identifier votre appareil de connexion sur un réseau.

Toutefois si vous utilisez le même appareil pour vous connecter à internet depuis un autre endroit, votre adresse IP ne sera plus la même car l’adresse IP dépend du réseau utilisé.

<br/>  

En revanche, l’adresse MAC ne change jamais et permet quant à elle d’identifier chacun des périphériques réseau, notamment sur un réseau local.

<br/>

??? note "Ressources"
    - [https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/1010-nsi-numerique-et-sciences-informatiques-reseaux.html](https://www.math93.com/lycee/nsi-1ere/nsi-1ere/146-pedagogie/lycee/nsi/010-nsi-numerique-et-sciences-informatiques-reseaux.html)

