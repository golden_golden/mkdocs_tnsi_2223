---
title : 'Couche liaison de données'
---

# La couche liaison_de_données


<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/mVfyplVauKo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>



## 1. L'adresse MAC

- Adresse d'une carte réseau
- Normalement, elle est unique au monde pour chaque carte réseau
- elle est codée sur 6 octets(48bits)


## 2. Protocole Ethernet

- Ethernet n'est pas le seul protocole de la couche 2 (OSI)
- Il définit le format du message envoyé : Trame
- Différents types de trames et format.


## 2. Le CRC

- Le CRC est une valeur mathématique qui est représentative des données envoyées, utilisé pour la détection des erreurs de transmissions.


??? note "Autre vidéo"
    <center><iframe width="560" height="315" src="https://www.youtube.com/embed/9buY7sx4HzY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></center>