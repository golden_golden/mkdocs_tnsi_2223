---
title : 'Couche physique'
---

# La couche physique



Le modèle OSI organise la communication entre appareils en différentes couches indépendantes et complémentaires. La couche physique est celle aillant pour rôle d'assurer la transmission physique des données entre deux équipements. Elle définit le matériel utilisé ainsi que les standards de connection.

Par exemple, elle définit le standard des câbles Ethernet et des fibres optiques ou encore des standards sans fil comme le wifi.

C'est également cette couche qui assure la synchronisation entre les équipements ainsi que l'encodage et la transmission des bits de données sur le support.

<br/>

Le système en couche du modèle OSI, nous permet d'utiliser des supports  différents sans changer les éléments des autres couches. L'information peut par exemples être transmise sur une fibre optique permettant des distance plus grande et un débit plus importants ou encore sur le wifi, une technologie sans fil permettant un usage nomade.


??? note "Pour en savoir plus" 
    *Hors programme !*
    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/a5IMJ4cLYf8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </center>

    <br/>


    [https://www.youtube.com/watch?v=OzCVjYFrrNc](https://www.youtube.com/watch?v=OzCVjYFrrNc)