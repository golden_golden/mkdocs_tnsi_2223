import os

DOSSIERS_CHEMIN = "/run/media/arnaud/TravailD1/Cours/Lycée/Informatique/30. T_NSI/mkdocs_tnsi_2223_prof (rapide)/docs/Thème_4/3.Protocoles_de_routages/1_Réseau informatique/2_Modèle OSI et TCPIP/00_organisation"
LISTE_DOSSIER = ["physique","liaison_de_données","réseau","transport","session","présentation","application"]



def creer_dossiers():
    numerotation = 1
    for dossier in LISTE_DOSSIER:
        chemin = f"{DOSSIERS_CHEMIN}/{numerotation}_{dossier[0].upper() + dossier[1:]}"
        os.makedirs(chemin, exist_ok=True)
        creer_fichier(chemin,dossier)
        numerotation += 1


def creer_fichier(chemin_fichier,nom_dossier):
    nom_fichier = f"{chemin_fichier}/{nom_dossier}.md"
    with open(nom_fichier, "w") as file:
        file.write(f"---\ntitle : 'Couche {nom_dossier}'\n---\n\n# La couche {nom_dossier}\n")


################
# resources


def creer_fichiers():
    for (chemin_dossier, dir, name) in os.walk(DOSSIERS_CHEMIN):
        if chemin_dossier[-1] in ["1", "2"]:
            creer_fichier(chemin_dossier)

#################$


######
# Programme principal
#####

creer_dossiers()
#creer_fichiers()
