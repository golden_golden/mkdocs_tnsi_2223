---
title : "DNS"

---


# Le protocole DNS : Adresse IP et nom de domaine

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/qzWdzAvfBoo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<br/>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/JBezndxU9us" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>