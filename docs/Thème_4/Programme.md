---
title : "Programme"

---

# Architectures matérielles, systèmes d’exploitation et réseaux

*Extrait du programme*

La réduction de taille des éléments des circuits électroniques a conduit à l’avènement de systèmes sur puce (*SoCs* pour *Systems on Chips* en anglais) qui regroupent dans un seul circuit nombre de fonctions autrefois effectuées par des circuits séparés assemblés sur une carte électronique. Un tel système sur puce est conçu et mis au point de façon logicielle, ses briques électroniques sont accessibles par des API, comme pour les bibliothèques logicielles.

Toute machine est dotée d’un système d’exploitation qui a pour fonction de charger les programmes depuis la mémoire de masse et de lancer leur exécution en leur créant des processus, de gérer l’ensemble des ressources, de traiter les interruptions ainsi que les entrées-sorties et enfin d’assurer la sécurité globale du système.

Dans un réseau, les routeurs jouent un rôle essentiel dans la transmission des paquets sur Internet : les paquets sont routés individuellement par des algorithmes. Les pertes logiques peuvent être compensées par des protocoles reposant sur des accusés de réception ou des demandes de renvoi, comme TCP.

La protection des données sensibles échangées est au cœur d’Internet. Les notions de chiffrement et de déchiffrement de paquets pour les communications sécurisées sont explicitées.

<br>

<table>
    <thead>
        <tr>
            <th >Contenus</th>
            <th >Capacités attendues</th>
            <th >Commentaires</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Composants intégrés d’un système sur puce.</td>
            <td>dentifier les principaux composants sur un schéma de circuit et les avantages de leur intégration en termes de vitesse et de consommation.</td>
            <td>Le circuit d’un téléphone peut être pris comme un exemple : microprocesseurs, mémoires locales, interfaces radio et filaires, gestion d’énergie, contrôleurs vidéo, accélérateur graphique, réseaux sur puce, etc.</td>
        </tr>
        <tr>
            <td>Gestion des processus et des ressources par un système d’exploitation</td>
            <td>Décrire la création d’un processus, l’ordonnancement de plusieurs processus par le système.<br>
            Mettre en évidence le risque de l’interblocage (deadlock).</td>
            <td>À l’aide d’outils standard, il s’agit d’observer les processus actifs ou en attente sur une machine.<br>
            Une présentation débranchée de l’interblocage peut être proposée.</td>
        </tr>
        <tr>
            <td>Protocoles de routage.</td>
            <td>Identifier, suivant le protocole de routage utilisé, la route empruntée par un paquet.</td>
            <td>En mode débranché, les tables de routage étant données, on se réfère au nombre de sauts (protocole RIP) ou au coût des routes (protocole OSPF).<br>
            Le lien avec les algorithmes de recherche de chemin sur un graphe est mis en évidence.</td>
        </tr>
        <tr>
            <td>Sécurisation des communications.</td>
            <td>Décrire les principes de chiffrement symétrique (clef partagée) et asymétrique (avec clef privée/clef publique).<br>
            Décrire l’échange d’une clef symétrique en utilisant un protocole asymétrique pour sécuriser une communication HTTPS.</td>
            <td>Les protocoles symétriques et asymétriques peuvent être illustrés en mode débranché, éventuellement avec description d’un chiffrement particulier.<br>
            La négociation de la méthode chiffrement du protocole SSL (Secure Sockets Layer) n’est pas abordée.</td>
        </tr>
    </tbody>
</table>