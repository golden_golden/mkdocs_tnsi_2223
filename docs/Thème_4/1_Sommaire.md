---
title : "Architectures matérielles, systèmes d’exploitation et réseaux"

---

# Architectures matérielles, systèmes d’exploitation et réseaux

## Sommaire 

1. Composants intégrés d’un système sur puce

2. Gestion des processus et des ressources par un système d’exploitation (Bac)
   
3. Protocoles de routage (Bac)
   
4. Sécurisation des communications.
   