---
title : "Programme"

---


# Bases de données

*Extrait du programme*

Le développement des traitements informatiques nécessite la manipulation de données de
plus en plus nombreuses. Leur organisation et leur stockage constituent un enjeu essentiel
de performance.

Le recours aux bases de données relationnelles est aujourd’hui une solution très répandue.
Ces bases de données permettent d’organiser, de stocker, de mettre à jour et d’interroger
des données structurées volumineuses utilisées simultanément par différents programmes
ou différents utilisateurs. Cela est impossible avec les représentations tabulaires étudiées en
classe de première.

Des systèmes de gestion de bases de données (SGBD) de très grande taille (de l’ordre du
pétaoctet) sont au centre de nombreux dispositifs de collecte, de stockage et de production
d’informations.

L’accès aux données d’une base de données relationnelle s’effectue grâce à des requêtes
d’interrogation et de mise à jour qui peuvent par exemple être rédigées dans le langage SQL
(Structured Query Language). Les traitements peuvent conjuguer le recours au langage SQL
et à un langage de programmation.

Il convient de sensibiliser les élèves à un usage critique et responsable des données.



<br>

<table>
    <thead>
        <tr>
            <th >Contenus</th>
            <th >Capacités attendues</th>
            <th >Commentaires</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel.</td>
            <td>Identifier les concepts définissant le modèle relationnel.</td>
            <td>Ces concepts permettent d’exprimer les contraintes d’intégrité (domaine, relation et référence).</td>
        </tr>
        <tr>
            <td>Base de données relationnelle.</td>
            <td>Savoir distinguer la structure d’une base de données de son contenu.<br> Repérer des anomalies dans le schéma d’une base de données.</td>
            <td>La structure est un ensemble de schémas relationnels qui respecte les contraintes du modèle relationnel.<br>
            Les anomalies peuvent être des redondances de données ou des anomalies d’insertion, de suppression, de mise à jour.<br>
            On privilégie la manipulation de données nombreuses et réalistes.</td>
        </tr>
        <tr>
            <td>Système de gestion de bases de données relationnelles.</td>
            <td>Identifier les services rendus par un système de gestion de bases de données relationnelles : persistance des données, gestion des accès concurrents, efficacité de traitement des requêtes, sécurisation des accès.</td>
            <td>Il s’agit de comprendre le rôle et les enjeux des différents services sans en détailler le fonctionnement.</td>
        </tr>
        <tr>
            <td>Langage SQL : requêtes d’interrogation et de mise à jour d’une base de données.</td>
            <td>Identifier les composants d’une requête.<br>
            Construire des requêtes d’interrogation à l’aide des clauses du langage SQL : SELECT, FROM, WHERE, JOIN.<br>
            Construire des requêtes d’insertion et de mise à jour à l’aide de : UPDATE, INSERT, DELETE.</td>
            <td>On peut utiliser DISTINCT, ORDER BY ou les fonctions d’agrégation sans utiliser les clauses GROUP BY et HAVING</td>
        </tr>
    </tbody>
</table>