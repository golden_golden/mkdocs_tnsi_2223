---
title : "Recherche dans des tables"
correctionvisible : True

---

[https://sql.sh/cours/order-by](https://sql.sh/cours/order-by)

# Rechercher dans des tables SQL


??? faq "Préalable pour les exercices"
    **Pré-requis :** téléchargez la base de données [livres.db](data/livres.db).

    ??? abstract "1. En ligne avec ```sqliteonline.com``` "
        - Rendez vous sur [https://sqliteonline.com/](https://sqliteonline.com/)
        - Par File / OpenDB, ouvrez le fichier  ```livres.db``` précédemment téléchargé.
        - Écrivez votre requête plus cliquez sur Run.  
        ![](data/sqlonline.png)

    ??? abstract "2. Avec un logiciel externe : DB Browser for SQLite :star: :star: :star:"
        - Installez ```DB Browser for SQLite```, téléchargeable à l'adresse [https://sqlitebrowser.org/](https://sqlitebrowser.org/)
        - Ouvrez le fichier ```livres.db```.  
        ![](data/dbbrowser.png)







## 1. Rechercher dans une table SQL

### 1.1 Présentation d’une requête SQL

Il est très fréquent de devoir lire les données issues d’une base de données. Pour cela on utilise la commandes SELECT.

<br/>

Il existe plusieurs commandes qui permettent de mieux gérer les données que l’ont souhaite lire. Voici un petit aperçu des fonctionnalités possibles qui sont abordées sur le reste du site :

- Joindre un autre tableau aux résultats 
- Filtrer pour ne sélectionner que certains enregistrements 
- Classer les résultats 
- Grouper les résultats pour faire uniquement des statistiques (note moyenne, prix le plus élevé …) 

  
<br/><br/>

Un requête SELECT peut devenir assez longue. Juste à titre informatif, voici une requête SELECT qui possède presque toutes les commandes possibles :

```sqlite
SELECT * 
FROM table 
WHERE condition 
GROUP BY expression 
HAVING condition { UNION | INTERSECT | EXCEPT } 
ORDER BY expression 
LIMIT count OFFSET start 
```

<br/>

**Remarque :** Cette requête imaginaire sert principale d’aide-mémoire pour savoir dans quel ordre sont utilisé chacun des commandes au sein d’une requête SELECT.



### 1.1.2 Descriptions des éléments constituants une requête SELECT, FROM

La commande SQL, ```SELECT``` permet de lire des données issues d’une base de données, elle retourne des enregistrements dans un tableau de résultat. Cette commande peut sélectionner une ou plusieurs colonnes d’une table.

<br/>

La commande SQL, ```FROM``` permet d’indiquer dans quelle table, on effectue la requête.

<br/>

L’utilisation basique de ces commandes s’effectue de la manière suivante :

```sqlite
SELECT nom_du_champ 
FROM nom_de_la_table
```

Cette requête va sélectionner (SELECT) le champ « nom_du_champ » provenant (FROM) du tableau appelé « nom_de_la_table ».


!!! example "Exemples"

    Imaginons une base de données appelée « client » qui contient des informations sur les clients d’une entreprise.

    <br/>

    *Table « client » :*

    <center>

    | Identifiant | prenom  |   nom   |   ville   |
    | :---------: | :-----: | :-----: | :-------: |
    |      1      | Pierre  | Dupond  |   Paris   |
    |      2      | Sabrina | Dupond  |  Nantes   |
    |      3      | Julien  | Martin  |   Lyon    |
    |      4      |  David  | Bernard | Marseille |
    |      5      |  Marie  |  Leroy  | Grenoble  |

    </center>


    - Si l’ont veut avoir la liste de toutes **les villes** des clients, il suffit d’effectuer la requête suivante :

    === "Requète"

        ```sqlite
        SELECT ville
        FROM client
        ```
    === "Résultat"

        | ville     |
        | --------- |
        | Paris     |
        | Nantes    |
        | Lyon      |
        | Marseille |
        | Grenoble  |

    <br/>

    - Si l’ont veut avoir tout **les nom et prénom** des clients, il suffit d’effectuer la requête suivante :
  
    === "Requète"

        ```sqlite
        SELECT prenom, nom
        FROM client
        ```

    === "Résultat"

        | prenom  |   nom   |
        | :-----: | :-----: |
        | Pierre  | Dupond  |
        | Sabrina | Dupond  |
        | Julien  | Martin  |
        |  David  | Bernard |
        |  Marie  |  Leroy  |

    <br/>

    - Si l’ont veut avoir **tout les élément de la table client**, il suffit d’effectuer la requête suivante :

    === "Requète"

        ```sqlite
        SELECT *
        FROM client
        ```

    === "Résultat"

        Cette requête retourne exactement les mêmes colonnes qu’il y a dans la base de données. Dans notre cas, le résultat sera donc :

        | Identifiant | prenom  |   nom   |   ville   |
        | :---------: | :-----: | :-----: | :-------: |
        |      1      | Pierre  | Dupond  |   Paris   |
        |      2      | Sabrina | Dupond  |  Nantes   |
        |      3      | Julien  | Martin  |   Lyon    |
        |      4      |  David  | Bernard | Marseille |
        |      5      |  Marie  |  Leroy  | Grenoble  |


??? faq "Exercice"
    À faire


### 1.1.3 Utiliser des alias : AS

Dans le langage SQL, il est possible d’utiliser des alias pour renommer temporairement une colonne ou une table dans une requête. Cette astuce est particulièrement utile pour faciliter la lecture des requêtes.



#### A. Alias sur une colonne

Permet de renommer le nom d’une colonne dans les résultats d’une requête SQL. C’est pratique pour avoir un nom facilement identifiable dans une application qui doit ensuite exploiter les résultats d’une recherche.

!!! example "Exemples"

    === "Requète"

        ```sqlite
        SELECT identifiant AS id, prenom As p
        FROM client
        ```

    === "Résultat"

        | id | p  |
        | :---------: | :-----: |
        |      1      | Pierre  |
        |      2      | Sabrina |
        |      3      | Julien  |
        |      4      |  David  |
        |      5      |  Marie  |



??? faq "Exercice"
    À faire



#### B. Alias sur une table

Permet d’attribuer un autre nom à une table dans une requête SQL. Cela peut aider à avoir des noms plus court, plus simple et plus facilement compréhensible.

```sqlite
SELECT * 
FROM client AS c
```

L’utilisation d’alias sur les tables est particulièrement utile lorsqu’il y a des jointures.


??? faq "Exercice"
    À faire





## 2. Trier les données

#### 2.1 Éviter les redondances avec DISTINCT

Pour éviter des redondances dans les résultats, il faut simplement ajouter DISTINCT après le mot SELECT.


!!! example "Exemples"

    **Sans Distinct :**
    === "Requète"

        ```sqlite
        SELECT nom
        FROM client
        ```

    === "Résultat"

        |   nom   |
        | :-----: |
        | Dupond  |
        | Dupond  |
        | Martin  |
        | Bernard |
        |  Leroy  |


    <br/>

    **Avec Distinct :**

    === "Requète"

        ```sqlite
        SELECT DISTINCT nom
        FROM client
        ```

    === "Résultat"

        |   nom   |
        | :-----: |
        | Dupond  |
        | Martin  |
        | Bernard |
        |  Leroy  |



??? faq "Exercice"
    À faire


??? note "Remarque"
    L’utilisation de la commande DISTINCT est très pratique pour éviter les résultats en doubles. Cependant, pour optimiser les performances il est préférable  d’utiliser la commande SQL GROUP BY lorsque c’est possible.



#### 2.2 Extraire des lignes qui respectent des conditions avec WHERE


La commande WHERE dans une requête SQL permet d’extraire les lignes d’une base de données qui respectent une condition. Cela permet d’obtenir uniquement les informations désirées. La façon la plus simple de l’utiliser est la suivante : 

```sqlite
SELECT nom_colonnes 
FROM nom_table 
WHERE condition 
```


!!! example "Exemples"

    Pour obtenir seulement la liste des clients qui ne s’appellent pas Dupond, il faut effectuer la requête suivante :

    === "Requète"

        ```sqlite
        SELECT * 
        FROM client 
        WHERE nom <> 'Dupond'
        ```

    === "Résultat"

        | identifiant | prenom |   nom   |   ville   |
        | :---------: | :----: | :-----: | :-------: |
        |      3      | Julien | Martin  |   Lyon    |
        |      4      | David  | Bernard | Marseille |
        |      5      | Marie  |  Leroy  | Grenoble  |



??? faq "Exercice"
    À faire

<br/>

##### A. Opérateurs de comparaisons

Il existe plusieurs opérateurs de comparaisons. La liste ci-jointe présente quelques uns des opérateurs les plus couramment utilisés.


<center>

|  Opérateur  | Signification |
| :---------: |  :---------:  |
|      =      |  Égal à       |
|      >      |  Supérieur à  |
|     <       |  Inférieur à  |
|     >=      |  Supérieur ou égal à |
|     <=      |  Inférieur ou égal à |
|     <>      |  Différent de |
|     \!=     |  Différent de *(hors norme ISO)* |
|     !<      |  Non inférieur à *(hors norme ISO)* |
|     !>      |  Non supérieur à *(hors norme ISO)* |

</center>



??? faq "Exercice"
    À faire

<br/>

##### B. Opérateurs logiques : AND et OR

Les opérateurs logiques AND et OR peuvent être utilisées au sein de la commande WHERE pour combiner des conditions.


!!! example "Exemples"

    === "Requète"

        ```sqlite
        SELECT *
        FROM client 
        WHERE (nom = "Dupond" AND prenom = "Pierre") OR nom = "Leroy"
        ```

    === "Résultat"

        | Identifiant | prenom |  nom   |  ville   |
        | :---------: | :----: | :----: | :------: |
        |      1      | Pierre | Dupond |  Paris   |
        |      5      | Marie  | Leroy  | Grenoble |


??? faq "Exercice"
    À faire

<br/>

##### C. Condition d’appartenance à un ensemble : IN

L’opérateur logique IN dans SQL s’utilise avec la commande WHERE pour vérifier si une colonne est égale à une des valeurs comprise dans set de valeurs déterminés. C’est une méthode simple pour vérifier si une colonne est égale à une valeur OU une autre valeur OU une autre valeur et ainsi de suite, sans avoir à utiliser de multiple fois l’opérateur OR.

!!! example "Exemples"

    === "Requète"

        ```sqlite
        SELECT *
        FROM client 
        WHERE identifiant IN (1, 2, 4 )
        ```

    === "Résultat"

        | identifiant | prenom  |   nom   |   ville   |
        | :---------: | :-----: | :-----: | :-------: |
        |      1      | Pierre  | Dupond  |   Paris   |
        |      2      | Sabrina | Dupond  |  Nantes   |
        |      4      |  David  | Bernard | Marseille |



**Remarque :** Il n'y a pas de limite du nombre d’arguments entre les parenthèses. Il est possible d’ajouter encore d’autres valeurs. Cette syntaxe peut être associée à l’opérateur NOT pour recherche toutes les lignes qui ne sont pas égales à l’une des valeurs stipulées.


??? faq "Exercice"
    À faire

<br/>

##### D. Condition d’appartenance à un intervalle : BETWEEN

L’opérateur BETWEEN est utilisé dans une requête SQL pour sélectionner un intervalle de données dans une requête utilisant WHERE. L’intervalle peut être constitué de chaînes de caractères, de nombres ou de dates. L’exemple le plus concret consiste par exemple à récupérer uniquement les enregistrements entre 2 dates définies.

!!! example "Exemples"

    === "Requète"

        ```sqlite
        SELECT *
        FROM client 
        WHERE identifiant BETWEEN 2 AND 4
        ```

    === "Résultat"

        | Identifiant | prenom  |   nom   |   ville   |
        | :---------: | :-----: | :-----: | :-------: |
        |      2      | Sabrina | Dupond  |  Nantes   |
        |      3      | Julien  | Martin  |   Lyon    |
        |      4      |  David  | Bernard | Marseille |


??? faq "Exercice"
    À faire

<br/>

###### E. IS NULL / IS NOT NULL

Dans le langage SQL, l’opérateur IS permet de filtrer les résultats qui contiennent la valeur NULL. Cet opérateur est indispensable car la valeur NULL est une valeur inconnue et ne peut par conséquent pas être filtrée par les opérateurs de comparaison (cf. égal, inférieur, supérieur ou différent).

```sqlite
SELECT * 
FROM `table` 
WHERE nom_colonne IS NOT NULL 
```

??? note "Remarque"
    L’opérateur IS retourne en réalité un booléen, c’est à dire une valeur TRUE, si la condition est vrai ou FALSE, si la condition n’est pas respectée. Cet opérateur est souvent utilisé avec la condition WHERE mais peut aussi trouvé son utilité lorsqu’une sous-requête est utilisée.



??? faq "Exercice"
    À faire


<br/>


###### E. D'autres possibilité


??? note "LIKE"

    L’opérateur LIKE est utilisé dans la clause WHERE des requêtes SQL. Ce mot-clé permet d’effectuer une recherche sur un modèle particulier. Il est par exemple possible de rechercher les enregistrements dont la valeur d’une colonne commence par telle ou telle lettre. Les modèles de recherches sont multiple.

    <br/>

    Dans cet exemple le « modèle » n’a pas été défini, mais il ressemble très généralement à l’un des exemples suivants : 

    - **LIKE ‘%a’ :** le caractère « % » est un caractère joker qui remplace tous les autres caractères. Ainsi, ce modèle permet de rechercher toutes les chaines de caractère qui se termine par un « a ». 

    - **LIKE ‘a%’ :** ce modèle permet de rechercher toutes les lignes de « colonne » qui commence par un « a ». 

    - **LIKE ‘%a%’ :** ce modèle est utilisé pour rechercher tous les enregistrement qui utilisent le caractère « a ». 

    - **LIKE ‘pa%on’ :** ce modèle permet de rechercher les chaines qui commence par « pa » et qui se terminent par « on », comme « pantalon » ou « pardon ». 

    - **LIKE ‘a_c’ **: peu utilisé, le caractère « _ » (underscore) peut être remplacé par n’importe quel caractère, mais un seul caractère uniquement (alors que le symbole pourcentage « % » peut être remplacé par un nombre incalculable de caractères . Ainsi, ce modèle permet de retourner les lignes « aac », « abc » ou même « azc ».


    !!! example "Exemples"

        === "Requète"

            ```sqlite
            SELECT *
            FROM client
            WHER Enom LIKE "%er%"
            ```

        === "Résultat"

            | Identifiant | prenom |   nom   |   ville   |
            | :---------: | :----: | :-----: | :-------: |
            |      4      | David  | Bernard | Marseille |
            |      5      | Marie  |  Leroy  | Grenoble  |




??? note "GROUP BY  et HAVING - Hors programme"

    ##### GROUP BY

    La commande GROUP BY est utilisée en SQL pour grouper plusieurs résultats et utiliser une fonction de totaux sur un groupe de résultat. Sur une table qui contient toutes les ventes d’un magasin, il est par exemple possible de liste regrouper les ventes par clients identiques et d’obtenir le coût total des achats pour chaque client.

    <br/>

    De façon générale, la commande GROUP BY s’utilise de la façon suivante : 

    ```sqlite
    SELECT colonne1, fonction(colonne2) 
    FROM table 
    GROUP BY colonne1 
    ```

    <br/>

    **Remarque :** cette commande doit toujours s’utiliser après la commande WHERE et avant la commande HAVING.

    <br/><br/>


    ##### HAVING

    La condition HAVING en SQL est presque similaire à WHERE à la seule différence que HAVING permet de filtrer en utilisant des fonctions telles que SUM(), COUNT(), AVG(), MIN() ou MAX(). 

    <br/>


    L’utilisation de HAVING s’utilise de la manière suivante : 

    ```sqlite
    SELECT colonne1, SUM(colonne2) 
    FROM nom_table 
    GROUP BY colonne1 
    HAVING fonction(colonne2) operateur valeur
    ```
    <br/>

    Cela permet donc de SÉLECTIONNER les colonnes DE la table « nom_table » en GROUPANT les lignes qui ont des valeurs identiques sur la colonne « colonne1″ et que la condition de HAVING soit respectée. 

    <br/>

    Important : HAVING est très souvent utilisé en même temps que GROUP BY bien que ce ne soit pas obligatoire.






### 3. Modifier l'affichage des résultats

#### 3.1 Ordre d'affichage : ORDER BY

La commande ORDER BY permet de trier les lignes dans un résultat d’une requête SQL. Il est possible de trier les données sur une ou plusieurs colonnes, par ordre ascendant ou descendant.

<br/>

Une requête où l’ont souhaite filtrer l’ordre des résultats utilise la commande ORDER BY de la sorte :

```sqlite
SELECT colonne1, colonne2 
FROM table 
ORDER BY colonne1
```

<br/>


-----
-----



Par défaut les résultats sont classés par ordre ascendant, toutefois il est possible d’inverser l’ordre en utilisant le suffixe DESC après le nom de la colonne. Par ailleurs, il est possible de trier sur plusieurs colonnes en les séparant par une virgule. Une requête plus élaboré ressemblerais alors cela :

```sqlite
SELECT colonne1, colonne2, colonne3 
FROM table 
ORDER BY colonne1 DESC, colonne2 ASC 
```


**A noter :** il n’est pas obligé d’utiliser le suffixe « ASC » sachant que les résultats sont toujours classé par ordre ascendant par défaut. Toutefois, c’est plus pratique pour mieux s’y retrouver, surtout si on a oublié l’ordre par défaut.



!!! example "Exemples"
    Pour l’ensemble de nos exemples, nous allons prendre une base “utilisateur” de test, qui contient les données suivantes :

        | Identifiant | prenom  |   nom   |   ville   |
        | :---------: | :-----: | :-----: | :-------: |
        |      1      | Pierre  | Dupond  |   Paris   |
        |      2      | Sabrina | Dupond  |  Nantes   |
        |      3      | Julien  | Martin  |   Lyon    |
        |      4      |  David  | Bernard | Marseille |
        |      5      |  Marie  |  Leroy  | Grenoble  |


    Pour récupérer la liste de ces utilisateurs par ordre alphabétique du nom de famille, il est possible d’utiliser la requête suivante :  

    === "Requète"

        ```sqlite
        SELECT *
        FROM client 
        WHERE identifiant BETWEEN 2 AND 4
        ```

    === "Résultat"

        | Identifiant | prenom  |   nom   |   ville   |
        | :---------: | :-----: | :-----: | :-------: |
        |      2      | Sabrina | Dupond  |  Nantes   |
        |      3      | Julien  | Martin  |   Lyon    |
        |      4      |  David  | Bernard | Marseille |


??? faq "Exercice"
    À faire


##### 3.2 DESC et ASC









##### 3.3 Ne pas affiché tous les résultats : LIMIT *(hors programme)*

La clause LIMIT est à utiliser dans une requête SQL pour spécifier le nombre maximum de résultats que l’ont souhaite obtenir. Cette clause est souvent associé à un OFFSET, c’est-à-dire effectuer un décalage sur le jeu de résultat. Ces 2 clauses permettent par exemple d’effectuer des système de pagination (exemple : récupérer les 10 articles de la page 4). 

<br/>

ATTENTION : selon le système de gestion de base de données, la syntaxe ne sera pas pareil. Ce tutoriel va donc présenter la syntaxe pour MySQL et pour PostgreSQL. 

<br/>

La syntaxe commune aux principales système de gestion de bases de données est la suivante : 

```sqlite
SELECT * 
FROM table 
LIMIT 10 
```

<br/>

Cette requête permet de récupérer seulement les 10 premiers résultats d’une table. Bien entendu, si la table contient moins de 10 résultats, alors la requête retournera toutes les lignes. 


??? note "Bon à savoir"
    La bonne pratique lorsque l’ont utilise LIMIT consiste à utiliser également la clause ORDER BY pour s’assurer que quoi qu’il en soit ce sont toujours les bonnes données qui sont présentées. En effet, si le système de tri est non spécifié, alors il est en principe inconnu et les résultats peuvent être imprévisible.




??? note "Performance"
    Ce dernier chapitre est destiné à un public averti. Il n’est pas nécessaire de le comprendre entièrement, mais simplement d’avoir compris les grandes lignes. Certains développeur pensent à tort que l’utilisation de LIMIT permet de réduire le temps d’exécution d’une requête. Or, le temps d’exécution est sensiblement le même car la requête va permettre de récupérer toutes les lignes (donc temps d’exécution identique) PUIS seulement les résultats définit par LIMIT et OFFSET seront retournés. Au mieux, utiliser LIMIT permet de réduire le temps d’affichage car il y a moins de lignes à afficher.







## 4. Traiter les données

### 4.1 Fonction de calcul

Il existe plusieurs fonctions qui peuvent être utilisées pour manipuler plusieurs enregistrements, il s’agit des fonctions d’agrégations statistiques, les principales sont les suivantes : 

- AVG() pour calculer la moyenne d’un set de valeur. Permet de connaître le prix du panier moyen pour de chaque client 
- COUNT() pour compter le nombre de lignes concernées. Permet de savoir combien d’achats a été effectué par chaque client 
- MAX() pour récupérer la plus haute valeur. Pratique pour savoir l’achat le plus cher 
- MIN() pour récupérer la plus petite valeur. Utile par exemple pour connaître la date du premier achat d’un client 
- SUM() pour calculer la somme de plusieurs lignes. Permet par exemple de connaître le total de tous les achats d’un client.

<br/>

Ces petites fonctions se révèlent rapidement indispensable pour travailler sur des données.








## 2. Recherche lorsqu'il y a plusieurs tables

#### 1.1.1 Présentation d’une requête SQL







#### 1.1.2 Le principe des jointures SQL



##### INNER JOIN

-----

 jointure interne pour retourner les enregistrements quand la condition est vrai dans les 2 tables. C’est l’une des jointures les plus communes.





#### 1.1.3 Sous-requête *(hors programme)*






<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  