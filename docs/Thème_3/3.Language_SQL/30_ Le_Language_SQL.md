---
title: "Language SQL"

---

# Langage SQL : requêtes d’interrogation et de mise à jour d’une base de données

[cours-sql-sh-.pdf](./pdf/cours-sql-sh-.pdf)




<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  