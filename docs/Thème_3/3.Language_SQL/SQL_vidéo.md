---
title: "Le language SQL - vidéo"

---




# Le Langage SQL



### B3 Le Langage SQL 1

[https://www.youtube.com/watch?v=sVcnZIiutcY](https://www.youtube.com/watch?v=sVcnZIiutcY)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/sVcnZIiutcY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### B3 Le Langage SQL 2

[https://www.youtube.com/watch?v=PTxpVhyMg60](https://www.youtube.com/watch?v=PTxpVhyMg60)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/PTxpVhyMg60" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



### B3 Le Langage SQL 3

[https://www.youtube.com/watch?v=cHRq_WuFdB8](https://www.youtube.com/watch?v=cHRq_WuFdB8)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/cHRq_WuFdB8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### B3 Le Langage SQL 4

[https://www.youtube.com/watch?v=C80KcsdzywM](https://www.youtube.com/watch?v=C80KcsdzywM)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/C80KcsdzywM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



### B3 Le Langage SQL 5

[https://www.youtube.com/watch?v=_9tlYxPqTXg](https://www.youtube.com/watch?v=_9tlYxPqTXg)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/_9tlYxPqTXg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>











