---
title : "Modification et création d’une tables SQL"
correctionvisible : True

---



# Modification et création d’une tables SQL




## 1. Modification d’une tables SQL

### 1.1 Insertion d’un nouvelle élément dans une table

##### INSERT

-----





### 1.2 Éliminer un élément d’une table

##### DELETE

-----





### 1.3 Modification d’un élément d’une table

##### UPDATE

----













## 2. Création de base de données et de tables SQL *(hors programme)*

### 2.1 Création et élimination d’une base de données

##### CREATE DATABASE
-------



##### DROP DATABASE
------




### 2.2 Création et élimination d’une table

##### CREATE TABLE
------



##### DROP TABLE

------




<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  