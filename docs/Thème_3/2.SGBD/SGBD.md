---
title : "Les SGBD"
correctionvisible : True

---




# Les système de gestions de bases de donnée (SGBD)

<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/8na73kwO-EI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[https://www.youtube.com/watch?v=8na73kwO-EI](https://www.youtube.com/watch?v=8na73kwO-EI)
</center>

## Les systèmes de gestion de base de données

Dans une base de données, l'information est stockée dans des fichiers, mais à la différence des fichiers au format CSV, il n'est pas possible de travailler sur ces données avec un simple éditeur de texte. Pour manipuler les données présentes dans une base de données (écrire, lire ou encore modifier), il est nécessaire d'utiliser un type de logiciel appelé "système de gestion de base de données" très souvent abrégé en SGBD. Il existe une multitude de SGBD : des gratuites, des payantes, des libres ou bien encore des propriétaires. Les SGBD permettent de grandement simplifier la gestion des bases de données :

- les SGBD permettent de gérer la lecture, l'écriture ou la modification des informations contenues dans une base de données
- les SGBD permettent de gérer les autorisations d'accès à une base de données. Il est en effet souvent nécessaire de contrôler les accès par exemple en permettant à l'utilisateur A de lire et d'écrire dans la base de données alors que l'utilisateur B aura uniquement la possibilité de lire les informations contenues dans cette même base de données.
- les fichiers des bases de données sont stockés sur des disques durs dans des ordinateurs, ces ordinateurs peuvent subir des pannes. Il est souvent nécessaire que l'accès aux informations contenues dans une base de données soit maintenu, même en cas de panne matérielle. Les bases de données sont donc dupliquées sur plusieurs ordinateurs afin qu'en cas de panne d'un ordinateur A, un ordinateur B contenant une copie de la base de données présente dans A, puisse prendre le relais. Tout cela est très complexe à gérer, en effet toute modification de la base de données présente sur l'ordinateur A doit entraîner la même modification de la base de données présente sur l'ordinateur B. Cette synchronisation entre A et B doit se faire le plus rapidement possible, il est fondamental d'avoir des copies parfaitement identiques en permanence. Ce sont aussi les SGBD qui assurent la maintenance des différentes copies de la base de données.
- plusieurs personnes peuvent avoir besoin d'accéder aux informations contenues dans une base données en même temps. Cela peut parfois poser problème, notamment si les 2 personnes désirent modifier la même donnée au même moment (on parle d'accès concurrent). Ces problèmes d'accès concurrent sont aussi gérés par les SGBD.
  
Comme nous venons de la voir, les SGBD jouent un rôle fondamental. L'utilisation des SGBD explique en partie la supériorité de l'utilisation des bases de données sur des solutions plus simples à mettre en œuvre; mais aussi beaucoup plus limitées comme les fichiers au format CSV.




---
Il en existe plusieurs, des gratuits, des payants, des libres, des propriétaires (nous en utiliserons dans le chapitre suivant).
---

lien bien 

[https://www.mathinfo.ovh/Terminale_NSI/B02_BDD_Relationnelles/](https://www.mathinfo.ovh/Terminale_NSI/B02_BDD_Relationnelles/)





-----

[https://nsijoliotcurie.fr/TNSI/index.php](https://nsijoliotcurie.fr/TNSI/index.php)




Avec la généralisation du traitement de volumes de données très importants, il a fallu développer de véritables Systèmes de Gestion de Base de Données (abrégé SGBD en français ou DBMS pour DataBase Management System en anglais).

<br/>

Ces logiciels spécialisés ont pour but de gérer une ou plusieurs bases de données. Ils permettent notamment de :

- gérer la lecture, l'écriture ou la modification des informations contenues dans une base de données ;
- gérer les autorisations d'accès à la base de données. Il est en effet souvent nécessaire de contrôler les accès par exemple en permettant à l'utilisateur A de lire et d'écrire dans la base de données alors que l'utilisateur B aura uniquement la possibilité de lire les informations contenues dans cette même base de données et que l’utilisateur C ne pourra travailler que sur une partie spécifique de la base de données.
- Gérer le stockage des fichiers de la base de données. Ces fichiers doivent pouvoir être sauvegardés sur plusieurs supports y compris lorsque la base de données est en fonctionnement.
- Gérer la redondance des données. Le système maintient plusieurs copies des données pour être capable de continuer à fonctionner si un des systèmes de stockage a une défaillance. Le SGBD se charge de créer et de synchroniser les copies ainsi que le basculement d’un système de stockage à l’autre en fonction del’état de fonctionnement et pour répartir la charge.
- Gérer les accès concurrents à la base de données. Plusieurs utilisateurs doivent pouvoir accéder simultanément à la base de données. C’est le SGBD qui gère les problèmes, notamment si 2 personnes désirent modifier la même donnée au même moment.
- Effectuer des transactions de bout en bout. Une transaction est une série d’opérations qui doivent être effectuées toutes de bout en bout ou bien pas du tout (c’est du tout ou rien). C’est ce qui se passe par exemple lorsqu’on utilise un moyen de paiement en ligne.

<br/>

Il existe aujourd’hui de très nombreux SGBD, les principaux étant [Oracle](https://www.oracle.com/database/), [MySQL](https://www.mysql.com/) (open source appartenant à
Oracle) et [Microsoft SQL server](https://www.microsoft.com/en-us/sql-server/).


<center>
![SGBD](images/SGBD.png){width="1100" loading="lazy"}
[https://db-engines.com/en/ranking_trend](https://db-engines.com/en/ranking_trend)
</center>


-----

Pour consulter cette base de données, l’utilisateur utilise un langage déclaratif, généralement le SQL (Structured Query Language – langage de requête structurée). Ce langage est standardisé mais chaque SGBD possède son SQL « maison » qui apporte quelques variantes.


En Terminale NSI, nous utiliserons SQLite.

<center>
![logo SQLite](images/logo_SQLite.png){width="50" loading="lazy"}
</center>

Ce SGBD à un fonctionnement simple, il est disponible sur tous les systèmes d’exploitation. Il existe même un module Python nommé sqlite3 qui permet de gérer la base de données à l’aide de commandes écrites en Python.


!!! note "Logiciel DB Browser"
    Afin de travailler au mieux sur les bases de données, il est conseillé (indispensable ?) d'installer sur vos machines personnelles [le logiciel DB Browser for SQLite](https://sqlitebrowser.org/) qui vous permettra de créer vos propres bases de données puis de les interroger à l'aide des requêtes SQL adéquates.

    <br/>

    <center>
    ![sqlite](images/DBBrowser.png){width="900" loading="lazy"}
    </center>

    <br/>

    Ce logiciel est disponible en version sans installation pour Windows en suivant [ce lien](https://download.sqlitebrowser.org/DB.Browser.for.SQLite-3.12.2-win64.zip).

----


[https://eskool.gitlab.io/tnsi/bdd/sgbd/](https://eskool.gitlab.io/tnsi/bdd/sgbd/)




<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script> 