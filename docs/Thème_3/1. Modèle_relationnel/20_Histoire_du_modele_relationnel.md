---
title : "Histoire du modèles relationnels"

---



# Modèles Relationnels


<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/iu8z5QtDQhY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>




## I.  Historique

<center>
![](./images/Edgar_F_Codd.jpg){width="180" loading="lazy"}
Edgard F. Codd. Source : wikimedia.org
</center>

<br/>

Lors de ses travaux chez IBM [Edgar Frank Codd](https://en.wikipedia.org/wiki/Edgar_F._Codd)   (1923-2003), insatisfait des modèles existant, chercha un modèle permettant de gérer un grand nombre de données. Mathématicien de formation, il se basa sur la théorie des ensembles et la logique des prédicats ([logique de premier ordre](https://fr.wikipedia.org/wiki/Calcul_des_pr%C3%A9dicats)) pour publier en 1970 un article où il proposait de stocker des données hétérogènes dans des tables, comme dans le schéma ci-dessous :


<br/>


<center>
![modèle relationnel](images/modeleRelationnel1.png){width="900" loading="lazy"}
[https://www.zonensi.fr/NSI/Terminale/C04/ModeleRelationnel/](https://www.zonensi.fr/NSI/Terminale/C04/ModeleRelationnel/)
</center>

<br/>

Ce modèle qualifié de relationnel est une manière de modéliser les relations existantes entre plusieurs informations et de les ordonner entre elles, ces grands principes sont :

!!! note "Les principes de base du modèle relationnel"

    - Les données sont regroupées dans différentes tables (qu'on appellera plutôt relations et qui donnent son nom au modèle). Chaque relation contient des éléments directement en lien avec le sujet général de la table.
    - Autant que possible, des données identiques ne doivent pas se trouver dans des tables différentes : on évite la redondance des données.
    - Les données ne doivent pas contenir elles-mêmes d'autres données : on parle d'atomicité des données.


<br/>

À l'époque où Edgar Frank Codd a proposer ce modèle, on le considéré comme une curiosité intellectuelle, car les ordinateurs en ce temps n'étaient pas capables de gérer de manière efficace des tables de données et il n'était pas évident qu'ils le soient un jour.

<br/>

Ce scepticisme n'a pas empêché Codd de poursuivre ses travaux et de concevoir un premier prototype de Sytème de Gestion de Bases de Données Relationnelles (SGBDR). Depuis les années 1980 cette technologie a mûri, pour être finalement adoptée par le monde de la recherche et des entreprises.

<br/>

En parallèle, Codd mis au point un langage de manipulation des données non-procédural, où l'utilisateur définit le résultat qu'il attend plutôt que la manière de l'obtenir. Ce langage nommé dans un premier temps SEQUEL (Structured English Query Language), rebaptisé par la suite SQL (Structured Query Language), fut adopté comme norme internationale par l'ISO en 1987.


??? note "Remarque"
    Edgar Frank Codd a reçu le prix Turing en 1981 pour ces travaux.


??? note "Remarque"
    Avec le Big Data et des usages spécifiques comme les réseaux sociaux ou l’intelligence artificielle, on observe cependant la montée en puissance d'autres modèles non relationnels
