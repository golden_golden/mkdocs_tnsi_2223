---
title : "Bases De Données"

---


# Bases De Données


<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/bhtzximjwk8?start=15&end=205" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[https://www.youtube.com/watch?v=bhtzximjwk8](https://www.youtube.com/watch?v=bhtzximjwk8)

</center>









## I. Introduction

Le développement de traitements informatiques nécessite la manipulation de données de plus en plus nombreuses. Leur organisation et leur stockage constituent un enjeu essentiel.

<br/>

*Comment gérer (mémoriser et traiter) efficacement un ensemble volumineux de données ?*

<br/>


En classe de Première, nous avons eu l'occasion de travailler sur des données structurées en les stockant de manière tabulaire (avec des fichiers CSV). Nous avons vu qu'il était possible d'utiliser un langage de programmation pour effectuer des traitements sur les données, mais nous avons également constaté que cette façon de faire ne convenait que pour des requêtes simples, lors que les données ne sont pas trop nombreuses, et qu'elle devient rapidement insuffisante pour répondre aux attentes actuelles.

La gestion et l'interrogation d'une grande quantité de données de nature très hétérogènes nécessite d'utiliser des solutions plus performantes, qu'un simple tableau. De nos jours, on utilise des structures plus complexes, les bases de données.

<br/>

Il est difficile de proposer une définition exacte des bases de données. On peut cependant proposer :

!!! note "Définition"
    Un ensemble organisé d'informations avec un objectif commun.

    *Laurent Audibert*

La définition est vague, mais dès lors que des informations sont rassemblées et stockées d'une manière organisée, on peut parler de bases de données, quel que soit le support utilisé (tablettes d'argile, papiers, fichiers, etc...)

<br/>

Dans le cadre informatique, une première proposition peut être :  

!!! note "Base de données informatisées"
    Une base de données (BDD,soit DB en anglais pour DataBase) informatisée est un ensemble structuré de données enregistrées sur des supports accessibles par l'ordinateur, représentant des informations du monde réel et pouvant être interrogées et mise à jour par une communauté d'utilisateurs.

    *Laurent Audibert*


Lorsque l'on s'intéresse aux bases de données, il est nécessaire de faire la distinction entre deux concepts : le modèles de données et la structures de données. Ces deux concepts se placent à différents niveaux d'abstraction.



!!! note "Modèles de données"
    Les modèles de données indiquent quelles caractéristiques d'une entité réelle on souhaite manipuler dans un programme, ainsi que les relations qui lient ces entités entre elles.

    
    *Par exemple, un groupe de musique va produire plusieurs albums et un chanteur peut chanter dans plusieurs groupes.*

    <br/>

!!! note "Structures de données"
    Une structure de donnée indique la manière dont on va organiser les données en machine. Ainsi pour un même modèle il existe plusieurs structures pouvant représenter ce modèle (tableau, listes chaînées, objets…)




<br/>

Il existe de nombreux modèles sur lequel reposent les bases de données, mais nous nous contenterons en Terminale NSI d'aborder le modèle relationnel, qui est un des modèles les plus répandue.

??? note "Remarque"

    Il existe des bases de données reposant sur d'autre type de modèle, comme :

    - le modèle réseaux : [les bases réseaux](https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es_r%C3%A9seau)
    - le modèle objets : [les bases objets](https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es_orient%C3%A9e_objet)
    - le modèle "no-sql" : [les bases « no-sql »](https://fr.wikipedia.org/wiki/NoSQL)
    - etc.

<br/>


Dans une base de données relationnelle, les informations sont structurées de façon à permette :

- d’insérer une (ou des) information(s) ;
- de supprimer une (ou des) information(s) ;
- de mettre à jour une (ou des) information(s) ;
- d’interroger les informations contenues *(avec des requêtes qui peuvent être complexes)*.


<br/>

??? note "Remarque"
    - Le volume des données peut être gigantesque (voir l'article :  [16000 malades oubliés à cause d'Excel](https://www.numerama.com/politique/653217-16-000-anglais-malades-du-covid-ont-ete-oublies-a-cause-dune-feuille-excel-trop-pleine.html));
    - les données peuvent être simultanément utilisées par différents programmes ou différents utilisateurs (exemples : sites marchands, réservations en ligne, etc.)

<br/>


!!! note "Un peu d'histoire"
    Avec les premiers ordinateurs les systèmes de stockage étaient des cartes perforées (pour les programmes ou de petites quantités de données) et les bandes magnétiques (pour les grandes quantités de données).

    <br/>

    Lorsqu’ils devaient manipuler beaucoup de données, les ordinateurs devaient donc lire une partie de la bande magnétique et la mettre en mémoire, traiter les données, puis retransférer le résultat sur bande avant de passer à la partie suivante.
    L’invention du disque dur par IBM en 1956, a permis d’accéder directement à de grandes quantités d’informations. Il n’y a alors plus la nécessité de traiter les données par lots comme avec les bandes magnétiques et un programme peut ainsi accéder potentiellement à l’ensemble des données.

    <br/>

    Le terme database (base de données) est apparu en 1964 pour désigner une collection d’informations partagées par différents utilisateurs d’un système d’informations militaire


   

<br/>

!!! note "Sous quelle forme sont stockées les données ?"
    Dans une base de données, les informations sont stockées dans des fichiers, mais à la différence des fichiers au format CSV, il est impossible de travailler avec ces fichiers directement avec un éditeur de texte. Pour manipuler les données présentes dans une base de données, il faut utiliser un logiciel appelé système de gestion de bases de données, abrégé SGBD *(Nous aborderons les SGBD dans une autre partie du cours)*.

