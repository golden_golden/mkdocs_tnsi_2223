---
title : "Le modèle relationnel"
correctionvisible : True

---


# Le Modèle Relationnel

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/bhtzximjwk8?start=205&playbackRate=2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
[https://www.youtube.com/watch?v=bhtzximjwk8](https://www.youtube.com/watch?v=bhtzximjwk8)
</center>


## 1. Représentation des Données en Tables/Relations

Le modèle relationnel consiste à **représenter les données dans des tableaux**, appelés ***Relations*** (ce qui donne son nom au modèle), ou ***Tables***, ou encore ***Entités***.


## 2. Enregistrements, Attributs et Champs

!!! info "Enregistrement"
    Dans une relation/table, chaque **ligne** d'une table est **un enregistrement** :fr: / un **record** :gb:, ou ***tuple*** :fr: :gb: ou ***t-uplet*** :fr: ou ***n-uplet*** :fr: ou ***vecteur*** :fr:

    * Il est **interdit** que deux enregistrements soient exactement identiques.
    * Le nombre d'enregistrements d'une relation/table s'appelle le ***Cardinal*** :fr: :gb: de la relation/table.

!!! info "Attiribut"
    Dans une relation/table, chaque **colonne** d'une table est appelée un un **attribut** :fr: / **attribute** :gb:

    * Le nombre de Colonnes d'une relation/table s'appelle le ***Degré*** de la relation/table


!!! info "Champ"
    Dans une relation/table, un ***Champ*** :fr: / ***Field*** :gb: est l'intersection d'un enregistrement avec un attribut : il s'agit donc d'**une cellule particulière** de la relation/table. Quelquefois néanmoins, par extension, le mot champ est utilisé à la place du mot attribut.

!!! example "Exemple"
    Considérons par exemple un site de VOD, qui loue des films en ligne pour 24h, dont la base de données possède une Relation/Table *film* :

    <center>
    ![](./images/enregistrements-attributs.png)
    Relation Film
    </center>

    Un autre **enregistrement** de la table *film* est, par exemple : `(426, Pulp Fiction, Tarantino, 2h29, 26/12/1994, 4.5)`




## 3. Domaine, Degré et Cardinal

!!! info "Domaine"
    Dans une relation/table, le **domaine** d'un attribut est le **type des données** qu'il contient, parmi les plus fréquents :

    - **entier** souvent noté `Int` ou `int`
    - **flottant** souvent noté `Float` ou `float`, 
    - **chaîne de caractères** souvent noté `String` ou `str` 
    - **booléen souvent noté `Boolean` ou `bool`
    - **date** souvent noté `date` ou `Timestamp` ou `timestamp`
    - etc…

!!! info "Degré"
    Le **degré** d'une relation/table est le nombre de champs/attributs.

!!! info "Cardinal"
    Le **cardinal** d'une relation/table est le nombre d’enregistrements.

!!! example "Site de VOD (suite)"
    
    <center>

    |film||||||
    |:-:|:-:|:-:|:-:|:-:|:-:|
    |Code|Titre|Réalisateur|Durée|Date de<br/>Sortie|Note<br/>Spectateurs|
    |142|Usual Suspects|Singer|1h46|19/07/1995|4.4|
    |426|Pulp Fiction|Tarantino|2h29|26/12/1994|4.5|
    | ... | ... | ... | ... | ... | ... |

    <figcaption>Relation/Table <b>film</b> du site VOD</figcaption>
    </center>

    Le **domaine** :

    - de l'attribut `Code` est `int`
    - de l'attribut `Titre` est `str`
    - de l'attribut `Réalisateur` est `str`
    - de l'attribut `Durée` est ... ça pourrait dépendre du logiciel de gestion de la base de données :
  
        * Soit il existe un type spécial pour les durées (TIME par exemple)
        * Soit on décide de stocker les durées en minutes *(ce dont nous conviendrons par la suite)*, auquel cas ce champ serait un entier `int`.
  
    - de l'attribut `Date de Sortie` est `Timestamp` *(un type spécifique pour les dates, encore une fois, en pratique cela pourrait dépendre du logiciel utilisé)*
    - de l'attribut `Note Spectateurs` est `Float`

    <br/>
  
    Le **degré** de la relation *utilisateur*  vaut $6$, car la relation/table *film* dispose de $6$ champs/attributs : `Code`, `Titre`, `Réalisateur`, `Durée`, `Date de Sortie`, `Note Spectateurs`


    Le **cardinal** de la relation *film* n'est pas accessible ici, car la table présenté n'est pas complète, il correspondrait au nombre de ligne (d'enregistrement) de la table.

## 4. Schéma Relationnel

!!! info "Schéma relationnel"
    Le **schéma** :fr: ou **schema** :gb: (relationnel) d'une relation/table est l'ensemble de :

    * ses **attributs** et 
    * des **domaines** associés (= types de données)

!!! example "Exemple"
    Le **schéma** relationnel de la table `film` est :
    
    `(Code:str, Titre:str, Réalisateur:str, Durée:int, Date de Sortie:Timestamp, Note Spectateurs:Float)`

## 5. Clés Primaires

### 5.1 Définition


Il est fréquent de souhaiter disposer dans une même table, d'un **identifiant unique** (**id**) pour chaque enregistrement :

!!! info "Clé Primaire"
    Une **Clé Primaire** d'une table est un attribut, ou bien un n-uplet d'attributs, qui permet d'**identifier de manière unique** (sans aucun risque d’ambiguïté) tout enregistrement de la table.

!!! example "Relation/Table des Personnes nées en France"
    <center>

    |Prénom|Nom|Ville de Naissance|Numéro de Sécurité Sociale|
    |:-:|:-:|:-:|:-:|
    |Laura|Breton|Marseille|2010845939026|
    |Paul|Dubreuil|Brest|1021384257228|
    |Leïla|Slimane|Lille|2991037214015|
    |..|..|..|..|

    Relation/Table *personne* (née en France)
    </center>  

    <br/>

    La clé primaire de la relation des personnes nées en France pourrait (par exemple) être leur [numéro de Sécurité Sociale](https://fr.wikipedia.org/wiki/Num%C3%A9ro_de_s%C3%A9curit%C3%A9_sociale_en_France).


    
<center>

|film||||||
|:-:|:-:|:-:|:-:|:-:|:-:|
|Code|Titre|Réalisateur|Durée|Date de<br/>Sortie|Note<br/>Spectateurs|
|142|Usual Suspects|Singer|1h46|19/07/1995|4.4|
|426|Pulp Fiction|Tarantino|2h29|26/12/1994|4.5|
| ... | ... | ... |... | ... | ... |

<figcaption>Relation/Table <b>utilisateur</b> du site VOD</figcaption>
</center> 
    
Dans un schéma relationnel (de table), pour signifier qu'un attribut (ou un tuple d'attributs) est la clé primaire de la table, on peut utiliser de manière équivalente (selon les auteurs et les exercices), les notations suivantes : 

* on peut **souligner** le ou les attributs : 
    * **Schéma** : film(<u>Code</u>:str, Titre:str, Réalisateur:str, Durée:int, Date de Sortie:Timestamp, Note Spectateurs:Float)  
    Dans cette notation, l'attribut `Code` est la clé primaire 

    * **Schéma** : film(<u>Code</u>:str, <u>Titre</u>:str, Réalisateur:str, Durée:int, Date de Sortie:Timestamp, Note Spectateurs:Float)  
    Dans cette notation, le couple d'attributs `(Code, Titre)` est la clé primaire

* on peut mettre un **astérisque** `*` devant le ou les attributs : 
    * **Schéma** : film(*Code:str, Titre:str, Réalisateur:str, Durée:int, Date de Sortie:Timestamp, Note Spectateurs:Float) 
  
* on peut mettre le ou les attributs **en gras**  : 
    * **Schéma** : film(**Code**:str, Titre:str, Réalisateur:str, Durée:int, Date de Sortie:Timestamp, Note Spectateurs:Float)  
  
* on peut mettre un **émoji clé** 🔑 (++ctrl+shift+u++ +$1F511$) devant le ou les attributs : 
    * **Schéma** : film(🔑Code:str, Titre:str, Réalisateur:str, Durée:int, Date de Sortie:Timestamp, Note Spectateurs:Float)
  
* On peut mettre un ***PK - Primary Key*** devant le ou les attributs
    * **Schéma** : film(*PK* Code:str, Titre:str, Réalisateur:str, Durée:int, Date de Sortie:Timestamp, Note Spectateurs:Float)


### 5.2 Choix de la Clé Primaire et attribut *id*

**Tout attribut de la Table, ou tout n-uplet de plusieurs attributs de la table**, peut être choisi comme clé primaire, **dès lors qu'il identifie de manière unique tout enregistrement de la table**.

<br/>

En pratique, lorsqu'aucun attribut de la relation ne se dégage naturellement pour être choisi comme clé primaire, l'architecte de la base de données définit fréquemment **mais non obligatoirement** un **Attribut `id`**, de domaine **entier**, comme clé primaire. Quelquefois noté `id_(ici le nom de la table)`.


Lorsqu'un attribut `id` existe et est le premier attribut de la table, il est sous-entendu, sauf mention contraire et bien que ce ne soit pas le seul choix possible, que c'est cet attribut `id` qui est choisi comme clé primaire de la table.

<br/>

En pratique, il est fréquent également de déléguer la gestion automatique des numéros d'`id` au logiciel de gestion de base de donnée, de façon à ce quelle s'auto-incrémente à chaque nouvel enregistrement dans la table, par exemple avec un `AUTOINCREMENT` dans MySQL.


!!! example "Choix d'une clé primaire dans la relation/table *film*"
    * Le choix le plus naturel est de choisir l'attribut `Code` comme clé primaire de la table *film*

    <center>

    |film||||||
    |:-:|:-:|:-:|:-:|:-:|:-:|
    |<u>Code</u>|Titre|Réalisateur|Durée|Date de<br/>Sortie|Note<br/>Spectateurs|
    |142|Usual Suspects|Singer|1h46|19/07/1995|4.4|
    |426|Pulp Fiction|Tarantino|2h29|26/12/1994|4.5|
    | ... | ... | ... |... | ... | ... |

    <figcaption>Relation/Table <b>film</b> avec une **clé primaire** <u>*Code*</u></figcaption>
    </center>  

    D'autres choix sont possibles, par exemple:

    - le Couple `(Code,Titre)` peut être choisi comme clé primaire
    - le Couple `(Code,Réalisateur)` peut être choisi comme clé primaire
    - etc.. (ici, tout tuple incluant `Code` sera convenable)
    - aucun autre attribut unique ne conviendra (par exemple : un `Réalisateur` peut faire plusieurs films, donc l'attribut `Réalisateur` n'identifie pas de manière unique un enregistrement de la relation *film*)
    - on aurait également pu ajouter un premier attribut `id`



!!! faq "Exercice"
    
    On décide que le site de VOD dispose des relations/tables supplémentaires suivantes: `zone_géographique`, `utilisateur`, `location`



??? faq "Question 1"

    <center>

    |zone<br/>géographique ||
    |:-:|:-:|
    | id | nom |
    | 1 | Castral-Roc |
    | 2 | Essos |
    | 3 | Port-Réal |
    | 4 | Westeros |
    | 5 | Winterfell |

    <figcaption>Relation/Table <b>zone_géographique</b></figcaption>
    </center>

    === "Énoncé"
        Déterminer des clés primaires possibles pour la relation *zone_géographique* du Site VOD :

    === "Correction"
        Des choix possibles pour la clé primaire de la relation `zone_géographique` sont les suivants :

        - l'attribut `id` est le choix le plus naturel de clé primaire de la relation `zone_géographique`
        - l'attribut `nom` est un choix possible de clé primaire de la relation `zone_géographique`
        - Le couple d'attributs `(id, nom)` est un choix possible de clé primaire de la relation `zone_géographique`



??? faq "Question 2"

    <center>

    |utilisateur |||||
    |:-:|:-:|:-:|:-:|:-:|
    | id | prenom | nom | email | id_zone |
    | 70 | Daenerys | TARGARYEN | daenerys.targaryen@gmail.com | 2 |
    | 45 | Jon | SNOW | jon.snow@gmail.com | 5 |
    | 38 | Cersei | LANNISTER | cersei.lannister@gmail.com | 3 |
    |  | ... | ... | ... |

    <figcaption>Relation/Table <b>utilisateur</b></figcaption>
    </center>

    === "Énoncé"
        Déterminer des clés primaires possibles pour la relation *utilisateur* du Site VOD :


    === "Correction"
        Des choix possibles pour la clé primaire sont les suivants

        - l'attribut `id` est le choix le plus naturel de clé primaire de la relation `utilisateur`
        - l'attribut `email` est un choix possible de clé primaire de la relation `utilisateur`
        - le couple `(id, email)` est un choix possible de clé primaire de la relation `utilisateur`
        - le couple `(id, prenom)` est un choix possible de clé primaire de la relation `utilisateur`
        - le couple `(prenom, email)` est un choix possible de clé primaire de la relation `utilisateur`
        - etc …


??? faq "Question 3"

    <center>

    |location|||||||
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    |id_utilisateur|date|Prénom|Nom|Titre|Réalisateur|Code|
    |70|15/03/2022|Daenerys|TARGARYEN|Troie|Petersen|650|
    |45|24/04/2022|Jon|SNOW|Lucy|Besson|305|
    |70|18/01/2023|Daenerys|TARGARYEN|Bright|Ayer|290|
    |...|...|...|...|...|...|...|

    <figcaption>Relation/Table <b>location</b> de films</figcaption>
    </center>

    === "Énoncé"
        Déterminer des clés primaires possibles pour la relation *location* du Site VOD :

    === "Correction"
        Des choix possibles pour la clé primaire sont les suivants

        - l'attribut `id_utilisateur` ne peut pas être choisi comme clé primaire, car un même utilisateur peut loueur plusieurs films sur le site 
        - l'attribut `Code` ne peut pas être choisi comme clé primaire, car un même (Code de) film peut être loué plusieurs fois sur le site
        - l'attribut `date` ne peut pas être choisi comme clé primaire, car plusieurs films peuvent être loués le même jour sur le site 
        - l'attribut `Prénom` (resp. `Nom`) ne peut pas être choisi comme clé primaire, car plusieurs personnes portant le même `Prénom` (resp. `Nom`) peuvent louer des films sur le site 
        -st le choix le plus naturel de clé primaire de la relation `acteur`
        - l'attribut `Titre` ne peut pas être choisi comme clé primaire, car un même (Titre de) film peut être loué plusieurs fois sur le site
        - etc…
        - le couple d'attributs `(id_utilisateur, Code)` ne peut pas être choisi comme clé primaire, car un même utilisateur peut louer plusieurs fois le même (Code de) film sur le site
        - le couple d'attributs `(date, Code)` ne peut pas être choisi comme clé primaire, car un même (Code de) film peut louer plusieurs fois le même jour sur le site
        - le 3-uplet d'attributs `(id_utilisateur, date, Code)` **peut être choisi comme clé primaire**, car un même (Code de) film ne peut être loué qu'une seule fois, le même jour, par un même utilisateur sur le site (nous avions dit que les locations de films duraient 24H…)


## 6. Clés Étrangères. Notion de Relation entre Tables

Nous avons vu que :

* l'attribut `id_utilisateur` **n'est pas** la clé primaire de la relation `location` précédente
* l'attribut `id_utilisateur` **est** la clé primaire de la relation `utilisateur`

On dit dans ce cas que `id_utilisateur` est une **clé étrangère** de la relation `location`, car elle fait référence à une clé primaire (`id`) d'une autre table/relation (`utilisateur`)

!!! info "Clés Étrangères"
    * Une **clé étrangère** est un attribut dans une relation/table, qui fait référence à la **clé primaire d'une** ***autre*** **relation/table**.
    * Une clé étrangère permet de mettre en relation un enregistement d'une table (appelée **table fille**) qui le contient, avec un enregistrement de la table (appelée **table parent**) auquel il fait référence
    * Dans ce dernier cas, on dit qu'**il existe une Relation entre ces deux Tables** (entre la table fille et la table mère)

!!! example "de Clé Étrangère dans la Table utilisateur"
    <center>

    |utilisateur |||||
    |:-:|:-:|:-:|:-:|:-:|
    | **id** | prenom | nom | email | id_zone |
    | 70 | Daenerys | TARGARYEN | daenerys.targaryen@gmail.com | 2 |
    | 45 | Jon | SNOW | jon.snow@gmail.com | 5 |
    | 38 | Cersei | LANNISTER | cersei.lannister@gmail.com | 3 |
    | ... | ... | ... | ... | ... |

    <figcaption>Relation/Table <b>utilisateur</b> avec une **clé primaire** <em><b>id</b></em><br/> et une **clé étrangère** <em><b>id_zone</b></em></figcaption>

    </center>

    On a mis en relation deux tables en considérant que l'attribut ***id_zone*** de la table ***utilisateur*** correspond à l'attribut ***id*** de la table ***utilisateur***


Dans un schéma relationnel (de table), pour signifier qu'un attribut (ou un tuple d'attributs) est une clé étrangère d'une table, on **ajoute usuellement un hashtag #** ou ***FK - Foreign Key*** devant le ou les attributs. 



!!! example ""

    - **Schéma** de la table *utilisateur* :
        * utilisateur(<u>id_utilisateur</u>, prenom, nom, email, #id_zone)  
        * utilisateur(*id_utilisateur, prenom, nom, email, #id_zone)  
        * utilisateur(🔑id_utilisateur, prenom, nom, email, #id_zone)  
        * utilisateur(*PK* id_utilisateur, prenom, nom, email, *FK* id_zone)


    Dans cette notation :
    - l'attribut `id_zone` est une clé étrangère.

    <br/><br/>

    - **Schéma**  de la table *location* : 
        * location(#<u>id_utilisateur</u>, <u>date</u>, Prénom, Nom, Titre, Réalisateur, #<u>Code</u>)
    
    Dans cette notation :

    - l'attribut `id_utilisateur` est une clé étrangère de la relation/table (fille) `location` faisant référence à la clé primaire `id` de la relation/table (mère) `utilisateur`
    - l'attribut `Code` est une clé étrangère de la relation/table (fille) `location` faisant référence à la clé primaire `Code` de la relation/table (mère) `film`
    - Noter que la notation ne permet pas de lever l’ambiguïté suivante: Cela pourrait signifier que le couple d'attributs `(id_zone, type)` est une clé étrangère d'une autre relation/table (ici, ce n'est pas le cas). Néanmoins, comme on tente souvent de choisir des clés primaires qui soient simples, cette situation d’ambiguïté se produit rarement.

    <br/><br/>

    **Interprétation d'un Enregistrement**

    - ***Daenerys TARGARYEN*** habite à **Wessos**, car son `id_zone` vaut 3
    - ***Jon SNOW*** habite à **Winterfell**, car son `id_zone` vaut 5
    - etc…


## 7. Redondance et contraintes sur les données  

### 7.1 Redondance des données

La relation *location* contient des informations qui sont déjà disponibles dans d'autres relations : on dit qu'elle est **redondante** et c'est quelque chose qu'il faut éviter. À la fois pour des raisons d'espace de stockage, mais aussi de **cohérence** : si une modification doit être faite *(un utilisateur change de prénom, ou de nom de famille, etc…)* cette modification ne doit être faite qu'à un seul endroit de notre base de données.


!!! example "Exemple"
    Une version non-redondante de la relation *location* serait donc celle-ci :

    <center>

    |location|||
    |:-:|:-:|:-:|
    |id_utilisateur|date|Code|
    |70|15/03/2022|650|
    |45|24/04/2022|305|
    |70|18/01/2022|290|
    |...|...|...|

    <figcaption>Relation/Table <b>location</b> de films, NON REDONDANTE</figcaption>
    </center>



### 7.2 Contraintes d'intégrité

#### A. Contrainte de domaine

Tout attribut d'un enregistrement doit respecter le domaine indiqué dans le schéma relationnel.

<br/>

Attention, certains domaines sont subtils. Par exemple, si une relation possède un attribut "Code Postal", le domaine de cet attribut devra être ```String``` plutôt que ```Entier```. Dans le cas contraire, un enregistrement possédant le code postal ```03150``` serait converti en ```3150``` (car pour les entiers, 03150 = 3150). Or le code postal ```3150``` n'existe pas.

#### B. Contrainte de relation

La contrainte de relation/table impose que tout enregistrement soit unique : cette contrainte est réalisée par l'existence obligatoire d'une clé primaire.

<br/>

Cette clé primaire est souvent créée de manière artificielle (voir `id_utilisateur` dans la table `location` ci-dessus par exemple).

#### C. Contrainte de référence

La cohérence entre les différentes tables d'une base de données est assurée par les clés étrangères : dans une table, la valeur d'un attribut qui est clé étrangère doit obligatoirement pouvoir être retrouvée dans la table dont cet attribut est clé primaire. La référence ne peut pas mener nulle part !

<br/>
!!! example "Example"
    Par exemple, la relation *location_v2* ci-dessous n'est pas valable :

    <center>

    |location_v2|||
    |:-:|:-:|:-:|
    |id_utilisateur|date|Code|
    |70|15/03/2022|650|
    |45|24/04/2022|305|
    |70|18/01/2022|290|
    |38|30/06/2022|<rb>387</rb>|
    |...|...|...|

    <figcaption>Relation/Table <b>location</b> de films, Non redondante, mais Contrainte de Référence Non Respectée</figcaption>
    </center>

    <br/>

    En effet, la valeur <rb>387</rb> de l'attribut `Code` (clé étrangère de la table *location_v2*) ne correspond à aucun enregistrement dans la table `film` dont il est clé primaire. La relation *location_v2* ne respecte donc pas la contrainte de référence et provoquerait une erreur du SGBD.
    
    
    <center>
    
    |film||||||
    |:-:|:-:|:-:|:-:|:-:|:-:|
    |Code|Titre|Réalisateur|Durée|Date de<br/>Sortie|Note<br/>Spectateurs|
    |142|Usual Suspects|Singer|1h46|19/07/1995|4.4|
    |426|Pulp Fiction|Tarantino|2h29|26/12/1994|4.5|
    |305|Lucy|Besson|1h29|6/08/2014|2.9|
    |310|Inception|Nolan|2h28|21/07/2010|4.5|
    |128|Her|Jonze|2h06|19/03/2014|4.1|
    |54|Jason Bourne|Greengrass|2h04|10/08/2016|3.4|
    |84|Un Homme à la Hauteur|Tirard|1h39|04/05/2016|3.0|
    |63|Titanic|Cameron|3h14|07/01/1998|4.3|
    |380|Le Nom de la Rose|Annaud|2h11|17/12/1986|4.2|
    |650|Troie|Petersen|2h35|13/05/2004|3.6|
    |290|Bright|Ayer|1h58|10/04/2020|3.1|
    |589|Django Unchained|Tarantino|2h44|16/01/2013|4.5|
    |740|Salt|Noyce|1h41|25/08/2010|2.9|
    |170|Narco|Lellouche|1h45|1/12/2004|2.6|
    |528|La Môme|Dahan|2h20|14/02/2007|3.8|
    |137|La Liste de Schindler|Spielberg|3h15|02/03/1994|4.6|
    |208|Le Parrain|Coppola|2h55|18/10/1972|4.5|
    |700|Rain Man|Levinson|2h13|15/03/1989|4.3|
    
    <figcaption>Relation/Table <b>film</b></figcaption>
    </center>




## 8. Conception et représentation d'une base de données

### 8.1 Première Schématisation des Relations entre Tables

On peut schématiser simplement la relation existant entre les tables `utilisateur` et `zone_géographique` précédentes (utilisateur et abonnement) comme suit:

!!! col _3 center
    ```mermaid
    classDiagram
        class utilisateur {
        🔑 id
        prenom
        nom
        email
        🔑 id_zone
        }
        class zone_géographique {
        🔑 id_zone
        nom
        }
        utilisateur -- zone_géographique
    ```

    <figcaption>
    <b>certains logiciels</b> notent 🔑 pour <br/>
    les <b>clés primaires</b> <rb>et/ou</rb> les <b>clés étrangères</b>
    </figcaption>

!!! col _3 center
    ```mermaid
    classDiagram
        class utilisateur {
          * id
          prenom
          nom
          email
          # id_zone
        }
        class zone_géographique {
          * id_zone
          nom
        }
        utilisateur -- zone_géographique
    ```

    <figcaption>
    Autres Notations Possibles : <br/>
    <enc>*** Clé primaire**</enc> ou <enc>**PK (Primary Key) Clé primaire**</enc> <br/><enc><bblue># Clé étrangère</bblue></enc> ou <enc><bblue>FK (Foreign Key) Clé étrangère</bblue></enc>
    </figcaption>

!!! col _3 center clear
    ```mermaid
    classDiagram
        class utilisateur {
          * id
          # id_zone
          -----------------
          prenom
          nom
          email
        }
        class zone_géographique {
          * id_zone
          nom
        }
        utilisateur -- zone_géographique
    ```

    <figcaption>
    Pour plus de clarté, il <em><b>peut arriver</b></em> <br/> qu'on réunisse "en haut" de la table (préalablement et séparément) <br/><b>la clé primaire</b> et <b>la/les clés étrangères</b> :<br/> nous ne le ferons pas.
    </figcaption>






??? example "Exemple plus complet"
    Considérons la base de données Tour de France 2020, contenant les relations suivantes :
    (d'après une idée de [Didier Boulle](http://webtic.free.fr/sql/mldr.htm))

    <br/>

    **Schéma relationnels :**

    - Equipes ( <ins>codeEquipe</ins>  ```String```,  nomEquipe ```String``` )
    - Coureurs ( <ins>dossard</ins>  ```Int```,  nomCoureur ```String```,  prénomCoureur ```String```,   codeEquipe* ```String``` )
    - Étapes ( <ins>numéroEtape</ins>  ```Int```,  villeDépart ```String```,  villeArrivée ```String```,   km ```Int``` )
    - Temps ( <ins>dossard</ins>*  ```Int```,  <ins>numéroEtape</ins> *  ```Int```,  tempsRéalisé ```String```)

    Remarquez que la clé primaire de cette relation est le couple dossard-numéroEtape.
 
    <br/>

    ??? note "Relations" 

        **Relation : Équipes**

        | codeEquipe | nomEquipe                      |
        |------|-----------------------------|
        | ALM  |  AG2R La Mondiale           |
        | AST  |  Astana Pro Team            |
        | TBM  |  Bahrain - McLaren          |
        | BOH  |  BORA - hansgrohe           |
        | CCC  |  CCC Team                   |
        | COF  |  Cofidis, Solutions Crédits |
        | DQT  |  Deceuninck - Quick Step    |
        | EF1  |  EF Pro Cycling             |
        | GFC  |  Groupama - FDJ             |
        | LTS  |  Lotto Soudal               |
        |  …   |  …                        |

        <br/>

        **Relation : Coureurs**

        | dossard | nomCoureur  | prénomCoureur | codeEquipe |
        |---------------|-------------|---------------|------------|
        | 141           | LÓPEZ       | Miguel Ángel  | AST        |
        | 142           | FRAILE      | Omar          | AST        |
        | 143           | HOULE       | Hugo          | AST        |
        | 11            | ROGLIČ      | Primož        | TJV        |
        | 12            | BENNETT     | George        | TJV        |
        | 41            | ALAPHILIPPE | Julian        | DQT        |
        | 44            | CAVAGNA     | Rémi          | DQT        |
        | 45            | DECLERCQ    | Tim           | DQT        |
        | 121           | MARTIN      | Guillaume     | COF        |
        | 122           | CONSONNI    | Simone        | COF        |
        | 123           | EDET        | Nicolas       | COF        |
        | …             | …           | …             | …          |


        <br/>

        **Relation : Étapes**

        | numéroEtape | villeDépart | villeArrivée      | km  |
        |-------------|-------------|-------------------|-----|
        | 1           | Nice        | Nice              | 156 |
        | 2           | Nice        | Nice              | 185 |
        | 3           | Nice        | Sisteron          | 198 |
        | 4           | Sisteron    | Orcières-Merlette | 160 |
        | 5           | Gap         | Privas            | 198 |
        | ...         | ...         | ...               | ... |


        <br/>


        **Relation : Temps**

        | dossard | numéroEtape | tempsRéalisé |
        |:-------------:|:-----------:|:------------:|
        | 41            | 2           | 04:55:27     |
        | 121           | 4           | 04:07:47     |
        | 11            | 5           | 04:21:22     |
        | 122           | 5           | 04:21:22     |
        | 41            | 4           | 04:08:24     |
        | ...           | ...         | ...          |



    **Représentation graphique**

    ![](images/schema_tdf.png)


!!! warning "Limites de cette Schématisation"
    La schématisation précédente, sous forme de **Diagramme**, est quelquefois suffisante pour modéliser la base de données. Du moins pour un premier jet. Il arrive néanmoins qu'elle trouve ses limites, car trop simpliste, en effet, il lui manque de prendre en compte les différents **types de relations** pouvant exister entre tables :

    * Relation 1 to 1
    * Relation 1 to n
    * Relation n to n


### 8.2 Conception et représentation d'une base de données
Pour concevoir une base de données, on utilise généralement le modèle entité-association.

Imaginons par exemple que l'on souhaite écrire une base de données pour un garage qui répare et entretien des véhicules.

<br/>

La base doit pouvoir contenir les clients ainsi que les véhicules. Chaque **client** peut *posséder* un ou plusieurs **véhicules**. Le garage emploie des **collaborateurs** qui peuvent *réaliser* des **interventions** sur les véhicules. À chaque intervention sur un véhicule, on peut être amené à *commander* des **pièces** qui *correspondent* au véhicule réparé ou entretenu.

Dans l'exemple précédent, les mots en gras représentent les entités de la base et ceux en italique (les verbes d'actions) représentent les associations. On peut alors réaliser un schéma entité-association pour la base de données garage :

<br/>

<center>
![](images/base_garrage.png)
Modèle entité-association de la base garage
</center>

<br/>

On peut ensuite lister toutes les caractéristiques des entités dont on aura besoin dans la base. Par exemple un client doit avoir un nom, prénom, adresse, numéro de téléphone et email pour pouvoir le contacter et éditer sa facture. Un véhicule doit avoir un numéro d'imatriculation, un marque et un type…


On affine ainsi le schéma entité-association pour otenir un Modèle Conceptuel des Données. Ce schéma ressemble beaucoup au schéma précédent mais on a ajouté les attribus de chaque entité ainsi que la **cardinalité** de chaque association.


<br/>

Les principales cardinalités sont les suivantes : 

![](images/cardinalite.png)


<br/>


Quelques exemples de modèles conceptuels des données pour comprende les cardinalités :


![](images/Diagramme_cardinalite.png)


??? faq "Application"
    Représenter un modèle conceptuel des données pour la base de données garage.

On remarque que dans ce modèle, chaque entité correspond à une relation du schéma relationnel. De même les caractéristiques des entités correspondent aux attributs des tables.

Pour se rapprocher encore du schéma relationnel de la base de données, On peut préciser els domaine de chaque attribut et transformer encore du schéma relationnel de la base de données, on peut préciser le domaine de chaque attribut et transformer les associations en clés étrangères d'une tables vers une autre. On obtient alors le Modèle Physique de Données à partir duquel on peut êcrire le schéma relationnel complet et définir complètement notre base de données.

<br/>

Le modèle physique des donées de notre base d'exemple pourrait donner ceci :

![](images/Represantation_base_eleve.png)

<br/>
La vue affiché par phpmyadmin
<center>
![](images/Represantation_base_eleve_mysql.png)
</center>



<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  