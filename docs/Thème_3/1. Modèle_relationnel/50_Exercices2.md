---
title : "Exercices sur le modèle relationnel"
correctionvisible : True

---

# Exercices sur le modèle relationnel

<center>
<iframe src="../pdf/Exercices modèle relationnel.pdf" height="800" width="1200"></iframe>
</center>



<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  