---
title : "Coder sans installations"

---

------À traiter
# Pour coder sans installations

En ligne, sans aucune installation, vous pouvez utiliser :

- La page que vous êtes en train de lire !
- Basthon : [https://console.basthon.fr/](https://console.basthon.fr/)




## IDE

{{ IDEv() }}

## Console

{{ terminal() }}




---- À faire

??? note "Limites des solutions en lignes"
    À faire




??? note "Sources"
    [https://glassus.github.io/terminale_nsi/T7_Divers/3_Thonny/cours/](https://glassus.github.io/terminale_nsi/T7_Divers/3_Thonny/cours/)