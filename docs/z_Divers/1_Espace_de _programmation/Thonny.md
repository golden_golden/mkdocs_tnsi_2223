---
title : "Thonny"

---


# Thonny

## 1. Présentation
Thonny à l'avantage d'être relativement simple. Il est donc conseiller pour l'enseignement de la NSI, notamment pour le niveau première.

![image](../assets/install_thonny.png){: .center}


## 2. Installer Thonny

- 1. Rendez vous sur la page officielle : [https://thonny.org/](https://thonny.org/)

- 2. Téléchargez et installez la version qui correspond à votre système d'exploitation (Windows, Mac, Linux).
   

## 3. Outils de débogage


## 4. Quelques plugins
[https://www.zonensi.fr/Miscellanees/Pygame/InstallationPygame/](https://www.zonensi.fr/Miscellanees/Pygame/InstallationPygame/)

??? note "Sources"
    [https://glassus.github.io/terminale_nsi/T7_Divers/3_Thonny/cours/](https://glassus.github.io/terminale_nsi/T7_Divers/3_Thonny/cours/)