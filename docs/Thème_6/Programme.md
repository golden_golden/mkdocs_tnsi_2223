---
title : "Programme"

---

# Algorithmique

*Extrait du programme*

Le travail de compréhension et de conception d’algorithmes se poursuit en terminale notamment via l’introduction des structures d’arbres et de graphes montrant tout l’intérêt d’une approche récursive dans la résolution algorithmique de problèmes.

On continue l’étude de la notion de coût d’exécution, en temps ou en mémoire et on montre l’intérêt du passage d’un coût quadratique en n2 à n log2 n ou de n à log2 n. Le logarithme en base 2 est ici manipulé comme simple outil de comptage (taille en bits d’un nombre entier).

<br>

<table>
    <thead>
        <tr>
            <th >Contenus</th>
            <th >Capacités attendues</th>
            <th >Commentaires</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Algorithmes sur les arbres binaires et sur les arbres binaires de recherche.</td>
            <td>Calculer la taille et la hauteur d’un arbre.<br>
            Parcourir un arbre de différentes façons (ordres infixe, préfixe ou suffixe ; ordre en largeur d’abord).<br>
            Rechercher une clé dans un arbre de recherche, insérer une clé.</td>
            <td>Une structure de données récursive adaptée est utilisée.<br>
            L’exemple des arbres permet d’illustrer la programmation par classe.<br>
            La recherche dans un arbre de recherche équilibré est de coût logarithmique.</td>
        </tr>
        <tr>
            <td>Algorithmes sur les graphes.</td>
            <td>Parcourir un graphe en profondeur d’abord, en largeur d’abord.<br>
            Repérer la présence d’un cycle dans un graphe.<br>
            Chercher un chemin dans un graphe.</td>
            <td>Le parcours d’un labyrinthe et le routage dans Internet sont des exemples d’algorithme sur les graphes.<br>
            L’exemple des graphes permet d’illustrer l’utilisation des classes en programmation.</td>
        </tr>
        <tr>
            <td>Méthode « diviser pour régner ».</td>
            <td>Écrire un algorithme utilisant la méthode « diviser pour régner »</td>
            <td>La rotation d’une image bitmap d’un quart de tour avec un coût en mémoire constant est un bon exemple.<br>
            L’exemple du tri fusion permet également d’exploiter la récursivité et d’exhiber un algorithme de coût en n log2 n dans les pires des cas.</td>
        </tr>
        <tr>
            <td>Programmation dynamique.</td>
            <td>Utiliser la programmation dynamique pour écrire un algorithme.</td>
            <td>Les exemples de l’alignement de séquences ou du rendu de monnaie peuvent être présentés.<br>
            La discussion sur le coût en mémoire peut être développée.</td>
        </tr>
        <tr>
            <td>Recherche textuelle.</td>
            <td>Étudier l’algorithme de Boyer-Moore pour la recherche d’un motif dans un texte.</td>
            <td>L’intérêt du prétraitement du motif est mis en avant.<br>
            L’étude du coût, difficile, ne peut être exigée.</td>
        </tr>
    </tbody>
</table>