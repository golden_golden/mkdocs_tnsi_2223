---
title : "Examples"

---



# Diviser pour régner



## 2- Études de quelques algorithmes utilisant la stratégie diviser pour régner

### 2.1- La multiplication du paysan russe

#### A. Présentation

La méthode du paysan russe est un très vieil algorithme de multiplication de deux nombres entiers déjà décrit (sous une forme légèrement différente) sur un papyrus égyptien rédigé vers1650 av. J.-C. Il s’agissait de la principale méthode de calcul en Europe avant l’introduction des chiffres arabes, et les premiers ordinateurs l’ont utilisé avant que la multiplication ne soit directement intégrée dans le processeur sous forme de circuit électronique.

<br/>

La multiplication dite « du paysan russe » ramène une multiplication de deux entiers naturels à une suite d’additions, de multiplications ou de divisions par 2. Elle est basée sur la propriété suivante : si a,b∈N alors

<br/>

<center>
<img src="./../images/a1.png">
</center>

<br/>


#### B. Implémentation en Python  

**Algorithmes de la multiplication russe**

```python
def multiplication_russe(a, b):
    if a == 0:
        return 0
    if a % 2 == 0:
        return multiplication_russe(a / 2, 2 * b)
    else:
        return multiplication_russe(a - 1, b) + b
```



**Algorithmes naïf de la multiplication**

```python
def multiplication_naif(x, y):
    if y == 1:
        return x
    else:
        return x + multiplication_naif(x, y - 1)
```




#### C. Performances

La complexité de l’Algorithmes naïf pour la multiplication de deux nombres à n chiffres est de O(n^2)

L ‘algorithmes de la multiplication du paysan russe permet en séparent les traitements de réduire le nombre de calcul.



Par exemple :

<br/>

<center>
<img src="./../images/a2.png">
</center>

<br/>

La multiplication russe à une complexité de O(log_2 n).





### 2.2 - Exponentiation rapide


#### A. Présentation

En informatique, l'exponentiation rapide est un algorithme utilisé pour calculer rapidement, de grandes puissances entières. En anglais, cette méthode est aussi appelée *square-and-multiply* (« mettre au carré et multiplier »).

<br/>

<center>
<img src="./../images/a3.png">
</center>

<br/>


#### B. Implémentation en Python  

**Algorithmes de l’exponentiation rapide**

```python
def puissance(x, n):
    if n == 1:
        return x
    if n % 2 == 0:
        return puissance(x ** 2, n / 2)
    else:
        return x * puissance(x ** 2, (n - 1) / 2)
```



**Algorithmes naïf d’une exponentiation **

```python
def puissance_naif(x, n):
    if n == 1:
        return x
    else:
        return x * puissance_naif(x, n - 1)
```



#### C. Performances

```python
t1= time.time()
puissance_naif(9**1000,290)
print("Durée algorithme naïf :",time.time()-t1)

t2 = time.time()
puissance(9**1000,290)
print("Durée algorithme rapide :",time.time()-t2)
```

```
Durée algorithme naïf : 1.3314800262451172
Durée algorithme rapide : 0.15516042709350586
```



*Nous ne détaillerons pas ici le calcul de la complexité.*

En comparant à la méthode ordinaire qui consiste à multiplier *x* par lui-même *n* – 1 fois, cet algorithme nécessite de l'ordre de 

O (log *n*) multiplications et ainsi accélère le calcul de *x^n* de façon spectaculaire pour les grands entiers.





### 2.3 - Recherche par dichotomie

#### A. Principe

La recherche dichotomique est un algorithme qui permet de traiter efficacement des données représentées dans un tableau de façon ordonnée.

L’idée centrale de cette approche repose sur l’idée de réduire de moitié l’espace de recherche à chaque étape : on regarde la valeur du milieu et si ce n’est pas celle recherchée, on sait qu’il faut continuer de chercher dans la première moitié ou dans la seconde. Plus précisément, en tenant compte du caractère trié du tableau, il est possible d’améliorer l’efficacité d’une telle recherche de façon conséquente en procédant ainsi : 

1. on détermine l’élément m au milieu du tableau ; 

2. si c’est la valeur recherchée, on s’arrête avec un succès ; 

3. sinon, deux cas sont possibles : 

   a. si m est plus grand que la valeur recherchée, comme la tableau est trié, cela signifie qu’il suffit de continuer à chercher dans la première moitié du tableau ; 

   b. sinon, il suffit de chercher dans la moitié droite. 

4. on répète cela jusque avoir trouvé la valeur recherchée, ou bien avoir réduit l’intervalle de recherche à un intervalle vide, ce qui signifie que la valeur recherchée n’est pas présente. 

   

À chaque étape, on coupe l’intervalle de recherche en deux, et on en choisit une moitié. On dit que l’on procède par dichotomie, du grec *dikha* (en deux) et *tomos* (couper). On peut trouver un exemple animé de l’exécution de cet algorithme à l’adresse


<br/>

<center>
<img src="./../images/a4.png">
</center>

<br/>

#### B. Implémentation

On renvoie un entier positif ou nul en cas de succès, qui correspond à une position de la valeur recherchée dans la tableau, et -1 en cas d’échec.

**Version itérative**

```python
def recherche_dichotomique(element, liste_triee):
    id_debut = 0
    id_fin = len(liste_triee) - 1
    id_milieu = (id_debut + id_fin) // 2
    while id_debut < id_fin:
        if liste_triee[id_milieu] == element:
            return id_milieu
        elif liste_triee[id_milieu] > element:
            id_fin = id_milieu - 1
        else:
            id_debut = id_milieu + 1
        id_milieu = (id_debut + id_fin) // 2
    return -1
```



**Version récursive**

```python
def recherche_dichotomique_recursive(element, liste_triee, id_debut=0, id_fin=-1):
    if id_fin == -1:
        id_fin = len(liste_triee) - 1
    id_milieu = (id_debut + id_fin) // 2

    if id_debut == id_fin:
        if liste_triee[id_debut] == element:
            return id_debut
        else:
            return -1
    if liste_triee[id_milieu] == element:
        return id_milieu
    elif liste_triee[id_milieu] > element:
        return recherche_dichotomique_recursive(element, liste_triee, id_debut, id_milieu - 1)
    else:
        return recherche_dichotomique_recursive(element, liste_triee, id_milieu + 1, id_fin)
```

Pour mieux comprendre les différences de ces algorithmes, vous pouvez  observer le déroulement de l'exécution sur [PythonTutor](http://www.pythontutor.com/).



**Algorithme naïf (itératif)**

```python
def recherche_itnaif(element, liste_triee):
    for i in liste_triee:
        if liste_triee[i] == element:
            return i
    return -1
```



**Algorithme naïf (récursif)**

```python
def recherche_rfnaif(element, liste_triee):
    id_liste = len(liste_triee)
    if id_liste == 0:
        return -1
    if liste_triee[-1] == element:
        return id_liste
    else:
        return recherche_rfnaif(element, liste_triee[:-1])
```



#### C. Performance

**Pour les versions naïves**

Comme tout algorithme ayant cette forme, la complexité est linéaire : le temps de recherche double lorsque la longueur de la liste double. Mais avec cette méthode, on n’exploite pas le caractère ordonné du tableau, ce qui fait savoir que telle valeur du tableau n’est pas la valeur recherchée n’apprend absolument rien sur les autres valeurs du tableau.



**Pour les versions dichotomiques**

Supposons que l’on doive effectuer une recherche dans un tableau trié contenant 174 valeurs (un annuaire, par exemple). En procédant par dichotomie, on regarde la valeur au milieu et suivant la cas, on s’arrête ou l’on continue la recherche dans l’une des deux moitiés restantes. En négligeant la valeur supprimée, les moitiés contiennent chacune 174/2 = 87 valeurs. La fois suivante, si l’on n’a pas trouvé la valeur recherchée, on continue de sélectionner l’une des moitiés de ce qui reste, qui contiennent 43 ou 44 valeurs. 

<br/>

Le processus se poursuit jusqu’à n’avoir au plus qu’une valeur : 

<br/>

<center>
<img src="./../images/a5.png">
</center>

<br/>

Pour pouvoir majorer le nombre maximum d’itérations, si le tableau contient l valeurs, et si on a un entier n tel que l ≤ 2^n , alors puisque qu’à chaque itération, on sélectionne une moitié de ce qui reste, au bout d’une itération, une moitié de tableau aura au plus 2^n / 2 = 2^(n−1) éléments, un quart aura au plus 2^(n−2) et au bout de k itérations, la taille de ce qui reste à étudier est de taille au plus 2^(n−k) . 

<br/>

En particulier, si l’on fait n itérations, il reste au plus 2^(n−n) = 1 valeur du tableau à examiner. On est sûr de s’arrêter cette fois-ci. On a donc montré que si l’entier n vérifie l ≤ 2^n , alors l’algorithme va effectuer au plus n itérations. La plus petite valeur est obtenue pour 
$$
n = ⌈log_2 l⌉
$$
Ainsi, la complexité de la fonction est de l’ordre du logarithme de la longueur de la liste.

<br/>

Pour être un peu plus précis dans notre raisonnement, en prenant compte de la valeur que l’on teste (et que l’on n’a donc pas besoin de concerver), il est plus adapté de majorer l par un entier de la forme 2^(n −1). L’exemple d’exécution présenté figure 1 indique cette majoration.

<br/>

**Comparaisons du temps d’exécution pour mille recherche en fonction de la taille de la liste.**

<br/>

<center>
<img src="./../images/a6.png">
</center>

<br/>

On constate que pour des listes très grande (> 100 000), l’écart entre la recherche linéaire et la recherche dichotomique est vraiment très conséquent.



#### D. En savoir plus sur cet algorithme (rappel de première)

Pour en savoir plus sur cet algorithme, notamment sur les notions de terminaison, etc …

cf. fiche Eduscol : https://cache.media.eduscol.education.fr/file/NSI/76/3/RA_Lycee_G_NSI_algo-dichoto_1170763.pdf

https://www.youtube.com/watch?v=JdwWMnU04pQ

<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/JdwWMnU04pQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>




### 2.4 - Trie fusion

#### A. Principe

l’algorithme du trie par fusion, appelée "merge sort" en anglais, suit fidèlement la méthodologie diviser pour régner. Intuitivement, il agit de la manière suivante : 

- **Diviser :** Diviser la suite de n éléments à trier en deux sous-suites de n / 2 éléments chacune.
- **Régner :** Trier les deux sous-suites de manière récursive en utilisant le tri par fusion.
- **Combiner :** Fusionner les deux sous-suites triées pour produire la réponse triée.



La récursivité s’arrête quand la séquence à trier a une longueur 1, auquel cas il n’y a plus rien à faire puisqu’une suite de longueur 1 est déjà triée.


<br/>

<center>
<img src="./../images/a7.png">
</center>

<br/>

La clé de voûte de l’algorithme du tri par fusion, c’est la fusion de deux séquences triées dans l’étape << combiner >>. Pour faire cette fusion, nous utilisons une procédure auxiliaire  `fusion`. 

La procédure suppose que les sous-tableaux  qui lui sont passé en paramètre sont triés. Elle les fusionne pour en faire un même sous-tableau trié. 



#### B. Implémentation

##### Implémentation de la partie principale du trie par fusion


```python
def trifusion(tab):
    n_elem_tab = len(tab)
    if n_elem_tab <= 1:
        return tab
    else:
        id_milieu = n_elem_tab // 2
        return fusion(trifusion(tab[:id_milieu]), trifusion(tab[id_milieu:]))
```



##### Implémentation de la procédure/ fonction de fusion

**Version récursive**

La fusion se prête très bien à une programmation récursive, avec les avantages et inconvénients habituels : élégante et facile à justifier, mais gourmande en mémoire. 

Voici une vision récursive de l’algorithme de fusion de deux tableaux triés `tab1` et `tab2` : 

- si l’un des deux tableaux est vide, renvoyer l’autre ; 

- sinon renvoyer le tableau formé par la plus petite des deux valeurs `tab1[0]` et `tab2[0]`, suivie de la fusion de `tab1` et `tab2`, l’un des deux ayant été privé de sa première valeur déjà placée !

```python
def fusion(tab1, tab2):
    if not tab1 and tab2:
        return tab2
    if not tab2 and tab1:
        return tab1
    if tab1[-1] > tab2[-1]:
        return fusion(tab1[:-1], tab2) + tab1[-1:]
    else:
        return fusion(tab1, tab2[:-1]) + tab2[-1:]
```

La complexité (en nombre de comparaisons d’éléments de tableau) au mieux est min (n1, n2), au pire n1 + n2. Mais le programme ci-dessus souffre de deux gros défauts : 

• le nombre de comparaisons est bien un O (n1 + n2), mais la complexité temporelle est quadratique, du fait des recopies de tableaux ; 

• la profondeur des appels récursifs est au pire également de n1 +n2, ce qui peut poser problème pour de grands tableaux dans certains langages (notamment Python. . . ). 



**Version itérative 1**

Les versions itératives permettent de diminuer le nombre de recopie des tableaux :

```python
def fusion_it(tab1, tab2):
    resultat = []
    index_tab1, index_tab2 = 0, 0
    index_max_tab1, index_max_tab2 = len(tab1), len(tab2)
    while index_tab1 < index_max_tab1 and index_tab2 < index_max_tab2:
        if tab1[index_tab1] <= tab2[index_tab2]:
            resultat.append(tab1[index_tab1])
            index_tab1 += 1
        else:
            resultat.append(tab2[index_tab2])
            index_tab2 += 1
    if tab1:
        resultat.extend(tab1[index_tab1:])
    if tab2:
        resultat.extend(tab2[index_tab2:])
    return resultat
```



Remarquons, quelques “bonnes pratiques” :

- utiliser les variables n1, n2 plutôt que de recalculer sans cesse len (t1) et len (t2)
- utiliser i+=1 plutôt que i=i+1 (la première version incrémente “en place”, tandis que la seconde
  crée une nouvelle instance de i avant de laisser le ramasse-miettes nettoyer l’ancienne. . . )
- utiliser t.append(x) plutôt que t=t+[x] (la première version “ajoute” x à la fin de t, tandis que la
  seconde recopie toutes les valeurs de t dans une nouvelle instance de t avant d’y adjoindre x…



**Version itérative 2**

```python

```



#### C. Correction de l’algorithme

##### Détermination de l’invariant de boucle

**Initialisation**

**Conservation**

**Terminaison**



#### D. Performances

**Coût spatial**

La création de multiple tableau, implique une complexité spatiale beaucoup plus importante que le trie par insertion.



**Coût temporel**

**Diviser :** L’étape diviser se contente de calculer le milieu du sous-tableau, ce qui consomme un temps constant. Donc :
$$
D(n) = Θ (n ).
$$
**Régner :** On résout récursivement deux sous-problèmes, chacun ayan la taille n/2, ce qui contribue pour 2T(n/2) au temps d’exécution.

**Combiner :** La procédure fusion sur un sous-tableau à n élément prend un temps Θ(n), de sorte que :
$$
C(n) = Θ (n ).
$$
Quand on ajoute les fonctions D(n) et C(n) pour l’analyse du tri par fusion, on ajout une fonction qui est Θ(n) et une fonction qui Θ(1).

Cette somme est une fonction linéaire de n, à savoir (n). L’ajouter au therme 2T(n/2) de l’étape “régner” donne la récurrence pour T(n), temps d’exécution du tri par fusion dans le cas le plus défavorables :

<br/>

<center>
<img src="./../images/a8.png">
</center>

<br/>

On peut montrer que : 
$$
T (n) = Θ (n \space log_2 n).
$$

Comme la fonction logarithme croit plus lentement que n’ importe quelle fonction linéaire, pour des entrées suffisamment grandes, le tri par fusion, avec son temps d’exécution Θ(n log_2 n), est plus efficace que le tri par insertion dont le temps d’exécution vaut Θ(n²) dans le cas le plus défavorable.

Si on réalise des mesures, on retrouve bien cette différence :

<br/>

<center>
<img src="./../images/a9.png">
</center>

<br/>

<br/>

<center>
<img src="./../images/a10.png">
</center>

<br/>

Cet algorithme est typique du paradigme “diviser pour régner” ;



#### D. Ressources

https://www.youtube.com/watch?v=OEmlVnH3aUg

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/OEmlVnH3aUg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Le trie fusion en action_: http://lwh.free.fr/pages/algo/tri/tri_fusion.html





### 2.5 - Rotation d’une image bitmap

#### Exercice
<br/>

<center>
<img src="./../images/a11.png">
</center>

<br/>


<br/>

<center>
<img src="./../images/a12.png">
</center>

<br/>



#### Correction

<center>
<img src="./../images/a13.png">
</center>

<br/>
<center>
<img src="./../images/a14.png">
</center>

<br/>


https://repl.it/@Golden52/Rotation-dune-image#main.py
