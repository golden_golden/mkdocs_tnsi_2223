---
title : "Diviser pour régner"

---



# Diviser pour régner


## 1- Qu’est ce que la stratégie diviser pour régner

### 1.1 - Introduction

En programmation, il existe différentes concepts algorithmique pour résoudre un problème, certains parle de paradigme algorithmique (à ne pas confondre avec les paradigmes de programmation).

<br/>

Le premier de ces concepts est celui de diviser-pour-régner, ou “*divide and conquer*”. Il s’agit d’une classe de techniques algorithmiques, où l’on casse le problème initial en plusieurs problèmes similaires mais plus petits qui sont résolus récursivement. On peut alors combiner ces solutions pour résoudre le problème initial.

<br/>

La récursivité est particulièrement adapté pour cette stratégie algorithmique, mais il est possible de l’appliquer avec des algorithmes itératifs.

<br/>

Généralement lorsque l’on utilise un algorithme récursif pour résoudre un problème de taille N, il fonctionne de la manière suivante :


- Extraire ou construire à partir de notre entrée un problème de taille N-1
- Résoudre le problème de taille N-1 (récursivité)
- Utiliser cette solution pour résoudre le problème initial

<br/>

Dans sa forme la plus simple et la plus courante, la méthode diviser pour régner consiste, pour résoudre un problème de taille N, à :

- Diviser : partager le problème en sous-problèmes de taille N/2
- Régner : résoudre ces différents sous-problèmes (généralement récursivement)
- Combiner : fusionner les solutions pour obtenir la solution du problème initial



### 1.2 - Définition

!!! note "Définition :"
    *Diviser pour régner* est une stratégie algorithmique de résolution de problèmes qui consiste à :

    - **Diviser** : découper le problème en sous-problèmes.
    - **Régner** : Résoudre les sous-problèmes
    - **Combiner** : Calculer une solution au problème initial à partir des solutions des sous-problèmes. 

<br/>

Le partitionnement doit viser à : 

- résoudre le moins de sous-problèmes possibles
- équilibrer la taille des sous-problèmes
- s’assurer de l’indépendance des sous-problèmes 

<br/>

On obtient en général moins d'appels récursifs, mais il faut faire attention à ce que la combinaison n’entraîne pas un surcoût rédhibitoire.

Comme les algorithmes correspondant sont essentiellement récursifs, calculer leur complexité implique généralement de résoudre une équation récursive.



### 1.3 - Objectifs

Dans le cas ou le problème possède des sous structures optimales, cette stratégie algorithmique permet généralement d’obtenir des algorithmes plus rapide, c’est à dire de meilleur complexité.

Dans la plupart des exemples décrits dans la suite de ce cours, la méthode “naïve” permet une résolution en temps polynomial, alors que les solutions utilisant la stratégie diviser pour régner permettent de se ramener à un polynôme de degré inférieur.


