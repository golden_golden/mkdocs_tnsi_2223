---
title : "Ressources"

---




# Diviser pour régner


## 3 - Annexes

### 3.1 - Méthode “diviser pour régner” dans le programme

#### Algorithmique

| Contenus                         | Capacités attendues                                          | Commentaires                                                 |
| -------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Méthode « diviser pour régner ». | Écrire un algorithme utilisant la méthode « diviser pour régner ». | La rotation d’une image bitmap d’un quart de tour avec un coût en mémoire constant est un bon exemple. L’exemple du tri fusion permet également d’exploiter la récursivité et d’exhiber un algorithme de coût en n log_2 n dans les pires des cas. |



### 3.2 - Fiche de révision du chapitre diviser pour Regnier

[https://www.youtube.com/watch?v=OEmlVnH3aUg](https://www.youtube.com/watch?v=OEmlVnH3aUg)


<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/OEmlVnH3aUg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


### 3.3 - Ressources pour la création de ce cours

- [http://www.xavierdupre.fr/blog/2013-12-01_nojs.html](http://www.xavierdupre.fr/blog/2013-12-01_nojs.html)
- cf. fiche Eduscol : [https://cache.media.eduscol.education.fr/file/NSI/76/3/RA_Lycee_G_NSI_algo-dichoto_1170763.pdf](https://cache.media.eduscol.education.fr/file/NSI/76/3/RA_Lycee_G_NSI_algo-dichoto_1170763.pdf)
- [https://www.youtube.com/watch?v=JdwWMnU04pQ](https://www.youtube.com/watch?v=JdwWMnU04pQ)