---
title : "Algorithmique"

---

# Algorithmique

## Sommaire 

1. Algorithmes sur les arbres binaires et sur les arbres binaires de recherche (Bac)
 
2. Algorithmes sur les graphes
   
3. Méthode « diviser pour régner » (Bac)
   
4. Programmation dynamique
   
5. Recherche textuelle
   

   